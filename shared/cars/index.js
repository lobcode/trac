const _ = require('lodash');
const mobx = require('mobx');
const {observable, action} = mobx;

const config = require('./config');
const KEY_LANG = 'rft_lang';

module.exports = observable({
	cars: [],
	get getCarsList() {
		return this.cars.toJS();
	},
	// Actions
	getCars: action(function getCars() {
		axios.get('https://endpoint.wheelsys.io/10235/link/v3/groups_wbstyr567.html', {
			params: {
				AGENT
			}
		})
			.then(({data}) => {
				parseString(data, function (err, result) {
					_this.cars = _.map(result.response.cargroup, (car) => {
						return {
							...car['$']
						}
					});
				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	getMockCars: action(function getMockCars() {
		let _this = this;
		axios.get('/api/groups')
			.then(({data}) => {
				parseString(data, function (err, result) {
					_this.cars = _.map(result.response.cargroup, (car) => {
						return {
							...car['$']
						}
					});
					console.log('[=====  CARS  =====>');
					console.log(_this.cars);
					console.log('<=====  /CARS  =====]');
				});
			})
			.catch(function (error) {
				console.log(error);
			});

	})
});
