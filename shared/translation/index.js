const _ = require('lodash');
const mobx = require('mobx');
const {observable, action} = mobx;

const config = require('./config');
const KEY_LANG = 'trac_lang';

module.exports = observable({
	// Observables:
	language: 'es',
	// Computeds:
	get getLang() {
		return (_.isNull(localStorage.getItem(KEY_LANG))) ? 'es' : localStorage.getItem(KEY_LANG);
	},
	get config() {
		return config;
	},
	// Actions:
	t(path) {
		return _.get(config, this.language + '.' + path, '');
	},
	init: action(function() {
		if (_.isNull(localStorage.getItem(KEY_LANG))) {
			localStorage.setItem(KEY_LANG, this.language);
		} else {
			this.setLanguage(localStorage.getItem(KEY_LANG));
		}
	}),
	setLanguage: action(function(lang) {
		localStorage.setItem(KEY_LANG, lang);
		this.language = lang;
	}),
	set: action(function(lang) {
		this.language = lang;
	})
});
