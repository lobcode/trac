module.exports = {
  es: {
    menu: {
      cars: {
        title: 'Alquiler de carros Costa Rica',
        titleNew: 'Alquiler',
        subMenu: {
          item1: 'Sedans',
          item2: 'SUVS',
          item3: 'Premium Fleet',
          item4: 'Mini Buses',
          item5: 'Pick ups',
        // item6: 'Minivans'
        }
      },
      products_services: {
        title: 'Productos y Servicios',
        subMenu: {
          item1: 'Mobile Hotspot',
          item2: 'GPS',
          item3: 'Quick Pass',
          item4: 'Canasta',
          item5: 'Sillas de seguridad para niños',
          item6: 'Servicio de Chofer',
          item7: 'Alquiler y Servicio de Chofer por horas',
          item8: 'Fast Lane',
          item9: 'Servicio de Bodas',
          item10: 'Combustible Prepago'
        }
      },
      fleet: {
        title: 'Flota'
      },
      locations: {
        title: 'Ubicaciones',
        subMenu: {
          item1: 'Main Offices',
          item2: 'Int. Airport Offices',
          item3: 'Liberia',
          item5: 'Nosara',
          item7: 'Puerto Jiménez'
        }
      },
      about: 'Nosotros',
      blog: 'Blog',
      news_updates: 'Noticias y Notificaciones',
      awards: 'Reconocimientos',
      contact_us: 'Contáctenos',
      online_payment_policies: 'Políticas pago en línea',
      special_offers: 'Ofertas Especiales',
      customer_care: 'Servicio al Cliente',
      policy: 'Política de Privacidad y Seguridad',
      travel_assistance: {
        title: 'Asistente de Viaje',
        subMenu: {
          item1: 'Consejos Prácticos',
          item2: 'Ley de Tránsito',
          item3: 'Preguntas Frecuentes'
        }
      },
      languages: 'Lenguaje',
      customer_form: 'Formulario',
      contact_form: 'Formulario de Contacto'
    },
    blog: {
      categories: 'Categorías',
      all_categories: 'Todas las categorías',
      back_to_blog: 'Regresar'
    },
    header: {},
    footer: {
      phone: 'Teléfono',
      textUsaCanada: 'Habilitado para Canadá y Estados Unidos (USA)'
    },
    discount: 'descuento!',
    specs: {
      motor: 'cc',
      manual: 'Manual',
      automatic: 'Automático',
      gasoline: 'Gasolina',
      diesel: 'Diesel',
      seats: 'Pasajeros',
      large_bag: 'Valijas',
      small_bag: 'Maletines',
      doors: 'Puertas'
    },
    portada:[{
      img: 'TRAC-HomeEs',
      type: 'clean',
      link: '/',
      btn_id: '',
    }],
    promociones:[{
      img: 'promo1Es',
      link: '/es/interbus',
      btn_id: '',
    },{
      img: 'promo2Es',
      type: 'clean',
      link: '/',
      btn_id: '',
    }],
    slider: [                
      {
        img: 'landing-fee-es',
        type: 'landing-fee',
        landing: 'landing-fee',        
        title: '',
        subtitle: '',
        btn_1:'Mas información',
        btn_url_1: '/es/sin-cargos-adicionales',
        btn_2: '',
        btn_url_2: '',
        btn_id: 'no_hidden_fees'
      },
      {
        img: 'slide-1',
        title: 'Vive la Experiencia',
        subtitle: 'Con toda Confianza.',
        btn_1: 'Suscripción de Eventos',
        btn_url_1: '/test1',
        btn_2: 'Nuestros Carros',
        btn_url_2: '/es/carros',
        btn_id: 'see_our_cars_btn'
      },
      {
        type: 'landing',
        img: 'librehumo2',
        hiddenButton: true
      },
      {
        img: 'interbus',
        title: 'Reserve ',
        subtitle: 'Con Interbus',
        btn_1: 'Suscripción de Eventos',
        btn_url_1: '/es/interbus',
        btn_2: 'Mas Información',
        btn_url_2: '/es/interbus',
        btn_id: 'see_our_cars_btn'
      }
    ],
    widget_book: {
      title: '<strong>Reserve ahora en</strong> 4 simples pasos',
      start_date: 'Desde',
      end_date: 'Hasta',
      pick_up_location: 'Lugar de retiro',
      drop_off_location: 'Lugar de devolución',
      btn_next: 'Siguiente',
      btn_next_id: 'next_btn_step1',
      total_days: 'Total de días',
      model: 'Modelo',
      price: 'Precio',
      total_price: 'Precio total',
      additional_accessories: 'Accesorios adicionales',
      booking_summary: 'Resumen de la reserva',
      different_location: 'Al seleccionar una estación de origen distinta a la de entrega, se aplica un sobrecargo por servicio'
    },
    widget_reserve_form: {
      vehicle: 'Vehículo',
      full_name: 'Nombre completo',
      email: 'Email',
      phone_number: 'Número de teléfono',
      ironman_number: 'Número de Ironman',
      book_now_btn: 'Reservar ahora',
      form: {
        vehicle: 'Vehículo',
        full_name: 'Nombre completo',
        email: 'Email',
        phone_number: 'Número de teléfono',
        start_date: 'Desde',
        end_date: 'Hasta',
        pick_up_location: 'Lugar de retiro',
        drop_off_location: 'Lugar de devolución',
        insurance: 'Seguro',
        driver_licence_number: 'Número de Licencia del conductor',
        email_settings: {
          title: 'Nueva Reservación',
          subject: 'Nueva Reservación'
        },
        notification: {
          success: {
            title: '¡Gracias por reservar con Toyota Rent a Car!',
            text: '<p>Uno de nuestros representantes se pondrá en contacto con usted para confirmar su reserva y le proporcionará un número de reserva.</p><p>Un representante de toyota enviará un correo electrónico dentro de la próxima media hora, a menos que sea después de las 6 pm hora local, el cual se enviará al día siguiente.</p>',
            type: 'success'
          },
          warning: {
            title: 'Alerta',
            text: 'Por favor revise los datos del formulario.',
            type: 'notice'
          },
          error: {
            title: 'Error',
            text: 'Ha ocurrido un error, por favor intente de nuevo',
            type: 'error'
          }
        }
      }
    },
    hot_deals: {
      main_title: 'Conoce nuestra flota',
      sub_title: ''
    },
    customer_form: {
      step_description_1: 'Necesitas llenar el siguiente formulario.',
      step_description_2: 'nosotros responderemos lo antes posible.',
      support_description: '',
      full_name: 'Nombre Completo',
      email: 'Correo Electrónico',
      telephone: 'Teléfono',
      ext: 'Ext',
      reserved_through: {
        title: 'Reservado a través',
        options: {
          option_1: 'Página Web',
          option_2: 'Agencia',
          option_3: 'Otro'
        }
      },
      location: 'Localización',
      rental_agreement_number: 'Contrato de Alquiler #',
      confirmation: 'Confirmación',
      perks_number: 'Beneficio #',
      type_of_issue: 'Tipo de Asunto',
      subject: 'Asunto',
      message: 'Mensaje',
      captcha_description: 'Ingrese el texto desplegado en la imágen',
      submit: 'Enviar',
      cancel: 'Cancelar',
      email_settings: {
        title: 'Formulario sobre el cliente',
        subject: 'Formulario sobre el cliente'
      },
      notification: {
        success: {
          title: 'Confirmación',
          text: 'El formulario fue enviado satisfactoriamente.',
          type: 'success'
        },
        warning: {
          title: 'Aviso',
          text: 'Favor verificar los datos ingresados.',
          type: 'notice'
        },
        error: {
          title: 'Error',
          text: 'Ocurrió un error inesperado, favor vuelta a intentar.',
          type: 'error'
        }
      }
    },
    contact_form: {
      step_description_1: 'Necesitas llenar el siguiente formulario.',
      step_description_2: 'nosotros responderemos lo antes posible.',
      support_description: '',
      full_name: 'Nombre Completo',
      email: 'Correo Electrónico',
      telephone: 'Teléfono',
      location: 'Localización',
      department: {
        title: 'Cuál departamento desea contactar',
        options: {
          item1: 'Reservaciones',
          item2: 'Servicio al cliente',
          item3: 'Administración'
        }
      },
      subject: 'Asunto',
      message: 'Mensaje',
      captcha_description: 'Ingrese el texto desplegado en la imágen',
      submit: 'Enviar',
      cancel: 'Cancelar',
      email_settings: {
        title: 'Formulario sobre el cliente',
        subject: 'Formulario sobre el cliente'
      },
      notification: {
        success: {
          title: 'Confirmación',
          text: 'El formulario fue enviado satisfactoriamente.',
          type: 'success'
        },
        warning: {
          title: 'Aviso',
          text: 'Favor verificar los datos ingresados.',
          type: 'notice'
        },
        error: {
          title: 'Error',
          text: 'Ocurrió un error inesperado, favor vuelta a intentar.',
          type: 'error'
        }
      }
    },
    pages: {
      home: {
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Alquiler de vehículos en Costa Rica marca Toyota, con el respaldo de ToyotaRent. Rente su coche directamente con la agencia.',
          keywords: 'renta, coches, vehículos, costa rica, alquiler, toyota, toyotarent'
        },
        content: '<h1>Renta de carros Costa Rica</h1><p>Toyota le ofrece una gran variedad de carros para la renta en Costa Rica, nuestros vehículos son de la mejor calidad y con estilos diferentes para satisfacer sus necesidades al momento de rentar un carro en Costa Rica. Rentar un auto es una de las mejores maneras de descubrir Costa Rica ya que le permite decidir el destino y la hora en la que quiere viajar.</p><h2 class="section-title">¿Por qué alquilar un carro?</h2><p>Una de las ventajas de alquilar un carro mientras visita Costa Rica es la facilidad de explorar diferentes rincones del país, por ejemplo, si usted desea visitas Monteverde rentar un carro le permite determinar la mejor hora para ir, además de poder disfrutar de las diferentes atracciones las cuales son complicadas de ver si usted va en un tour o bien en bus público.</p><p>Alquilar un carro es Costa Rica con Toyota es fácil, además de ser económico ya que ponemos a su disposición carros de todos los modelos. Por otra parte, si usted necesita rentar un 4X4 Toyota es líder en el mercado con los mejores carros ideales para visitar lugares donde las calles son de difícil acceso.</p><h2 class="section-title">¿Qué tipo de carro rentar?</h2><p>Todo depende del lugar que usted guste visitar, generalmente  recomienda rentar un 4X4 ya que el clima en Costa Rica provoca que las calles se tornen complicadas para los vehículos sencillos. Por otra parte, si usted va a estar en la ciudad únicamente o bien en lugares donde las calles son de asfalto un automóvil es una buena opción.</p><p>Le invitamos a ver todas las opciones de alquiler de carro en Costa Rica, desde donde podrá comprar precios, recuerde que también ofrecemos servicio de chat en línea de manera que usted pueda hacer sus consultas desde nuestro sitio web.</p>'
      },
      cars: {
        permalink: 'carros',
        seo: {
          title: 'Alquiler de autos Costa Rica con el  mejor precio y respaldo | Toyota CR',
          description: 'Somos líderes en alquiler de autos en Costa Rica, Toyota le ofrece las mejores marcas, respaldo y precio. Contacte con nosotros o reserve en línea ¡ahora!',
          keywords: 'alquiler, vehículos, costa rica, toyota, compactos, sedanes, todoterreno'
        },
        content: '<h1>¿Necesita Alquilar un carro? Toyota Costa Rica es su mejor opción</h1><p>Toyota es líder en alquiler de carros en Costa Rica, contamos con una amplia flotilla de vehículos en excelentes condiciones para que su experiencia sea la mejor. Contamos con vehículos tipo automóvil, Mini Buses, Pick Ups y una gran línea de vehículos de lujo, todos con facilidad de renta y con los precios más competitivos del mercado costarricense.</p><p>Alquilar un carro en Costa Rica es una excelente opción ya que de esa manera usted puede planear su viaje al memento más conveniente, Costa Rica es un país lleno de lugres hermosos tales como el parque nacional Manuel Antonio o bien las hermosas playas de Guanacaste en donde es mucho más fácil llegar si maneja por usted mismo, por otra parte puede conocer pueblos que están de camino.</p><h2 class="section-title">¿Cuánto cuesta alquilar un carro con Toyota?</h2><p>Esto dependerá del modelo que usted escoja, usted puede visitar la sección de reservas (en la parte superior de esta página) para tener un precio exacto por la renta del vehículo, Toyota siempre le proveerá los mejores precios del mercado de manera que usted puede ahorrar mucho dinero rentando un carro con nosotros.</p><p>Ya sea que usted necesite rentar un carro para un viaje de familia, negocios u personal Toyota tiene el auto ideal para usted, permitiéndole movilizarse por el país con un auto de alta calidad y seguridad – Somos líderes en el mercado, nuestra calidad y compromiso es nuestro mayor meta para con nuestros usuarios.</p><h2 class="section-title">¡Reserve en línea hoy mismo!</h2><p>Toyota pone a sus disposición el sistema de reservas en línea el cual facilita el alquiler de carros, desde la comodidad de su hogar puede hacer la reserva en 4 simples pasos. Por otra parte usted puede elegir entre nuestras estratégicas locaciones para recoger o bien entregar el vehículo.</p>',
        categories: {
          sedans: {
            title: 'Sedanes',
            category: 'sedans',
            compact: {
              type: 'compact',
              title: 'Compacto',
              description: 'Altísimo desempeño y diseño elegante que permiten una confortable experiencia en su próxima renta. '
            },
            standard: {
              type: 'standard',
              title: 'Estándar',
              description: 'La próxima vez que alquile un vehículo, disfrute del estilo y excelente desempeño del Corolla.'
            }
          },
          suvs: {
            title: '4x4',
            category: 'suvs',
            compact: {
              type: 'compact',
              title: 'Compacto 4x4',
              description: 'Si desea un carro económico y 4x4, ¡el Daihatsu Bego será la mejor elección!'
            },
            standard: {
              type: 'standard',
              title: 'Estándar 4x4',
              description: 'El Rav4 ofrece un estilo deportivo y comodidad sorprendente. Ya sea dejándose llevar por la ciudad o en su viaje de placer es la decisión correcta para hacer de su próxima renta la más memorable.'
            },
            full: {
              type: 'full',
              title: 'Full Crossover 4x4',
              description: 'Espacioso, cómodo y elegante, rentando un Prado o Fortuner no hay necesidad de dejar nada ni a nadie en casa. '
            }
          },
          premium_fleet: {
            title: 'Flota Premium',
            category: 'premium-fleet',
            premium: {
              type: 'premium',
              title: 'Premium 4x4 Auto',
              description: 'Los modelos TOYOTA Prado o Lexus RX 350 H ofrecen todo el confort y estilo que usted necesita. Ya sea manejando en la ciudad o hacia un destino de ensueño.'
            },
            grand: {
              type: 'grand',
              title: 'Grand Premium 4x4',
              description: 'Si lo que desea es experimentar un vehículo de lujo, nada mejor que el Sequoia, con su equipamiento interior y la potencia, hace que sus viajes sean de puro disfrute. Además, es tan espacioso que sus pasajeros viajarán cómodos con todo su equipaje. Pruebe un verdadero vehículo de lujo. '
            }
          },
          mini_buses: {
            title: 'Mini Buses',
            category: 'mini-buses',
            micro_bus: {
              type: 'micro-bus',
              title: 'Microbus',
              description: 'Si planea una actividad con su equipo de trabajo o con su numerosa familia, inicie el mejor de sus viajes rentando la Hiace para 12 a 16 pasajeros. '
            }
          },
          pick_ups: {
            title: 'Pick Ups',
            category: 'pick-ups',
            double_cap: {
              type: 'double-cap',
              title: 'Double Cap 4x4',
              description: 'El confiable desempeño y la potencia del Hilux le garantiza las mejores aventuras en su próxima renta.'
            }
          },
        // minivans: {
        //   title: 'Minivans',
        //   category: 'minivans',
        //   minivans: {
        //     type: 'minivans',
        //     title: 'Minivans',
        //     description: 'Si deseas disfrutar con tu familia o amigos de un automóvil espacioso y económico, el Avanza es el indicado, una minivan de 3 filas de asientos para 7 personas, cómoda por dentro y moderna por fuera.'
        //   }
        // }
        }
      },
      booking: {
        page_title: 'Reserva',
        step2: 'Paso 2',
        step2_title: 'Seleccione el vehículo',
        step3: 'Paso 3',
        step3_title: 'Servicios de Productos',
        step4: 'Paso 4',
        step4_title: 'Reserve su vehículo',
        leave_confirmation: 'Usted está seguro de que desea salir y cancelar su reserva?',
        car_no_available_title: 'El vehículo selecionado no está disponible',
        car_no_available_description: 'Por favor intente cambio la información de la reserve o seleccione uno de los vahículos que aparecen abajo.',
        change_booking_btn: 'Cambiar Reserve',
        product_services: 'Servicios de Productos',
        book_your_car: 'Reserve su vehículo',
        select_car: 'Seleccione el vehículo',
        or_similar: 'o similar',
        day: 'día',
        book_car: 'Reservar Carro',
        pay_now_save: '¡Pague Ahora y ahorre tiempo!',
        pay_now: 'Pagar Ahora',
        pay_later: 'Pagar Después',
        pay: 'Pague',
        and_save: 'y ahorre',
        or: 'o',
        i_am_not: 'No estoy',
        interested: 'interesado',
        in_save_money: 'en ahorrar tiempo',
        car_available: 'Vehículo disponible',
        car_no_available: 'Vehículo no disponible',
        pick_available_car: 'Ver vehículos disponibles',
        change_car: 'Cambiar vahículo',
        payment_info: 'Información de Pago',
        cc_number: 'Número de la tarjeta',
        cc_exp: 'Fecha de expiración',
        check_name: 'Nombre del titular de la tarjeta',
        pay_btn: 'Realizar Pago',
        unselected_insurance: 'No has seleccionado la opción de seguro básico, ¿estás seguro que deseas continuar sin seguro?',
        unselected_insurance_btn: 'Si, continuar.',
        basic_insurance_info: '<p>*CDW, cubre daños directos del vehículo de alquiler en caso de que se cumplan lostérminos del contrato de alquiler.</p><p>*LDW, cubre la propiedad de terceros en caso de accidente en caso de que se cumplanlos términos del contrato de alquiler.</p><p>*Robo, cubre en caso de robo del vehículo de alquiler cuando se cumplen los términos delcontrato de alquiler (Incluyen deducibles).</p><p>Pregunte a nuestro staff por convenientes protecciones adicionales.</p>',
        payment_notification: {
          success: {
            title: 'Confirmación',
            text: 'Pago Realizado.',
            type: 'success'
          },
          warning: {
            title: 'Alerta',
            text: 'Por favor revise los datos del formulario.',
            type: 'notice'
          },
          error: {
            title: 'Error',
            text: 'Ha ocurrido un error, por favor intente de nuevo',
            type: 'error'
          }
        },
        product_options: {
          H004: 'LPF',
          I003: 'Conductor Adicional',
          K001: 'Booster',
          K002: 'GPS',
          K004: 'Quick Pass',
          K007: 'Siempre Wi Fi',
          K008: 'Silla de Bebé',
          E001: 'Seguro Básico',
          I004: 'Fast Pass',
          I006: 'Fast Return'
        },
        product_options_info: {
          H004: {
            price:'',
            description: ''
          },
          I003: {
            price: '- <b>$10</b> por día',
            description: '<p>Le ofrecemos el servicio de conductor adicional para garantizar que, en caso de accidente si el vehículo esté siendo conducido por un tercero, éste será protegido con la cobertura adquirida.</p>'
          },
          K001:{
            price: '- <b>$10</b> por día',
            description: '<p>Si en su viaje se acompaña de niños menores de 12 años, según la legislación nacional, es obligatorio portar este accesorio para la seguridad de los más pequeños, nosotros le ofrecemos tanto sillas para bebés como boosters para los más grandes.</p>'
          },
          K002:{
            price: '- <b>$12</b> por día',
            description: '<p>Este sistema le facilitará su viaje, guiando su camino a través de rutas preestablecidas a nivel nacional.</p>'
          }, 
          K004:{
            price: '- <b>$10</b> por día',
            description: '<p>El Quick Pass es un sistema de pago electrónico de los peajes ubicados en la Ruta 27 (autopista a Caldera), este dispositivo le permitirá pasar el o los peajes por un carril exclusivo, evitándole presas y ahorrándole más tiempo.</p>'
          },
          K007: {
            price: '- <b>$20</b> por día',
            description: '<p>¡Internet Wifi adonde vayas! Disfruta de tus apps preferidas y disfruta de funcionalidad GPS.</p>'
          },
          K008: {
            price: '- <b>$10</b> por día',
            description: '<p>Si en su viaje se acompaña de niños menores de 12 años, según la legislación nacional, es obligatorio portar este accesorio para la seguridad de los más pequeños, nosotros le ofrecemos tanto sillas para bebés como boosters para los más grandes.</p>'
          },
          E001: {
            price:'',
            description: '<p>*CDW, cubre daños directos del vehículo de alquiler en caso de que se cumplan lostérminos del contrato de alquiler.</p><p>*LDW, cubre la propiedad de terceros en caso de accidente en caso de que se cumplanlos términos del contrato de alquiler.</p><p>*Robo, cubre en caso de robo del vehículo de alquiler cuando se cumplen los términos delcontrato de alquiler (Incluyen deducibles).</p><p>Pregunte a nuestro staff por convenientes protecciones adicionales.</p>',
          },
          I004: {
            price: '- <b>$10</b>',
            description: '<p>Simplifique su proceso de alquiler con este servicio, le entregaremos el vehículo directamente a la salida de su vuelo en el Aeropuerto Juan Santamaría en Alajuela o Daniel Oduber en Liberia, con la documentación y el trámite listos</p><p>Este servicio es exclusivo para la estación de Alajuela y Liberia. Solicítelo al realizar la reservación en línea.</p>'
          },
          I006: {
            price: '- <b>$10</b>',
            description: '<p>Este servicio le ofrece la facilidad de dejar el vehículo directamente en el Aeropuerto Juan Santamaría en Alajuela sin tener que dirigirse a la estación y sin preocuparse de perder su vuelo. Solicítelo al realizar la reservación en línea.</p>'
          }
        },        
        error: {
          'ERR/0': 'There is a stop sale for that Group/Period/Station',
          'ERR/10': 'No hay vehículos disponibles',
          'ERR/100': 'Parámetro requerido no enviado',
          'ERR/101': 'Grupo del vehículo no encontrado',
          'ERR/102': 'Lugar de Retiro/Entrega no especificado',
          'ERR/103': 'Opción adional no encontrada',
          'ERR/106': 'Combinación de Fecha/Hora erroneo',
          'ERR/108': 'Número de referencia ya existe',
          'ERR/110': 'Precio de reserva no encontrado',
          'ERR/111': 'Precio de reserva ha expirado',
          'ERR/112': 'El precio no es valido para ésta reserva',
          'ERR/114': 'El grupo especificado no es soportado por la hubicación',
          'ERR/999': 'Sistema fuera de servicio por mantenimiento',
          'ERR/1000': 'Error de comunicación, por favor intente más tarde.'
        },
        thank_you: {
          page_title: '¡Muchas gracias!',
          quote_no: 'Reservación No.',
          customer_information: 'Información del Cliente',
          book_details: 'Detalle de la reserva',
          car_detail: 'Información del vehículo',
          model: 'Modelo:',
          baseRate: 'Precio Base:',
          totalRate: 'Precio Total:',
          additionalOptionsList: 'Lista de Opciones Adicionales:'
        },
        form: {
          first_name: 'Nombre',
          last_name: 'Apellidos',
          phone: 'Teléfono',
          email: 'Correo electrónico',
          country: 'País',
          city: 'Ciudad / Provincia',
          zip: 'Zipcode',
          address: 'Dirección',
          airline: 'Aerolínea',
          flight: 'Vuelo',
          quote_no: 'Reservación No.',
          customer_information: 'Información del Cliente',
          book_details: 'Detalle de la reserva',
          car_detail: 'Información del vehículo',
          model: 'Modelo:',
          baseRate: 'Precio Base:',
          totalRate: 'Precio Total:',
          additionalOptionsList: 'Lista de Opciones Adicionales:',
          start_date: 'Desde',
          end_date: 'Hasta',
          pick_up_location: 'Lugar de retiro',
          drop_off_location: 'Lugar de devolución',
          total_days: 'Total de días',
          additional_accessories: 'Accesorios adicionales',
          payment_info: 'Información de Pago',
          check_name: 'Nombre del titular de la tarjeta: ',
          cc_number: 'Número de la tarjeta: ',
          irn: 'Número IRN: ',
          remark: 'Observaciones: ',
          no_insurance: '<p>*Rate does not include mandatory insurance and extras. Time and Mileage included only.</p>',
          titleQuote: 'Esta cotización está sujeta a disponibilidad y precio real en el momento de la reserva.',
          conditions: '<p><b>Please take in consideration that any change of the pickup and drop off location incur extra charges. Any additional extras and insurance are payable at the rental desk.</b></p><p>&nbsp;</p><p>Important Information</p><p>Rental period is 24 hours from delivery time in rental agreement</p><p>&nbsp;</p><p>Requirements:</p><p>Driver(s) must be 21 years old or older.</p><p>License Driver, ID/Passport valid, on hand.</p><p>Make US$1.000,00 guarantee deposit charged to a credit card (will be released 2 weeks from closing rental agreement); or US$2.000,00 when using debit card (will be released from 4 to 6 weeks from closing rental agreement).</p><p>Passengers arriving to Juan Santamar&iacute;a International (SJO) and Daniel Oduber International Airport (LIR), our agents will be waiting for you at the terminal exit to provide free shuttle service to our office.</p>',
          footer: '<p><a href="mailto:sales@toyotarent.com">sales@toyotarent.com</a> | 506-2105-3400 <br/>San José, Costa Rica</p>',
          email_settings: {
            title: 'Nueva Reservación',
            subject: 'Nueva Reservación'
          },
          notification: {
            success: {
              title: 'Confirmación',
              text: 'Se ha enviado el correo de confirmación de reserva.',
              type: 'success'
            },
            warning: {
              title: 'Alerta',
              text: 'Por favor revise los datos del formulario.',
              type: 'notice'
            },
            error: {
              title: 'Error',
              text: 'Ha ocurrido un error, por favor intente de nuevo',
              type: 'error'
            }
          }
        }
      },
      products_services: {
        permalink: 'productos-servicios',
        seo: {
          title: 'Productos y servicios para renta de carros | Toyota CR',
          description: 'En Toyota Renta ponemos a su disposición una gran variedad de productos y accesorios a la hora que renta un carro en Costa Rica ¡Descubra aquí!',
          keywords: 'costa rica, gps, quick passs, mobile hotspot, canasta, racks, renta, alquiler, vehículos'
        },
        products: {
          title: 'Productos',
          sections: {
            sections_1: {
              classname: 'mobile-hotspot',
              title: 'Mobile Hotspot',
              description: '<p>¡Internet Wifi adonde vayas! Disfruta de tus apps preferidas y disfruta de funcionalidad GPS.</p>'
            },
            section_2: {
              classname: 'gps-global-positioning-system',
              title: 'GPS',
              description: '<p>Este sistema le facilitará su viaje, guiando su camino a través de rutas preestablecidas a nivel nacional.</p>'
            },
            section_3: {
              classname: 'toll-pass',
              title: 'Quick Pass',
              description: '<p>El Quick Pass es un sistema de pago electrónico de los peajes ubicados en la Ruta 27 (autopista a Caldera), este dispositivo le permitirá pasar el o los peajes por un carril exclusivo, evitándole presas y ahorrándole más tiempo.</p>'
            },
            section_4: {
              classname: 'roof-racks',
              title: 'Canasta',
              description: '<p>Una canasta es un accesorio que es instalado en el techo del automóvil, permitiendo colocar ahí el equipaje, maximizando así el espacio interior del vehículo.</p> <p>Éstas son de gran utilidad para transportar sus maletas o tablas de surf.</p>'
            },
            section_5: {
              classname: 'kids-and-baby-seats',
              title: 'Sillas de seguridad para niños',
              description: '<p>Si en su viaje se acompaña de niños menores de 12 años, según la legislación nacional, es obligatorio portar este accesorio para la seguridad de los más pequeños, nosotros le ofrecemos tanto sillas para bebés como boosters para los más grandes.</p>'
            },
            section_6: {
              classname: 'easy-connect',
              title: 'Easy Connect',
              description: ''
            }
          }
        },
        services: {
          title: 'Servicios',
          sections: {
            section_1: {
              classname: 'chauffeur-services',
              title: 'Servicio de Chofer',
              description: '<p>Confíe su viaje en nuestros experimentados choferes, quienes estarán a su disposición para trasladarlo al lugar donde usted desea vacacionar o se dirija a hacer negocios. Si es su necesidad puede solicitar un chofer bilingüe (Inglés/Español).</p><p>Condiciones del servicio:</p><p><ul><li>El precio incluye la alimentación del conductor. No incluye hospedaje ni viáticos para el conductor fuera del Gran Área Metropolitana (GAM).</li><li>Se requiere un depósito y la compra de un seguro de protección.</li></ul></p>'
            },
            section_2: {
              classname: 'hourly-car-rental',
              title: 'Alquiler y Servicio de Chofer por horas',
              description: '<p>Realice sus diligencias sin preocupaciones, ponemos a su disposición los servicios de chofer privado con cualquiera de nuestros vehículos, el cual se encargará de trasladarlo donde usted le solicite.</p><p>Condiciones del servicio:</p><p><ul><li>Este servicio se encuentra disponible en cualquiera de los vehículos de la flota regular y la línea de lujo Premium.</li><li>No se requiere ningún depósito ni la compra de un seguro de protección para contratar un traslado privado.</li><li>Le brindamos éste servicio por un máximo de 50 kilómetros a recorrer.</li><li>La tarifa incluye el vehículo, chofer y combustible.</li></ul></p>'
            },
            section_3: {
              classname: 'fast-lane',
              title: 'Fast Pass',
              description: '<p>Simplifique su proceso de alquiler con este servicio, le entregaremos el vehículo directamente a la salida de su vuelo en el Aeropuerto Juan Santamaría en Alajuela o Daniel Oduber en Liberia, con la documentación y el trámite listos</p><p>Este servicio es exclusivo para la estación de Alajuela y Liberia. Solicítelo al realizar la reservación en línea.</p>'
            },
            section_4: {
              classname: 'fast-lane',
              title: 'Fast Return',
              description: '<p>Este servicio le ofrece la facilidad de dejar el vehículo directamente en el Aeropuerto Juan Santamaría en Alajuela sin tener que dirigirse a la estación y sin preocuparse de perder su vuelo. Solicítelo al realizar la reservación en línea.</p>'
            },
            section_5: {
              classname: 'wedding-services',
              title: 'Servicio de Bodas',
              description: '<p>El traslado de los novios o de los familiares en un lujoso vehículo a elegir de la línea Premium, decorado con flores naturales y acompañado de una botella de vino espumante para brindar por ese momento tan especial.</p>'
            },
            section_6: {
              classname: 'prepaid-fuel',
              title: 'Combustible Prepago',
              description: '<p>Si usted lo desea podemos cargar a su cuenta el tanque de gasolina del automóvil al momento de la renta, este servicio le ofrece facilidad ya que no tendrá que devolverlo lleno al momento de la devolución.</p>'
            }
          }
        }
      },
      locations: {
        permalink: 'ubicaciones',
        linkSection: '/es/ubicaciones',        
        seo: {
          title: 'Alquiler de carros Costa Rica por ubicación | Toyota CR',
          description: 'Toyota CR pone a su disposición una gran variedad de locaciones en los puntos más estratégicos de Costa Rica, para la renta de caro ¡Ver más!',
          keywords: 'alquiler, vehículos, costa rica, nosara, guanacaste, coches, carros, tambor'
        },
        title: 'Ubicaciones',
        titleGoogleMap: 'Ubicaciones',
        showLocationBtn: 'Mostrar todas las ubicaciones',
        address: 'Dirección',
        phone: 'Teléfono',
        horario: 'Horario',
        fax: 'Fax',
        code: {
          1: 'San José',
          3: 'Liberia',
          4: 'Nosara',
          5: 'San José Aeropuerto'
        },
        sections: {
          location_1: {
            slug: 'liberia',
            title: 'Liberia, Guanacaste',
            address: '800 metros de la entrada al Aeropuerto Internacional de Liberia. ',
            phone: '(506) 2105-3481',
            schedule: 'De lunes a domingo / 6 a.m. a 10 p.m.',
            fax: '(506) 2668-117',
            waze_url: {
              lat: '10.593333',
              lon: '-85.544444'
            },
            province: 'guanacaste',
            coordenadas:{
              lat: 10.624393, 
              lng: -85.518574
            } 
          },
          location_2: {
            slug: 'alajuela',
            title: 'International Airport - Alajuela',
            address: 'En frente del Hotel Hampton Inn Hotel  ',
            phone: '(506) 2105-3466',
            fax: '(506) 2443-5044',
            schedule: 'De lunes a domingo / 6 a.m. a 10 p.m.',
            waze_url: {
              lat: '9.993611',
              lon: '-84.208889'
            },
            province: 'alajuela',
            coordenadas:{
              lat: 10.002366, 
              lng: -84.195287
            }
          },
          location_3: {
            slug: 'main-office',
            title: 'Oficinas Centrales',
            address: 'Oficentro Santamaría, oficina 5B, Río Segundo, Alajuela.',
            phone: '(506) 2105-3400',
            // fax: '(506) 2222-7875',
            shedule: 'De lunes a viernes / 8 a.m. a 5:30 p.m.',
            waze_url: {
              lat: '9.9448051662096',
              lon: '-84.11815340114'
            },
            province: 'alajuela',
            coordenadas:{
              lat: 10.00369,
              lng: -84.19601
            }            
          },
          location_5: {
            title: 'Paseo Colón',
            titleLocation: 'Car Rental Paseo Colón, Costa Rica',
            address: '300 metros este de Purdy Motor Paseo Colón.',
            schedule: 'De lunes a viernes / 7 a.m. a 7 p.m. De sábado a domingo / 7 a.m. a 5 p.m.',
            phone: '(506) 2105-3460',
            seo: {
              title: 'Paseo Colón Car Rental, Costa Rica | Book Now - Toyotarent.com',
              description: "Let's discover Paseo Colón Costa Rica with Toyota, we are your best Paseo Colón car rental with the best brands and rates. Make your reservation today!"
            },
            article: '<p>We offer excellent options to get around Paseo Colón, Guanacaste Toyota Car Rental is your best option in price and quality all our cars are of the best quality.</p><p>Discover Paseo Colón by driving by yourself, you can easily visit beaches, forests, and towns in this beautiful area, Toyota is the number one Paseo Colón car rental.</p>',
            slug: 'paseo-colon',
            waze_url: {
              lat: '9.93572',
              lon: '-84.09291'
            },
            province: 'san-jose',
            coordenadas:{
              lat: 9.9355393,
              lng: -84.0951355
            }            
          },
          location_6: {
            title: 'Dominical',
            titleLocation: 'Dominical',
            address: 'Centro Comercial del Río, Osa, Puntarenas.',
            schedule: 'De lunes a domingo / 9 a.m. a 5:00 p.m.',
            phone: '(506) 2105-3460',
            seo: {
              title: 'Dominical Car Rental, Costa Rica | Book Now - Toyotarent.com',
              description: "Let's discover Dominical Costa Rica with Toyota, we are your best Dominical car rental with the best brands and rates. Make your reservation today!"
            },
            article: '<p>We offer excellent options to get around Nosara, Guanacaste Toyota Car Rental is your best option in price and quality all our cars are of the best quality.</p><p>Discover Dominical by driving by yourself, you can easily visit beaches, forests, and towns in this beautiful area, Toyota is the number one Dominical car rental.</p>',
            slug: 'dominical',
            // waze_url: {
            //   lat: '9.93572',
            //   lon: '-84.09291'
            // },
            province: 'puntarenas'
            // coordenadas:{
            //   lat: 9.9355393,
            //   lng: -84.0951355
            // }            
          }                      
        }
      },
      customer_care: {
        permalink: 'servicio-al-cliente',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'ToyotaRent es la única agencia que pone a sus disposición un servicio al cliente de calidad, garantizando una renta de vehículos segura.',
          keywords: 'servicio al cliente, TRAC, toyotarent, toyota, costa rica, alquiler, vehículos, carros, coches'
        },
        title: 'Servicio al Cliente',
        sub_title: 'Llámenos o envienos un correo  a:',
        call_us: '',
        email: 'Correo Electrónico: <a href="mailto:sales@toyotarent.com" title="Contáctenos a nuestro correo electrónico">sales@toyotarent.com</a>',
        form: {}
      },
      special_offers: {
        permalink: 'ofertas-especiales',
        seo: {
          title: 'Servicio al Cliente | Toyota CR',
          description: 'El mejor servicio al cliente al momento de alquilar un carro en Costa Rica. Toyota ofrece respaldo y buen servicio ¡Contactar!',
          keywords: 'alquiler, sedanes, toyota, ofertas especiales, promociones, toyotarent'
        },
        title: 'Ofertas Especiales',
        sections: [
          {
            title: 'Hacienda Pinilla',
            image: 'special_offers/haciendapinilla.jpg',
            url: 'www.haciendapinilla.com',
            content: '<h3>10% de descuento en Hacienda Pinilla en hospedaje en villas de 2 o 3 habitaciones, completamente equipadas.</h3><br><p>En la magnífica costa del Pacífico en la provincia de Guanacaste de Costa Rica, se extienden 4500 acres de paisaje natural, en ellos descubrirá un complejo residencial desarrollado en el estilo de hacienda colonial.</p><p>Tres playas adornan las 3 millas de la costa que bordea Hacienda Pinilla, ofreciendo una gama de actividades para todas las edades, que incluyen pesca, buceo, surf o simplemente disfrutar de un día junto al mar. La naturaleza abarca cada rincón de Hacienda Pinilla, desde el campo de golf diseñado profesionalmente hasta las playas, lagos, bosques, colinas y valles.</p><p>Las mejores vacaciones en la playa ocurren cuando la comodidad, la experiencia, la exclusividad y el buen gusto convergen. En Hacienda Pinilla, ofrecemos todo eso y más. Brindamos diferentes opciones de hospedaje cómodo rodeado por el verdor de la pampa de Guanacaste.</p><p>Si desea probar la vida cotidiana costarricense en Hacienda Pinilla, elija entre nuestra exclusiva colección de villas para vacacionar, todas cuentan con diseños únicos y tradicionales con muebles de la mejor calidad. Desde villas de dos a cuatro habitaciones, Hacienda Pinilla ofrece lo mejor en propiedades de alquiler en Costa Rica que necesita para tener unas vacaciones extraordinarias. Recibirá acceso exclusivo a la interminable variedad de comodidades dentro del complejo, incluyendo nuestro campo de golf, club de playa, restaurantes, establos, spas, canchas de tenis, senderos para bicicleta de montaña, paseos a caballo, famosos lugares para practicar surf y las impresionantes playas.</p><a rel="nofollow" href="tel:2680-7000 ">Para reservaciones contactar: 2680-7000</a><br><a rel="nofollow"  href="mailto:reservations@haciendapinilla.com">Email: reservations@haciendapinilla.com</a>',
            greatDeal: true,
            discount: '10'
          },
        ]
      },
      about: {
        permalink: 'nosotros',
        seo: {
          title: 'Acerca de Toyota Rent Costa Rica | Toyota CR',
          description: 'Somos especialistas en renta de carros en Costa Rica, estamos comprometidos en brindarle la mejor experiencia de renta en el país.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Sobre Nosotros',
        content: '<p><b>TOYOTA Rent a Car</b> es una compañía que tiene más de 50 años en el mercado de los rentacars, formando parte de las experiencias maravillosas que ofrece Costa Rica. Contamos con la confianza, lealtad y compromiso de nuestros clientes, brindarles servicios de alta calidad y enfocados en la responsabilidad social ambiental. Contamos con ubicaciones en Liberia, San José y Alajuela. </p><p>Durante los últimos años <b>TOYOTA Rent a Car</b> ha trabajado en el mejoramiento del servicio ya que, por medio de la diferenciación de servicios buscamos identificarnos más con las necesidades de nuestros clientes y por esto, implementamos servicios diferenciados como: entrega de vehículos directamente en los aeropuertos, servicio de chofer, servicios de boda, combustible prepagado y automóviles premium.</p><p>Adicionalmente, buscamos crear experiencias al cliente por medio de la satisfacción en el servicio, a razón de que muchos de nuestros clientes esperan vivir unas vacaciones en nuestro país enfocadas en la aventura y las experiencias únicas. </p><p>Hemos adoptado un enfoque ambiental en el desarrollo de todos los procesos para reducir la huella de carbono por el uso de combustibles. El impacto producido por estos servicios es medido y mitigado a través del mantenimiento continuo de los autos y de acciones ecológicas específicas como la plantación de árboles nativos en Guanacaste. De esta manera, se garantiza a los clientes que están contribuyendo activamente con la conservación del entorno natural. Tenemos programas de reciclaje en cada una de nuestras estaciones y oficinas administrativas con el fin de cuidar el medio ambiente y crear una cultura en nuestros clientes y nuestros colaboradores.</p><h2>Nuestros valores</h2><h3>Nuestra Visión</h3><p>“Ser la empresa de renta de vehículos líder en brindar una experiencia única y diferente al cliente, mediante procesos y servicios simples, seguros y personalizados”.</p><h3>Nuestra Misión</h3><p>“Brindamos la mejor experiencia del cliente a través de la innovación, guiados por nuestros valores corporativos y con proyección social”</p><h3>Hacemos lo que decimos.</h3><p>Nos expresamos claramente. Mantenemos nuestras promesas. Podemos renegociar si cambian los hechos.</p><h3>Somos dueños.</h3><p>Actuamos como si todos fuéramos el gerente general y no buscamos culpar a los demás. Nos preocupamos lo suficiente como para decirnos a nosotros mismos cuando no cumplimos un objetivo o meta. Siempre aprendemos de nuestros errores.</p><h3>Decimos la verdad.</h3><p>Actuamos con integridad tanto con nuestros clientes como con nuestros compañeros.</p><h3>Nos preocupamos por los demás.</h3><p>Aplicamos la regla de oro: tratamos a las personas como a ellos les gusta ser tratados.</p><h3>Sobrepasamos expectativas.</h3><p>Nuestra meta es ganar siempre en todo lo que hacemos. Impulsamos innovación y cambio en todo. Contratamos y desarrollamos personas enfocadas en cumplir metas GRANDES y RETADORAS</p><h2>Conciencia Ambiental</h2><h3>¿Qué significa tener el corazón verde?</h3><p>Tenemos conciencia del impacto que generamos en nuestros públicos de interés y tomamos acciones para reducirlas y/o compensarlas.</p><h3>¿Por qué tenemos el corazón verde?</h3><p>Porque medimos las emisiones de CO2 de nuestra flota y así creamos conciencia de nuestro impacto al ambiente.</p><p>Certificados con el cuarto nivel de CST (Certificación de Sostenibilidad Turística).</p><p>TOYOTA Rent a Car aplica y obedece los lineamientos conforme a la regulación nacional aplicable a nuestra actividad, así como la Ley de Conservación de Vida Silvestre. En virtud de esta ley, se considera ilegal mantener en cautiverio animales silvestres y algunas especies de plantas.</p><p>Compensamos nuestra huella de carbono mediante certificados de servicios ambientales de FONAFIFO (Fondo Nacional de Financiamiento Forestal), el cual es entregado por parte del Ministerio de Ambiente, Energía y Telecomunicaciones.</p><p>Todos nuestros vehículos TOYOTA que tienen motor de gasolina, cuentan con el sistema VVT-i, el cual asegura una potente aceleración menor y más eficiente consumo de combustible a mayor temperatura lo que reduce la emisión de gases de efecto invernadero.</p>'
      },
      practical_tips: {
        permalink: 'consejos-practicos',
        seo: {
          title: 'Consejos Prácticos al Alquilar un carro | Toyota CR',
          description: '¿Va a Renta un carro en Costa Rica? Le recomendamos leer la siguiente información por expertos, la cual le ayudara a tener mayor seguridad al conducir.',
          keywords: 'asistente viajes, toyota, alquiler, carros, coches, vehículos, toyota, costa rica, trac'
        },
        title: 'Consejos Prácticos',
        content: '<h3>Antes de iniciar su trayecto con un vehículo de Toyota Rent a Car</h3><p><ul><li>Infórmese sobre las condiciones climáticas antes de iniciar su viaje.</li><li>Dado el estado de las carreteras, por favor conduzca con extrema precaución.</li><li>Realice un chequeo general del vehículo, verificando las luces, los neumáticos, nivel de aceite, entre otros.</li><li>Asegúrese de llevar toda la documentación necesaria, tanto personal como del vehículo.</li><li>Recuerde que todos los pasajeros del vehículo deben utilizar el cinturón de seguridad, incluyendo los de atrás.</li><li>Si usted se acompaña en su viaje de niños menores de 12 años, por regulación de la Ley de Tránsito ellos deben viajar en una silla o asiento de seguridad.</li><li>Cambie su dinero, en bancos o agencias autorizadas, nunca en las calles o a personas.</li><li>Asegúrese de haber descansado un mínimo de 8 horas la noche anterior, si realizará un viaje largo.</li><li>Para obtener información sobre la situación del país, consulte el sitio web oficial turístico de Costa Rica <a href="http://www.visitcostarica.com" target="_blank"></a></li></ul></p><h3>Durante el viaje</h3><p><ul><li>Llame gratis al 800-123-RENT, en cualquier momento para consultas durante su estadía en el país.</li><li>Mantenga siempre las puertas con seguros y las ventanas cerradas.</li><li>Evite conducir a altas horas de la noche o con lluvia intensa.</li><li>Transite únicamente por calles y caminos oficiales.</li><li>En viajes largos, trate de tomar pequeños descansos.</li><li>Evite detenerse en lugares oscuros o solitarios, busque siempre zonas públicas o transitadas.</li><li>No deje objetos de valor o documentos dentro del vehículo, incluso si éste se encuentra cerrado.</li><li>Respete la señalización y normas de tránsito.</li><li>En caso de que se reviente un neumático, estaciónese correctamente y con la debida señalización. Use las luces intermitentes del auto, colóquese el chaleco reflector. Ponga el primer triángulo a 50 metros detrás del automóvil y el segundo triángulo a 20 metros delante del vehículo.</li><li>Si golpean el vehículo de manera sospechosa o lo están siguiendo durante el viaje, busque el lugar público más cercano y marque el 911 de inmediato.</li><li>En la medida de lo posible estacione en parqueos públicos y no en la calle.</li></ul></p><h3>Recuerde</h3><p><ul><li>Para que la cobertura de la protección aplique, deben cumplirse todas las cláusulas descritas en el acuerdo de alquiler.</li><li>Cualquier daño ocasionado al automóvil será su responsabilidad y correrá por cuenta propia, de acuerdo a las especificaciones establecidas en el contrato.</li><li>Las multas de tránsito también son su responsabilidad, éstas deben ser canceladas en un banco oficial, nunca le pague a los oficiales o a personas desconocidas por una infracción.</li><li>Cuando vaya a devolver el vehículo, el tanque de combustible debe estar lleno, a menos que lo haya pre pagado al momento de la renta. En este caso, lo devolverá lo más vacío posible para su mayor conveniencia.</li></ul></p>'
      },
      transit_law: {
        permalink: 'ley-de-transito',
        seo: {
          title: 'Preguntas frecuentes al alquilar un carro | Toyota CR',
          description: 'En esta página usted puede encontrar respuesta a las preguntas más comunes a la hora de rentar un carro en Costa Rica.',
          keywords: 'ley de tránsito, costa rica, TRAC, vehículos, coches, alquiler'
        },
        title: 'Ley de tránsito',
        content: '<h2>Ley de tránsito en Costa Rica</h2><p>La Ley de Tránsito vigente en Costa Rica desde marzo de 2010, establece una mayor severidad en las penas y multas.  Le agradecemos por favor tomar un momento para leer esta información que resume las faltas graves y medias que se establecen y así como sus respectivas sanciones. Ante cualquier duda, le sugerimos dirigirse a cualquiera de nuestros colaboradores de Atención al Cliente clarificar la legislación y penas asociadas, o bien conocer el documento de ley completo.</p><h4>Pena de Cárcel y Retención del Vehículo</h4><p><ul><li>Conducción Temeraria Penal (piques, alcohol con más de 0,75 gramos por litro de sangre y velocidad superior a 150 Km/h), homicidio culposo y/o lesiones culposas.</li></ul></p><p><strong>Nota: Esta infracción implicaría un cargo de lucro cesante y administrativo – legal por decomiso del vehículo rentado.</strong></p><h4>₡227.000 ($400 aprox.) + 30% Timbre del PANI</h4><p><ul><li>Conducción Temeraria Categoría A: Concentración de alcohol igual o superior a 0.5% gramos por litro de sangre, drogas, velocidad superior a 120 Km/h y falso adelantamiento.</li><li>Circular sin haber obtenido licencia de conducir o con la licencia suspendida.</li><li>No utilizar los dispositivos de seguridad para personas menores de edad (booster y sillas de bebé). Aplica para menores de 12 años.</li></ul></p><h4>₡170.250 ($300 aprox) + 30% Timbre del PANI</h4><p><ul><li>Conducción Temeraria Categoría B: Circular con 20 Km/h o más de exceso sobre el límite de velocidad fijado por las señales de tránsito y circular a una velocidad superior a los 25km./h al pasar frente a la entrada y salida de centros educativos, hospitales, clínicas.</li><li>Irrespeto las señales de tránsito fijas, luz roja de un semáforo o virar en ‘U’.</li><li>Utilizar el teléfono celular sin manos libres.</li><li>No utilizar el cinturón de seguridad</li></ul></p><h4>¢113.500 ($200 aprox.) + 30% timbre PANI</h4><p><ul><li>Rebasar por el lado derecho en carreteras de dos carriles con sentidos de vía contraria.</li><li>Detenerse en medio de una intersección y obstruir la circulación.</li><li>Circular un vehículo en la playa.</li><li>Transportar basura o desechos en un vehículo de cualquier naturaleza y arrojarlos en la vía pública.</li></ul></p><h4>₡90.800 ($160 aprox) + 30% Timbre del PANI</h4><p><ul><li>Violar la preferencia de paso.</li><li>Llevar exceso de pasajeros.</li><li>Estacionarse en lugares no permitidos o de discapacitados.</li></ul></p><h4>₡68.100 ($120 aprox.) + 30% Timbre del PANI</h4><p><ul><li>No guardar la distancia con el vehículo que transita adelante.</li><li>Circular en retroceso.</li><li>Conducir con la licencia de conducir vencida.</li><li>Circular por más de tres meses, sin obtener la licencia nacional (solo para licencias extranjeras).</li></ul></p><h4>₡45.400 ($80 aprox.) + 30% Timbre del PANI</h4><p><ul><li>Conducir un vehículo sin portar la licencia de conducir.</li></ul></p><h4>Límites máximos de velocidad</h4><p><ul><li>Áreas Urbanas: 40 Kilómetros por hora.</li><li>Autopistas: 80 Kilómetros por hora.</li><li>Zonas Residenciales: 60 Kilómetros por hora.</li><li>Zonas Escolares: 25 Kilómetros por hora.</li></ul></p><h3 class="important">IMPORTANTE</h3><p><ul><li>Las multas que se generen durante su periodo de renta serán cobradas al final de la misma.</li><li>Nuestros vehículos son revisados diariamente en sistema con el COSEVI para verificar infracciones.</li><li>Según el tipo de infracción cometida, se deberá adicionar gastos de lucro cesante y administrativos-legales.</li></ul></p><p><em>Gracias por su cooperación y conducción responsable durante su viaje.</em></p>'
      },
      faq: {
        permalink: 'preguntas-frecuentes',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Todo lo que necesita conocer sobre la renta de vehículos en Costa Rica. ToyotaRent pone a sus disposición una sección de preguntas frecuentes.',
          keywords: 'FAQ, TRAC, costa rica, alquiler, coches, vehículos, toyota'
        },
        title: 'Preguntas Frecuentes',
        sections: {
          question_1: {
            title: '¿Se puede utilizar la tarjeta de crédito de otra persona para realizar el depósito y la renta del auto?',
            content: '<p>Esto no es posible, cada una de las rentas son personales y solo es aceptada la tarjeta del contratante, el cual será el único autorizado para conducir el vehículo.</p>'
          },
          question_2: {
            title: '¿Se puede utilizar vehículo 4×4 rentado para pasar por ríos o lagunas?',
            content: '<p>No se puede conducir un vehículo propiedad de TOYOTA Rent a Car por ríos, lagunas o similares. Recuerde que los daños ocurridos al automóvil son su responsabilidad, según las especificaciones establecidas en el acuerdo de renta.</p>'
          },
          question_3: {
            title: '¿Puede haber un conductor adicional?',
            content: '<p>Únicamente la persona que haya firmado el acuerdo de alquiler estará autorizada para conducir el vehículo. Sin embargo, puede haber un conductor adicional, el cual deberá estar previamente registrado en el mismo contrato para que sea cubierto por el seguro correspondiente.</p>'
          },
          question_4: {
            title: '¿Puedo devolver el vehículo en una oficina diferente donde lo alquilé?',
            content: '<p>Para su comodidad, puede devolver el vehículo en una oficina diferente, esto tendrá un costo adicional de acuerdo con la estación donde lo entregue.</p>'
          },
          question_5: {
            title: '¿Qué sucede si le ocasiono algún daño al vehículo rentado?',
            content: '<p>El vehículo será su responsabilidad durante el tiempo total de la renta, los daños y perjuicios ocasionados al automóvil correrán por su cuenta. De acuerdo a las condiciones establecidas previamente en el contrato de renta.</p>'
          },
          question_6: {
            title: '¿Es mi responsabilidad alguna multa de tránsito?',
            content: '<p>Cualquier infracción de tránsito es completa responsabilidad de la persona que firmó el acuerdo de alquiler. Recuerde que estas deben cancelarse en bancos o sucursales autorizadas, nunca le pague una multa a un oficial de tránsito o a una persona desconocida.</p>'
          },
          question_7: {
            title: '¿Qué debo hacer si el vehículo presenta algún tipo de problema mecánico?',
            content: '<p>Si puede, diríjase a una gasolinera o bien a un lugar público donde logre estacionar el vehículo. Llame inmediatamente al número gratuito 800-123 Rent (800-123- 7368).</p><p>Si el problema no le permite continuar el camino, encienda las luces intermitentes del auto, póngase el chaleco reflector y coloque los triángulos, el primero detrás del automóvil 50 metros y el segundo triángulo a 20 metros adelante del carro.</p>'
          },
          question_8: {
            title: '¿Cómo se realiza el depósito de garantía?',
            content: '<p>Este se realiza solamente con una tarjeta de crédito o débito, la cual debe pertenecer a la persona que firma el contrato de renta.</p>'
          },
          question_9: {
            title: '¿Puedo viajar con mascotas en el carro?',
            content: '<p>Para evitar posibles daños al vehículo, recomendamos no viajar con mascotas.</p>'
          },
          question_10: {
            title: '¿Cómo funciona la póliza de cancelación o modificación de reservas?',
            content: '<p>Si necesita modificar o cancelar una reservación, deberá hacerlo antes de 24 horas, para poder evitar cargos extras.</p>'
          },
          question_11: {
            title: '¿Qué necesito para alquilar un vehículo?',
            content: '<p><ul><li>Contar con la edad mínima de 21 años</li><li>Tener una licencia de conducir nacional o extranjera</li><li>Una tarjeta de crédito o débito para realizar el depósito de seguridad</li><li>Cédula de identidad o pasaporte al día</li></ul></p>'
          },
          question_12: {
            title: '¿Puedo utilizar el seguro de mi tarjeta de crédito en Costa Rica?',
            content: '<p>Sí, siempre y cuando la marca correspondiente brinde cobertura en Costa Rica.</p>'
          },
          question_13: {
            title: '¿Qué debo hacer en caso de un accidente?',
            content: '<p><ul><li>No mover el vehículo.</li><li>Mantener la calma.</li><li>Llamar al 911 para reportar el accidente y solicitar ayuda.</li><li>Llamar al INS Instituto Nacional de Seguros para reportar el accidente 800-800-8000.</li><li>Encienda las luces intermitentes del auto, póngase el chaleco reflector y coloque el primer triángulo a 50 metros detrás del automóvil y el segundo triángulo a 20 metros delante del carro.</li><li>Llamarnos inmediatamente al número gratuito 800-123Rent (800-123- 7368).</li><ul></p>'
          },
          question_14: {
            title: '¿Ofrecen servicios de traslado de o a hoteles?',
            content: '<p>No, solamente en los aeropuertos Juan Santamaría (Estación de Alajuela) y Daniel Oduber (Estación de Liberia).</p>'
          },
          question_15: {
            title: '¿Puedo alquilar un vehículo para salir de Costa Rica e ir a otro país? (Nicaragua/Panamá)',
            content: '<p>Los vehículos de alquiler son para uso exclusivo en el territorio nacional, estos no pueden salir del país.</p>'
          },
          question_16: {
            title: '¿Cómo puedo llegar a las oficinas?',
            content: '<p>Puede verificar la ubicación de nuestras oficinas en la página “Ubicaciones” de nuestro sitio web. Presione aquí para ser redirigido. O si lo desea, puede adquirir el servicio Fast Pass, el cual le facilita el traslado y los trámites de alquiler entregándole el auto rentado en el Aeropuerto Juan Santamaría o Daniel Oduber en Liberia (este servicio aplica únicamente para la estación de Alajuela y la de Liberia).</p>'
          },
          question_17: {
            title: '¿Cuál es el período mínimo y máximo del alquiler de un vehículo?',
            content: '<p>El período mínimo que puede rentar un vehículo es de 24 horas y el máximo de 30 días.</p>'
          },
          question_18: {
            title: '¿Cuál es el monto requerido para el depósito de garantía?',
            content: '<p>Se establece en monto estándar de $1.000 con tarjeta de crédito o de $2.000 con débito. Para la línea de vehículos Premium, el depósito es de $2.000 con tarjeta de crédito.</p>'
          },
          question_19: {
            title: '¿Cuál es la edad mínima para alquilar un vehículo?',
            content: '<p>La edad mínima para alquilar un vehículo es de 21 años.</p>'
          }
        }
      },
      online_payment_policies: {
        permalink: 'politicas-pago-en-linea',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Cancele su reservación de alquiler de coches en Costa Rica de manera segura y rápida con ToyotaRent.',
          keywords: 'toyota, costa rica, vehículos, alquiler, coches, trac, toyotarent'
        },
        title: 'Políticas Pago en Línea',
        content: '<h2>Política de Reservas:</h2><p>Nuestras tarifas se encuentran expresadas en Dólares Americanos. No obstante, de acuerdo a condiciones propias del negocio, Toyota Rent a Car se reserva el derecho de modificar estas tarifas publicándolas en nuestro sitio web.</p><p>En caso de que su reserva quede en un lapso donde se combinen las Temporada Alta y Baja, se aplicará la tarifa que rija el primer día de renta.La tarjeta de crédito con la que se realiza el prepago debe ser la misma que se utilizará para cancelar el monto completo del servicio, y la misma debe ser presentada físicamente al momento de retirar el vehículo.</p><p>El servicio de Pago en Línea le permite acceder a un descuento de 10% en su tarifa de tiempo y kilometraje en su reservación.</p><p>Para hacer efectivo este servicio de reserva, Toyota Rent a Car cobrará un 10% del monto total de su reserva de la tarjeta de crédito que usted ingrese en nuestro sistema.Su reserva será confirmada un día después de haberla recibido.</p><h2>Política de Cancelación y No Show:</h2><p>En caso de cancelar su reservación o no presentarse a retirar el vehículo en la fecha de inicio de su reserva –No Show-, Toyota Rent a Car no devolverá el monto debitado de su cuenta en el momento que realizó el servicio de Pago en Línea.</p><h2>Tarjeta Crédito o Débito</h2><p>Usted debe presentar en el momento de retirar el automóvil, la tarjeta de debido o crédito que utilizó en el pago en línea, además de su identificación.<p>Para  depósitos de garantía NO SE ACEPTAN TARJETAS DE DÉDITO, debido a que el proceso de liberación de los flotantes puede tardar hasta 45 días hábiles.</p> <h2>Llegadas tardías / Politicas de no llegada </h2><p>Todas las reservaciones se mantendrán por 1 hora de la especificada en la reservación, después de transcurrido este tiempo se denominara como NO SE PRESENTO (NO SHOW). Si el cliente llega después de pasado ese tiempo el vehículo no se encontrara asegurado. Haremos todo lo que sea posible por proveer otro vehículo, este puede ser de una categoría y tarifas diferentes a las ofrecidas en la reserva original.</p><p>Los números de vuelo provistos son meramente informativos, La responsabilidad de informar de cualquier retraso recae en el cliente.</p>'
      },
      awards: {
        permalink: 'premios',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'ToyotaRent ha sido premiado por su calidad del servicio y programas sociales en Costa Rica. Cuando alquila un vehículo Toyota está apoyando esos programas.',
          keywords: 'premios, reconocimientos, trac, toyota, alquiler, vehículos, carros, coches'
        },
        title: 'Reconocimientos',
        content: '<p>Durante los últimos cinco años, Toyota Rent a Car ha aplicado una fórmula exitosa que hizo a la empresa merecedora del Premio a la Excelencia en tres distintas categorías: Liderazgo y Planificación Estratégica, Enfoque al Cliente y el Mercadeo y Enfoque al Talento Humano.</p><p>Estos galardones son el resultado de una apuesta que Toyota Rent a Car ha hecho: una propuesta de valor basada en brindar servicios únicos, diferentes y de la más alta calidad. Y no solo eso: la empresa entendió que para convertir esa propuesta en una realidad, debía invertir en la profesionalización del recurso humano.</p><p>En Toyota Rent A Car creemos que los valores corporativos son el eje transversal que guía la conducta individual hacia el alcance de los objetivos estratégicos. Los colaboradores son motivados a crecer dentro de la empresa y a acercarse cada vez más a la excelencia. A esto lo llamamos “cerrar brechas”. Cada uno de los miembros del equipo cierra sus brechas a través de un programa de capacitación y un sistema de evaluación 180° que señala objetivamente los avances logrados y los pasos por seguir. Esto ha generado altos niveles de motivación en los colaboradores y un fortalecimiento de la cultura organizacional que trasciende a la compañía y es percibida por los clientes.</p><p>Adicionalmente, la compañía estudia las necesidades de los diferentes perfiles de cliente para desarrollar servicios innovadores que respondan a ellas, diferenciándose de la competencia y estableciendo relaciones duraderas con los clientes con base en la entrega de valor.</p><p>Toyota Rent a Car cuenta con un programa de innovación para la introducción de nuevos productos y servicios, donde los colaboradores proponen e implementan ideas innovadoras, que llegan a convertirse en servicios.</p><p>Como parte de su oferta diferenciada, cuenta con la línea de vehículos premium, que combina modelos Toyota de lujo como el Camry Híbrido, Prado y Sequoia con servicios complementarios innovadores, como servicios de chofer, entrega de vehículos directamente en el aeropuerto, atención para bodas, combustible prepago y guarda equipaje, además de los pequeños detalles que hacen la vida de los clientes más placentera.</p><p>Adicionalmente, han adoptado un enfoque ambiental en el desarrollo de todos sus procesos para reducir su huella de carbono por el uso de combustibles. El impacto producido por sus servicios es medido y mitigado a través del mantenimiento continuo de los autos y de acciones ecológicas específicas como la plantación de árboles nativos en Guanacaste. De esta manera, se garantiza a los clientes que su preferencia está contribuyendo activamente con la conservación del entorno natural.</p><p>Las prácticas de excelencia implementadas por la compañía le han permitido gozar de un clima organizacional muy saludable, con un nivel de satisfacción interna de 85% y un índice de productividad que aumentó en un 12% en el último año.</p><p>Este premio marca el inicio de un nuevo ciclo para Toyota Rent a Car, en donde cada día, con aún más responsabilidad y visión, el esfuerzo se enfocará en la innovación a través de productos, servicios e iniciativas que enriquezcan las experiencias de nuestros clientes, protejan nuestro medio ambiente y contribuyan con el crecimiento de nuestro personal.</p>'
      },
      news_updates: {
        permalink: 'noticias-notificaciones',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Las mejores promociones y noticias sobre Costa Rica y todo sobre el alquiler de coches Toyota. Entérese de cada promoción especial en este canal.',
          keywords: 'noticias, TRAC, toyota, costa rica, vehículos, carros, coches, toyotarent'
        },
        title: 'Noticias y Notificaciones',
        articles: [
          {
            title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
            summary: '',
            date: '',
            thumbnail: '',
            content: ''
          }
        ]
      },
      contact_us: {
        permalink: 'contactenos',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Contacte a nuestro equipo especializado en renta de coches en Costa Rica. Alquile su vehículo sin problema alguno con ToyotaRent',
          keywords: 'contacto, alquiler, vehículos, coches, carros, costa rica, trac, toyotarent'
        },
        issues: [
          'Tipo de Asunto *',
          'Disputas de Conductores Adicionales',
          'Después de las horas de retorno',
          'Clase de coche reservada no disponible',
          'Comisiones',
          'Cobertura / seguro',
          'Reclamaciones por daños',
          'Disputa de daños',
          'Cantidad del depósito',
          'Conductor / equipo especial discapacitados',
          'Disputas de Retorno Temprano / Tardío',
          'Asistencia en carretera de emergencia',
          'Indiferencia de los empleados',
          'Exceso de espera',
          'Puntos de viajero frecuente',
          'Disputa de Carga de Combustible',
          'Restricciones geográficas',
          'Gobierno',
          'Lost & Found',
          'Conflicto unidireccional',
          'Otro',
          'Pague ahora',
          'Extensión de alquiler',
          'Extensión de Alquiler - Solicitud Final',
          '2ª solicitud de extensión de alquiler',
          'Solicitar copia del recibo',
          'Error de reserva',
          'Disputas en el borde de la carretera',
          'Servicio de transporte',
          'Disputas fiscales',
          'Peajes',
          'Cargos totales',
          'Tránsito / infracción',
          'Caída no autorizada',
          'Disputas por Edad',
          'Actualizar las disputas',
          'Limpieza del vehículo',
          'Problema mecánico del vehículo'
        ]
      },
      landing: {
        iron_man: {
          seo: {
            title: 'Iron man | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          reserve_today: 'Reserve hoy!',
          make_reservation_btn: 'Realizar Reservación',
          table: {
            header: {
              col_1: 'Model',
              col_2: 'Daily with basic insurance',
              col_3: 'Daily with full insurance',
              col_4: 'Weekly with basic insurance',
              col_5: 'Weekly with full insurance',
            },
            data: [
              {
                col_1: 'Model',
                col_2: 'Daily with basic insurance',
                col_3: 'Daily with full insurance',
                col_4: 'Weekly with basic insurance',
                col_5: 'Weekly with full insurance',
              },{
                col_1: 'Yaris',
                col_2: '$42',
                col_3: '$56',
                col_4: '$268',
                col_5: '$369'
              },
              {
                col_1: 'Bego',
                col_2: '$53',
                col_3: '$66',
                col_4: '$338',
                col_5: '$432'
              },
              {
                col_1: 'Fortuer',
                col_2: '$75',
                col_3: '$88',
                col_4: '$478',
                col_5: '$571'
              },
              {
                col_1: 'Prado',
                col_2: '$90',
                col_3: '$103',
                col_4: '$568',
                col_5: '$661'
              }                                                           
            ]
          }          
        },
        envision: {
          seo: {
            title: 'Envision | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          form: {
            vehicles: [
              'Bego or similar, Rush or similar'
            ],
            insurance: [
              'Diario con seguro básico',
              'Diario con seguro completo',
              'Semanal con seguro básico',
              'Semanal con seguro completo'
            ],
            legend: '<p><strong>Protección Básica Obligatoria:</strong> La Protección Básica Obligatoria debe ser comprada junto con el alquiler del vehículo. La protección obligatoria otorga al cliente un deducible máximo de US $ 1,200 por daños causados al vehículo alquilado (exoneración de daños por colisión de CDW). El cliente tiene derecho a un deducible mínimo de US $ 300 o 20% del valor total de todos los daños causados a la propiedad de terceros (LDW Liability Damage Waiver).</p><p>El seguro básico está incluido en el precio que proporcionamos, así como todos los cargos e impuestos adicionales. El seguro básico obligatorio tiene que ser comprado por ley en Costa Rica.</p>'
          },
          reserve_today: '¡Reserve hoy!',
          envision: 'Envision',
          make_reservation_btn: '¡Pagar ahora!',
          table: {
            header: {
              col_1: 'ART',
              col_2: 'Precio regular',
              col_3: 'Precio Envision',
            },
            data: [
              {
                col_1: 'ART',
                col_2: 'Precio regular',
                col_3: 'Precio Envision',
              },              
              {
                col_1: 'Sillas de seguridad para niños',
                col_2: '$6 x día',
                col_3: '$5 x día',
              },
              {
                col_1: 'Quick Pass',
                col_2: '$10 x día',
                col_3: '$8 x día',
              },
              {
                col_1: 'Mobile Hot Spot ',
                col_2: '$12.99 x día',
                col_3: '$10.99 x día',
              },
              {
                col_1: 'Seguro Titanium ',
                col_2: '$31.42 x día ',
                col_3: '$27.32 x día',
              },              
              {
                col_1: 'Fast Pass ',
                col_2: '$15  Fee',
                col_3: '$13 Fee',
              },                  
              {
                col_1: 'Fast Return (Solo clientes con paquete de seguro Titanium)',
                col_2: '$15  Fee',
                col_3: '$13 Fee',
              },                   
            ],
            footer: 'Seguro Básico es obligatorio en Costa Rica, el seguro completo es opcional.'
          },
          content: '<h3>Tarifa Especial para Envision</h3><ul><li>$55 USD por día</li><li>$275 USD por semana</li></ul><h3>Incluye:</h3><ul><li>Seguro Básico</li><li>Conductor Adicional Gratis</li></ul><h3>Condiciones</h3><ul><li>Recoge en Aeropuerto SJO y Regresa Aeropuerto SJO </li><li>Solo tarifas pre pagadas</li></ul>'
        },
        jungle_jam: {
          seo: {
            title: 'Jungle Jam | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          form: {
            vehicles: [
              'Yaris: Compacto, 5 pasajeros',
              'Corolla: Compacto, 5 pasajeros',
              'Bego: Pequeño 4x4 SUV, 5 pasajeros',
              'Rav4: Mediano 4x4 SUV, 5 pasajeros',
              'Prado, Grande 4x4 SUV, 7 pasajeros'
            ],
            insurance: [
              'Diario con seguro básico',
              'Diario con seguro completo',
              'Semanal con seguro básico',
              'Semanal con seguro completo'
            ],
            legend: '<p><strong>Protección Básica Obligatoria:</strong> La Protección Básica Obligatoria debe ser comprada junto con el alquiler del vehículo. La protección obligatoria otorga al cliente un deducible máximo de US $ 1,200 por daños causados al vehículo alquilado (exoneración de daños por colisión de CDW). El cliente tiene derecho a un deducible mínimo de US $ 300 o 20% del valor total de todos los daños causados a la propiedad de terceros (LDW Liability Damage Waiver).</p><p>El seguro básico está incluido en el precio que proporcionamos, así como todos los cargos e impuestos adicionales. El seguro básico obligatorio tiene que ser comprado por ley en Costa Rica.</p>'
          },
          table: {
            header: {
              col_1: 'Vehículo',
              col_2: 'Diario soin seguro básico',
              col_3: 'Diario con seguro básico',
              col_4: 'Diario sin seguro completo',
              col_5: 'Diario con seguro completo',
              col_6: 'Semanal soin seguro básico',
              col_7: 'Semanal con seguro básico',
              col_8: 'Semanal sin seguro completo',
              col_9: 'Semanal con seguro completo'
            },
            data: [
              {
                col_1: 'Yaris',
                col_2: '$37',
                col_3: '$54',
                col_4: '$37',
                col_5: '$78',
                col_6: '$222',
                col_7: '$341',
                col_8: '$341',
                col_9: '$509'
              },
              {
                col_1: 'Corolla',
                col_2: '$44',
                col_3: '$62',
                col_4: '$44',
                col_5: '$87',
                col_6: '$265',
                col_7: '$390',
                col_8: '$265',
                col_9: '$565'
              },
              {
                col_1: 'Bego',
                col_2: '$52',
                col_3: '$74',
                col_4: '$52',
                col_5: '$99',
                col_6: '$313',
                col_7: '$464',
                col_8: '$313',
                col_9: '$639'
              },
              {
                col_1: 'Rav-4',
                col_2: '$68',
                col_3: '$97',
                col_4: '$68',
                col_5: '$124',
                col_6: '$405',
                col_7: '$608',
                col_8: '$405',
                col_9: '$797'
              },
              {
                col_1: 'Prado',
                col_2: '$95',
                col_3: '$123',
                col_4: '$95',
                col_5: '$153',
                col_6: '$567',
                col_7: '$767',
                col_8: '$567',
                col_9: '$977'
              }
            ],
            footer: 'Seguro Básico es obligatorio en Costa Rica, el seguro completo es opcional.'
          },
          content: '<h3>Exclusive Benefits for Jungle Jam Participants</h3><ul><li>The security deposit if you only choose basic insurance will be $750, if full insurance is chosen than coverage will drop to $0.</li><li>Awarded a complimentary accessory either rack, basket, cooler, baby seat or booster, each of these has a normal cost of $ 5 per day.</li><li>Granted free additional driver, which normally has a cost of $3 per day.</li><li>No amount will be charged if dropped off at any of our offices, for example rented in Quepos and returned to our office in Liberia.</li><li>No additional cost to deliver or pick-up rental car from our office in Dominical or Envision Site.</li><li>Moneyback guarantee with every reservation made by you. This means that our cars will be there no matter what happens. If something does happen we will give them their money back for their reservation or their car will be free. We are the most reliable company and we want to offer this to you because we know that companies overbook and people arrive and have no car available.</li><li>We will extend to 4 hours the time of giving back their car. So, if they rent at 12 midday... on the return date they can do it at 4pm with no extra charge.</li></ul>'
        },
        promo: {
          seo: {
            title: 'Promo | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          legend: {
            text: 'Descuento'
          },
          form: {
            vehicles: {
              v: 'Avanza: Minivan, Auto, 7 pasajeros',
              f: 'Bego: Compacto 4x4, Auto, 5 pasajeros',
              g: 'Bego: Compacto 4x4, Man, 5 pasajeros',
              e: 'Corolla: Carro estandar, Auto, 5 pasajeros',
              l: 'Fortuner: Full 4x4, Auto, 7 pasajeros',
              q: 'Hiace: Microbus, Man, 12 pasajeros',
              u: 'Hiace: Microbus, Man, 16 pasajeros',
              m: 'Hilux: Pickup, Man, 5 pasajeros',
              s: 'Prado: Full 4x4, Auto, 7 pasajeros',
              p: 'Prado: Premium 4x4, 7 pasajeros',
              h: 'Rav4: Estandar 4x4, Auto, 5 pasajeros',
              c: 'Yaris: Carro compacto, Man, 5 pasajeros',
              d: 'Yaris: Carro compacto, Auto, 5 pasajeros'
            },
            insurance: [
              'Seguro básico',
              'Seguro completo'
            ],
            legend: '<p><strong>Protección Básica Obligatoria:</strong> La Protección Básica Obligatoria debe ser comprada junto con el alquiler del vehículo. La protección obligatoria otorga al cliente un deducible máximo de US $ 1,200 por daños causados al vehículo alquilado (exoneración de daños por colisión de CDW). El cliente tiene derecho a un deducible mínimo de US $ 300 o 20% del valor total de todos los daños causados a la propiedad de terceros (LDW Liability Damage Waiver).</p><p>El seguro básico está incluido en el precio que proporcionamos, así como todos los cargos e impuestos adicionales. El seguro básico obligatorio tiene que ser comprado por ley en Costa Rica.</p>'
          },
          table: {
            header: {
              col_1: 'Vehículo',
              col_2: 'Diario soin seguro básico',
              col_3: 'Diario con seguro básico',
              col_4: 'Diario sin seguro completo',
              col_5: 'Diario con seguro completo',
              col_6: 'Semanal soin seguro básico',
              col_7: 'Semanal con seguro básico',
              col_8: 'Semanal sin seguro completo',
              col_9: 'Semanal con seguro completo'
            },
            data: [
              {
                col_1: 'Yaris',
                col_2: '$37',
                col_3: '$54',
                col_4: '$37',
                col_5: '$78',
                col_6: '$222',
                col_7: '$341',
                col_8: '$341',
                col_9: '$509'
              },
              {
                col_1: 'Corolla',
                col_2: '$44',
                col_3: '$62',
                col_4: '$44',
                col_5: '$87',
                col_6: '$265',
                col_7: '$390',
                col_8: '$265',
                col_9: '$565'
              },
              {
                col_1: 'Bego',
                col_2: '$52',
                col_3: '$74',
                col_4: '$52',
                col_5: '$99',
                col_6: '$313',
                col_7: '$464',
                col_8: '$313',
                col_9: '$639'
              },
              {
                col_1: 'Rav-4',
                col_2: '$68',
                col_3: '$97',
                col_4: '$68',
                col_5: '$124',
                col_6: '$405',
                col_7: '$608',
                col_8: '$405',
                col_9: '$797'
              },
              {
                col_1: 'Prado',
                col_2: '$95',
                col_3: '$123',
                col_4: '$95',
                col_5: '$153',
                col_6: '$567',
                col_7: '$767',
                col_8: '$567',
                col_9: '$977'
              }
            ],
            footer: 'Seguro Básico es obligatorio en Costa Rica, el seguro completo es opcional.'
          },
          content: '<h3>Exclusive Benefits for Jungle Jam Participants</h3><ul><li>The security deposit if you only choose basic insurance will be $750, if full insurance is chosen than coverage will drop to $0.</li><li>Awarded a complimentary accessory either rack, basket, cooler, baby seat or booster, each of these has a normal cost of $ 5 per day.</li><li>Granted free additional driver, which normally has a cost of $3 per day.</li><li>No amount will be charged if dropped off at any of our offices, for example rented in Quepos and returned to our office in Liberia.</li><li>No additional cost to deliver or pick-up rental car from our office in Dominical or Envision Site.</li><li>Moneyback guarantee with every reservation made by you. This means that our cars will be there no matter what happens. If something does happen we will give them their money back for their reservation or their car will be free. We are the most reliable company and we want to offer this to you because we know that companies overbook and people arrive and have no car available.</li><li>We will extend to 4 hours the time of giving back their car. So, if they rent at 12 midday... on the return date they can do it at 4pm with no extra charge.</li></ul>'
        },
        promo_7x5: {
          seo: {
            title: 'Promo 7x5 | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: '¡7x5!',
          sub_title: 'Rente un vehículo por 7 días y los últimos dos días son gratis'
        },
        bego: {
          seo: {
            title: 'Promo Bego | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Por solo $45',
          extra_title: 'Seguro básico incluido',
          restriction_apply: '*Aplican resticciones'
        },
        blackBego: {
          seo: {
            title: 'Promo Bego Viernes Negro | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Only for $40',
          img: 'begoBlack.png',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restriction apply'
        },
        blackYaris: {
          seo: {
            title: 'Promo Sedan Viernes Negro | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Only for $40',
          img: 'yarisBlack.png',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restriction apply'
        },
        hiaci: {
          seo: {
            title: 'Promo Hiace | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Hiace',
          sub_title: 'Por solo $90',
          extra_title: 'Seguro básico incluido',
          restriction_apply: '*Aplican resticciones'
        },      
        santasPromociones: {
          seo: {
            title: 'Santas Promociones | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          img: 'santa-promocion',
          title: 'Santas Promociones',
          content: '<ul><li>Fortuner por solo $100</li><li>Bego por solo $50</li><li>Yaris por solo $35</li></ul>',
          extra_title: 'Seguro básico incluido',
          restriction_apply: '*Aplican resticciones'
        },                  
      },
      resources: {
        permalink: 'resources',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Resources',
        content: '<ul><li><a href="http://www.traveltourismdirectory.info/Accommodation___Lodging/Timeshare/">Visit Timeshares</a></li><li><a href="http://visitgoodwood.com/" title="Directory of Great Travel Sites">Directory of Great Travel Sites</a></li><li><a href="http://travel-websites.org/">Travel Websites</a></li><li><a href="http://www.silverspurmotel.net" target="_blank">Silver Spur Motel  Burns, Oregon</a></li><li><a href="http://www.lowratesflights.com/">US To Europe</a></li><li><a href="http://www.best-travel-deals-tips.com" target="_blank">Best Travel Deals Tips</a></li><li><a href="http://www.greatville.com">GreatVille Travel Directory</a> </li><li><a href= "http://www.willgoto.com">Willgoto, World travel directory and travel guide</a></li><li><a href="http://www.travel-niche.com/">Travel Niche directory.</li></ul>'
      },
      policy: {
        permalink: 'politica-de-privacidad-y-seguridad',
        seo: {
          title: 'Política de Privacidad y Seguridad',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Política de Privacidad y Seguridad',
        content: '<h2>Política de Privacidad</h2><p>Esta política de Privacidad explica cómo Sociedad Rentacar Centroamericana, propietario del sitio web Toyotarent.com (Toyotarent), usa y protege la información que es proporcionada por sus usuarios al momento de utilizar este sitio web. Esta Política de Privacidad es aplicable independientemente del dispositivo que se esté utilizando para acceder al sitio web. Toyotarent se reserva el derecho de cambiar esta Política de Privacidad en cualquier momento, por lo que recomendamos revisar ocasionalmente para asegurarse que esté de acuerdo con los cambios.</p><h2>Recopilación de Información</h2><p>Toyotarent necesita recopilar su información personal para rentarle un vehículo o proveerle alguno de nuestros otros servicios. La información es recolectada en el mostrador al momento de rentar un vehículo, reserva en línea y reserva por teléfono e incluye:</p><ul><li>Nombre completo.</li><li>Número de teléfono.</li><li>Información demográfica (País, ciudad, código postal, dirección física)Dirección de correo electrónico.</li><li>Información de su vuelo, esto para brindarle un servicio personalizado a su llegada a nuestro país.</li><li>Información de pago como los detalles de su tarjeta de débito / crédito.</li></ul><p>En ocasiones es posible que combinemos su información personal con información recibida de otras fuentes para proveerle un servicio personalizado y sin problemas.</p><p>Esta información personal la podemos obtener de un tercero, como una agencia de viajes o su empleador. Podemos recopilar información técnica sobre sus dispositivos cuando usted utiliza nuestro sitio web. En la sección Datos en Línea puede encontrar más información de qué información es recopilada.</p><h2>Datos en Línea</h2><p>Cuando usted visita nuestro sitio web, Toyotarent recopila automáticamente información técnica. Esta sección le explicará más acerca de la información que recopilamos y porqué la recopilamos.<p><p>Dirección IP y otra información de navegación: Cuando visita nuestro sitio web recopilamos su dirección IP para ayudar en el diagnóstico de problemas, administración del sistema y auditar el uso de nuestro sitio web. Podemos recopilar la información del navegador, los archivos vistos en nuestro sitio (páginas HTML, imágenes, etc.), Sistema Operativo, fecha y hora de acceso.</p><p>Cookies y otras tecnologías: Para ayudar a reconocerlo utilizamos cookies, scripts y otras tecnologías. Utilizamos esta información para conocer el uso de nuestro sitio web para identificar tendencias y patrones y poder ofrecer una experiencia personalizada de navegación.</p><h2>Mercadeo</h2><p>Toyotarent en ocasiones puede compartir su información personal con terceros para realizar promociones o enviar comunicaciones de mercadeo por correo electrónico. La información que recopilamos nos ayuda a:</p><ul><li>Enviar información relevante sobre ofertas y promociones especiales por correo electrónico.</li><li>Personalizar la experiencia de utilización en nuestro sitio web.</li></ul><h2>Analytics</h2><p>Nosotros utilizamos sistemas analíticos como Google Analytics, para ayudarnos a comprender la funcionalidad de nuestro sitio web en sus dispositivos. La información que se administra sólo muestra el comportamiento de navegación en nuestro sitio web y no está conectada directamente a usted.</p><h2>Seguridad de la información</h2><p>Para seguridad de su información personal es muy importante para nosotros. Nos aseguramos de qué su información esté protegida de uso sin autorización, acceso, alteración, destrucción o pérdida. Para proteger su información personal sensible, utilizamos firewalls y métodos de encriptación (TLS) para asegurar la confidencialidad e integridad de la información.</p><h2>Legislación aplicable</h2><p>Esta política de privacidad y seguridad de Toyotarent se rigen de acuerdo con la Ley de Protección de la persona frente al tratamiento de sus datos personales, número 8968 de la República de Costa Rica.</p><h2>Puntos de contacto</h2><p>En caso de dudas, preguntas o sugerencias puede comunicarse con Toyotarent a través del formulario en línea que se encuentra en la sección Servicio al Cliente, al correo electrónico servicio sac@toyotarent.com o al número de teléfono +(506)-2105- 3400.</p>'
      },
      interbus: {
        permalink: 'interbus',
        seo: {
          title: 'Reserve con Interbus',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Reserve con Interbus',
        content: '<iframe src="https://www.interbusonline.com/apps/book/bookIframe.php?pinz=1670&pinx=4625ce59976cc377bdc3845cab1538b15f36a658&type=0" width="300" height="500" frameborder="0" allowtransparency="true"></iframe>'
      },
      no_hidden_fees: {
        permalink: 'sin-cargos-adicionales',
        title: 'Sin cargos adicionales',
        content: '<h2>Sin cargos adicionales a la tarifa mostrada en la web</h2><h3>Más información:</h3><ul><li>Si usted desea adquirir protecciones adicionales, puede hacerlo en el counter.</li><li>La tarifa que usted está adquiriendo en este sitio web incluye libre kilometraje y los adicionales que usted escoja.</li><li>Como parte de nuestra política ambiental, tenemos un cobro único de $1,85 el cual no es obligatorio. Con este cobro nos aseguramos de que estamos devolviendo el impacto generado al ambiente. Más información <a  rel="nofollow" href="http://www.fonafifo.go.cr/" target="_black">http://www.fonafifo.go.cr/<l/i></ul>'
      },
      blog: {
        permalink: 'blog',
        seo: {
          title: 'Alquiler de carros en Costa Rica por Toyota – toyotarent.com ',
          description: 'Alquiler de vehículos en Costa Rica marca Toyota, con el respaldo de ToyotaRent. Rente su coche directamente con la agencia.',
          keywords: 'renta, coches, vehículos, costa rica, alquiler, toyota, toyotarent'
        },
        title: 'Blog',
        content: '',
        btnReadMore: 'Leer más',
        textCategoria: 'Categorias'        
      },      
    },
    cta: {
      book_service: 'Reservar Servicio',
      book_now: 'Reservar ahora',
      book_car: 'Reservar Vehículo',
      terms_conditions: 'Términos y Condiciones',
      contact_us: 'Contáctenos',
      waze: 'Cómo llegar'
    },
    book: {
      or_similar: 'o similar',
      day: 'día'
    },
    enjoy_great_deals: {
      main_title: 'Disfute de Grandes Ofertas!'
    },
    email_code: {
      title: 'Bienvenido a Reactive Finance Tracking !!!',
      subject: 'RFT [Credenciales]',
      credential: 'Tu credenciales son:',
      user: 'Usuario: ',
      code: 'Código: '
    },
    actions: {
      edit: 'Editar',
      delete: 'Eliminar',
      save: 'Guardar',
      update: 'Actualizar'
    },
    months: {
      january: 'Enero',
      february: 'Febrero',
      march: 'Marzo',
      april: 'Abril',
      may: 'Mayo',
      june: 'Junio',
      july: 'Julio',
      august: 'Agosto',
      september: 'Septiembre',
      october: 'Octubre',
      november: 'Noviembre',
      december: 'Diciembre'
    },
    calendar_days: ['Dom', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sab'],
    calendar_months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
  },
  en: {
    menu: {
      cars: {
        title: 'Car Rental Costa Rica',
        titleNew: 'Renting',
        subMenu: {
          item1: 'Sedans',
          item2: 'SUVS',
          item3: 'Premium Fleet',
          item4: 'Mini Buses',
          item5: 'Pick ups',
        // item6: 'Minivans'
        }
      },
      products_services: {
        title: 'Products & Services',
        subMenu: {
          item1: 'Mobile Hotspot',
          item2: 'GPS (Global Positioning System)',
          item3: 'Toll Pass',
          item4: 'Roof Racks',
          item5: 'Kids and baby seats',
          item6: 'Chauffeur Service',
          item7: 'Hourly Car Rental',
          item8: 'Fast Lane',
          item9: 'Wedding Services',
          item10: 'Prepaid Fuel'
        }
      },
      locations: {
        title: 'Locations',
        subMenu: {
          item1: 'Main Offices',
          item2: 'Int. Airport Offices',
          item3: 'Liberia',
          item5: 'Nosara',
          item7: 'Puerto Jiménez'
        }
      },
      fleet: {
        title: 'Fleet'
      },
      about: 'About Us',
      blog: 'News',
      news_updates: 'News & Updates',
      awards: 'Awards',
      contact_us: 'Contact Us',
      online_payment_policies: 'On-Line Payment Policies',
      special_offers: 'Special Offers',
      customer_care: 'Customer Care',
      policy: 'Privacy And Security Policy',
      travel_assistance: {
        title: 'Travel Assistance',
        subMenu: {
          item1: 'Practical Tips',
          item2: 'Transit Law',
          item3: 'FAQ'
        }
      },
      languages: 'Languages',
      customer_form: 'Customer Form',
      contact_form: 'Contact Form'
    },
    blog: {
      categories: 'Categories',
      all_categories: 'All Categories',
      back_to_blog: 'Back'
    },
    header: {},
    footer: {
      phone: 'Phone',
      textUsaCanada: 'Enabled for Canada and the United States (USA)'
    },
    discount: 'discount!',
    specs: {
      motor: 'cc',
      manual: 'Manual',
      automatic: 'Automatic',
      gasoline: 'Gasoline',
      diesel: 'Diesel',
      seats: 'Passengers',
      large_bag: 'Large bag',
      small_bag: 'Small bag',
      doors: 'Doors'
    },
    portada:[{
      img: 'TRAC-Home1',
      type: 'clean',
      link: '/',
      btn_id: '',
    }],
    promociones:[{
      img: 'promo1',
      link: '/interbus',
      btn_id: '',
    },{
      img: 'promo2',
      type: 'clean',
      link: '/',
      btn_id: '',
    }],
    slider: [                   
      {
        img: 'landing-fee-en',
        type: 'landing-fee',
        landing: 'landing-fee',
        title: '',
        subtitle: '',
        btn_1: 'More info',
        btn_url_1: '/no-hidden-fees',
        btn_2: '',
        btn_url_2: '',
        btn_id: 'sin_cargos_adicionales'
      },      
      {
        img: 'slide-1',
        title: 'Live the Experience',
        subtitle: 'With Confidence.',
        btn_1: 'Suscription Events',
        btn_url_1: '/test1',
        btn_2: 'See Our Cars',
        btn_url_2: '/car-rental-costa-rica',
        btn_id: 'see_our_cars_btn'
      },
      {
        type: 'landing',
        img: 'librehumoEn2',
        hiddenButton: true
      },
      {
        img: 'interbus',
        title: 'Book',
        subtitle: 'With Interbus',
        btn_1: 'More Information',
        btn_url_1: '/interbus',
        btn_2: 'More Information',
        btn_url_2: '/interbus',
        btn_id: 'interbus_btn'
      }
    ],
    widget_book: {
      title: '<strong>Book now in</strong> 4 simple steps',
      start_date: 'Start Date',
      end_date: 'End Date',
      pick_up_location: 'Pick up Location',
      drop_off_location: 'Drop off Location',
      btn_next: 'book now',
      btn_next_id: 'next_btn_step1',
      total_days: 'Total days',
      model: 'Model',
      price: 'Price',
      total_price: 'Total price',
      additional_accessories: 'Additional Accessories',
      booking_summary: 'Booking Summary',
      different_location: 'By selecting different pick-up and drop-off  locations, you might be subject to an additional service fee'
    },
    widget_reserve_form: {
      vehicle: 'Vehicle',
      full_name: 'Full name',
      email: 'Email',
      phone_number: 'Phone number',
      ironman_number: 'Ironman Number',
      book_now_btn: 'Book Now',
      form: {
        vehicle: 'Vehicle',
        full_name: 'Full name',
        email: 'Email',
        phone_number: 'Phone number',
        start_date: 'Start Date',
        end_date: 'End Date',
        pick_up_location: 'Pick up Location',
        drop_off_location: 'Drop off Location',
        insurance: 'Insurance',
        driver_licence_number: "Driver's Licence Number",
        email_settings: {
          title: 'New Reservation',
          subject: 'New Reservation'
        },
        notification: {
          success: {
            title: 'Thank you for reserving with Toyota Rent a Car!',
            text: '<p>One of our representatives will contact you to confirm your reservation and provide you with a reservation number.</p><p>A toyota representative will send an email within the next half hour unless it is after 6pm local time which then it will be sent the next day.</p>',
            type: 'success'
          },
          warning: {
            title: 'Alerta',
            text: 'Por favor revise los datos del formulario.',
            type: 'notice'
          },
          error: {
            title: 'Error',
            text: 'Ha ocurrido un error, por favor intente de nuevo',
            type: 'error'
          }
        }
      }
    },
    hot_deals: {
      sub_title: 'Car Rental Costa Rica',
      main_title: 'Meet our fleet'
    },
    customer_form: {
      step_description_1: 'You may also fill out the following form.',
      step_description_2: 'We will respond as soon as possible.',
      support_description: '',
      full_name: 'Full Name',
      email: 'Email Address',
      telephone: 'Telephone',
      ext: 'Ext',
      reserved_through: {
        title: 'Reserved Through',
        options: {
          option_1: 'Web Page',
          option_2: 'Agency',
          option_3: 'Other'
        }
      },
      location: 'Location',
      rental_agreement_number: 'Rental Agreement #',
      confirmation: 'Confirmation',
      perks_number: 'Perks #',
      type_of_issue: 'Type of issue',
      subject: 'Subject',
      message: 'Message',
      captcha_description: 'Enter the text shown on the image',
      submit: 'Submit',
      cancel: 'Cancel',
      email_settings: {
        title: 'Customer Form',
        subject: 'Customer Form'
      },
      notification: {
        success: {
          title: 'Confirmation',
          text: 'The form was successfully submitted.',
          type: 'success'
        },
        warning: {
          title: 'Warning',
          text: 'Please check the form values.',
          type: 'notice'
        },
        error: {
          title: 'Error',
          text: 'An unexpected error occurred, please try again.',
          type: 'error'
        }
      }
    },
    contact_form: {
      step_description_1: 'You may also fill out the following form.',
      step_description_2: 'We will respond as soon as possible.',
      support_description: '',
      full_name: 'Full Name',
      email: 'Email Address',
      telephone: 'Telephone',
      location: 'Location',
      department: {
        title: 'What department you wish to contact? ',
        options: {
          item1: 'Reservation',
          item2: 'Customer services',
          item3: 'Administration'
        }
      },
      subject: 'Subject',
      message: 'Message',
      captcha_description: 'Enter the text shown on the image',
      submit: 'Submit',
      cancel: 'Cancel',
      email_settings: {
        title: 'Customer Form',
        subject: 'Customer Form'
      },
      notification: {
        success: {
          title: 'Confirmation',
          text: 'The form was successfully submitted.',
          type: 'success'
        },
        warning: {
          title: 'Warning',
          text: 'Please check the form values.',
          type: 'notice'
        },
        error: {
          title: 'Error',
          text: 'An unexpected error occurred, please try again.',
          type: 'error'
        }
      }
    },
    pages: {
      blog: {
        permalink: 'blog',
        seo: {
          title: 'Toyota Car Rental Costa Rica | Find The Best Car Rental Deals',
          description: 'Toyota Car Rental Costa Rica.The best way to discover Costa Rica is driving by yourself. Rent a car at the best price and with the best support. BOOK HERE',
          keywords: 'car rental, costa rica, rent a car, toyota cars, toyota rent, toyota car, rental car, car rentals'
        },
        title: 'Blog',
        content: '',
        btnReadMore: 'Read more',
        textCategoria: 'Categories'
      },      
      home: {
        seo: {
          title: 'Toyota Car Rental Costa Rica | Find The Best Car Rental Deals',
          description: 'Toyota Car Rental Costa Rica.The best way to discover Costa Rica is driving by yourself. Rent a car at the best price and with the best support. BOOK HERE',
          keywords: 'car rental, costa rica, rent a car, toyota cars, toyota rent, toyota car, rental car, car rentals'
        },
        content: '<h1><b>Car Rental San Jose Costa Rica</b></h1><span style="font-weight: 400;">Are you looking for car rental in Costa Rica? Well, you are on the right website. Toyota Rent a Car, is a well-known company located in San Jose with branch offices </span><b><i>near the airport</i></b><span style="font-weight: 400;">. We provide great prices, fleet, and customer service.</span><span style="font-weight: 400;">If you’re looking for a car rental before your visit to Costa Rica, or even if you just landed on our country, </span><i><span style="font-weight: 400;">do not hesitate</span></i><span style="font-weight: 400;"> and make your online reservation by using the BOOK NOW button in order to get a really good price.</span><h2 class="section-title"><b>We really make you save money!</b></h2><span style="font-weight: 400;">Do not spend your money on renting an expensive car in Costa Rica, spend it on what it is worth... </span><b>discovering this beautiful country</b><span style="font-weight: 400;">.</span><span style="font-weight: 400;">It is our goal to help you live an unforgettable experience in Costa Rica by renting a comfortable car with a </span><b>low price</b><span style="font-weight: 400;">. We want you to save money while you enjoy the nature of the country.</span><h2 class="section-title"><b>What should you know before to rent a car?</b></h2><span style="font-weight: 400;">First of all, it’s very important to make sure that you bring a credit card and a driver license. You must be at least 21 years old to complete your reservation. We also highly recommend reading </span><a href="https://www.toyotarent.com/faq"><span style="font-weight: 400;">FAQS page</span></a><span style="font-weight: 400;"> for you to know how traffic laws are applied in Costa Rica.</span><h2 class="section-title"><b>Car inspection and documentation</b></h2><p><span style="font-weight: 400;">We recommend inspecting the car before officially receiving it, so that you can make sure everything is in order. Our customer service team will give you the car in perfect conditions; however, it is recommended that you also take some time to inspect it.</span></p><h2 class="section-title"><b>Our Car Rentals Locations</b></h2><span style="font-weight: 400;">We have several offices on the most strategic areas of Costa Rica such as:</span><ul><li style="font-weight: 400;"><a href="https://www.toyotarent.com/locations/paseo-colon"><span style="font-weight: 400;">Paseo Colo´n, San Jose</span></a></li><li style="font-weight: 400;"><a href="https://www.toyotarent.com/locations/airport"><span style="font-weight: 400;">International Airport - Alajuela</span></a></li><li style="font-weight: 400;"><a href="https://www.toyotarent.com/locations/dominical"><span style="font-weight: 400;">Dominical Beach</span></a></li><li style="font-weight: 400;"><a href="https://www.toyotarent.com/locations/liberia"><span style="font-weight: 400;">Liberia, Guanacaste</span></a></li</ul>'
      },
      cars: {
        permalink: 'car-rental-costa-rica',
        seo: {
          title: 'Car Rental Costa Rica | The Best Toyota Cars at The Best Price',
          description: 'The best Toyota Costa Rica car rentals. Rent your car today and take advantage of the great discounts, Best cars at the best price. Start your trip here.',
          keywords: 'car rental, costa rica, rent a car, promotions, fleet, toyota cars, toyota rent, toyota car, rental car'
        },
        content: '<h1>Car Rental Costa Rica</h1><p>If you&rsquo;re going to visit Toyota is your best <strong>Car Rental Costa Rica</strong> choice, we have a large variety of cars for city and adventure. Toyota car rental also provide you the best customer service and insurances, we have more than 4 offices around Costa Rica where you can pick up and/or drop-off your car rental, our offices are located on: <strong>Liberia</strong>, <strong>Alajuela Airport</strong>, <strong>San Jos&eacute;</strong>, <strong>Nosara</strong>, <strong>Puerto Jim&eacute;nez</strong>, and <strong>Dominical</strong> as you can see Toyota is one of the best <strong>Costa Rica car rental</strong> locations.</p><h2 class="section-title">Our Car Rentals Options</h2><p>Toyota <strong>car rental Costa Rica</strong> has some of the most comfortable cars, you can choose your car according to the location that you&rsquo;re planning to visit, we highly recommend 4x4 if you&rsquo;re planning to explore Costa Rica there are places such as Monteverde, Guanacaste, Manuel Antonio and others that have attractions with difficult access so 4x4 is the best <strong>car rental Costa Rica</strong> choice.</p><p><strong>Yaris Sedan</strong></p><ul><li>It&rsquo;s a beautiful car great option if you are planning to visit easy-access locations or if you are going to stay at San Jose area. The price is $30 per day and has the following facilities 1500 cc, Manual, Gasoline, 4 Doors, 4 Passengers, 2 Small bag, 1 Small bag. The Yaris Sedan is one of the <strong>best car rental</strong> option for city destinations.</li></ul><p><strong>SUVs Terios Bego</strong></p><ul><li>The 4x4 Terios Bego is one of the best <strong>Car Rental choices in Cos Rica</strong> you&rsquo;ll be able to visit great destinations driving by yourself, the Terios Bego is a 4x4 that allows the easy driving on complicated roads and is very comfortable. The price per day start on $39 and include 1500 cc, Manual, Gasoline, 4 Doors, 5 Passengers, 2 Small bag, 3 Small bag.</li></ul><p><strong>Premium Fleet</strong></p><ul><li>Thinking about luxury <strong>transportation in Costa Rica</strong>? Toyota makes possible! We have some of the most comfortable &amp; luxuries brans 4x4 Prado and Lexus cars &amp; Sequoia &ndash; If you are looking for the most luxury <strong>car rental in San Jose</strong>, or around Costa Rica we have it for you.</li></ul><p><strong>Mini Buses</strong></p><ul><li>Looking for a <strong>family car rental Costa Rica</strong>? Our Mini Buses are the best option we have Hiace 12 (12 pax) and Hiace 16 (16 pax) it&rsquo;s the best way to move around Costa Rica with family or a group of friends.</li></ul><p><strong>Pick Ups</strong></p><ul><li>Categorized as one of the best adventure cars, the Double Cap 4x4 will let you move around Costa Rica easily, it&rsquo;s a comfortable and luxury car great for surfing travelers.</li></ul><h2  class="section-title">Variety is a characteristic of our car rental in Costa Rica</h2><p>In Toyota Car Rental we have the options you need, but, as well, we have the alternatives you really want. It&rsquo;s that mixture of comfortability, practicality and high quality what will guide your selection among a wide variety of car options, if you&rsquo;re renting a car for a whole week or if you have the need just for several hours.</p><p>Our high standards and the capability of adaptation to your needs makes us the best car rental in Costa Rica. We have what you&rsquo;re looking for; it doesn&rsquo;t matter if you&rsquo;re just planning to drive downtown, crossing the main roads of San Jos&eacute;, and maybe just having short rides through the city.</p><p>On the contrary, if you&rsquo;re looking for a wild adventure while getting into natural spots and streets difficult to drive, then check out for our bigger cars. We&rsquo;re about to show you some of the options we have to drive all over Costa Rica.</p><p>One of the basic cars, but of the favorites among our customers is the Yaris, which blends outstanding value with the unexpected. It&rsquo;s an elegant car that gives high performance for smooth rides. Corolla is also on the list of cars with great looks and excellent performance for this kind of rides. They&rsquo;re both also eco cars, what means its eco-friendly.</p><p>If your choice could be influenced by ecological matters, then look for the &ldquo;eco man&rdquo; or &ldquo;eco-car&rdquo; logo on each of our car rental for Costa Rica. Let us tell you more about all of the options you&rsquo;ll find in Toyota Car Rental.</p><p>If you&rsquo;re looking for a compact car but you need to take it into the rural backcountry, then check all of our bigger cars, starting by the Daihatsu Terios Bego. It&rsquo;s a smart choice for versatility, comfort, and reliability. The same goes for the double cap Hilux pick up, the ticket for your adventures.</p>',
        categories: {
          sedans: {
            title: 'Sedans',
            category: 'sedans',
            compact: {
              type: 'compact',
              title: 'Compact',
              description: 'High performance and elegant design that delivers a comfortable experience for your next rental.'
            },
            standard: {
              type: 'standard',
              title: 'Standard',
              description: 'Next time you rent a car, enjoy the great looks and great mileage of the Corolla.'
            }
          },
          suvs: {
            title: "SUV's",
            category: 'suvs',
            compact: {
              type: 'compact',
              title: 'Compact 4x4',
              description: 'Daihatsu Bego, your best choice for off-road and optimal mileage!'
            },
            standard: {
              type: 'standard',
              title: 'Standard 4x4',
              description: 'The Rav4 offers sporty styling and surprising comfort. Whether you are zipping around town or planning a long road trip picking the Rav4 will be your best choice to make your next car rental the most memorable!'
            },
            full: {
              type: 'full',
              title: 'Full Size SUV',
              description: 'Roomy, comfortable and elegant with a Prado or Fortuner rental, there’s no need to leave anything, or anyone, behind!'
            }
          },
          premium_fleet: {
            title: 'Premium Fleet',
            category: 'premium-fleet',
            premium: {
              type: 'premium',
              title: 'Premium 4x4 Auto',
              description: 'The TOYOTA Prado or Lexus RX 350 H offer all the comfort and style you are looking for. It is sure to turn heads whether you are driving in the city or headed to a dreamy destination.'
            },
            grand: {
              type: 'grand',
              title: 'Grand Premium 4x4',
              description: 'If it is personal luxury you crave, the Sequoia delivers a fully furnished interior, smooth ride and all the amenities you would expect. Best of all, there’s plenty of space for passengers and luggage. Rent a true luxury car! '
            }
          },
          mini_buses: {
            title: 'Mini Buses',
            category: 'mini-buses',
            micro_bus: {
              type: 'micro-bus',
              title: 'Microbus',
              description: 'Planning a team office activity or have a great big family? Your best journey begins with our 12-15 passenger Hiace rental! '
            }
          },
          pick_ups: {
            title: 'Pick Ups',
            category: 'pick-ups',
            double_cap: {
              type: 'double-cap',
              title: 'Double Cap 4x4',
              description: 'The reliable performance and power of the Hilux ensure you have the best adventures for your next rental'
            }
          },
        // minivans: {
        //   title: 'Minivans',
        //   category: 'minivans',
        //   minivans: {
        //     type: 'minivans',
        //     title: 'Minivans',
        //     description: 'Whether you want to enjoy with your family or friends, Avanza is the right choice for you, 7 passengers three-row seating, comfortable inside and modern design outside.'
        //   }
        // }
        }
      },
      booking: {
        page_title: 'Booking',
        step2: 'Step 2',
        step2_title: 'Select Car',
        step3: 'Step 3',
        step3_title: 'Product Services',
        step4: 'Step 4',
        step4_title: 'Book your car',
        leave_confirmation: 'Do you really want to leave and cancel your booking?',
        car_no_available_title: 'Car selected is not available',
        car_no_available_description: 'Please try changing the booking information or select one the cars listed below.',
        change_booking_btn: 'Change booking',
        product_services: 'Product Services',
        book_your_car: 'Book your car',
        select_car: 'Select Car',
        or_similar: 'or similar',
        day: 'day',
        book_car: 'Book Now',
        pay_now_save: 'Pay Now to save time!',
        pay_now: 'Pay now',
        pay_later: 'Pay later',
        pay: 'Pay',
        and_save: 'and save',
        or: 'or',
        i_am_not: "I'm not",
        interested: 'interested',
        in_save_money: 'in saving time',
        car_available: 'Car available',
        car_no_available: 'Selected car is not available',
        pick_available_car: 'Pick another car from available list',
        change_car: 'Change car',
        payment_info: 'Payment Information',
        cc_number: 'Credit card number',
        cc_exp: 'Expiry Date',
        check_name: 'Cardholder Name',
        pay_btn: 'Make Payment',
        unselected_insurance: "You didn't select any basic insurance are you sure you want to continue?",
        unselected_insurance_btn: 'Yes, continue.',
        basic_insurance_info: '<p>*CDW Collision Damage Waiver: Helps you to reduce financial liability for physicaldamage to the rented vehicle as long as you comply with to the terms of the rentalagreement.</p><p>*LDW Loss Damage Waiver: Helps you to reduce financial liability for collision damage tothe vehicle or the property of third parties as long as you comply with to the terms of therental agreement.</p><p>*Theft Coverage: Helps you reduce financial liability in case the rented car is stolen(Deductibles included), as long as you comply with to the terms of the rental agreement.</p><p>Ask our staff for additional protections.</p>',
        payment_notification: {
          success: {
            title: 'Confirmation',
            text: 'Payment OK.',
            type: 'success'
          },
          warning: {
            title: 'Warning',
            text: 'Please check the form values.',
            type: 'notice'
          },
          error: {
            title: 'Error',
            text: 'An unexpected error occurred, please try again.',
            type: 'error'
          }
        },
        product_options: {
          H004: 'LPF',
          I003: 'Additional driver',
          K001: 'Booster Seat',
          K002: 'GPS',
          K004: 'Quick Pass',
          K007: 'Mobile Hot Spot',
          K008: 'Child Car Seat',
          E001: 'Basic Insurance',
          I004: 'Fast Pass',
          I006: 'Fast Return'
        },
        product_options_info: {
          H004: {
            price:'',
            description: ''
          },
          I003: {
            price: '- <b>$10</b> per day',
            description: '<p>We offer you the additional driver service to make sure that in the event of accident if the car is driven by the extra driver, this is going to be covered by the insurance purchased at the time of the rental.</p>'
          },
          K001:{
            price: '- <b>$10</b> per day',
            description: '<p>In Costa Rica, law mandates that children under the age of 12 are required to ride on a safety seat. We offer both baby car seats and child boosters.</p>'
          },
          K002:{
            price: '- <b>$12</b> per day',
            description: '<p>This system will simplify your travel with pre-established national routes.</p>'
          }, 
          K004:{
            price: '- <b>$10</b> per day',
            description: '<p>It is the electronic payment device for the tolls on Costa Rica’s Route 27 (highway to Caldera). Toll Pass enables you to skip long toll lines and zip right through the exclusive lane.</p>'
          },
          K007: {
            price: '- <b>$20</b> per day',
            description: '<p>WIFI anywhere you go! Enjoy your favorite apps and GPS functionality</p>'
          },
          K008: {
            price: '- <b>$10</b> per day',
            description: '<p>In Costa Rica, law mandates that children under the age of 12 are required to ride on a safety seat. We offer both baby car seats and child boosters.</p>'
          },
          E001: {
            price:'',
            description: '<p>*CDW Collision Damage Waiver: Helps you to reduce financial liability for physicaldamage to the rented vehicle as long as you comply with to the terms of the rentalagreement.</p><p>*LDW Loss Damage Waiver: Helps you to reduce financial liability for collision damage tothe vehicle or the property of third parties as long as you comply with to the terms of therental agreement.</p><p>*Theft Coverage: Helps you reduce financial liability in case the rented car is stolen(Deductibles included), as long as you comply with to the terms of the rental agreement.</p><p>Ask our staff for additional protections.</p>',
          },
          I004: {
            price: '- <b>$10</b>',
            description: '<p>Make your rental experience easier with this service, we deliver the vehicle right at the exit of Juan Santamaría International Airport in Alajuela or Daniel Oduber Airport in Liberia. This is an exclusive service provided in Alajuela and Liberia stations.</p><p>Request your service when you book online.</p>'
          },
          I006: {
            price: '- <b>$10</b>',
            description: '<p>This service will let you return the vehicle right at the Juan Santamaría International Airport, no need to worry about driving the car to our location. Request your service when you book online.</p>'
          }     
        },        
        error: {
          'ERR/0': 'There is a stop sale for that Group/Period/Station',
          'ERR/10': 'No Cars available',
          'ERR/100': 'Required Parameter is Missing',
          'ERR/101': 'Vehicle Group Not Found',
          'ERR/102': 'Pickup/Return Station Not Found',
          'ERR/103': 'Additional Option not Found',
          'ERR/106': 'Invalid date/time combination',
          'ERR/108': 'Reference Number already used',
          'ERR/110': 'Price Quote Not Found',
          'ERR/111': 'Price Quote Has Expired',
          'ERR/112': 'Price Quote not valid for this reservation request.',
          'ERR/114': 'Group not Supported at specified location',
          'ERR/999': 'System is down for maintenance',
          'ERR/1000': 'Communication Error. Please try again later.'
        },
        thank_you: {
          page_title: 'Thank you!',
          quote_no: 'Quote No.',
          customer_information: 'Customer Information',
          book_details: 'Book Details',
          car_detail: 'Car Detail',
          model: 'Model:',
          baseRate: 'Base Rate:',
          totalRate: 'Total Rate:',
          additionalOptionsList: 'Additional Options List:'
        },
        form: {
          first_name: 'First Name',
          last_name: 'Last Name',
          phone: 'Phone',
          email: 'E-mail',
          country: 'Country',
          city: 'City',
          zip: 'Zipcode',
          address: 'Address',
          airline: 'Airline',
          flight: 'Flight',
          quote_no: 'Quote No.',
          customer_information: 'Customer Information',
          book_details: 'Book Details',
          car_detail: 'Car Detail',
          model: 'Model:',
          baseRate: 'Base Rate:',
          totalRate: 'Total Rate:',
          additionalOptionsList: 'Additional Options List:',
          start_date: 'Start Date',
          end_date: 'End Date',
          pick_up_location: 'Pick up Location',
          drop_off_location: 'Drop off Location',
          total_days: 'Total days',
          additional_accessories: 'Additional Accessories',
          payment_info: 'Payment Information',
          cc_number: 'Credit card number: ',
          check_name: 'Cardholder Name: ',
          irn: 'IRN Number: ',
          remark: 'Remarks: ',
          no_insurance: '<p>*Rate does not include mandatory insurance and extras. Time and Mileage included only.</p>',
          titleQuote: 'This quote is subject to availability and actual pricing at the time of booking.',
          conditions: '<p><b>Please take in consideration that any change of the pickup and drop off location incur extra charges. Any additional extras and insurance are payable at the rental desk.</b></p><p>&nbsp;</p><p>Important Information</p><p>Rental period is 24 hours from delivery time in rental agreement</p><p>&nbsp;</p><p>Requirements:</p><p>Driver(s) must be 21 years old or older.</p><p>License Driver, ID/Passport valid, on hand.</p><p>Make US$1.000,00 guarantee deposit charged to a credit card (will be released 2 weeks from closing rental agreement); or US$2.000,00 when using debit card (will be released from 4 to 6 weeks from closing rental agreement).</p><p>Passengers arriving to Juan Santamar&iacute;a International (SJO) and Daniel Oduber International Airport (LIR), our agents will be waiting for you at the terminal exit to provide free shuttle service to our office.</p>',
          footer: '<p><a href="mailto:sales@toyotarent.com">sales@toyotarent.com</a> | 506-2105-3400 <br/>San José, Costa Rica</p>',
          email_settings: {
            title: 'New Reservation',
            subject: 'New Reservation'
          },
          notification: {
            success: {
              title: 'Confirmation',
              text: 'The form was successfully submitted.',
              type: 'success'
            },
            warning: {
              title: 'Warning',
              text: 'Please check the form values.',
              type: 'notice'
            },
            error: {
              title: 'Error',
              text: 'An unexpected error occurred, please try again.',
              type: 'error'
            }
          }
        }
      },
      products_services: {
        permalink: 'products-services',
        seo: {
          title: 'Costa Rica Rental Car Services',
          description: 'Why choose Toyota Rent a Car? Because you can get the best Costa Rica Rental Car Services: cellular telephones, free luggage racks, surf, racks, coolers, and many others services when renting a car with TOYOTA Car Rental',
          keywords: 'car rental services, costa rica, rent a car, toyota cars, toyota rent, toyota car, rental car, car rentals'
        },
        products: {
          title: 'Products',
          sections: {
            sections_1: {
              classname: 'mobile-hotspot',
              title: 'Mobile Hotspot',
              description: '<p>WIFI anywhere you go! Enjoy your favorite apps and GPS functionality.</p>'
            },
            sections_2: {
              classname: 'gps-global-positioning-system',
              title: 'GPS',
              description: '<p>This system will simplify your travel with pre-established national routes.</p>'
            },
            sections_3: {
              classname: 'toll-pass',
              title: 'Toll Pass',
              description: '<p>It is the electronic payment device for the tolls on Costa Rica’s Route 27 (highway to Caldera). Toll Pass enables you to skip long toll lines and zip right through the exclusive lane.</p>'
            },
            sections_4: {
              classname: 'roof-racks',
              title: 'Roof Racks',
              description: '<p>Maximize the use of space within your vehicle by requesting one with a roof rack. Roof racks are very useful to transfer extra luggage, surfboards or to free up space inside the vehicle.</p>'
            },
            sections_5: {
              classname: 'kids-and-baby-seats',
              title: 'Baby Car Seats & Child Booster Seats',
              description: '<p>In Costa Rica, law mandates that children under the age of 12 are required to ride on a safety seat. We offer both baby car seats and child boosters.</p>'
            },
            section_6: {
              classname: 'easy-connect',
              title: 'Easy Connect',
              description: ''
            }            
          }
        },
        services: {
          title: 'Services',
          sections: {
            section_1: {
              classname: 'chauffeur-services',
              title: 'Chauffeur Services',
              description: '<p>Whether you are here on vacation or for business, our experienced and bilingual drivers are available to take you anywhere in the country.</p> <p>We have bilingual (English/Spanish) drivers available for you.</p><p>Terms of service:</p><ul><li>The price includes boarding for the driver within the Greater Metropolitan Area (GAM).</li><li>Driver’s lodging and boarding are not included for travelling outside of the Greater Metropolitan Area (GAM).</li><li>An extra security deposit and insurance is required to be purchased.</li></ul>'
            },
            section_2: {
              classname: 'hourly-car-rental',
              title: 'Chauffer Service and Car Rental Hourly',
              description: '<p>Take care of your business with no worries, we offer you chauffer service to take you wherever you need to go on an hour-based fee.</p><p>Terms of service:</p><ul><li>This service is available for Standard and Premium Luxury vehicle rentals.</li><li>Service does not require a deposit or insurance to be purchased.</li><li>Service is limited to a maximum distance of 30 miles (50 kilometers).</li><li>The fee covers vehicle rental, driver and fuel costs.</li></ul>'
            },
            section_3: {
              classname: 'fast-lane',
              title: 'Fast Pass',
              description: '<p>Make your rental experience easier with this service, we deliver the vehicle right at the exit of Juan Santamaría International Airport in Alajuela or Daniel Oduber Airport in Liberia. This is an exclusive service provided in Alajuela and Liberia stations.</p><p>Request your service when you book online.</p>'
            },
            section_4: {
              classname: 'fast-lane',
              title: 'Fast Return',
              description: '<p>This service will let you return the vehicle right at the Juan Santamaría International Airport, no need to worry about driving the car to our location. Request your service when you book online.</p>'
            },
            section_5: {
              classname: 'wedding-services',
              title: 'Wedding Services',
              description: '<p>A special day requires a special transfer in one of our Premium vehicles. Fresh flowers and a complimentary bottle of sparkling wine are included in our wedding transfer service for the bride and groom and their families on the special day.</p>'
            },
            section_6: {
              classname: 'prepaid-fuel',
              title: 'Prepaid Fuel',
              description: '<p>To simplify the return of the vehicle, you may choose to pay for a full tank of fuel in advance at the time of rental.</p><p>This way you won’t have to worry about filling up when you are ready to return the vehicle.</p>'
            }
          }
        }
      },
      locations: {
        permalink: 'locations',
        linkSection: '/locations',        
        seo: {
          title: 'Car Rentals Costa Rica | Best Location SJO & Liberia Check Here',
          description: 'If you need to rent a car in Costa Rica here you can find our locations. Nosara, Liberia, San Jose, Puntarenas & SJO Airport Make your reservation today.',
          keywords: 'car rental, costa rica, rent a car in SJO, toyota cars, toyota rent, toyota car, rental car in LIR'
        },
        title: 'Locations',
        titleGoogleMap: 'Find us',
        showLocationBtn: 'Show all locations',
        address: 'Address',
        phone: 'Phone',
        fax: 'Fax',
        horario: 'Schedule',
        code: {
          1: 'San Jose Downtown',
          3: 'Liberia Airport',
          4: 'Nosara',
          5: 'San Jose Airport'
        },
        sections: {
          location_1: {
            title: 'Liberia, Guanacaste',
            titleLocation: 'Liberia Car Rental, Costa Rica',
            address: '800 meters east from Daniel Oduber International Airport, Liberia.',
            schedule: 'Monday to Sunday / 6 a.m. - 10 p.m',
            phone: '(506) 2105-3481',
            seo: {
              title: 'Liberia Car Rental Costa Rica | Book Online Today - Toyotarent.com',
              description: "Liberia Car Rental Costa Rica, book online and get the best price guarantee by Toyota Costa Rica. Sedan, 4x4, SUV's BOOK NOW"
            },
            article: '<p>If you are planning to visit Liberia, we are your best car rental choice in town close to Liberia Airport, Toyota car rental has a large brands variety and the best 4x4 of the area.</p> <p>There are many places close to Liberia that you can visit driving by yourself, discover the beautiful nature of the area and explore the Guanacaste beaches with our Liberia car rental.</p><p>Liberia, Guanacaste is a popular travel destination where you can enjoy the great white sand beaches and beautiful parks, there are great surfing spots and nightlife.</p>',
            slug: 'liberia',
            waze_url: {
              lat: '10.593333',
              lon: '-85.544444'
            },
            province: 'guanacaste',
            coordenadas:{
              lat: 10.624393, 
              lng: -85.518574
            }            
          },
          location_2: {
            title: 'International Airport - Alajuela',
            titleLocation: 'Car Rental San Jose Airpot, Costa Rica',
            address: 'In front of Hampton Inn Hotel.',
            phone: '(506) 2105-3466',
            fax: '(506) 2443-5044',
            schedule: 'Monday to Sunday / 6 a.m. a 10 p.m.',
            seo: {
              title: 'Car Rental Costa Rica International Airport | Book Now - Toyotarent.com ',
              description: 'Toyota offers the best car rental at Alajuela international Aiport, best rates and best brands. We are your best choice Reserve Here.'
            },
            article: '<p>Very close to Alajuela Airport is located Toyota car rental one of the best Costa Rica airport car rentals with the best cars at the best price. </p><p>If you are planning to explore Costa Rica renting a car is one of the best options, feel free to contact us, you can also make your online reservations.</p>',
            slug: 'airport',
            waze_url: {
              lat: '9.993611',
              lon: '-84.208889'
            },
            province: 'alajuela',
            coordenadas:{
              lat: 10.002366, 
              lng: -84.195287
            }              
          },
          location_3: {
            title: 'Alajuela-Main Office',
            titleLocation: 'Car Rental San Jose, Costa Rica',
            address: 'Oficentro Santamaría, oficina 5B, Río Segundo, Alajuela.',
            phone: '(506) 2105-3400',
            // fax: '(506) 2222-7875',
            schedule: 'De lunes a viernes / 8 a.m. a 5:30 p.m.',
            seo: {
              title: 'San Jose Car Rental, Costa Rica | Book Now - Toyotarent.com',
              description: 'Best San Jose car rental, Costa Rica. Toyota has the best brands and rates make your reservation today and get the best guarantee.'
            },
            article: "<p>If you're looking for a car rental in San Jose Costa Rica, Toyota is your best option, the best sedan, 4x4 cars explore Costa Rica driving by yourself Toyota make it easy. </p><p>Discover our great selection of cars for rent, we make sure you always get the best price on rent a car in San Jose Costa Rica. </p>",
            slug: 'san-jose',
            waze_url: {
              lat: '9.9448051662096',
              lon: '-84.11815340114'
            },
            province: 'san-jose',
            coordenadas:{
              lat: 9.935761, 
              lng: -84.092485
            }             
          },
          location_5: {
            title: 'Paseo Colón',
            titleLocation: 'Car Rental Paseo Colón, Costa Rica',
            address: '300 metros este de Purdy Motor Paseo Colón.',
            schedule: 'Monday to Friday / 7 a.m. to 7 p.m. - Saturday to Sunday / 7 a.m. at 5 p.m',
            phone: '(506) 2105-3460',
            seo: {
              title: 'Paseo Colón Car Rental, Costa Rica | Book Now - Toyotarent.com',
              description: "Let's discover Paseo Colón Costa Rica with Toyota, we are your best Paseo Colón car rental with the best brands and rates. Make your reservation today!"
            },
            article: '<p>We offer excellent options to get around Nosara, Guanacaste Toyota Car Rental is your best option in price and quality all our cars are of the best quality.</p><p>Discover Nosara by driving by yourself, you can easily visit beaches, forests, and towns in this beautiful area, Toyota is the number one Nosara car rental.</p>',
            slug: 'paseo-colon',
            waze_url: {
              lat: '9.93572',
              lon: '-84.09291'
            },
            province: 'san-jose',
            coordenadas:{
              lat: 9.9355393,
              lng: -84.0951355
            }            
          },
          location_6: {
            title: 'Dominical',
            titleLocation: 'Dominical',
            address: 'Centro Comercial del Río, Osa, Puntarenas.',
            schedule: 'De lunes a domingo / 9 a.m. a 5:00 p.m.',
            phone: '(506) 2105-3460',
            seo: {
              title: 'Dominical Car Rental, Costa Rica | Book Now - Toyotarent.com',
              description: "Let's discover Dominical Costa Rica with Toyota, we are your best Dominical car rental with the best brands and rates. Make your reservation today!"
            },
            article: '<p>We offer excellent options to get around Nosara, Guanacaste Toyota Car Rental is your best option in price and quality all our cars are of the best quality.</p><p>Discover Dominical by driving by yourself, you can easily visit beaches, forests, and towns in this beautiful area, Toyota is the number one Dominical car rental.</p>',
            slug: 'dominical',
            // waze_url: {
            //   lat: '9.93572',
            //   lon: '-84.09291'
            // },
            province: 'puntarenas'
            // coordenadas:{
            //   lat: 9.9355393,
            //   lng: -84.0951355
            // }            
          }                      
        }
      },
      customer_care: {
        permalink: 'customer-care',
        seo: {
          title: 'Costa Rica car rental customer care l rental car with the best customer care',
          description: 'For all our customers we oﬀer an US Toll Free Number. Fell free to contact Toyota Rent a Car a any time! We’ll be waiting for you!',
          keywords: 'car rental, transit law, costa rica, rent a car, toyota cars, toyota rent-a-car, toyota car, rental car'
        },
        title: 'Customer Care',
        sub_title: 'Give us a call or send us your inquiry (contact form below):',
        call_us: '',
        email: 'Email: <a href="mailto:sales@toyotarent.com" title="Contact Us">sales@toyotarent.com</a>',
        form: {}
      },
      special_offers: {
        permalink: 'special-offers',
        seo: {
          title: 'Costa Rica Car Rental Promotions | Hotels & Tours Discounts',
          description: 'Take advantage of the big promotions in hotels and tour, by booking on our website you will get great discounts. Do not wait any longer and try here.',
          keywords: 'car rental, costa rica, rent a car, promotions, toyota cars, toyota rent, toyota car, rental car'
        },
        title: 'Special Offers',
        sections: [
          {
            title: 'Hacienda Pinilla',
            discount: '10',
            image: 'special_offers/haciendapinilla.jpg',
            url: 'www.haciendapinilla.com',
            content: '<h3>10% discount at Hacienda Pinilla in 2 or 3 bedroom villas, fully equipped.</h3><br><p>On the magnificent Pacific coast in the Guanacaste province of Costa Rica, stretching over 4500 acres of natural landscape, you will discover a residential complex developed in the colonial hacienda style. Three beaches adorn the 3 miles of coastline bordering Hacienda Pinilla, offering a range of activities for all ages, including fishing, snorkeling, surfing or just enjoying a day by the sea. Nature embraces every corner of Hacienda Pinilla, from the professionally designed golf course to the beaches, lakes, forests, hills and valleys.</p><p>The best beach vacations happen when comfort, experience, exclusivity and good taste converge. At Hacienda Pinilla, we offer all that and more. We provide different options for comfortable lodging surrounded by the greenery of the Guanacaste pampas.</p><p>If you prefer to get a taste of Costa Rican daily life at Hacienda Pinilla, choose from our exclusive collection of vacation villas, all feature unique yet traditional designs with top quality furnishings. From two to four bedroom villas, Hacienda Pinilla offers the best in Costa Rica rental properties you need to have an extraordinary vacation.</p><p>You will receive exclusive access to the endless array of amenities within the resort including our championship golf course, beach club, restaurants, stables, spas, tennis courts, mountain bike trails, horseback riding tours, famous surf spots and the stunning beaches. </p><a rel="nofollow" target="_black" href="tel:2680-7000 ">For reservations contact: 2680-7000</a><br/><a rel="nofollow" target="_black" href="mailto:reservations@haciendapinilla.com">Email: reservations@haciendapinilla.com</a>',
            greatDeal: true
          },
          {
            title: 'Overseas Pacific Realty',
            image: 'special_offers/overseas-pacific.jpg',
            url: 'http://www.overseaspacificrealty.com',
            content: '<p>Overseas Pacific Realty is a real estate agency located in beautiful Playa Flamingo, Costa Rica, offering some of the most unique real estate opportunities in Guanacaste</p><p>Proud members of the Costa Rica MLS, we have over a 1,000 properties for sale on our web site, many of them on Costa Rica’s Gold Coast with ocean view luxury homes, beach front condos, development land, building lots, hotels for sale, gated communities and much more.</p><p>With many decades of combined experience, and intimate knowledge of the Guanacaste real estate market, we have positioned ourselves to be the go-to realtor in this booming region of Costa Rica.</p><a href="mailto:info@overseaspacificrealty.com">Email:  info@overseaspacificrealty.com</a><br><a href="tel:+506-2654-6070">Phone: Costa Rica +506-2654-6070 - N. America 1-888-597-3126</a><br><address>Address: 25 m sur antes del puente Flamingo Marina<br>Guanacaste, Playa Flamingo</address>',
            greatDeal: true
          },          
          {
            title: '360 Splendor del Pacifico',
            image: 'special_offers/360-pool.jpg',
            url: 'http://www.360flamingo.com',
            content: '<p>The 360 Splendor del Pacifico Luxury Residences and a Toyota Rental Car are the perfect combination for exploring the best of Costa Rica’s north Pacific coastline.</p><p>To the north, there are zip lines and volcanoes. To the south, there are surf shops and microbrews. To the east, there are the quaint Costa Rican ranch towns. And to the west is the Pacific horizon, where game fish are plentiful, the ocean is calm and the beaches alternate between pure white and silky black sands.</p><p>Sitting atop the Flamingo Beach north ridge, the Residences boast expansive coastal views from every side of the 36 luxury condominiums and world-class penthouses. Each condo can transform from a 1 or 3 bedroom into a two-bedroom holiday stay, with soundproof pass-through doors and balconies that overlook the Flamingo Marina and the prestigious Flamingo Beach community.</p><p>At the 360 Splendor del Pacifico Luxury Residences, your rented vehicle is safe, secure and ready to go whenever you are.</p><p>Explore the best Flamingo Beach has to offer.</p><p>Check-out out our seasonal offers!</p><a rel="nofollow" target="_black" href="http://www.360flamingo.com">Website. www.360flamingo.com</a><br><a rel="nofollow" target="_black" href="mailto:info@360flamingo.com">E-mail. info@360flamingo.com</a><address>Address. Playa Flamingo. Costa Rica.</address>',
            greatDeal: true
          }
        ]
      },
      about: {
        permalink: 'about',
        seo: {
          title: 'Costa Rica car rentals, Toyota cars in Costa Rica, car hire Costa Rica',
          description: 'Car Rental in Liberia (LIR) and San Jose Airport (SJO) Costa Rica. Toyota Rental Car oﬀers many oﬃce locations around Costa Rica country to give you more chance to pick‐up or return your rental car. Find us at: San José Downtown, Liberia Guanacaste (LIR), San Jose Int. Airport (SJO), Manuel Antonio Quepos, Nosara & Tambor Beach',
          keywords: 'car rental, costa rica, rent a car, toyota cars, toyota rent, oﬃces, toyota car, rental car'
        },
        title: 'About Us',
        content: '<h2>About Toyota Rent a car</h2><p>TOYOTA Rent a Car is a company with more than 45 years in the rental car business, being part of the wonderful experiences offered by Costa Rica. Our clients trust, loyalty and commitment motivate us to provide high quality services focused on environmental awareness. We have different locations throughout the country: Liberia, San José, Alajuela and Nosara.</p><p>For the past few years TOYOTA Rent a Car has worked on improving its services through the service differentiation to meet our customer needs. We have implemented services such as: car delivery right in the airports, chauffeur service, wedding service, prepaid fuel and premium car fleet.</p><p>Additionally, we seek to build a customer experience through the services we provide, most of our customers come to our country to have adventure experiences on their vacations.</p><p>To reduce our carbon footprint, we have developed environmental friendly process. The impact produced by our services is measured and mitigated running maintenance plans in our fleet and ecological actions like planting native trees in Guanacaste. This way, we make sure our customers are helping with the conservation of the environment. We have recycling programs in each of our stations and main office with the goal of taking care of the environment and to encourage our customers and employees to do the same.</p><h2>Core Values</h2><h3>We do what we say</h3><p>We express ourselves clearly. We keep our promises and if facts change we can negotiate</p><h3>We are owners</h3><p>We act as general managers and we do not seek to blame others. We care enough to tell ourselves when a goal or objective is not reached. We learn from our mistakes</p><h3>We tell the truth</h3><p>We always act with integrity in front of our clients and colleagues</p><h3>We care about others</h3><p>The golden rule: we treat people the way they like to be treated</p><h3>We exceed expectations</h3><p>Our goal is to always win. As a brand we promote innovation and change. We hire and develop people focused on reaching high and challenging goals.</p><h2>Environmental Awareness</h2><h3>What is Green Heart?</h3><p>Having a Green Heart means being aware of the impact we have on our stakeholders and taking actions to reduce and/or compensate for such impact.</p><h3>Why are we Green Heart?</h3><p><ul><li>Because we measure the CO2 emissions of our fleet and create awareness of our environmental impact.</li><li>Fourth tier Sustainable Tourism Certification.</li><li>Toyota Rent-a-car applies and obeys the rules to conform to the national regulations which govern our activities, as well as the Law for the Conservation of Wildlife. Under this Law, the keeping of wild animals in captivity and of some plant species is considered illegal.</li><li>We compensate our carbon footprint through the purchase of Carbon Offset Credits from the National Fund for Forest Financing, known as FONAFIFO. FONAFIFO executes the national Environmental Services Program whose mission is to preserve and recuperate Costa Rican forests through a strategic financial stimulus plan.</li><li>All of our gasoline-fuel TOYOTA vehicles have a VVT-I engine. This unique TOYOTA technology ensures high-level performance and more efficient fuel consumption while reducing CO2 emissions.</li></ul></p>'
      },
      practical_tips: {
        permalink: 'practical-tips',
        seo: {
          title: 'Costa Rica Car Rental Advise | All you need to know about',
          description: 'Find important information to help you find out what you need to rent a car in Costa Rica. Talk to one of our experts about how to rent a car.',
          keywords: 'car rental, costa rica, rent a car practical advices, toyota cars, toyota rent, toyota car, rental car'
        },
        title: 'Practical Tips',
        content: '<h3>Before you begin your trip with TOYOTA Rent-a-car</h3><p><ul><li>Get information about the weather conditions before you begin your trip.</li><li>Carry out a general inspection of the vehicle. Make sure the lights are working properly, check the tire pressure, oil level, etc.</li><li>Make sure you have all your personal documents (driver’s license, ID, Passport) as well as the rental agreement and vehicle permits.</li><li>Remember that all passengers must wear the seatbelt at all times.</li><li>According to Costa Rican legislation, all children under the age of 12 must travel in a safety baby or booster seat.</li><li>Do not exchange currency on the street or with strangers. Make sure to carry out all your monetary exchange transactions at a bank or authorized agency.</li><li>For local information visit Costa Rica’s official tourism website <a href="http://www.visitcostarica.com" target="_blank"></a>.</li><li>Make sure you have a minimum of eight hour rest the night before you drive.</li></ul></p><h3>During your trip</h3><p><ul><li>Call our toll-free number 800-123Rent at any time for any questions during your time in the country.</li><li>Keep your windows up and doors locked at all times.</li><li>Avoid driving late at night or in heavy rain conditions.</li><li>Travel exclusively on official highways and roads.</li><li>Take short breaks during long trips.</li><li>Avoid stopping in dark or lonesome places. Always try to stop at public or busy stations.</li><li>Do not leave valuable belongings inside the vehicle, even when it is locked.</li><li>Abide by the traffic signs and regulations.</li><li>If you get a flat tire, park the vehicle correctly on the side of the road. Turn on the emergency lights and put on the safety vest. Place the two safety triangles, place the first triangle 50 meters away from the back of the vehicle and the second one 20 meters away from the front of vehicle.</li><li>If you are hit by another vehicle or notice that you are being followed suspiciously, find the closest public place and call 911 immediately.</li><li>Avoid parking the vehicle on the street instead park it on parking lots.</li></ul></p><h3>Remember</h3><ul><li>In order to get full coverage from your insurance policy, you must meet all terms in the rental agreement.</li><li>The person who signed the rental agreement is responsible for all damages to the vehicle.</li><li>All traffic violations are complete responsibility of the person who signed the rental agreement. These must be paid at the designated local banks. Do not pay any kind of traffic infractions to local transit police officers or strangers.</li><li>You must return the vehicle with a full tank of gas unless you decided to prepay at the time of the rental. In which case you should try to return it with the least possible amount of fuel.</li></ul>'
      },
      transit_law: {
        permalink: 'transit-law',
        seo: {
          title: 'Costa Rica Car Rental Laws | Legal information about car rental',
          description: 'Before renting a car in Costa Rica it is good to have information regarding the traffic laws, in this page you will find everything related to driving laws in Costa Rica.',
          keywords: 'car rental, transit law, costa rica, rent a car, toyota cars, toyota rent-a-car, toyota car, rental car'
        },
        title: 'Transit Law',
        content: '<h2>Transit Law in Costa Rica</h2><p>Effective since 2010, Costa Rica’s Traffic Law establishes severe penalties to traffic violations. Please take a moment to read the following information which summarizes the general traffic rules and sanctions which you will be liable for at the time of renting a vehicle. If you have any questions or would like to read the complete document, please reach out to our Customer Service department.</p><h4>Prison and Vehicle Impoundment</h4><p><ul><li>Reckless Driving (driving under the influence of alcohol with more than 0.75 grams per liter of blood or at a speed faster than 90 miles/hour or 150 Km/h), murder or manslaughter.</li></ul></p><p><strong>Note: this traffic violation will incur in administrative and legal fees for vehicle impoundment on top of the rental contract.</strong></p><h4>₡227,000 colones ($400 approx.) + 30% Tax in favor of the Child Protection Agency</h4><p><ul><li>Reckless Driving Category A: Driving under the influence of 0.5% or more grams of alcohol per liter of blood, drugs, at the speed faster than 75 miles/hour (120 Km/h) and passing in double yellow line.</li><li>Driving without a license or with an invalid license.</li><li>Not using child safety seats for children under the age of 12.</li></ul></p><h4>₡170,250 colones ($300 approx) + 30% Tax in favor of the Child Protection Agency </h4><p><ul><li>Reckless Driving Category B: Driving 10 to 12 miles above the limit or driving faster than 15 miles/hour through school crossing or in front of hospitals and clinics.</li><li>Violating traffic signs, traffic lights or illegal “U” turns.</li><li>Using a mobile phone while driving without a hands-free device.</li><li>Not wearing a seatbelt.</li></ul></p><h4>₡113,500 colones ($200 approx) + 30% Tax in favor of the Child Protection Agency</h4><p><ul><li>Overtake vehicles on the right on roads with two lanes.</li><li>Stopping in the middle of an crossroad.</li><li>Driving on a beach.</li><li>Littering public spaces.</li></ul></p><h4>₡90,800 colones ($160 approx) + 30% Tax in favor of the Child Protection Agency</h4><p><ul><li>Violation of pedestrian crossings.</li><li>Carrying excess passengers</li><li>Parking in prohibited or handicapped spaces.</li></ul></p><h4>₡68,100 colones ($120 approx.) + 30% Tax in favor of the Child Protection Agency</h4><p><ul><li>Not keeping the distance between your vehicle and the vehicle at front.</li><li>Driving in reverse.</li><li>Driving with an expired license.</li><li>Driving for more than three months with a foreign driver’s license.</li></ul></p><h4>₡45,400 colones ($80 aprox.) + 30% Tax in favor of the Child Protection Agency</h4><p><ul><li>Driving without driver’s license.</li> <li>Exceed the speed limits</li><li>Urban area: 40 km/h.</li><li> Highway: 80 km/h.</li><li>Residential area: 60 km/h.</li><li>School area: 25 km/h.</li> </ul></p><h3 class="important">IMPORTANT NOTICE</h3><p><ul><li>Traffic violations that occur during your rental be charged to the renter at the type of the vehicle return.</li><li>Our system scans the national traffic violations database daily to identify traffic violations from our vehicles.</li><li>According to the type of traffic violation, the renter may incur in additional administrative and legal fees.</li></ul></p><p><em>Thanks for your cooperation. Please drive safely during your travel in Costa Rica.</em></p>'
      },
      faq: {
        permalink: 'faq',
        seo: {
          title: 'Costa Rica Car rentals FAQ | Costa Rica rent‐a‐car, Toyota rental car in Costa Rica',
          description: 'What does the car rental price include? How old should I be to Rent‐a‐Car in Costa Rica? Which are the car rental requirements? Does Toyota Rent a Car provide shuttle bus from/to local hotels? What does the rent price include? Get response for any of these questions when renting a car in Toyota Rent a Car, Costa Rica.',
          keywords: 'car rental, faq, costa rica, rent a car, toyota cars, toyota rent, toyota car, rental car'
        },
        title: 'FAQ',
        sections: {
          question_1: {
            title: 'Can I rent a vehicle or make the security deposit with someone else’s credit card?',
            content: '<p>No, you cannot. Rental agreements are personal, and it is accepted only the debit or credit card of the person signing the agreement, who is also the only person allowed to drive the vehicle.</p>'
          },
          question_2: {
            title: 'Can I cross rivers or lakes with the 4x4 vehicle I just rented?',
            content: '<p>No, you cannot drive a vehicle rented from TOYOTA Rent a Car through rivers, lakes or similar. Remember you are responsible for any damage to the vehicle according to the terms and conditions of the rental agreement.</p>'
          },
          question_3: {
            title: 'Are additional drivers permitted?',
            content: '<p>Additional drivers are permitted if they are registered at the time of the signing of the rental agreement. This way, the additional driver will also be covered by the insurance policy.</p>'
          },
          question_3: {
            title: 'Can I return the vehicle at a different location from where I picked it up?',
            content: '<p>For your convenience you can return the vehicle at a different station, this will have an extra charge depending on where you wish to return the vehicle.</p>'
          },
          question_4: {
            title: 'What happens if I damage my rental vehicle?',
            content: '<p>During the time of the rental, the rental vehicle is your responsibility. According to the terms and conditions on the rental agreement, any damage to the vehicle is your full responsibility.</p>'
          },
          question_5: {
            title: 'Are traffic violations my responsibility?',
            content: '<p>The person who signs the rental agreement is responsible of any traffic violations made during the time of the rental. Keep in mind that they must to be paid at the authorized local banks. Do not pay any kind of traffic infractions to local transit police officers or strangers.</p>'
          },
          question_6: {
            title: 'What should I do in case of mechanical failure?',
            content: '<p>If possible, head over to the nearest gas station or any public space where you can park the vehicle. Call our toll-free assistance number 800-123Rent (800-123-7368).</p><p>If you are not able to get any further, turn on the emergency lights or flashers, put on the safety vest and place the safety triangles, put the first 50 meters away from the back of the vehicle and the second one 20 meters away from the front of the vehicle.</p>'
          },
          question_7: {
            title: 'How can I make the security deposit?',
            content: '<p>The security deposit is made charging a debit or credit card in the name of the person signing the rental agreement.</p>'
          },
          question_8: {
            title: 'Can I travel with pets?',
            content: '<p>We recommend that you do not travel with to avoid damages to the vehicle.</p>'
          },
          question_9: {
            title: 'How does the cancellation or change policy work?',
            content: '<p>Any changes or cancellations must be done before the end of the first 24 hours to avoid extra fees.</p>'
          },
          question_10: {
            title: 'What are the requirements to rent a vehicle?',
            content: '<p><ul><li>Minimum age 21 years.</li><li>Have a valid local or foreign driver’s license.</li><li>Debit or credit card to get the authorization for the security deposit.</li><li>Valid ID or passport.</li></ul></p>'
          },
          question_11: {
            title: 'Can I use my credit card’s insurance policy?',
            content: '<p>Yes, you can, as far as the credit card company provides coverage in Costa Rica.</p>'
          },
          question_12: {
            title: 'What should I do in case of an accident?',
            content: '<p><ul><li>Do not move the vehicle.</li><li>Remain calm.</li><li>Call 911 to report the accident and request assistance.</li><li>Call the INSTITUTO NACIONAL DE SEGUROS (INS) at 800-800-8000 to report the accident.</li><li>Turn on the emergency lights or flashers, put on the safety vest and place the safety triangles, put the first 50 meters away from the back of the vehicle and the second one 20 meters away from the front of the vehicle.</li><li>Call us right way at 800-123Rent (800-123-7368)</li></ul></p>'
          },
          question_13: {
            title: 'Is there a vehicle drop-off and pick-up service to/from hotels?',
            content: '<p>No, drop-off and pick-up service is only available for the Juan Santamaria International Airport (Alajuela station) and Daniel Oduber International Airport (Liberia station).</p>'
          },
          question_14: {
            title: 'Can I rent a vehicle to travel outside of Costa Rica?',
            content: '<p>Rental vehicles cannot travel outside of the country. The rental agreement limits travel exclusively within Costa Rican territory.</p>'
          },
          question_15: {
            title: 'Where are your offices and how do I get there?',
            content: '<p>The locations and addresses to our offices can be found in the LOCATIONS section of our website. However, with our Fast Pass service you can pick up your vehicle right as you exit the airport. This service is exclusive for the Juan Santamaria International and Daniel Oduber airports in San José and Liberia.</p>'
          },
          question_16: {
            title: 'Is there a minimum or maximum rental time for a vehicle?',
            content: '<p>Yes, the minimum rental time is 24 hours and the maximum period of rental time is 30 days.</p>'
          },
          question_17: {
            title: 'How much is the security deposit?',
            content: '<p>The standard security deposit amount with a credit card is $1,000. The standard security deposit amount with a debit card is $2,000. Premium vehicle rentals require a credit card deposit amount of $2,000.</p>'
          },
          question_18: {
            title: 'What is the minimum age to rent a vehicle?',
            content: '<p>The minimum age to rent a vehicle is 21 years.</p>'
          },
          question_19: {
            title: 'Are additional drivers permitted?',
            content: '<p>Additional drivers are permitted if they are registered at the time of the signing of the rental agreement. This way, the additional driver will also be covered by the insurance policy.</p>'
          }          
        }
      },
      online_payment_policies: {
        permalink: 'online-payment-policies',
        seo: {
          title: '',
          description: 'Cancel your car reservation in Costa Rica safely and quickly with ToyotaRent.',
          keywords: 'toyota, costa rica, vehicles, rent, cars, trac, toyotarent'
        },
        title: 'On-Line Payment Policies',
        content: '<h2>Reservation Policies:</h2><p>Our rates are expressed in US dollars. However, according to business conditions, Toyota Rent a Car reserves the right to modify any rates through publication in its website.</p><p>Should the reservation take place in a period of time when the High and Low season overlap, the applicable rate for the entire period would be the rate effective on the first day of rental.</p><p>At the time of pick up of the prepaid rental, drivers must physically present the same credit card used for online prepayment. This same card must also be used to cancel the full service amount.</p><p>The On-line Payment Service allows you to be eligible for a 10% discount on your time and mileage rate.</p><p>To redeem this discount, at the time of reservation, Toyota Rent a Car shall charge 10% of your reservation amount to the credit card you have entered into our system.</p><p>Your reservation will be confirmed within the next 24 hours.</p><h2>Cancellation and No Show Policy:</h2><p>In case of cancellation or no-show, Toyota Rent a Car will not refund any amount prepaid or charged to your Credit Card through the On-Line Payment Service.</p><h2>Credit or Debit Car:</h2><p>Credit or debit card used to complete an online prepaid reservation must be presented at the rental pick-up counter as a form of payment along with identification.</p><p>Debit Cards ARE NOT ACCEPTED for security deposits , because the process of releasing the holdings might take up to 45 workings days.</p> <h2>Delayed Collections/ No show policy</h2><p>Reservations will be held for 1 hour from the specified reservation time after which they will be classed as a no-show.  If a customer arrives after this time then a vehicle is not assured.  We will do everything possible to provide a vehicle. This may include offering a different vehicle and rates may be different to the original booking.</p><p>Flight numbers provided are for informational purposes only. It is the responsibility of the customer to inform the location of any delays.</p>'
      },
      awards: {
        permalink: 'awards',
        seo: {
          title: 'Costa Rica car rental accreditations | rental car in Costa Rica',
          description: 'Costa Rica Car Rental Accreditations: Institute of Tourism (ICT), The Chamber of National Tourism (We represents the entire consolidated and united Costa Rica Business sector, reason why it is one of the organizations, within civil society, with the greatest leadership in national incidence) and many others',
          keywords: 'car rental, costa rica, rent a car, toyota rental accreditations, toyota rent, toyota car, rental car'
        },
        title: 'Awards',
        content: '<p>Over the past five years, Toyota Rent a Car has implemented a formula for success that led the company to receive the Excellence Award in three different categories: Leadership and Strategic Planning, Focus on Customers and Marketing and Focus on Human Talent.</p><p>These awards are the result of Toyota Rent a Car’s bet on a value proposition based on providing unique, differentiated services of the highest quality. The company also realized that to make this proposition a reality, it had to invest in professionalizing its human resources.</p><p>At Toyota Rent A Car we believe that corporate values are the cornerstones that drive individual behavior to achieve strategic objectives. Associates are encouraged to grow within the company and move ever closer to excellence. We call this “closing gaps.” Each team member closes his or her gaps with a training program and a 180° assessment system which objectively points out progress made and next steps. This has resulted in high levels of motivation in associates, while also strengthening the organizational culture to transcend the company and be perceived by customers.</p><p>Additionally, the company is studying the needs of different customer profiles to develop innovative services to respond to them, thus standing out from the competition and establishing lasting relationships with customers based on delivering value.</p><p>Toyota Rent a Car has an innovation program to introduce new products and services, and associates can suggest and implement innovative ideas that later become services.</p><p>As part of its differentiated offer, it features a premium vehicle line that combines luxury models like the Toyota Camry Hybrid, Prado and Sequoia with innovative complementary services such as drivers, vehicle delivery directly at the airport, wedding services, prepaid fuel and luggage space, as well as small details that make life more pleasant for customers.</p><p>Additionally, we have adopted an environmental approach in the development of all our processes in order to reduce our carbon footprint from fuel use. We measure the impact of our services and mitigate it with ongoing maintenance of the automobiles and specific green actions such as planting native trees in Guanacaste. Thus, customers are guaranteed that their choice is actively contributing to the conservation of the natural environment.</p><p>Excellence practices implemented by the company have allowed us to enjoy a very healthy organizational climate, with an inner satisfaction level of 85% and a productivity index which increased 12% this past year.</p><p>This award marks the beginning of a new cycle for Toyota Rent a Car. Each day, with increasing responsibility and vision, efforts will focus on innovation through products, services and initiatives that enhance customer experience, protect our environment and contribute to the growth of our staff.</p>'
      },
      news_updates: {
        permalink: 'news-updates',
        seo: {
          title: 'Costa Rica car rental news | rental car in Costa Rica Toyota Rent',
          description: 'Important news and updates about Costa Rica Tourism. Rent a car with Toyota Rent and learn about the best places to visit, hotel information, flying details and more!',
          keywords: 'car rental, costa rica, rent a car, toyota rental news, toyota rent, toyota car, rental car, costa rica updates'
        },
        title: 'News & Updates',
        articles: [
          {
            title: '',
            summary: '',
            date: '',
            thumbnail: '',
            content: ''
          }
        ]
      },
      contact_us: {
        permalink: 'contact-us',
        seo: {
          title: 'Costa Rica car rental | rental car in Costa Rica SJO, LIR, Toyota rent a car',
          description: 'Contact the best Costa Rica Car Rental in San José, (SJO) Alajuela Int. Airport, (LIR) Liberia‐Guanacaste & Tambor, Península de Nicoya‐Puntarenas, Manuel Antonio, Quepos. We have an oﬃce available in every place around Costa Rica.',
          keywords: 'car rental, costa rica, rent a car, toyota cars, toyota rent, toyota car, rental car'
        },
        issues: [
          'Type of issue *',
          'Additional Driver Dispute',
          'After Hours Return',
          'Car Class Reserved Not Available',
          'Commissions',
          'Coverage / Insurance',
          'Damage Claims',
          'Damage Dispute ',
          'Deposit Amount',
          'Disabled Driver / Special Equipment',
          'Early / Late Return Dispute',
          'Emergency Roadside Assistance',
          'Employee Indifference',
          'Excessive Wait',
          'Frequent Flyer Points',
          'Fuel Charge Dispute',
          'Geographical Restrictions',
          'Government',
          'Lost & Found',
          'One-Way Dispute',
          'Other',
          'Pay Now',
          'Rental Extension',
          'Rental Extension - Final Request',
          'Rental Extension 2nd Request',
          'Request copy of Receipt',
          'Reservation failure',
          'Roadside Dispute',
          'Shuttle service',
          'Tax Dispute',
          'Toll charges',
          'Total charges',
          'Traffic ticket/Violation',
          'Unauthorized drop',
          'Under Age Dispute',
          'Upgrade disputes',
          'Vehicle Cleanliness',
          'Vehicle Mechanical Issue'
        ]
      },
      landing: {
        iron_man: {
          seo: {
            title: 'Iron man | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          reserve_today: 'Reserve today!',
          make_reservation_btn: 'Make reservation',
          table: {
            header: {
              col_1: 'Model',
              col_2: 'Daily with basic insurance',
              col_3: 'Daily with full insurance',
              col_4: 'Weekly with basic insurance',
              col_5: 'Weekly with full insurance',
            },
            data: [
              {
                col_1: 'Model',
                col_2: 'Daily with basic insurance',
                col_3: 'Daily with full insurance',
                col_4: 'Weekly with basic insurance',
                col_5: 'Weekly with full insurance',
              },              
              {
                col_1: 'Yaris',
                col_2: '$42',
                col_3: '$56',
                col_4: '$268',
                col_5: '$369'
              },
              {
                col_1: 'Bego',
                col_2: '$53',
                col_3: '$66',
                col_4: '$338',
                col_5: '$432'
              },
              {
                col_1: 'Fortuer',
                col_2: '$75',
                col_3: '$88',
                col_4: '$478',
                col_5: '$571'
              },
              {
                col_1: 'Prado',
                col_2: '$90',
                col_3: '$103',
                col_4: '$568',
                col_5: '$661'
              }                                                           
            ]
          }          
        },
        envision: {
          seo: {
            title: 'Envision | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          form: {
            vehicles: [
              'Bego or similar, Rush or similar'
            ],
            insurance: [
              'Daily with basic insurance',
              'Daily with full insurance',
              'Weekly with basic insurance',
              'Weekly with full  insurance'
            ],
            legend: '<p><strong>Basic Mandatory Protection:</strong> Basic Mandatory Protection must be purchased along with the rent of the vehicle. The mandatory protection entitles the client a maximum deductible of US $ 1,200 over damages caused to the rented vehicle (CDW Collision damage waiver). The client is entitled to a minimum deductible of US $300 or 20% of total value of all damages caused to third party property (LDW Liability Damage Waiver).</p><p>Basic insurance is included in the price we provide as well as all extra fees and taxes. Basic Mandatory Insurance has to be purchased by law in Costa Rica.</p>'
          },
          reserve_today: 'Reserve now!',
          envision: 'Envision',
          make_reservation_btn: 'Pay Now!',
          table: {
            header: {
              col_1: 'ART',
              col_2: 'Precio regular',
              col_3: 'Precio Envision',
            },
            data: [
              {
                col_1: 'ART',
                col_2: 'Regular price',
                col_3: 'Envision price'
              },              
              {
                col_1: 'Booster Seat / Child Seat',
                col_2: '$6 x día',
                col_3: '$5 x día',
              },
              {
                col_1: 'Quick Pass',
                col_2: '$10 x día',
                col_3: '$8 x día',
              },
              {
                col_1: 'Mobile Hot Spot ',
                col_2: '$12.99 x día',
                col_3: '$10.99 x día',
              },
              {
                col_1: 'Seguro Titanium ',
                col_2: '$31.42 x día ',
                col_3: '$27.32 x día',
              },              
              {
                col_1: 'Fast Pass ',
                col_2: '$15  Fee',
                col_3: '$13 Fee',
              },                  
              {
                col_1: 'Fast Return (For Titanium insurence customer only)',
                col_2: '$15  Fee',
                col_3: '$13 Fee',
              },                   
            ],
            footer: 'Basic Insurance is mandatory in Costa Rica, the full insurance is optional'
          },
          content: '<h3>Envision Special Offer </h3><ul><li>$55 USD per DAY</li><li>$275 USD per WEEK</li></ul><h3>Includes</h3><ul><li>Basic Insurance</li><li>Free Additional Driver</li>    </ul><h3>Terms & Conditions  </h3><ul><li>Pick Up and Return vehicle at Juan Santa María Airport (SJO)</li><li>Only Prepaid rate </li>    </ul>'
        },
        jungle_jam: {
          seo: {
            title: 'Jungle Jam | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          form: {
            vehicles: [
              'Yaris: Compact car, 5 passengers',
              'Corolla: Compact car, 5 passengers',
              'Bego: Small 4x4 SUV, 5 passengers',
              'Rav4: Mid-size 4x4 SUV, 5 passengers',
              'Prado, Large 4x4 SUV, 7 passengers'
            ],
            insurance: [
              'Daily with basic insurance',
              'Daily with full insurance',
              'Weekly with basic insurance',
              'Weekly with full  insurance'
            ],
            legend: '<p><strong>Basic Mandatory Protection:</strong> Basic Mandatory Protection must be purchased along with the rent of the vehicle. The mandatory protection entitles the client a maximum deductible of US $ 1,200 over damages caused to the rented vehicle (CDW Collision damage waiver). The client is entitled to a minimum deductible of US $300 or 20% of total value of all damages caused to third party property (LDW Liability Damage Waiver).</p><p>Basic insurance is included in the price we provide as well as all extra fees and taxes. Basic Mandatory Insurance has to be purchased by law in Costa Rica.</p>'
          },
          table: {
            header: {
              col_1: 'Vehicle',
              col_2: 'Daily without basic insurance',
              col_3: 'Daily with basic insurance',
              col_4: 'Daily without full insurance',
              col_5: 'Daily with full insurance',
              col_6: 'Weekly without basic insurance',
              col_7: 'Weekly with basic insurance',
              col_8: 'Weekly without full insurance',
              col_9: 'Weekly with full insurance'
            },
            data: [
              {
                col_1: 'Yaris',
                col_2: '$37',
                col_3: '$54',
                col_4: '$37',
                col_5: '$78',
                col_6: '$222',
                col_7: '$341',
                col_8: '$341',
                col_9: '$509'
              },
              {
                col_1: 'Corolla',
                col_2: '$44',
                col_3: '$62',
                col_4: '$44',
                col_5: '$87',
                col_6: '$265',
                col_7: '$390',
                col_8: '$265',
                col_9: '$565'
              },
              {
                col_1: 'Bego',
                col_2: '$52',
                col_3: '$74',
                col_4: '$52',
                col_5: '$99',
                col_6: '$313',
                col_7: '$464',
                col_8: '$313',
                col_9: '$639'
              },
              {
                col_1: 'Rav-4',
                col_2: '$68',
                col_3: '$97',
                col_4: '$68',
                col_5: '$124',
                col_6: '$405',
                col_7: '$608',
                col_8: '$405',
                col_9: '$797'
              },
              {
                col_1: 'Prado',
                col_2: '$95',
                col_3: '$123',
                col_4: '$95',
                col_5: '$153',
                col_6: '$567',
                col_7: '$767',
                col_8: '$567',
                col_9: '$977'
              }
            ],
            footer: 'Basic Insurance is mandatory in Costa Rica, the full insurance is optional'
          },
          content: '<h3>Exclusive Benefits for Jungle Jam Participants</h3><ul><li>The security deposit if you only choose basic insurance will be $750, if full insurance is chosen than coverage will drop to $0.</li><li>Awarded a complimentary accessory either rack, basket, cooler, baby seat or booster, each of these has a normal cost of $ 5 per day.</li><li>Granted free additional driver, which normally has a cost of $3 per day.</li><li>No amount will be charged if dropped off at any of our offices, for example rented in Quepos and returned to our office in Liberia.</li><li>No additional cost to deliver or pick-up rental car from our office in Dominical or Envision Site.</li><li>Moneyback guarantee with every reservation made by you. This means that our cars will be there no matter what happens. If something does happen we will give them their money back for their reservation or their car will be free. We are the most reliable company and we want to offer this to you because we know that companies overbook and people arrive and have no car available.</li><li>We will extend to 4 hours the time of giving back their car. So, if they rent at 12 midday... on the return date they can do it at 4pm with no extra charge.</li></ul>'
        },
        promo: {
          seo: {
            title: 'Promo | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          legend: {
            text: 'Discount'
          },
          form: {
            vehicles: {
              v: 'Avanza: Minivan, Auto, 7 passengers',
              f: 'Bego: Small 4x4, Auto, 5 passengers',
              g: 'Bego: Small 4x4, Man, 5 passengers',
              e: 'Corolla: Standard car, Auto, 5 passengers',
              l: 'Fortuner: Full 4x4, Auto, 7 passengers',
              q: 'Hiace: Microbus, Man, 12 passengers',
              u: 'Hiace: Microbus, Man, 16 passengers',
              m: 'Hilux: Pickup, Man, 5 passengers',
              s: 'Prado: Full 4x4, 7 Auto, passengers',
              p: 'Prado: Premium 4x4, 7 passengers',
              h: 'Rav4: Mid-size 4x4, Auto, 5 passengers',
              c: 'Yaris: Compact car, Man, 5 passengers',
              d: 'Yaris: Compact car, Auto, 5 passengers'
            },
            insurance: [
              'Basic insurance',
              'Full insurance'
            ],
            legend: '<p><strong>Basic Mandatory Protection:</strong> Basic Mandatory Protection must be purchased along with the rent of the vehicle. The mandatory protection entitles the client a maximum deductible of US $ 1,200 over damages caused to the rented vehicle (CDW Collision damage waiver). The client is entitled to a minimum deductible of US $300 or 20% of total value of all damages caused to third party property (LDW Liability Damage Waiver).</p><p>Basic insurance is included in the price we provide as well as all extra fees and taxes. Basic Mandatory Insurance has to be purchased by law in Costa Rica.</p>'
          },
          table: {
            header: {
              col_1: 'Vehicle',
              col_2: 'Daily without basic insurance',
              col_3: 'Daily with basic insurance',
              col_4: 'Daily without full insurance',
              col_5: 'Daily with full insurance',
              col_6: 'Weekly without basic insurance',
              col_7: 'Weekly with basic insurance',
              col_8: 'Weekly without full insurance',
              col_9: 'Weekly with full insurance'
            },
            data: [
              {
                col_1: 'Yaris',
                col_2: '$37',
                col_3: '$54',
                col_4: '$37',
                col_5: '$78',
                col_6: '$222',
                col_7: '$341',
                col_8: '$341',
                col_9: '$509'
              },
              {
                col_1: 'Corolla',
                col_2: '$44',
                col_3: '$62',
                col_4: '$44',
                col_5: '$87',
                col_6: '$265',
                col_7: '$390',
                col_8: '$265',
                col_9: '$565'
              },
              {
                col_1: 'Bego',
                col_2: '$52',
                col_3: '$74',
                col_4: '$52',
                col_5: '$99',
                col_6: '$313',
                col_7: '$464',
                col_8: '$313',
                col_9: '$639'
              },
              {
                col_1: 'Rav-4',
                col_2: '$68',
                col_3: '$97',
                col_4: '$68',
                col_5: '$124',
                col_6: '$405',
                col_7: '$608',
                col_8: '$405',
                col_9: '$797'
              },
              {
                col_1: 'Prado',
                col_2: '$95',
                col_3: '$123',
                col_4: '$95',
                col_5: '$153',
                col_6: '$567',
                col_7: '$767',
                col_8: '$567',
                col_9: '$977'
              }
            ],
            footer: 'Basic Insurance is mandatory in Costa Rica, the full insurance is optional'
          },
          content: '<h3>Exclusive Benefits for Jungle Jam Participants</h3><ul><li>The security deposit if you only choose basic insurance will be $750, if full insurance is chosen than coverage will drop to $0.</li><li>Awarded a complimentary accessory either rack, basket, cooler, baby seat or booster, each of these has a normal cost of $ 5 per day.</li><li>Granted free additional driver, which normally has a cost of $3 per day.</li><li>No amount will be charged if dropped off at any of our offices, for example rented in Quepos and returned to our office in Liberia.</li><li>No additional cost to deliver or pick-up rental car from our office in Dominical or Envision Site.</li><li>Moneyback guarantee with every reservation made by you. This means that our cars will be there no matter what happens. If something does happen we will give them their money back for their reservation or their car will be free. We are the most reliable company and we want to offer this to you because we know that companies overbook and people arrive and have no car available.</li><li>We will extend to 4 hours the time of giving back their car. So, if they rent at 12 midday... on the return date they can do it at 4pm with no extra charge.</li></ul>'
        },
        promo_7x5: {
          seo: {
            title: 'Promo 7x5 | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: '7x5!',
          sub_title: 'Rent a car for 7 days and your last two days are free.'
        },
        bego: {
          seo: {
            title: 'Promo Bego | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Only for $45',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restriction apply'
        },
        blackBego: {
          seo: {
            title: 'Promo Bego Viernes Negro | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Only for $40',
          img: 'begoBlackEn.png',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restriction apply'
        },
        blackYaris: {
          seo: {
            title: 'Promo Sedan Viernes Negro | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Bego',
          sub_title: 'Only for $40',
          img: 'yarisBlackEn.png',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restriction apply'
        },
        hiaci: {
          seo: {
            title: 'Promo Hiace | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          title: 'Hiace',
          sub_title: 'Only for $90',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restrictions apply'
        }, 
        santasPromociones: {
          seo: {
            title: 'Easter Rates | Toyota Rent a Car',
            description: '',
            keywords: ''
          },
          img: 'santa-promocionEn',
          title: 'Easter Rates',
          content: '<ul><li>Fortuner only for $100</li><li>Bego only for $50</li><li>Yaris only for $35</li></ul>',
          extra_title: 'Basic insurance included',
          restriction_apply: '*Restrictions apply'
        },                 
      },
      resources: {
        permalink: 'resources',
        seo: {
          title: '',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Resources',
        content: '<ul><li><a target="_black" href="http://www.traveltourismdirectory.info/Accommodation___Lodging/Timeshare/">Visit Timeshares</a></li><li><a target="_black" href="http://visitgoodwood.com/" title="Directory of Great Travel Sites">Directory of Great Travel Sites</a></li><li><a target="_black" href="http://travel-websites.org/">Travel Websites</a></li><li><a target="_black" href="http://www.silverspurmotel.net" target="_blank">Silver Spur Motel  Burns, Oregon</a></li><li><a target="_black" href="http://www.lowratesflights.com/">US To Europe</a></li><li><a target="_black" href="http://www.best-travel-deals-tips.com" target="_blank">Best Travel Deals Tips</a></li><li><a target="_black" href="http://www.greatville.com">GreatVille Travel Directory</a> </li><li><a href= "http://www.willgoto.com">Willgoto, World travel directory and travel guide</a></li><li><a target="_black" href="http://www.travel-niche.com/">Travel Niche directory.</li></ul>'
      },
      policy: {
        permalink: 'privacy-and-security-policy',
        seo: {
          title: 'Privacy and Security Policy',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Privacy and Security Policy',
        content: '<h2>Privacy Policy</h2><p>This Privacy Policy explains how Sociedad Rentacar Centroamericana, owner of the website Toyotarent.com (Toyotarent), uses and protects the information collected from their users when they use this website. This Privacy Policy applies regardless of the type of device you use to access to the website.</p><p>Toyotarent reserves the right to change this Privacy Policy at any time, we recommend you occasionally check back to make sure you agree with the changes.</p><h2>Data collection</h2><p>Toyotarent needs to collect personal information from you to rent a vehicle or provide our other services. We collect personal information at the counter when renting a car, online quote or telephone reservation, including:</p><ul><li>Full name.</li><li>Phone number.</li><li>Demographic information (Country, city, address, zip code).</li><li>Email address.</li><li>Flight information.</li><li>Payment information such as credit or debit card details.</li></ul><p>Sometimes, we may combine your personal information with other information we received from other sources so we can provide you a customized and hassle-free service. We may obtain this personal information from a third party, such as through a travel agency or your employer.</p><p>We may collect technical information about your devices when you use our website. To learn more about our online data collection, go to Online Data section.</p><h2>Online Data</h2><p>When you visit our website, Toyotarent automatically collects technical information. This section will explain you about what technical information we collect and why we collect it.</p>IP address and browsing information: We may collect your IP address when you visit our website to help us diagnose problems, for system administration and audit the use of our website. We may also collect your browser type, the files visited on our site (HTML pages, images, etc.), Operating System, date/time stamp.</p><p>Cookies and other technologies: We use cookies, scripts and other tecnologies to help us recognize you. We use these technologies to track website usage, trends, patterns and customized your browsing experience.</p><h2>Marketing</h2><p>Toyotarent sometimes shares personal information with third-parties for promotions or sending marketing communications through email address.</p><p>The personal information we collect help us to:</p><ul><li>Send you relevant information about offers and special promotions through email address.</li><li>Personalized your website visit.</li></ul><h2>Analytics</h2><p>We use analytics software, such as Google Analytics, to help us to better understand the functionality of our website on your device. The information records only the browsing behavior on our website and it is not connected to you.</p><h2>Safeguards</h2><p>The security of your personal information is very important to us. We take reasonable steps to make sure your information is protected from unauthorized access, alteration, destruction or loss. To protect sensitive personal information, we use we use firewalls and Transport Layer Security (TLS) encryption to ensure confidentiality and integrity of the information.</p><h2>Applicable Law</h2><p>This privacy and security policy is governed by the Ley de Protección de la persona frente al tratamiento de sus datos personales, number 8968 of Costa Rica.</p><h2>Key contacts</h2><p>In case of doubts, questions or suggestions, you can get in touch with Toyotarent through the online form located on the Customer Care section, send an email message to sac@toyotarent.com or call us to +(506)-2105- 3400.</p>'
      },
      interbus: {
        permalink: 'interbus',
        seo: {
          title: 'Book with Interbus',
          description: 'Renta de vehículos marca Toyota con la garantía y el respaldo de ToyotaRent. Alquile su vehículo de manera segura y sin preocupaciones.',
          keywords: 'toyota, costa rica, vehículos, alquiler, renta, coches, trac'
        },
        title: 'Book with Interbus',
        content: '<iframe src="https://www.interbusonline.com/apps/book/bookIframe.php?pinz=1670&pinx=4625ce59976cc377bdc3845cab1538b15f36a658&type=0" width="300" height="500" frameborder="0" allowtransparency="true"></iframe>'
      },
      no_hidden_fees: {
        permalink: 'no-hidden-fees',
        title: 'No hidden fees',
        content: '<h2>No hidden fees</h2><h3>More info:</h3><ul><li>If you wish to add any aditional coverage, ask us at the counter.</li><li>On this website the rate includes unlimited mileage and any additional options you choose.</li><li>We take serious measures to protect the environment; as part of our environment policy we have is not mandatory one time charge fee of $1,85.</li></ul>'
      },        
    },
    cta: {
      book_service: 'Book Service',
      book_now: 'Book Now',
      book_car: 'Book a Car',
      terms_conditions: 'Terms and Conditions',
      contact_us: 'Contact Us',
      waze: 'How to get there'
    },
    book: {
      or_similar: 'or similar',
      day: 'day'
    },
    enjoy_great_deals: {
      main_title: 'Enjoy Great Deals!'
    },
    login: {
      title: 'Log In',
      email: 'Email',
      email_place: 'John@gmail.com',
      code: 'Code',
      code_place: 'Enter your code',
      login: 'Log In',
      signup: 'Sign Up',
      error: 'An error occurred or your credentials are incorrect. :( '
    },
    signup: {
      title: 'Sign Up',
      email: 'Email',
      email_place: 'John@gmail.com',
      signup: 'Sign Up',
      error: 'An error occurred or the email was already registred. :( ',
      success: 'Your access code was sent to your email ! :) '
    },
    email_code: {
      title: 'Welcome to Reactive Finance Tracking !!!',
      subject: 'RFT [Credentials]',
      credential: 'Your credentials are: ',
      user: 'User: ',
      code: 'Code: '
    },
    settings: {
      exchange: {
        exchange: 'Exchange Rate',
        rate: 'Exchange Rate (USD)',
        rate_place: 'Local -> USD',
        rate_ok: 'The exchange rate was saved',
        rate_error: 'Wrong exchange rate value',
        currency: 'Local Currency',
        currency_place: 'e.g. USD',
        currency_ok: 'The local currency was saved',
        currency_error: 'Wrong local currency value'
      },
      category: {
        category: 'Category',
        category_place: 'Name',
        category_ok: 'The Categoría was saved',
        category_error: 'Wrong Categoía value or it already was listed',
        category_update_ok: 'The Categoría was updated',
        grid: {
          category: 'Category',
          active: 'Active'
        }
      },
      label: {
        label: 'Label',
        label_place: 'Name',
        label_ok: 'The Label was saved',
        label_error: 'Wrong Label value or it already was listed',
        label_update_ok: 'The Label was updated',
        grid: {
          label: 'Label',
          active: 'Active'
        }
      },
      entries: {
        add: 'Add',
        add_new_title: 'Add a new transaction',
        add_update_title: 'Update transaction',
        update: 'Update',
        entry_date: 'Entry Date',
        amount: 'Amount',
        amount_place: '1,000.00',
        description: 'Description',
        entry_ok: 'The transaction was saved',
        entry_error: 'Wrong transaction value(s)',
        entry_update_ok: 'The transaction was updated'
      }
    },
    actions: {
      edit: 'Edit',
      delete: 'Delete',
      save: 'Save',
      update: 'Update'
    },
    months: {
      january: 'January',
      february: 'February',
      march: 'March',
      april: 'April',
      may: 'May',
      june: 'June',
      july: 'July',
      august: 'August',
      september: 'September',
      october: 'October',
      november: 'November',
      december: 'December'
    },
    calendar_days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    calendar_months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  }
};
