import React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import { Row, Col, Pagination, Pager } from 'react-bootstrap';
import _ from 'lodash';

import Layout from '../shared/internal-layout';
export default observer(['store'], React.createClass({
  displayName: 'Blog',
  componentWillMount() {
    this.translation = this.props.store.translation;
    this.header = this.props.store.header;
    this.blog = this.props.store.blog;
    this.blog.getPosts();
	},  
	showPosts() {
    const posts = this.blog.posts;
    const lang = this.translation.language;
    const textReadmore = this.translation.t('pages.blog.btnReadMore');
    const postUrl = `${_.isEqual(lang, 'es') ? '/es' : ''}/${this.translation.t('pages.blog.permalink')}`;

    return _.map(posts, (post, index) => {
      const item = post[lang];
      const link = `${postUrl}/${item.slug}`;
      return (
        <div className="post" key={index}>
          <h2><Link to={link}> {item.titulo} </Link></h2>
          {/* <img src={item.imagen.thumbnail} className='img pull-right' /> */}
          <div dangerouslySetInnerHTML={{ __html: item.excerpt }} />
          <p className='read-more'>
            <Link className='btn small' to={link}>{textReadmore}</Link>
          </p>
        </div>
      );
    });
	},
  render() {
    const pageTitle = this.translation.t('pages.blog.title');
    const pageTitleCategoria = this.translation.t('pages.blog.textCategoria');
    const seo = this.translation.t('pages.blog.seo');  

    return (
      <Layout pageTitle={pageTitle} seo={seo}>
        <section className='main-section main-blog'>
          <div className='container'>
            <Row>
              <Col xs={12} sm={8} md={9}>
                <div className="blog">
                  {this.showPosts()}
                </div>
              </Col>
              <Col xs={12} sm={4} md={3}>
              {/* TODO hacer componente */}
              <div className='category-list'>
                <h3>{pageTitleCategoria}</h3>
                <div className='list-group'>
                  <Link to='/blog?cat=all' className='active list-group-item'> All Categories
                  </Link>
                  {/* <Link to='/item' className='false list-group-item'> Name 1
                  </Link> */}
                </div>
              </div>
              </Col>
            </Row>
            {/* TODO hacer componente */}
            {/* <ul className="pagination">
              <li className="disabled">
                <Link to="/?page=1">
                <span className="glyphicon glyphicon-chevron-left"></span>
              </Link>
              </li>
              <li className="active"><Link to="./?page=1">1</Link></li>
              <li className="disabled">
                <Link to="/?page=1">
                <span className="glyphicon glyphicon-chevron-right"></span>
              </Link>
              </li>
            </ul>						 */}
          </div>
        </section>
      </Layout>
    );
  }
}));
