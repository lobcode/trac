import React from 'react';
import {observer} from 'mobx-react';
import {Link, browserHistory} from 'react-router';
import LazyLoad from 'react-lazy-load';
import {Row, Col, Pagination, Pager} from 'react-bootstrap';
import _ from 'lodash';

import Layout from '../shared/internal-layout';

export default observer(['store'], React.createClass({
	displayName: 'Blog',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.metatags = this.props.store.metaTags;
		this.blog = this.props.store.blog;
		const slug = _.get(this.props.params, 'slug');
    if (!slug) {
      browserHistory.push('/404');
		}		
    this.blog.getPost(slug, this.translation.language);
	},
	showPost() {
		const post = this.blog.post;
		const isFetching = this.blog.isFetching;
		const lang = this.translation.language;
		const postUrl = `${_.isEqual(lang, 'es') ? '/es' : ''}/${this.translation.t('pages.blog.permalink')}`;

		if(post && !_.isEmpty(post) && !isFetching) {
			const item = post[lang];
			return (
				<article>
						<p><Link to={postUrl}>&larr; back to the blog</Link></p>
						<header>
							<h1>{item.titulo}</h1>
							<br/>
						</header>
						<div className="post my-5">
						{item.imagen && item.imagen.full &&
							<LazyLoad>
								<img className="my-2 img-responsive" src={item.imagen.full}/>
							</LazyLoad>
						}						
							<br/>
							<div dangerouslySetInnerHTML={{ __html: item.contenido }} />
						</div>
				</article>
			);
		}

		if(post && _.isEmpty(post) && !isFetching) {
			browserHistory.push('/404');
		}

	},
	render() {
		const pageTitle = this.translation.t('pages.blog.title');

		return (
			<Layout pageTitle={pageTitle} seo={this.metatags}>
				<section className="main-section main-blog">
					<div className="container">
						<Row>
							<Col xs={12} sm={8} md={9}>
								{this.showPost()}
							</Col>
							<Col xs={12} sm={4} md={3}>
							{/* TODO hacer componente */}
								<div className="category-list">
									<h3>Categories</h3>
									<div className="list-group">
										<Link to="/blog" className="active list-group-item">All Categories</Link>
									</div>	
								</div>						
							</Col>
						</Row>
						{/* TODO hacer componente */}
						{/* <ul className="pagination">
							<li className="disabled">
								<Link to="/?page=1">
									<span className="glyphicon glyphicon-chevron-left"></span>
								</Link>
							</li>
							<li className="active"><Link to="./?page=1">1</Link></li>
						
							<li className="disabled">
								<Link to="/?page=1">
									<span className="glyphicon glyphicon-chevron-right"></span>
								</Link>
							</li>
						</ul>						 */}
					</div>
				</section>
		</Layout>
		);
	}
}));
