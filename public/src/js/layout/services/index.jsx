import React from 'react';
import {observer} from 'mobx-react';
import {Row, Col} from 'react-bootstrap';
import _ from 'lodash';
import {Link} from 'react-router';

import Layout from '../shared/internal-layout';
import MenuInternal from '../shared/menu-internal';

export default observer(['store'], React.createClass({
	displayName: 'Services',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	render() {
		const pageTitle = this.translation.t('header.menu.services');
		const seo = this.translation.t('pages.services.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
			<section className="main-section services">
				<MenuInternal />
				<div className="container">
					<Row>
						CARS LIST
					</Row>
				</div>
			</section>
		</Layout>
		);
	}
}));
