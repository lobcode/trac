import React from 'react';
import {observer} from 'mobx-react';
import {Navbar, Nav, NavDropdown, MenuItem, Row, Col} from 'react-bootstrap';

import FacebookIcon from '../../shared/images/facebook-icon';
import WazeIcon from '../../shared/images/waze-icon';
import YouTube from '../../shared/images/youtube-icon';

export default observer(['store'], React.createClass({
	componentWillMount() {
		this.translation = this.props.store.translation;
	},
	trackCall() {
    dataLayer.push({
			"event": "click",
			"btnName": "btnCall"
		});
	},
	trackCallUsa() {
    dataLayer.push({
			"event": "click",
			"btnName": "btnCallUsa"
		});
	},
	trackRedSocial(name) {
    dataLayer.push({
			"event": "click",
			"btnName": name
		});
	},
	render() {
		return (
			<footer className="footer-main">
			</footer>
			// <footer className="main-footer">
			// 	<div className="menu-footer">
			// 		<div className="container-fluid">
			// 			<Row>
			// 				<Col xs={12}>
			// 					<Navbar bsStyle="inverse" fluid>
			// 						<Nav>
			// 							<NavDropdown eventKey={1} title={this.translation.t('menu.products_services.title')} id="product-services-footer">
			// 								<MenuItem eventKey={1.1} href="/products-services">{this.translation.t('menu.products_services.subMenu.item1')}</MenuItem>
			// 								<MenuItem eventKey={1.2} href="/products-services">{this.translation.t('menu.products_services.subMenu.item2')}</MenuItem>
			// 								<MenuItem eventKey={1.3} href="/products-services">{this.translation.t('menu.products_services.subMenu.item3')}</MenuItem>
			// 								<MenuItem eventKey={1.4} href="/products-services">{this.translation.t('menu.products_services.subMenu.item4')}</MenuItem>
			// 								<MenuItem eventKey={1.5} href="/products-services">{this.translation.t('menu.products_services.subMenu.item5')}</MenuItem>
			// 								<MenuItem divider />
			// 								<MenuItem eventKey={1.6} href="/products-services">{this.translation.t('menu.products_services.subMenu.item6')}</MenuItem>
			// 								<MenuItem eventKey={1.7} href="/products-services">{this.translation.t('menu.products_services.subMenu.item7')}</MenuItem>
			// 								<MenuItem eventKey={1.8} href="/products-services">{this.translation.t('menu.products_services.subMenu.item8')}</MenuItem>
			// 								<MenuItem eventKey={1.9} href="/products-services">{this.translation.t('menu.products_services.subMenu.item9')}</MenuItem>
			// 								<MenuItem eventKey={1.10} href="/products-services">{this.translation.t('menu.products_services.subMenu.item10')}</MenuItem>
			// 							</NavDropdown>
			// 							<NavDropdown eventKey={2} title={this.translation.t('menu.locations.title')} id="locations-footer">
			// 								<MenuItem eventKey={2.1} href="/locations">{this.translation.t('menu.locations.subMenu.item1')}</MenuItem>
			// 								<MenuItem eventKey={2.2} href="/locations">{this.translation.t('menu.locations.subMenu.item2')}</MenuItem>
			// 								<MenuItem eventKey={2.3} href="/locations">{this.translation.t('menu.locations.subMenu.item3')}</MenuItem>
			// 								<MenuItem eventKey={2.4} href="/locations">{this.translation.t('menu.locations.subMenu.item4')}</MenuItem>
			// 								<MenuItem eventKey={2.5} href="/locations">{this.translation.t('menu.locations.subMenu.item5')}</MenuItem>
			// 								<MenuItem eventKey={2.6} href="/locations">{this.translation.t('menu.locations.subMenu.item6')}</MenuItem>
			// 								<MenuItem eventKey={2.7} href="/locations">{this.translation.t('menu.locations.subMenu.item7')}</MenuItem>
			// 								<MenuItem eventKey={2.8} href="/locations">{this.translation.t('menu.locations.subMenu.item8')}</MenuItem>
			// 							</NavDropdown>
			// 							<NavDropdown eventKey={3} title={this.translation.t('menu.about.title')} id="about-footer">
			// 								<MenuItem eventKey={3.1} href="/about">{this.translation.t('menu.about.subMenu.item1')}</MenuItem>
			// 								<MenuItem eventKey={3.2}>{this.translation.t('menu.about.subMenu.item2')}</MenuItem>
			// 								<MenuItem eventKey={3.3} href="/faq">{this.translation.t('menu.about.subMenu.item3')}</MenuItem>
			// 								<MenuItem eventKey={3.4}>{this.translation.t('menu.about.subMenu.item4')}</MenuItem>
			// 								<MenuItem eventKey={3.5} href="/contact">{this.translation.t('menu.about.subMenu.item5')}</MenuItem>
			// 							</NavDropdown>
			// 							<NavDropdown eventKey={4} title={this.translation.t('menu.book.title')} id="book-footer">
			// 								<MenuItem eventKey={4.1} href="/cars" title={this.translation.t('menu.book.subMenu.item1')}>{this.translation.t('menu.book.subMenu.item1')}</MenuItem>
			// 								<MenuItem eventKey={4.2} href="/special-offers" title={this.translation.t('menu.book.subMenu.item2')}>{this.translation.t('menu.book.subMenu.item2')}</MenuItem>
			// 								<MenuItem eventKey={4.3} title={this.translation.t('menu.book.subMenu.item3')}>{this.translation.t('menu.book.subMenu.item3')}</MenuItem>
			// 							</NavDropdown>
			// 							<NavDropdown eventKey={5} title={this.translation.t('menu.faq.title')} id="faq-footer">
			// 								<MenuItem eventKey={5.1}>{this.translation.t('menu.faq.subMenu.item1')}</MenuItem>
			// 								<MenuItem eventKey={5.2}>{this.translation.t('menu.faq.subMenu.item2')}</MenuItem>
			// 								<MenuItem eventKey={5.3}>{this.translation.t('menu.faq.subMenu.item3')}</MenuItem>
			// 								<MenuItem eventKey={5.4}>{this.translation.t('menu.faq.subMenu.item4')}</MenuItem>
			// 								<MenuItem eventKey={5.5}>{this.translation.t('menu.faq.subMenu.item5')}</MenuItem>
			// 							</NavDropdown>
			// 						</Nav>
			// 					</Navbar>
			// 				</Col>
			// 			</Row>
			// 		</div>
			// 	</div>
			// 	<div className="bottom-footer">
			// 		<div className="container-fluid">
			// 			<Row>
			// 				<Col className="contact-info" xs={12} sm={6}>
			// 					<p>&copy;{new Date().getFullYear()} <strong>Toyota Rent a Car Costa Rica</strong></p>
			// 					<p>{this.translation.t('footer.phone')} <a href="tel:+506 2105-3400" title={this.translation.t('footer.phone')} onClick={this.trackCall} >+506 2105-3400</a></p>
			// 					<br/>
			// 					<p><strong>{this.translation.t('footer.textUsaCanada')}</strong></p>
			// 					<p>{this.translation.t('footer.phone')} <a onClick={this.trackCallUsa} href="tel:800-123-7368" rel="nofollow" title={this.translation.t('footer.phoneUsa')}> 800-123 RENT</a></p>
			// 					<div className="social-links">
			// 						<a onClick={(e) => this.trackRedSocial('btnFacebook')} href="https://www.facebook.com/toyotarentacarcostarica" rel="nofollow" target="_blank" title="Facebook">
			// 							<FacebookIcon
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>
			// 						<a onClick={(e) => this.trackRedSocial('btnWaze')} href="https://toyotarent.com/we-are-on-waze/" title="Waze">
			// 							<WazeIcon
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>
			// 						<a onClick={(e) => this.trackRedSocial('btnYouTube')} href="https://www.youtube.com/channel/UCHce0ttDH2KfTHqnuGiUj7Q" title="You Tube">
			// 							<YouTube
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>									
			// 					</div>
			// 				</Col>
			// 				<Col className="logos" xs={12} sm={6}>
			// 					<img src="/src/images/turismo-sostenible.svg" alt="Turismo Sostenible"/>
			// 					<img src="/src/images/excelencia-logo.png" alt="Excelencia"/>
			// 				</Col>
			// 			</Row>
			// 		</div>
			// 	</div>
			// </footer>
		);
	}
}));
