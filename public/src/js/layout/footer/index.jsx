import React from 'react';
import {observer} from 'mobx-react';
import {Navbar, Nav, NavItem, Row, Col} from 'react-bootstrap';
import LazyLoad from 'react-lazy-load';

import FacebookIcon from '../../shared/images/facebook-icon';
import WazeIcon from '../../shared/images/waze-icon';
import YouTube from '../../shared/images/youtube-icon';
import ArrowRightIcon from '../../shared/images/arrow-right';
import {getPagePath} from '../../shared/utils';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';
export default observer(['store'], React.createClass({
	componentWillMount() {
		this.translation = this.props.store.translation;
	},
	trackCall() {
    dataLayer.push({
			"event": "click",
			"btnName": "btnCall"
		});
	},
	trackCallUsa() {
    dataLayer.push({
			"event": "click",
			"btnName": "btnCallUsa"
		});
	},
	trackRedSocial(name) {
    dataLayer.push({
			"event": "click",
			"btnName": name
		});
	},
	render() {
		return (
			<footer className="footer-main">
				<div className="container">
					<Row>
						<Col xs={12} md={7}>
							<Navbar fluid>
								<Nav>
									<h3>The Fleet</h3>
									<NavItem href="/">Sedans</NavItem>
									<NavItem href="/">SUVs</NavItem>
									<NavItem href="/">Premium</NavItem>
									<NavItem href="/">Mini Buses</NavItem>
									<NavItem href="/">Pick Ups</NavItem>
								</Nav>
								<Nav>
									<h3>Locations</h3>
									<NavItem href="/">SJO Intl Airport</NavItem>
									<NavItem href="/">Liberia</NavItem>
									<NavItem href="/">Main Office</NavItem>
									<NavItem href="/">Nosara</NavItem>
									<NavItem href="/">Dominical</NavItem>
								</Nav>
								<Nav>
									<h3>TRAC</h3>
									<NavItem href="/">Renting</NavItem>
									<NavItem href="/">About Us</NavItem>
									<NavItem href="/">News</NavItem>
									<NavItem href="/">Contact Us</NavItem>
									<NavItem href="/">Online Payment</NavItem>
									<NavItem href="/">Privicy & Security</NavItem>
								</Nav>
								<Nav>
									<h3>Travel Assistance</h3>
									<NavItem href="/">Insurance</NavItem>
									<NavItem href="/">Remarks</NavItem>
									<NavItem href="/">Tips</NavItem>
									<NavItem href="/">Products</NavItem>
									<NavItem href="/">Services</NavItem>
									<NavItem href="/">FAQ’s</NavItem>
								</Nav>
							</Navbar>
						</Col>
						<Col xs={12} md={5}>
							<h3 className="footer-main__subtitle">Call us now</h3>
							<a className="footer-main__phone" href="/">+506 21053400</a>

							{/* <form className="footer-main__form" action="#">
								<label htmlFor="newsletterFooter">Subscribe to get better deals</label>
								<div className="footer-main__newsletter">
									<input type="email" placeholder="Email"/>
									<button type="submit">
										<ArrowRightIcon
											width="20"
											height="20"
											className="icon"
											color="ffffff"
			 							/>
									</button>
								</div>
							</form> */}
						</Col>
					</Row>
					<Row>
						<Col xs={12} md={7}>
							<a href="">
								<img src={`${IMG_S3_PATH}trac-logo-new.png`} alt=""/>
							</a>
							<a href="https://www.facebook.com/toyotarentacarcostarica" rel="nofollow" target="_blank" title="Facebook">
								<FacebookIcon
									width="60"
									height="60"
									className="icon"
									color="617d8a"
								/>
							</a>
							<a href="https://www.waze.com/ul?ll=-84.09515760%2C9.93575520&navigate=yes&zoom=16" title="Waze">
								<WazeIcon
									width="60"
									height="60"
									className="icon"
									color="617d8a"
								/>
							</a>
							<a href="https://www.youtube.com/channel/UCHce0ttDH2KfTHqnuGiUj7Q" target="_blank" title="YouTube">
								<YouTube
									width="60"
									height="60"
									className="icon"
									color="617d8a"
								/>
							</a>	
						</Col>
						<Col xs={12} md={5}>
							<LazyLoad><img src={`${IMG_S3_PATH}esential-cr.png`} alt="Esential Costa Rica"/></LazyLoad>
							<LazyLoad><img src={`${IMG_S3_PATH}logo-del-codigo-en-espanol.png`} alt="Esential Costa Rica"/></LazyLoad>
							<LazyLoad><img src={`${IMG_S3_PATH}turismo-sostenible.png`} alt="Turismo Sostenible"/></LazyLoad>
							<LazyLoad><img src={`${IMG_S3_PATH}excelencia-logo.png`} alt="Excelencia"/></LazyLoad>
							<p>©2018 ToyotaRent.com All Rights Reserved</p>
						</Col>
					</Row>
				</div>
			</footer>
			// <footer className="main-footer">
			// 	<div className="menu-footer">
			// 		<div className="container-fluid">
			// 			<Row>
			// 				<Col xs={12}>
			// 					<Navbar bsStyle="inverse" fluid>
			// 						<Nav>
			// 							<NavItem eventKey={1} href={getPagePath('news_updates')}>{this.translation.t('menu.news_updates')}</NavItem>
			// 							<NavItem eventKey={3} href={getPagePath('contact_us')}>{this.translation.t('menu.contact_us')}</NavItem>
			// 							<NavItem eventKey={4} href={getPagePath('online_payment_policies')} title={this.translation.t('menu.online_payment_policies')}>{this.translation.t('menu.online_payment_policies')}</NavItem>
			// 							<NavItem eventKey={5} href={getPagePath('policy')} title={this.translation.t('menu.policy')}>{this.translation.t('menu.policy')}</NavItem>										
			// 						</Nav>
			// 					</Navbar>
			// 				</Col>
			// 			</Row>
			// 		</div>
			// 	</div>
			// 	<div className="bottom-footer">
			// 		<div className="container-fluid">
			// 			<Row>
			// 				<Col className="contact-info" xs={12} sm={6} md={7}>
			// 					<p>&copy;{new Date().getFullYear()} <strong>Toyota Rent a Car Costa Rica</strong></p>
			// 					<p>{this.translation.t('footer.phone')} <a onClick={this.trackCall} href="tel:+506 2105-3400" rel="nofollow" title={this.translation.t('footer.phone')}>+506 2105-3400</a></p>
			// 					<br/>
			// 					<p><strong>{this.translation.t('footer.textUsaCanada')}</strong></p>
			// 					<p>{this.translation.t('footer.phone')} <a onClick={this.trackCallUsa} href="tel: 800-123-7368" rel="nofollow" title={this.translation.t('footer.phoneUsa')}> 800-123 RENT</a></p>

			// 					<div className="social-links">
			// 						<a onClick={(e) => this.trackRedSocial('btnFacebook')} href="https://www.facebook.com/toyotarentacarcostarica" rel="nofollow" target="_blank" title="Facebook">
			// 							<FacebookIcon
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>
			// 						<a onClick={(e) => this.trackRedSocial('btnWaze')} href="https://www.waze.com/ul?ll=-84.09515760%2C9.93575520&navigate=yes&zoom=16" title="Waze">
			// 							<WazeIcon
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>
			// 						<a onClick={(e) => this.trackRedSocial('btnYouTube')} href="https://www.youtube.com/channel/UCHce0ttDH2KfTHqnuGiUj7Q" target="_blank" title="YouTube">
			// 							<YouTube
			// 								width="48"
			// 								height="48"
			// 								className="icon"
			// 							/>
			// 						</a>									
			// 					</div>
			// 				</Col>
			// 				<Col className="logos" xs={12} sm={6} md={5}>
			// 					<br/>
			// 					<Row>
			// 							<Col className="logos" xs={3}><LazyLoad><img src={`${IMG_S3_PATH}logo-del-codigo-en-espanol.png`} alt="Esential Costa Rica"/></LazyLoad></Col>
			// 							<Col className="logos" xs={3}><LazyLoad><img src={`${IMG_S3_PATH}esential-cr.png`} alt="Esential Costa Rica"/></LazyLoad></Col>
			// 							<Col className="logos" xs={3}><LazyLoad><img src={`${IMG_S3_PATH}turismo-sostenible.png`} alt="Turismo Sostenible"/></LazyLoad></Col>
			// 							<Col className="logos" xs={3}><LazyLoad><img src={`${IMG_S3_PATH}excelencia-logo.png`} alt="Excelencia"/></LazyLoad></Col>									
			// 					</Row>
			// 				</Col>
			// 			</Row>
			// 		</div>
			// 	</div>
			// </footer>
		);
	}
}));
