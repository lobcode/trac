import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Row, Col, Button} from 'react-bootstrap';
import {scroller} from 'react-scroll';

import WidgetReserveForm from '../shared/widget-reserve-form';
import Layout from '../shared/internal-layout/landing';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Jungle Jam',
	getInitialState() {
		return {
			isFormVisible: false
		};
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.reserve = this.props.store.reserve;

		this.reserve.isTypeCarReserve = true;
	},
	bookCar() {
		this.setState({isFormVisible: true})
	},
	getTableHeader() {
		const headers = this.translation.t('pages.landing.jungle_jam.table.header');
		return _.map(headers, (header, index) => {
			return (
				<TableHeaderColumn key={index} dataField={index} isKey={index === 'col_1'}>{header}</TableHeaderColumn>
			);
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.jungle_jam.seo');
		const tableData = this.translation.t('pages.landing.jungle_jam.table.data');
		const tableFooter = this.translation.t('pages.landing.jungle_jam.table.footer');
		const content = this.translation.t('pages.landing.jungle_jam.content');
		const form = this.translation.t('pages.landing.jungle_jam.form');

		return (
			<Layout seo={seo} className="jungleJam">
				<section className="main-section">
					<div className="container">
						<Row>
							<Col xs={12} sm={4}>
								<figure>
									<img src="/src/images/landings/jungle-jam/logo-jungle-jam.png" alt="Jungle Jam"/>
								</figure>
							</Col>
							<Col xs={12} smOffset={3} sm={5}>
								<WidgetReserveForm landing={seo.title} type={form} />
							</Col>
							<Col xs={12}>
								<div className="legend label-warning" dangerouslySetInnerHTML={{__html: form.legend}} />
							</Col>
						</Row>
					</div>
				</section>
				<section>
					<div className="container">
						<Row>
							<Col xs={12}>
								<div className="react-bs-table">
									<BootstrapTable data={tableData} hover>
										{this.getTableHeader()}
									</BootstrapTable>
								</div>
								<p className="table-footer text-center">{tableFooter}</p>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<div dangerouslySetInnerHTML={{__html: content}} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
