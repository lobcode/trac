import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col, Button} from 'react-bootstrap';

import Layout from '../shared/internal-layout/landing';
import WidgetReserveForm from '../shared/widget-reserve-form/santasPromociones';

import {scroller} from 'react-scroll';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Santas Promociones',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	bookCar() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.santasPromociones.seo');
		const title = this.translation.t('pages.landing.santasPromociones.title');
		const content = this.translation.t('pages.landing.santasPromociones.content');
		const extraTitle = this.translation.t('pages.landing.santasPromociones.extra_title');
		const restrictionApply = this.translation.t('pages.landing.santasPromociones.restriction_apply');
		const imgName = this.translation.t('pages.landing.santasPromociones.img');
		const img = `${IMG_S3_PATH}slider/${imgName}.jpg`;

    return (
			<Layout seo={seo} className="promo-santasPromociones">
				<section className="main-section" style={{ backgroundImage: `url(${img})` }}>
					<div className="container">
						<Row>
							<Col xs={12} sm={5}>
								<div className="box-title">
									<p className="title">{title}</p>
									<div className="content" dangerouslySetInnerHTML={{__html: content}} />
									<p className="restriction_apply mb-none">{extraTitle}</p>
                  <p className="restriction_apply">{restrictionApply}</p>
								</div>
							</Col>
							<Col xs={12} smOffset={2} sm={5}>
								<WidgetReserveForm landing={seo.title} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
