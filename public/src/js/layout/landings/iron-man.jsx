import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';

import {Row, Col, Button} from 'react-bootstrap';
import {scroller} from 'react-scroll';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import Layout from '../shared/internal-layout/landing';
import WidgetReserveForm from '../shared/widget-reserve-form/iron-man';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Iron Man',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	bookCar() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	getTableHeader() {
		const headers = this.translation.t('pages.landing.iron_man.table.header');
		// Test
		return _.map(headers, (header, index) => {
			return (
				<TableHeaderColumn key={index} dataField={index} isKey={index === 'col_1'}>{header}</TableHeaderColumn>
			);
		});
	},	
	render() {
		const seo = this.translation.t('pages.landing.iron_man.seo');
		const tableData = this.translation.t('pages.landing.iron_man.table.data');

		return (
			<Layout seo={seo} className="iron-man">
				<section className="main-section">
					<div className="container">
						<Row>
							<Col xs={12} sm={4}>
								<h3 className="text-center">{this.translation.t('pages.landing.iron_man.reserve_today')}</h3>
								<img src={`${IMG_S3_PATH}landings/iron-man/logo-ironman-2.png`} alt="Iron Man 70.3 Costa Rica"/>
								<Button className="btn secondary">{this.translation.t('pages.landing.iron_man.make_reservation_btn')}</Button>
							</Col>
							<Col xs={12} smOffset={3} sm={5}>
								<WidgetReserveForm landing={seo.title} />
							</Col>
						</Row>
					</div>
				</section>
				<section>
					<div className="container">
						<Row>
							<Col xs={12}>
								<div className="react-bs-table fix-table 2">
									<BootstrapTable data={tableData} hover>
										{this.getTableHeader()}
									</BootstrapTable>
								</div>
							</Col>
						</Row>
					</div>
				</section>				
		</Layout>
		);
	}
}));
