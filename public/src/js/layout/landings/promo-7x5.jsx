import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col, Button} from 'react-bootstrap';

import Layout from '../shared/internal-layout/landing';
import WidgetReserveForm from '../shared/widget-reserve-form';

import {scroller} from 'react-scroll';

export default observer(['store'], React.createClass({
	displayName: '7x5',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	bookCar() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.promo_7x5.seo');
		const title = this.translation.t('pages.landing.promo_7x5.title');
		const subTitle = this.translation.t('pages.landing.promo_7x5.sub_title');
		return (
			<Layout seo={seo} className="promo-7x5">
				<section className="main-section">
					<div className="container">
						<Row>
							<Col xs={12} sm={5}>
								<div className="box-title">
									<p className="title">{title}</p>
									<p className="sub-title">{subTitle}</p>
								</div>
							</Col>
							<Col xs={12} smOffset={2} sm={5}>
								<WidgetReserveForm landing={seo.title} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
