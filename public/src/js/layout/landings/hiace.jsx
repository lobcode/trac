import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col, Button} from 'react-bootstrap';

import Layout from '../shared/internal-layout/landing';
import WidgetReserveForm from '../shared/widget-reserve-form/hiaci';

import {scroller} from 'react-scroll';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Hiace',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	bookCar() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.hiaci.seo');
		const title = this.translation.t('pages.landing.hiaci.title');
		const subTitle = this.translation.t('pages.landing.hiaci.sub_title');
		const extraTitle = this.translation.t('pages.landing.hiaci.extra_title');
		const restrictionApply = this.translation.t('pages.landing.hiaci.restriction_apply');
    return (
			<Layout seo={seo} className="promo-hiaci">
				<section className="main-section">
					<div className="container">
						<Row>
							<Col xs={12} sm={5}>
								<div className="box-title">
									<p className="title">{title}</p>
									<p className="sub-title">{subTitle}</p>
									<p className="restriction_apply mb-none">{extraTitle}</p>
                  <p className="restriction_apply">{restrictionApply}</p>
								</div>
							</Col>
							<Col xs={12} smOffset={2} sm={5}>
								<WidgetReserveForm landing={seo.title} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
