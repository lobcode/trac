import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col, Button} from 'react-bootstrap';

import Layout from '../../shared/internal-layout/landing';
import WidgetReserveForm from '../../shared/widget-reserve-form/yaris';

import {scroller} from 'react-scroll';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Yaris',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	bookCar() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.blackYaris.seo');
		const title = this.translation.t('pages.landing.blackYaris.title');
		const img = this.translation.t('pages.landing.blackYaris.img');
    return (
			<Layout seo={seo} className="promo-blackYaris">
				<section className="main-section">
					<div className="container">
						<Row>
							<Col xs={12} sm={5}>
								<div className="box-title">
									<img className="center-block" src={`${IMG_S3_PATH}landings/yaris-black/${img}`} alt="Yaris Black"/>
								</div>
							</Col>
							<Col xs={12} smOffset={2} sm={5}>
								<WidgetReserveForm landing={seo.title} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
