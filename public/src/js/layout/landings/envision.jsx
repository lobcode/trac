import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Row, Col, Button} from 'react-bootstrap';
import classNames from 'classnames';
import {scroller} from 'react-scroll';
import WidgetReserveForm from '../shared/widget-reserve-form';

import Layout from '../shared/internal-layout/landing';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Envision',
	getInitialState() {
		return {
			isFormVisible: false
		};
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.reserve = this.props.store.reserve;

		this.reserve.isTypeCarReserve = true;
	},
	bookCar() {
		this.setState({isFormVisible: true})
	},
	getTableHeader() {
		const headers = this.translation.t('pages.landing.envision.table.header');
		return _.map(headers, (header, index) => {
			return (
				<TableHeaderColumn key={index} dataField={index} isKey={index === 'col_1'}>{header}</TableHeaderColumn>
			);
		});
	},
	render() {
		const seo = this.translation.t('pages.landing.envision.seo');
		const tableData = this.translation.t('pages.landing.envision.table.data');
		const tableFooter = this.translation.t('pages.landing.envision.table.footer');
		const content = this.translation.t('pages.landing.envision.content');
		const form = this.translation.t('pages.landing.envision.form');

		return (
			<Layout seo={seo} className="envision">
				<section className="main-section pR" >
					<div className="space-envision">
						<div className={classNames({'hidden ': this.state.isFormVisible})}>
							<h4 className="title text-center titleEnvisionDesktop">{this.translation.t('pages.landing.envision.reserve_today')}</h4>
							<Button onClick={this.bookCar} className="btn">{this.translation.t('pages.landing.envision.make_reservation_btn')}</Button>
							<h4 className="title text-center titleEnvisionMovil">{this.translation.t('pages.landing.envision.reserve_today')}</h4>							
						</div>
					</div>	
					<div className="container">
						<Row>
							<Col sm={3}></Col>
							<Col xs={12} sm={6} className={classNames({'hidden ': this.state.isFormVisible})}>
							</Col>
							<Col xs={12} sm={6} className={classNames({'show': this.state.isFormVisible, 'hidden': !this.state.isFormVisible})}>
								<WidgetReserveForm landing={seo.title} type={form} />
							</Col>
							<Col xs={12} className={classNames({'show': this.state.isFormVisible, 'hidden': !this.state.isFormVisible})}>
								<div className="legend label-warning" dangerouslySetInnerHTML={{__html: form.legend}} />
							</Col>
							<Col sm={3}></Col>
						</Row>
					</div>
				</section>
				<section>
					<div className="container">
						<Row>
							<Col xs={12}>
								<div className="react-bs-table fix-table 2">
									<BootstrapTable data={tableData} hover>
										{this.getTableHeader()}
									</BootstrapTable>
								</div>
								<p className="table-footer text-center">{tableFooter}</p>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<div dangerouslySetInnerHTML={{__html: content}} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
