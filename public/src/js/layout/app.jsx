import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import classNames from 'classnames';

import {scroller} from 'react-scroll';

import OnScroll from '../shared/scroll-detect';

//import DevTools from 'mobx-react-devtools';

import Header from './header';
import Footer from './footer';

export default observer(['store'], React.createClass({
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.translation.init();
		this.book = this.props.store.book;
		this.tracking = this.props.store.tracking;
		this.tracking.init();
		this.book.isCustomer();

		//this.props.store.book.getAvailableCars();

		//Load Local Data
		this.props.store.extraOptionList.loadOptions();
		this.props.store.groupsCarList.loadCars();
		this.props.store.stations.loadStations();
		this.props.store.google.getSiteKey();
	},
	componentDidMount() {
		OnScroll('.widget-book', '.btn-book', 'hidden');
		OnScroll('.main-header', '.btn-book', 'fixed', false);
		// const _this = this;
		// window.onbeforeunload = function (e) {
		// 	if (_this.book.bookingActive) {
		// 		return "Are you sure you want leave?";
		// 	} else {
		//
		// 	}
		//s
		// 	return;
		// };
	},

	onUnload(event) {

	},
	routerWillLeave(nextLocation) {
		// return false to prevent a transition w/o prompting the user,
		// or return a string to allow the user to decide:
		// if (!this.state.isSaved)
		// 	return 'Your work is not saved! Are you sure you want to leave?'
		console.log(nextLocation);

	},

	toggleBook() {
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	onSetOpen(open) {
		this.book.bookingSidebarOpen = open;
	},
	render() {
		const pageSection = location.pathname === '/' || location.pathname === '/es' ? 'page-home' : 'page-internal';
		const hideBookCarBtn = _.startsWith(this.props.location.pathname, '/step');
		const Loading = this.book.showLoading ? (
			<div className="loader"></div>
		) : null;
		return (
			<div className={pageSection}>
				{Loading}
				<Header/>
				<div className="track-container" id="main-wrap">
					{this.props.children}
				</div>
				<button className={classNames('btn btn-book', {'hide-btn': hideBookCarBtn})} onClick={this.toggleBook}>{this.translation.t('pages.booking.book_car')}</button>
				<Footer/>
			</div>
		);
	}
}));
