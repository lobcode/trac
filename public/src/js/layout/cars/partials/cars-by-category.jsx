import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import {Row, Col, Button, Thumbnail} from 'react-bootstrap';
import LazyLoad from 'react-lazy-load';
import _ from 'lodash';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Cars by Category',
	propTypes: {
		category: React.PropTypes.object,
		bookCar: React.PropTypes.func.isRequired
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.groupsCarList = this.props.store.groupsCarList;
	},
	getCarImage(category, type) {
		const car = _.find(this.groupsCarList.cars, {category, type});
		const image = _.get(car, 'image', '');
		const isEco = _.get(car, 'eco', false);
		return (
			<figure className={classNames({'eco': isEco})}>
				<LazyLoad>
					<img src={IMG_S3_PATH + image} alt={category} className="img-responsive" />
				</LazyLoad>
			</figure>
		);
	},
	convertToSlug(text){
		const slug =
			text 
			.toLowerCase()
			.replace(/ /g,'-')
			.replace(/[^\w-]+/g,'');
		return 'cars_book_car_btn_' + slug; 
	},	
	jsonLD(obj){
		return(
			<script 
			  type="application/ld+json"
    		dangerouslySetInnerHTML={{ __html: JSON.stringify(obj) }}
  		/>
		);
	},
	getCarList(category, type, catDes) {
		const cars = _.filter(this.groupsCarList.cars, {category, type, 'active': true});
		return _.map(cars, ({image, name, group, code}, index) => {

			let obj = {
					"@context": "http://schema.org",
					"@type": "Person",
					"name" : "Toyota Rent",
					"makesOffer" : {
							"@type" :"Offer",
							"itemOffered" : {
									"@type" : "Car",
									"name" : name,
									"description" : catDes,
							"image" : IMG_S3_PATH + image,
							"color" : "Diferent Colors"
						}
					}
			};
			const insertJson = this.jsonLD(obj);

			return (
			<Col xs={12} sm={6} md={4} key={index}>
				{insertJson}
				<LazyLoad>
					<Thumbnail src={IMG_S3_PATH + image}>
						<div className="car-header">
							<p className="title">{name}</p>
							<p className="sub-title">{group}</p>
						</div>
						<Button id={this.convertToSlug(name)} className="btn" onClick={() => this.props.bookCar(code)}>{this.translation.t('cta.book_car')}</Button>
					</Thumbnail>
				</LazyLoad>
			</Col>
			);
		});
	},
	getCategoryTypes() {
		return _.map(this.props.category, (categoryType, index) => {
			const {category} = this.props.category;
		
			if (_.isObject(categoryType)) {
				const categoryCarImage = this.getCarImage(category, categoryType.type);
				const categoryCarList = this.getCarList(category, categoryType.type, categoryType.description);
				return (
					<Row key={index} className="item-type">
						<Col sm={4}>
							{categoryCarImage}
						</Col>
						<Col sm={8}>
							<h3>{categoryType.title}</h3>
							<p>{categoryType.description}</p>
							<div>{categoryCarList}</div>
						</Col>
					</Row>
				);
			}
		});
	},

	render() {
		const categoryType = this.getCategoryTypes();
		return (
			<div className="list-type">
				{categoryType}
			</div>
		);
	}
}));
