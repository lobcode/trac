import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import {Row, Col} from 'react-bootstrap';

import CarsByCategory from './partials/cars-by-category';
import Layout from '../shared/internal-layout';
import MenuInternal from '../shared/menu-internal';

import {Element, scroller} from 'react-scroll';

import OnScroll from '../../shared/scroll-detect';

export default observer(['store'], React.createClass({
	displayName: 'Cars',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.groupsCarList = this.props.store.groupsCarList;
		this.book = this.props.store.book;
	}, 
	componentDidMount() {
		OnScroll('.main-header', '.menu-internal', 'fixed', false);
	},
	bookCar(carCode) {
		this.book.booking.code = carCode;
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
	},
	getSubMenu() {
		const subMenu = this.translation.t('menu.cars.subMenu');
		return _.reduce(subMenu, (items, item) => {
			items.push({
				name: item,
				link: _.kebabCase(item)
			});
			return items;
		}, []);
	},
	getCategory() {
		const categories = this.translation.t('pages.cars.categories');
		return _.map(categories, (category, index) => {
			return (
				<Element className="car-category" key={index} id={category.category} name={category.category}>
					<div className="container">
						<h2>{category.title}</h2>
						<CarsByCategory
							category={category}
							bookCar={this.bookCar}
						/>
					</div>
					<hr />
				</Element>
			);
		});
	},
	render() {
		const pageTitle = this.translation.t('menu.cars.title');
		const seo = this.translation.t('pages.cars.seo');
		const subMenuItems = this.getSubMenu();
		const categoryList = this.getCategory();
   const content = !_.isEmpty(this.translation.t('pages.cars.content')) ? (
				<div>
					<div className="container">
						<Row>
							<Col xs={12} className="page-main-content">
								<div dangerouslySetInnerHTML={{__html: this.translation.t('pages.cars.content')}} />
							</Col>
						</Row>
					</div>
					<hr/>
				</div>
   ) : null;

		return (
			<Layout pageTitle={pageTitle} seo={seo}>
				<MenuInternal
					items={subMenuItems}
				/>
			<section className="main-section cars">
				{categoryList}
			</section>
				{content}
		</Layout>
		);
	}
}));
