import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import $ from 'jquery';
import {parseString} from 'xml2js';
import Layout from '../shared/book-layout';
import {Row, Col, Button, Modal, Alert} from 'react-bootstrap';
import {DropdownList} from 'react-widgets';
import classNames from 'classnames';

import Notify from '../../shared/notify';

import {browserHistory, Link} from 'react-router';

export default observer(['store'], React.createClass({
	displayName: 'Step 4',
	getInitialState() {
		return {
			isTermsModalOpen: false,
			isAlertOpen: false,
			alertType: '',
			alertText: ''
		}
	},
	componentWillMount() {

		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
		this.google = this.props.store.google;
		this.tracking = this.props.store.tracking;
		this.book.booking.paymentMethod = 'later';

		this.book.booking.step = 4;
		
		this._expMonth = '';
		this._expYear = '';
		const _this = this;
		//Save user's IP Address
		$.get("http://ipinfo.io", function(response) {
    _this.book.booking.paymentInfo.ipAddress = response.ip;
		}, "jsonp");

	},
	addAdminCharges() {
		const car = _.find(this.book.cars, {'code': this.book.booking.code});
		const optionList = _.chain(car.optionList)
			.reduce((result, option) => {
				if (option.code === 'H002' || option.code === 'H004') {
					result.push(Number(option.rate) / 100);
				}
				return result;
			}, [])
			.sum()
			.value();

		this.book.booking.totalrate = this.book.booking.totalrate + optionList;

	},
	paymentMethod(method) {
		this.book.booking.paymentMethod = method;
	},
	showTermsConditions() {
		this.setState({
			isTermsModalOpen: true
		});
	},
	closeTermsConditions() {
		this.setState({
			isTermsModalOpen: false
		});
	},
	onReCaptchaChange(value) {
		this.google.recaptcha = value;
		this.google.verify();
	},
	sendEmail(byPay) {
		const notification = this.translation.t('pages.booking.form.notification');
		const {success, warning, error} = notification;

		if (byPay) {
    this.book.sendEmailPay()
      .then(({data}) => {
        if (data.emailConfirmation) {
          Notify(success.title, success.text, success.type);
          // this.cleanForm();
        } else {
          Notify(warning.title, warning.text, warning.type);
        }

      })
      .catch((err) => {
				console.log(err);
        //Notify(error.title, error.text, error.type);
      });
		} else {
    this.book.sendEmail()
      .then(({data}) => {
        if (data.emailConfirmation) {
          Notify(success.title, success.text, success.type);
          // this.cleanForm();
        } else {
          Notify(warning.title, warning.text, warning.type);
        }

      })
			.catch((err) => {
				console.log(err);
        //Notify(error.title, error.text, error.type);
      });
		}
		dataLayer.push({
			'event': 'formSubmitted',
			'formName': 'Toyota Retals - Book'
		});
	},
	cleanForm() {
		// this.cleanForm();
	},
	reservation() {
		const notification = this.translation.t('customer_form.notification');
		const errorDescription = this.translation.t('pages.booking.error');
		const {success, warning, error} = notification;
		let _this = this;

		this.book.newReservation()
			.then(({data}) => {
				this.book.showLoading = false;
					parseString(data, function (err, {response}) {
						if (response.reservation[0].$.status === 'OK') {
							_this.book.booking.irn = response.reservation[0].$.irn;
							_this.book.booking.refno = response.reservation[0].$.refno;
							_this.book.booking.id =  _this.book.booking.refno;

							//Check UTM
							// if (!_.isEmpty(_this.tracking.utm)) {
							_.assign(_this.book.booking.utm, _this.tracking.utm);
							// }
							
							//Save Reservation
							_this.book.saveReservation();
							//send mail
							_this.sendEmail();

							if (_.isEqual(_this.book.booking.paymentMethod, 'later')) {
         browserHistory.push('/thank-you-reserve');
       }
						} else {
							Notify(error.title, _.get(errorDescription, response.reservation[0].$.status), 'error');
						}
					});
			})
			.catch((err) => {
				console.log(err);
				Notify(error.title, error.text, error.type);
			});
	},
	payReservation() {
		this.book.payReservation().then(({data}) => {

			const iframe = this.refs.pay3D.contentWindow.document;
			iframe.open();
			iframe.write(data);
			iframe.close();
		})
		.catch((err) => {
			console.log(err);
		});
	},
	setExpDate(value) {
		const valueNumber = Number(value);

		if (valueNumber > 12) {
			this._expYear = value;
		} else {
			this._expMonth = value;
		}

		this.book.booking.paymentInfo.ccexp = `${this._expMonth}${this._expYear}`;
	},
	handleAlertDismiss() {
		this.setState({
			isAlertOpen: false,
			alertType: '',
			alertText: ''
		});
	},
	getTransactionStatus({data}) {
		let _this = this;
		if (_.isObject(data) && !_.isEmpty(data)) {
			this.book.showLoading = false;

			this.setState({
				isAlertOpen: true,
				alertType: !_.isEqual(data.response, '1') ? 'danger' : 'success',
				alertText: data.responsetext
			});

			if (_.isEqual(data.response, '1')) {
				this.book.booking.paymentInfo.transactionId = data.transactionid;
				this.addAdminCharges();
     //send mail
     _this.sendEmail(true);
				browserHistory.push('/thank-you-payment');
			}
		} else {
			this.book.showLoading = false;
			this.setState({
				isAlertOpen: true,
				alertType: 'danger',
				alertText: this.translation.t('pages.booking.error.ERR/1000')
			});
		}
	},
	render() {
		const form = this.translation.t('pages.booking.form');
		const payment = this.translation.t('pages.booking');
		const MockCountry = [{value: '-1', text: `${form.country}`}, {value:'AF',text:'Afghanistan'},{value:'AX',text:'Åland Islands'},{value:'AL',text:'Albania'},{value:'DZ',text:'Algeria'},{value:'AS',text:'American Samoa'},{value:'AD',text:'Andorra'},{value:'AO',text:'Angola'},{value:'AI',text:'Anguilla'},{value:'AQ',text:'Antarctica'},{value:'AG',text:'Antigua and Barbuda'},{value:'AR',text:'Argentina'},{value:'AM',text:'Armenia'},{value:'AW',text:'Aruba'},{value:'AU',text:'Australia'},{value:'AT',text:'Austria'},{value:'AZ',text:'Azerbaijan'},{value:'BS',text:'Bahamas'},{value:'BH',text:'Bahrain'},{value:'BD',text:'Bangladesh'},{value:'BB',text:'Barbados'},{value:'BY',text:'Belarus'},{value:'BE',text:'Belgium'},{value:'BZ',text:'Belize'},{value:'BJ',text:'Benin'},{value:'BM',text:'Bermuda'},{value:'BT',text:'Bhutan'},{value:'BO',text:'Bolivia, Plurinational State of'},{value:'BQ',text:'Bonaire, Sint Eustatius and Saba'},{value:'BA',text:'Bosnia and Herzegovina'},{value:'BW',text:'Botswana'},{value:'BV',text:'Bouvet Island'},{value:'BR',text:'Brazil'},{value:'IO',text:'British Indian Ocean Territory'},{value:'BN',text:'Brunei Darussalam'},{value:'BG',text:'Bulgaria'},{value:'BF',text:'Burkina Faso'},{value:'BI',text:'Burundi'},{value:'KH',text:'Cambodia'},{value:'CM',text:'Cameroon'},{value:'CA',text:'Canada'},{value:'CV',text:'Cape Verde'},{value:'KY',text:'Cayman Islands'},{value:'CF',text:'Central African Republic'},{value:'TD',text:'Chad'},{value:'CL',text:'Chile'},{value:'CN',text:'China'},{value:'CX',text:'Christmas Island'},{value:'CC',text:'Cocos (Keeling) Islands'},{value:'CO',text:'Colombia'},{value:'KM',text:'Comoros'},{value:'CG',text:'Congo'},{value:'CD',text:'Congo, the Democratic Republic of the'},{value:'CK',text:'Cook Islands'},{value:'CR',text:'Costa Rica'},{value:'CI',text:'Côte dIvoire'},{value:'HR',text:'Croatia'},{value:'CU',text:'Cuba'},{value:'CW',text:'Curaçao'},{value:'CY',text:'Cyprus'},{value:'CZ',text:'Czech Republic'},{value:'DK',text:'Denmark'},{value:'DJ',text:'Djibouti'},{value:'DM',text:'Dominica'},{value:'DO',text:'Dominican Republic'},{value:'EC',text:'Ecuador'},{value:'EG',text:'Egypt'},{value:'SV',text:'El Salvador'},{value:'GQ',text:'Equatorial Guinea'},{value:'ER',text:'Eritrea'},{value:'EE',text:'Estonia'},{value:'ET',text:'Ethiopia'},{value:'FK',text:'Falkland Islands (Malvinas)'},{value:'FO',text:'Faroe Islands'},{value:'FJ',text:'Fiji'},{value:'FI',text:'Finland'},{value:'FR',text:'France'},{value:'GF',text:'French Guiana'},{value:'PF',text:'French Polynesia'},{value:'TF',text:'French Southern Territories'},{value:'GA',text:'Gabon'},{value:'GM',text:'Gambia'},{value:'GE',text:'Georgia'},{value:'DE',text:'Germany'},{value:'GH',text:'Ghana'},{value:'GI',text:'Gibraltar'},{value:'GR',text:'Greece'},{value:'GL',text:'Greenland'},{value:'GD',text:'Grenada'},{value:'GP',text:'Guadeloupe'},{value:'GU',text:'Guam'},{value:'GT',text:'Guatemala'},{value:'GG',text:'Guernsey'},{value:'GN',text:'Guinea'},{value:'GW',text:'Guinea-Bissau'},{value:'GY',text:'Guyana'},{value:'HT',text:'Haiti'},{value:'HM',text:'Heard Island and McDonald Islands'},{value:'VA',text:'Holy See (Vatican City State)'},{value:'HN',text:'Honduras'},{value:'HK',text:'Hong Kong'},{value:'HU',text:'Hungary'},{value:'IS',text:'Iceland'},{value:'IN',text:'India'},{value:'ID',text:'Indonesia'},{value:'IR',text:'Iran, Islamic Republic of'},{value:'IQ',text:'Iraq'},{value:'IE',text:'Ireland'},{value:'IM',text:'Isle of Man'},{value:'IL',text:'Israel'},{value:'IT',text:'Italy'},{value:'JM',text:'Jamaica'},{value:'JP',text:'Japan'},{value:'JE',text:'Jersey'},{value:'JO',text:'Jordan'},{value:'KZ',text:'Kazakhstan'},{value:'KE',text:'Kenya'},{value:'KI',text:'Kiribati'},{value:'KP',text:'Korea, Democratic People\'s Republic of'},{value:'KR',text:'Korea, Republic of'},{value:'KW',text:'Kuwait'},{value:'KG',text:'Kyrgyzstan'},{value:'LA',text:'Lao People\'s Democratic Republic'},{value:'LV',text:'Latvia'},{value:'LB',text:'Lebanon'},{value:'LS',text:'Lesotho'},{value:'LR',text:'Liberia'},{value:'LY',text:'Libya'},{value:'LI',text:'Liechtenstein'},{value:'LT',text:'Lithuania'},{value:'LU',text:'Luxembourg'},{value:'MO',text:'Macao'},{value:'MK',text:'Macedonia, the former Yugoslav Republic of'},{value:'MG',text:'Madagascar'},{value:'MW',text:'Malawi'},{value:'MY',text:'Malaysia'},{value:'MV',text:'Maldives'},{value:'ML',text:'Mali'},{value:'MT',text:'Malta'},{value:'MH',text:'Marshall Islands'},{value:'MQ',text:'Martinique'},{value:'MR',text:'Mauritania'},{value:'MU',text:'Mauritius'},{value:'YT',text:'Mayotte'},{value:'MX',text:'Mexico'},{value:'FM',text:'Micronesia, Federated States of'},{value:'MD',text:'Moldova, Republic of'},{value:'MC',text:'Monaco'},{value:'MN',text:'Mongolia'},{value:'ME',text:'Montenegro'},{value:'MS',text:'Montserrat'},{value:'MA',text:'Morocco'},{value:'MZ',text:'Mozambique'},{value:'MM',text:'Myanmar'},{value:'NA',text:'Namibia'},{value:'NR',text:'Nauru'},{value:'NP',text:'Nepal'},{value:'NL',text:'Netherlands'},{value:'NC',text:'New Caledonia'},{value:'NZ',text:'New Zealand'},{value:'NI',text:'Nicaragua'},{value:'NE',text:'Niger'},{value:'NG',text:'Nigeria'},{value:'NU',text:'Niue'},{value:'NF',text:'Norfolk Island'},{value:'MP',text:'Northern Mariana Islands'},{value:'NO',text:'Norway'},{value:'OM',text:'Oman'},{value:'PK',text:'Pakistan'},{value:'PW',text:'Palau'},{value:'PS',text:'Palestinian Territory, Occupied'},{value:'PA',text:'Panama'},{value:'PG',text:'Papua New Guinea'},{value:'PY',text:'Paraguay'},{value:'PE',text:'Peru'},{value:'PH',text:'Philippines'},{value:'PN',text:'Pitcairn'},{value:'PL',text:'Poland'},{value:'PT',text:'Portugal'},{value:'PR',text:'Puerto Rico'},{value:'QA',text:'Qatar'},{value:'RE',text:'Réunion'},{value:'RO',text:'Romania'},{value:'RU',text:'Russian Federation'},{value:'RW',text:'Rwanda'},{value:'BL',text:'Saint Barthélemy'},{value:'SH',text:'Saint Helena, Ascension and Tristan da Cunha'},{value:'KN',text:'Saint Kitts and Nevis'},{value:'LC',text:'Saint Lucia'},{value:'MF',text:'Saint Martin (French part)'},{value:'PM',text:'Saint Pierre and Miquelon'},{value:'VC',text:'Saint Vincent and the Grenadines'},{value:'WS',text:'Samoa'},{value:'SM',text:'San Marino'},{value:'ST',text:'Sao Tome and Principe'},{value:'SA',text:'Saudi Arabia'},{value:'SN',text:'Senegal'},{value:'RS',text:'Serbia'},{value:'SC',text:'Seychelles'},{value:'SL',text:'Sierra Leone'},{value:'SG',text:'Singapore'},{value:'SX',text:'Sint Maarten (Dutch part)'},{value:'SK',text:'Slovakia'},{value:'SI',text:'Slovenia'},{value:'SB',text:'Solomon Islands'},{value:'SO',text:'Somalia'},{value:'ZA',text:'South Africa'},{value:'GS',text:'South Georgia and the South Sandwich Islands'},{value:'SS',text:'South Sudan'},{value:'ES',text:'Spain'},{value:'LK',text:'Sri Lanka'},{value:'SD',text:'Sudan'},{value:'SR',text:'Suriname'},{value:'SJ',text:'Svalbard and Jan Mayen'},{value:'SZ',text:'Swaziland'},{value:'SE',text:'Sweden'},{value:'CH',text:'Switzerland'},{value:'SY',text:'Syrian Arab Republic'},{value:'TW',text:'Taiwan, Province of China'},{value:'TJ',text:'Tajikistan'},{value:'TZ',text:'Tanzania, United Republic of'},{value:'TH',text:'Thailand'},{value:'TL',text:'Timor-Leste'},{value:'TG',text:'Togo'},{value:'TK',text:'Tokelau'},{value:'TO',text:'Tonga'},{value:'TT',text:'Trinidad and Tobago'},{value:'TN',text:'Tunisia'},{value:'TR',text:'Turkey'},{value:'TM',text:'Turkmenistan'},{value:'TC',text:'Turks and Caicos Islands'},{value:'TV',text:'Tuvalu'},{value:'UG',text:'Uganda'},{value:'UA',text:'Ukraine'},{value:'AE',text:'United Arab Emirates'},{value:'GB',text:'United Kingdom'},{value:'US',text:'United States'},{value:'UM',text:'United States Minor Outlying Islands'},{value:'UY',text:'Uruguay'},{value:'UZ', text:'Uzbekistan'},{value:'VU',text:'Vanuatu'},{value:'VE',text:'Venezuela, Bolivarian Republic of'},{value:'VN',text:'Viet Nam'},{value:'VG',text:'Virgin Islands, British'},{value:'VI',text:'Virgin Islands, U.S.'},{value:'WF',text:'Wallis and Futuna'},{value:'EH',text:'Western Sahara'},{value:'YE',text:'Yemen'},{value:'ZM',text:'Zambia'},{value:'ZW',text:'Zimbabwe'}];
		// const isPaymentMethodDefined = _.isEmpty(this.book.booking.paymentMethod);
		const isPaymentMethodDefined = false;

		const isPayNow = _.isEqual(this.book.booking.paymentMethod, 'now') && !_.isEmpty(this.book.booking.irn);
		const months = [
			{value: '01', text: '01'},
			{value: '02', text: '02'},
			{value: '03', text: '03'},
			{value: '04', text: '04'},
			{value: '05', text: '05'},
			{value: '06', text: '06'},
			{value: '07', text: '07'},
			{value: '08', text: '08'},
			{value: '09', text: '09'},
			{value: '10', text: '10'},
			{value: '11', text: '11'},
			{value: '12', text: '12'},
		];
		const year = [
			{value: '16', text: '2016'},
			{value: '17', text: '2017'},
			{value: '18', text: '2018'},
			{value: '19', text: '2019'},
			{value: '20', text: '2020'},
			{value: '21', text: '2021'},
			{value: '22', text: '2022'},
			{value: '23', text: '2023'},
			{value: '24', text: '2024'},
			{value: '25', text: '2025'},
			{value: '26', text: '2026'},
			{value: '25', text: '2027'},
		];

		const modalContent = this.translation.t('pages.online_payment_policies.content');
		const alert = this.state.isAlertOpen ?
			(
				<Alert bsStyle={this.state.alertType} onDismiss={this.handleAlertDismiss}>
					<h4>{this.state.alertText}</h4>
				</Alert>
			) : null;

		//Get URL params from the iframe and sent it to the main page
		if (!_.isUndefined(typeof window.postMessag) && !_.isEmpty(this.props.location.query)) {
			window.parent.postMessage(this.props.location.query,"*");
		}
		window.addEventListener('message', this.getTransactionStatus, false);

		return (
			<Layout
				className="book-steps step-4"
			>	
				<iframe className="hidden" id="FileFrame" src="about:blank" ref="pay3D" />
				<Modal show={this.state.isTermsModalOpen} onHide={this.closeTermsConditions}>
					<Modal.Header closeButton>
						<Modal.Title>{this.translation.t('cta.terms_conditions')}</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div dangerouslySetInnerHTML={{__html: modalContent}} />
					</Modal.Body>
				</Modal>
				<div className="container">
					<div className={classNames('pay-now-save', {'hidden': !isPaymentMethodDefined})}>
						<h3>{this.translation.t('pages.booking.pay_now_save')}</h3>
						<Row>
							<Col xs={12} sm={5}>
								<div className="green-box">
									<div className="legend hidden">
										<span>{this.translation.t('pages.booking.pay')}</span>
										<span className="text-b">50%</span>
										<span>{this.translation.t('pages.booking.and_save')}</span>
										<span className="text-b">$45</span>
									</div>
									<div className="legend">
										<span className="text-b text-pt">{this.translation.t('pages.booking.pay_now')}</span>
									</div>
									<Button id="pay_now_btn" onClick={() => this.paymentMethod('now')}>{this.translation.t('pages.booking.pay_now')}</Button>
								</div>
							</Col>
						
							<Col xs={12} sm={1}>
								<span className="text-c">
									{this.translation.t('pages.booking.or')}
								</span>
							</Col>
							<Col xs={12} sm={6}>
								<div className="green-box">
									<div className="legend">
										<span className="text-l1">{this.translation.t('pages.booking.i_am_not')}</span>
										<span className="text-l2">{this.translation.t('pages.booking.interested')}</span>
										<span className="text-l3">{this.translation.t('pages.booking.in_save_money')}</span>
									</div>
									<Button id="pay_later_btn" onClick={() => this.paymentMethod('later')}>{this.translation.t('pages.booking.pay_later')}</Button>
								</div>
							</Col>
						</Row>
					</div>
					<div className={classNames('form-container', {'hidden': isPaymentMethodDefined})}>
						<Row className={classNames('payment-info',{'show': isPayNow && _.isEmpty(this.book.booking.paymentInfo.transactionId)})}>
							<Col xs={12}>
								{alert}
							</Col>
							<Col xs={12}>
								<h2>{payment.payment_info}</h2>
							</Col>
							<Col xs={12} sm={6}>
								<label>{`${payment.cc_number} *`}</label>
								<input
									type="text"
									className="form-control"
									value={this.book.booking.paymentInfo.ccnumber}
									placeholder={`${payment.cc_number}`}
									onChange={({target}) => this.book.booking.paymentInfo.ccnumber = target.value}
								/>
							</Col>
							<Col xs={12} sm={6}>
								<label>{`${payment.check_name} *`}</label>
								<input
									type="text"
									className="form-control"
									value={this.book.booking.paymentInfo.checkname}
									placeholder={`${payment.check_name}`}
									onChange={({target}) => this.book.booking.paymentInfo.checkname = target.value}
								/>
							</Col>
							<Col xs={12} sm={6}>
								<label>CVV</label>
								<input
									type="text"
									className="form-control"
									value={this.book.booking.paymentInfo.cvv}
									placeholder="CVV"
									onChange={({target}) => this.book.booking.paymentInfo.cvv = target.value}
								/>
							</Col>
							<Col xs={12} sm={6}>
								<label>{payment.cc_exp}</label>
								<div className="exp-date">
									<DropdownList
										valueField="value"
										textField="text"
										className="exp-el"
										data={months}
										onChange={({value}) => this.setExpDate(value)}
									/>
									<span className="exp-el">
										/
									</span>
									<DropdownList
										valueField="value"
										textField="text"
										className="exp-el"
										data={year}
										onChange={({value}) => this.setExpDate(value)}
									/>
								</div>
							</Col>
							<Col xs={12} className="text-center">
								<Button
									className="btn lg payReservation"
									disabled={!this.book.isPaymentInfoValid}
									onClick={this.payReservation}
								>
									{payment.pay_btn}
								</Button>
							</Col>
						</Row>
						<div className={classNames({'hidden': isPayNow && _.isEmpty(this.book.booking.paymentInfo.transactionId)})}>
							<Row>
								<Col xs={12} sm={6}>
									<input
										ref="fullName"
										type="text"
										className="form-control"
										value={this.book.booking.customerName}
										placeholder={`${form.first_name} *`}
										onChange={({target}) => this.book.booking.customerName = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.customerLastName}
										placeholder={`${form.last_name} *`}
										onChange={({target}) => this.book.booking.customerLastName = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.customerPhone}
										placeholder={`${form.phone} *`}
										onChange={({target}) => this.book.booking.customerPhone = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.customerEmail}
										placeholder={`${form.email} *`}
										onChange={({target}) => this.book.booking.customerEmail = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<DropdownList
										data={MockCountry}
										textField="text"
										value={(!_.isEmpty(this.book.booking.country)) ? this.book.booking.country : MockCountry[0]}
										onChange={(value) => this.book.booking.country = value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.city}
										placeholder={`${form.city}`}
										onChange={({target}) => this.book.booking.city = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.zipcode}
										placeholder={`${form.zip}`}
										onChange={({target}) => this.book.booking.zipcode = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.address}
										placeholder={`${form.address}`}
										onChange={({target}) => this.book.booking.address = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.airline}
										placeholder={`${form.airline}`}
										onChange={({target}) => this.book.booking.airline = target.value}
									/>
								</Col>
								<Col xs={12} sm={6}>
									<input
										type="text"
										className="form-control"
										value={this.book.booking.flight}
										placeholder={`${form.flight}`}
										onChange={({target}) => this.book.booking.flight = target.value}
									/>
								</Col>
							</Row>
							<Row>
								<Col xs={12}>
									<Button
										className="btn reservationBoton"
										disabled={!this.book.isValid}
										onClick={this.reservation}
									>
										{this.translation.t('cta.book_now')}
									</Button>
								</Col>
								<Col xs={12} className="text-center">
									<Button
										className="link"
										onClick={this.showTermsConditions}
										bsStyle="link"
									>
										{this.translation.t('cta.terms_conditions')}
									</Button>
								</Col>
							</Row>
						</div>

					</div>
				</div>
			</Layout>
		);
	}
}));
