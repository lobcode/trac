import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import numeral from 'numeral';
import _ from 'lodash';
import LazyLoad from 'react-lazy-load';
import {Button} from 'react-bootstrap';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Car Detail',
	propTypes: {
		carInfo: React.PropTypes.object.isRequired,
		days: React.PropTypes.string,
		bookCar: React.PropTypes.func
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	getCarSpecs() {
		return _.map(this.props.carInfo.specs, (spec, index) => {
			let specNameValue = '';

			switch (index) {
				case 'motor':
					specNameValue = `${spec} ${this.translation.t('specs.motor')}`;
					break;
				case 'transmission':
					specNameValue = this.translation.t(`specs.${spec}`);
					break;
				case 'fuel':
					specNameValue = this.translation.t(`specs.${spec}`);
					break;
				case 'doors':
					specNameValue = `${spec} ${this.translation.t('specs.doors')}`;
					break;
				case 'seats':
					specNameValue = `${spec} ${this.translation.t('specs.seats')}`;
					break;
				case 'large_bag':
					specNameValue = `${spec} ${this.translation.t('specs.large_bag')}`;
				case 'small_bag':
					specNameValue = `${spec} ${this.translation.t('specs.small_bag')}`;
			}
			return (
				<li className={index} key={index}>
					{specNameValue}
				</li>
			);
		});
	},
	convertToSlug(text){
		const slug =
			text
			.toLowerCase()
			.replace(/ /g,'-') 
			.replace(/[^\w-]+/g,'');
		return 'book_car_btn_step2_' + slug; 
	}, 
	render() {
		const carSpecsList = this.getCarSpecs();
		const {name, baserate, code, model, group, image, eco} = this.props.carInfo;
		const rate = Number(baserate);
		const days = Number(this.props.days);
		const subTitle = `${group} ${this.translation.t('pages.booking.or_similar')}`;
		const baseRate = numeral(rate).format('$0,00');
		const dailyRate = `${numeral(rate / days).format('$0,00')}/${this.translation.t('pages.booking.day')}`;
		const carBookInfo = {
			code,
			model,
			baserate
		};
		return (
			<div className={classNames('car-card', {'is-eco': eco})}>
				<div className="car-header">
					<p className="title">{name}</p>
					<p className="sub-title">{subTitle}</p>
				</div>
				<div className={classNames('rate', {'hidden': _.isUndefined(baserate)})}>
					<p className="total-rate">{baseRate}</p>
					<p className="daily-rate">{dailyRate}</p>
				</div>
				<figure>
					<LazyLoad>
						<img src={IMG_S3_PATH + image} alt={group} className="img-responsive car-thumbnail" />
					</LazyLoad>
				</figure>
				<div className="specs here2">
					<ul className="spec-list">
						{carSpecsList}
					</ul>
				</div>
				<Button id={this.convertToSlug(name)} className={classNames('btn small', {'hidden': !this.props.bookCar})} onClick={() => this.props.bookCar(code)}>{this.translation.t('pages.booking.book_car')}</Button>
			</div>
		);
	}
}));
