import React from 'react';
import { observer } from 'mobx-react';
import _ from 'lodash';
import LazyLoad from 'react-lazy-load';
import Layout from '../shared/book-layout';
import { Row, Col, Checkbox, Modal, Button, Label, Popover, OverlayTrigger } from 'react-bootstrap';
import classNames from 'classnames';
import Parser from 'html-react-parser';

import { browserHistory, Link } from 'react-router';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Step 3',
	componentWillMount() {
		//this.props.store.extraOptionList.loadOptions();
		//this.props.store.book.getMockAvailableCars();
		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
		this.book.booking.options = [];
		this.updateOptionSelected('H004');
		this.updateOptionSelected('H002');
		//this.extraOptionList = this.props.store.extraOptionList;
		//this.groupsCarList = this.props.store.groupsCarList;
		this.book.booking.step = 3;
		this.state = {
			modalInsuranceOpen: false
		}

	},
	updateOptionSelected(code) {
		this.book.booking.options = _.xor(this.book.booking.options, [`${code}`]);
		this.updateOptionName();
		this.updateTotalRate();
	},
	updateOptionName() {
		const options = this.translation.t('pages.booking.product_options');
		this.book.booking.optionsName = _.reduce(this.book.booking.options, (result, code) => {
			result.push(options[code]);
			return result
		}, []);
	},
	updateTotalRate() {
		const car = _.find(this.book.cars, { 'code': this.book.booking.code });
		const constOptionsSelected = _.chain(this.book.booking.options)
			.reduce((result, code) => {



				let optionCost = _.chain(car.optionList)
					.find({ code })
					.get('rate')
					.value();

				optionCost = (Number(optionCost));

				if (code === 'H004' && optionCost === 1094) {
					optionCost = optionCost - 99;
				}

				result = result + (optionCost / 100);

				return result;

			}, 0)
			.value();


		this.book.updateTotalRate(constOptionsSelected);

	},
	getBasicInsuranceInfo(description, index, title) {
		const popoverInfo = (
			<Popover id="{`popover-trigger-hover-focus-${index}`} " title={title}>
				<div className="popover-info" dangerouslySetInnerHTML={{ __html: description }} />
			</Popover>
		);

		return (
			<OverlayTrigger trigger={['hover', 'focus']} placement="right" overlay={popoverInfo}>
				<i className="fa fa-info-circle " aria-hidden="true" />
			</OverlayTrigger>

		);
	},
	getOptions() {
		const car = _.find(this.book.cars, { 'code': this.book.booking.code });
		const options = this.translation.t('pages.booking.product_options');
		const optionsInfo = this.translation.t('pages.booking.product_options_info');

		return _.map(options, (option, index) => {

			if (_.find(car.optionList, { code: index })) {
				let addTolist = true;
				// Agregar la opcion de fast lane solo si son  Alajuela y Liberia
				if (index === 'I004') {
					addTolist = false;
					if (this.book.booking.pickupStation === '5' || this.book.booking.pickupStation === '3') addTolist = true;
				} else if (index === 'I006') {
					addTolist = false;
					// Agregar la opcion de Fast Return solo alajuela
					if (this.book.booking.pickupStation === '5') addTolist = true;
				}

				if (addTolist) {
					let isOptionSelected = _.includes(this.book.booking.options, index);
					let decription = optionsInfo[index].description;
					const basicInsuranceInfo = decription ? this.getBasicInsuranceInfo(decription, index, option) : null;
					let isDisabled = false;


					if (index === 'H004') {
						// isOptionSelected = true;
						isDisabled = true;
					}

					return (
						<li className={index} key={index}><Checkbox checked={isOptionSelected} value={index} disabled={isDisabled} onChange={() => this.updateOptionSelected(index)}>{option} {Parser(optionsInfo[index].price)} {basicInsuranceInfo}</Checkbox></li>
					);
				}
			}
		});
	},
	getSelectedCar() {
		const car = _.find(this.book.cars, { 'code': this.book.booking.code });
		const title = car.name;
		const subTitle = `${car.group} ${this.translation.t('pages.booking.or_similar')}`;
		const image = car.image;
		const isEco = car.eco;

		return (
			<div className="car-card">
				<div className="car-header">
					<p className="title">{title}</p>
					<p className="sub-title">{subTitle}</p>
				</div>
				<figure className={classNames({ 'is-eco': isEco })}>
					<LazyLoad>
						<img src={IMG_S3_PATH + image} alt={title} className="img-responsive car-thumbnail" />
					</LazyLoad>
				</figure>
			</div>
		);
	},
	_closeModal() {
		this.setState({
			modalInsuranceOpen: false
		});
	},
	_nextValid() {
		if (_.includes(this.book.booking.options, 'E001')) {
			this._nextStep();
		} else {
			this.setState({
				modalInsuranceOpen: true
			});
		}
	},
	_nextStep() {
		dataLayer.push({
			"event": "click",
			"btnName": "btnStep4"
		});
		browserHistory.push('/step4');
	},
	_nextStepNoInsurance() {
		this.book.booking.noInsurance = true;
		this._nextStep();
	},
	render() {
		const selectedCar = !_.isEmpty(this.book.booking.code) && !_.isEmpty(this.book.cars) ? this.getSelectedCar() : null;
		const optionsList = this.getOptions();

		return (
			<Layout
				className="book-steps step-3"
			>
				<Modal show={this.state.modalInsuranceOpen} onHide={this._closeModal} bsStyle="danger">
					<Modal.Header closeButton>
						<Modal.Title> <Label bsStyle="danger">{this.translation.t('pages.booking.product_options.E001')}</Label></Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{this.translation.t('pages.booking.unselected_insurance')}
					</Modal.Body>
					<Modal.Footer>
						<Button className="btn secondary" onClick={this._nextStepNoInsurance}>{this.translation.t('pages.booking.unselected_insurance_btn')}</Button>
					</Modal.Footer>
				</Modal>
				<div className="container">
					<Row>
						<Col xs={12} sm={5} className="pull-right">
							{selectedCar}
						</Col>
						<Col xs={12} sm={7} className="pull-left">
							<ul className="list-options">
								{optionsList}
							</ul>
						</Col>
					</Row>
					<Row>
						<Col xs={12} className="text-center">
							<Button className="btn lg" id="next_btn_step3" onClick={this._nextValid}>{this.translation.t('widget_book.btn_next')}</Button>
						</Col>
					</Row>
				</div>
			</Layout>
		);
	}
}));
