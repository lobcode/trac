import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import Layout from '../shared/book-layout';
import {Row, Col, Modal, Button, Alert} from 'react-bootstrap';

import Notify from '../../shared/notify';

import {browserHistory} from 'react-router';

import CarDetail from './partials/car-detail';

export default observer(['store'], React.createClass({
	displayName: 'Step 2',
	componentWillMount() {

		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
		this.groupsCarList = this.props.store.groupsCarList;
		this.book.booking.step = 2;

		this.props.store.book.getAvailableCars();
	},
	showError(errorType) {
		Notify(this.translation.t(`pages.booking.error.${errorType}`), 'error');
	},
	handleAlertDismiss() {
		browserHistory.push('/');
	},
	getCarByCode(code) {
		return _.find(this.groupsCarList.cars, {code});
	},
	bookCar(code) {
		const car = _.find(this.book.cars, {code});
		this.book.booking.code = car.code;
		this.book.booking.model = car.model;
		this.book.booking.baserate = car.baserate;
		this.book.booking.totalrate = this.applyCustomsCharges(car.totalrate);
		this.book.bookingActive = true;
		dataLayer.push({
			"event": "click",
			"btnName": "btnStep3"
		});
		browserHistory.push('/step3');
	},
	applyCustomsCharges(rate) {

		if (this.book.booking.pickupStation == 3 || this.book.booking.returnStation == 5 || this.book.booking.pickupStation == 5 || this.book.booking.returnStation == 5) {
			//Apply 10% over total cost on Alajuela and Liberia stations
			return rate = rate * 1.10;
		} else {
			return rate;
		}
	},
	listCars() {
		return _.map(this.book.cars, (car, index) => {
			//<cargroup code="V" name="MINIVAN"/>
			//is not in stock
			if(!_.isUndefined(car.code)) {
				return (
					<Col key={index} xs={12} sm={4} md={3}>
						<CarDetail
							carInfo={car}
							days={this.book.booking.duration}
							bookCar={this.bookCar}
						/>
					</Col>
				);
			}
		});

	},
	render() {
		const listAvailableCars = !_.isEmpty(this.book.cars) ? this.listCars() : null;
		const noAvailableCars = (!_.isEmpty(this.book.booking.id) && _.isEmpty(this.book.cars)) ?
			<Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
				<h4>{this.translation.t('pages.booking.car_no_available_title')}</h4>
				<p>{this.translation.t('pages.booking.car_no_available_description')}</p>
				<p>
					<Button onClick={this.handleAlertDismiss}>{this.translation.t('pages.booking.change_booking_btn')}</Button>
				</p>
			</Alert> : null;

		if(!_.isEmpty(this.book.errorType)) {
			this.showError(this.book.errorType);
		}

		return (
			<Layout
				className="book-steps step-2"
			>
				<div className="container">
					<Row className="list-cars-available">
						{noAvailableCars}
						{listAvailableCars}
					</Row>
				</div>
			</Layout>
		);
	}
}));
