import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import Layout from '../shared/internal-layout';
import {Row, Col, Alert} from 'react-bootstrap';

import numeral from 'numeral';

export default observer(['store'], React.createClass({
	displayName: 'Thank you',
	componentWillMount() {
		this.stations = this.props.store.stations;
		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
		this.google = this.props.store.google;
		this.tracking = this.props.store.tracking;
	},
	componentWillUnmount() {
		this.book.clearReservation();
	},
	componentDidMount() {
		this.tracking.trackLead();
	},
	getStation(code) {
		return _.chain(this.stations.getStationList)
			.find({'code': code.toString()})
			.get('name')
			.value();
	},
	getOptions() {
		const options = this.translation.t('pages.booking.product_options');
		return _.map(this.book.booking.options, (code, index) => {
			return (
				<li key={index}>{_.get(options, code)}</li>
			)
		})
	},
	render() {
		const pageTitle = this.translation.t('pages.booking.thank_you.page_title');
		const listOptions = this.getOptions();
		return (
			<Layout
				className="book-steps thank-you"
				pageTitle={pageTitle}
			>
				<noscript>
					<div style={{display: 'inline'}}>
						<img height="1" width="1" style={{borderStyle: 'none'}} alt="" src="//www.googleadservices.com/pagead/conversion/852681510/?label=JknZCJnQsHEQpsbLlgM&amp;guid=ON&amp;script=0"/>
					</div>
				</noscript>
				<section className="main-section thank-you">
					<div className="container">
						<Row>
							<Col xs={12} className="text-center">
								<Alert bsStyle="warning">
									<strong>{this.translation.t('pages.booking.thank_you.quote_no')}</strong> {this.book.booking.refno} ({this.book.booking.agentCode})
									<p>{this.translation.t('pages.booking.form.titleQuote')}</p>
								</Alert>
							</Col>
							<Col xs={12} sm={6}>
								<h4>{this.translation.t('pages.booking.thank_you.customer_information')}</h4>
								<p><strong>{this.translation.t('pages.booking.form.first_name')}</strong> {this.book.booking.customerName} <strong>{this.translation.t('pages.booking.form.last_name')}</strong> {this.book.booking.customerLastName}</p>
								<p><strong>{this.translation.t('pages.booking.form.phone')}</strong> {this.book.booking.customerPhone}</p>
								<p><strong>{this.translation.t('pages.booking.form.email')}</strong> {this.book.booking.customerEmail}</p>
								<p><strong>{this.translation.t('pages.booking.form.country')}</strong> {this.book.booking.country.text}</p>
								<p><strong>{this.translation.t('pages.booking.form.city')}</strong> {this.book.booking.city}</p>
								<p><strong>{this.translation.t('pages.booking.form.zip')}</strong> {this.book.booking.zipcode}</p>
								<p><strong>{this.translation.t('pages.booking.form.address')}</strong> {this.book.booking.address}</p>
								<p><strong>{this.translation.t('pages.booking.form.airline')}</strong> {this.book.booking.airline}</p>
								<p><strong>{this.translation.t('pages.booking.form.flight')}</strong> {this.book.booking.flight}</p>
							</Col>
							<Col xs={12} sm={6}>
								<h4>{this.translation.t('pages.booking.thank_you.book_details')}</h4>
								<p><strong>{this.translation.t('pages.booking.form.start_date')}</strong> {this.book.booking.dateFrom} / {this.book.booking.timeFrom}</p>
								<p><strong>{this.translation.t('pages.booking.form.pick_up_location')}</strong> {this.getStation(this.book.booking.pickupStation)}</p>
								<p><strong>{this.translation.t('pages.booking.form.end_date')}</strong> {this.book.booking.dateTo} / {this.book.booking.timeTo}</p>
								<p><strong>{this.translation.t('pages.booking.form.drop_off_location')}</strong> {this.getStation(this.book.booking.returnStation)}</p>
								<p><strong>{this.translation.t('pages.booking.form.total_days')}</strong> {this.book.booking.duration}</p>
							</Col>
							<h4>{this.translation.t('pages.booking.thank_you.car_detail')}</h4>
							<Col xs={12} sm={6}>
								<p><strong>{this.translation.t('pages.booking.thank_you.model')}</strong> {this.book.booking.model}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.baseRate')}</strong> ${Math.round(this.book.booking.baserate * 100) / 100}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.totalRate')}</strong> ${Math.round(this.book.booking.totalrate * 100) / 100}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.additionalOptionsList')}</strong></p>
								<ul>
									{listOptions}
								</ul>
							</Col>
							<h4>{this.translation.t('pages.booking.thank_you.payment_info')}</h4>
							<Col xs={12} sm={6}>
								<p><strong>{this.translation.t('pages.booking.thank_you.check_name')}</strong> {this.book.booking.paymentInfo.checkname}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.cc_number')}</strong> {this.book.ccNumber}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.irn')}</strong> {this.book.booking.irn}</p>
								<p><strong>{this.translation.t('pages.booking.thank_you.remark')}</strong> {this.book.remarksMessage}</p>
							</Col>

						</Row>
					</div>
				</section>
			</Layout>
		);
	}
}));
