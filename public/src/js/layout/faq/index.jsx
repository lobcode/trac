import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';

import {Row, Col, Accordion, Panel} from 'react-bootstrap';

import Layout from '../shared/internal-layout';

export default observer(['store'], React.createClass({
	displayName: 'FAQ',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	getFAQList() {
		const faqList = this.translation.t('pages.faq.sections');
		let count = 0;
		return _.map(faqList, (faq) => {
			count++;
			const {content, title} = faq;
			return (
				<Panel header={title} eventKey={count} key={count}>
					<div dangerouslySetInnerHTML={{__html: content}} />
				</Panel>
			);
		})
	},

	render() {
		const pageTitle = this.translation.t('pages.faq.title');
		const content = this.getFAQList();
		const seo = this.translation.t('pages.faq.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
				<section className="faq main-section">
					<div className="container">
						<Row>
							<Col xs={12}>
								<Accordion>
								{content}
								</Accordion>
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
