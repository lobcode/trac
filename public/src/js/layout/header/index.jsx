import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import { browserHistory, Router, Route } from 'react-router';
import {Link} from 'react-router';
import {Navbar, Nav, NavItem, Button, NavDropdown, MenuItem, Toggle, Row, Col} from 'react-bootstrap';
import classNames from 'classnames';
import PhoneIcon from '../../shared/images/phone-icon';
import MenuSide from 'react-burger-menu';

import {LinkContainer, IndexLinkContainer} from 'react-router-bootstrap';

import {getPagePath} from '../../shared/utils';
const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	getInitialState() {
		return {
			isMenuOpen: false
		}
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
		this.auth = this.props.store.auth;

	},
	handleSelect(eventKey) {
		event.preventDefault();
		if (eventKey === '2') {
			this.props.store.translation.setLanguage('es');
		} else if (eventKey === '3') {
			this.props.store.translation.setLanguage('en');
		}
	},
	updateURL(currentLang, nextLang) {
		const path = _.chain(window.location.pathname)
			.split('/')
			.drop(1)
			.value();
		let page = '';

		if (_.size(path) === 1) {
			//Home case
			if (_.isEmpty(path[0])) {
				browserHistory.push('/es');
			} else if (_.isEqual(path[0], 'es')) {
				browserHistory.push('/');
			} else {
			//in this case path = ["pageName"] => lang = en
				page = path[0];
			}
		} else {
			//in this case path = ["es", "pageName"] => lang = es
			page = path[1];
		}

		const keyPath = _.chain(this.translation.config)
			.get(`${currentLang}.pages`)
			.findKey({'permalink': page})
			.value();

		const nextPath = _.chain(this.translation.config)
			.get(`${nextLang}.pages.${keyPath}.permalink`)
			.value();

		if (!_.isUndefined(nextPath)) {
			if (_.isEqual(nextLang, 'es')) {
				browserHistory.push(`/es/${nextPath}`);
			} else {
				browserHistory.push(`/${nextPath}`);
			}
		}
	},
	changeLang(lang) {
		const currentLang = this.props.store.translation.language;

		if(!_.isEqual(lang, currentLang)) {
			this.updateURL(currentLang, lang);
			this.props.store.translation.setLanguage(lang);
		}

	},
	toggleMenu() {
		const toggleMenu = !this.state.isMenuOpen;
		this.setState({
			isMenuOpen: toggleMenu
		});

	},
	handleMenuState({isOpen}) {
		if(!isOpen) {
			this.setState({
				isMenuOpen: false
			});
		}
	},
	getMenuIcon() {
		const iconType = this.state.isMenuOpen ? 'times' : 'bars';

		return (`<i class="fa fa-${iconType}" aria-hidden="true" />`);
	},

	callPhone(id, text) {
		return (
			<p
				style={{ margin: '0'}}
			>
				<a
					onClick={(e) => {
						e.preventDefault();
						window.open(`https://admin.2tollfree.click/customers/callbox/${id}/22`, '2tollfree', 'width=315,height=625,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');
					}}
					className="callPhone"
				>
					<PhoneIcon
						width="15"
						height="15"
						className="icon"
						color='fff'
					/>
					<strong>{text}</strong>
				</a>
			</p>
		);
	},

	render() {
		const currentLang = this.props.store.translation.language;
		const Menu = MenuSide['stack'];
		const menuStyle = {
			bmOverlay: {
				zIndex: 2,
				left: 0,
				top: 0
			}
		};
		return (
			<div>

				<header className="bg-green py-3">
					<div className="container">
						<Row className="d-flex">
							<Col xs={4}>
								<Link to={_.isEqual(currentLang, 'en') ? '/' : '/es'}>
									<img src={`${IMG_S3_PATH}trac-logo-new.png`} alt="Logo Costa Rica"/>
								</Link>
							</Col>
							<ul xs={8} className="d-flex justify-content-between align-items-center col-xs-8 menu-items hidden-xs">

									<li id="menu_item_fleet"  title={this.translation.t('menu.fleet.title')}>
										<Link to={getPagePath('cars')}>
											{this.translation.t('menu.fleet.title')}
										</Link>
									</li>

									<li id="menu_item_locations"  title={this.translation.t('menu.locations.title')}>
										<Link to={getPagePath('locations')}>
											{this.translation.t('menu.locations.title')}
										</Link>
									</li>
									<li id="menu_item_customer_faq"  title={this.translation.t('menu.travel_assistance.title')}>
										<Link to={getPagePath('faq')}>
											{this.translation.t('menu.travel_assistance.title')}
										</Link>
									</li>

									<li id="menu_item_our_cars" title={this.translation.t('menu.cars.titleNew')}>
										<Link to={getPagePath('cars')} >
											{this.translation.t('menu.cars.titleNew')}
										</Link>
									</li>
									<li id="menu_item_about"  href={getPagePath('about')}>
										<Link to={getPagePath('about')} >
											{this.translation.t('menu.about')}
										</Link>
									</li>
									<li id="menu_item_blog" >
										
										<Link to={getPagePath('blog')}>
											{this.translation.t('menu.blog')}
										</Link>
									</li>
									<li>
										<span>
											<button className={classNames({'active': _.isEqual(currentLang, 'es')})} onClick={() => this.changeLang('es')}>ES</button>
											<button className={classNames({'active': _.isEqual(currentLang, 'en')})} onClick={() => this.changeLang('en')}>EN</button>
										</span>
									</li>
							</ul>
							<Col xs={8} className="pull-right visible-xs">
								<Button className="menu-icon pull-right" onClick={this.toggleMenu} dangerouslySetInnerHTML={{__html: this.getMenuIcon()}} />
								<Menu
									pageWrapId={'main-wrap'}
									outerContainerId={'trac'}
									isOpen={this.state.isMenuOpen}
									width={280}
									right
									customBurgerIcon={false}
									customCrossIcon={false}
									styles={menuStyle}
									onStateChange={this.handleMenuState}
								>
									<div>
										<Nav className="main-menu">
											<NavItem eventKey={2} id="menu_item_our_cars" href={getPagePath('cars')} title={this.translation.t('menu.cars.title')}>{this.translation.t('menu.cars.title')}</NavItem>
											<NavItem eventKey={3} id="menu_item_products" href={getPagePath('products_services')} title={this.translation.t('menu.products_services.title')}>{this.translation.t('menu.products_services.title')}</NavItem>
											<NavItem eventKey={4} id="menu_item_locations" href={getPagePath('locations')} title={this.translation.t('menu.locations.title')}>{this.translation.t('menu.locations.title')}</NavItem>
											<NavItem eventKey={5} id="menu_item_offers" href={getPagePath('special_offers')} title={this.translation.t('menu.special_offers')}>{this.translation.t('menu.special_offers')}</NavItem>
											<NavItem eventKey={6} id="menu_item_customer_care" href={getPagePath('customer_care')} title={this.translation.t('menu.customer_care')}>{this.translation.t('menu.customer_care')}</NavItem>

											<NavDropdown eventKey={7} title={this.translation.t('menu.travel_assistance.title')} id="travel-assistance">
												<MenuItem eventKey={7.1} id="menu_item_customer_practical_tips" href={getPagePath('practical_tips')} title={this.translation.t('menu.travel_assistance.subMenu.item1')}>{this.translation.t('menu.travel_assistance.subMenu.item1')}</MenuItem>
												<MenuItem eventKey={7.2} id="menu_item_customer_transit_law" href={getPagePath('transit_law')} title={this.translation.t('menu.travel_assistance.subMenu.item2')}>{this.translation.t('menu.travel_assistance.subMenu.item2')}</MenuItem>
												<MenuItem eventKey={7.3} id="menu_item_customer_faq" href={getPagePath('faq')} title={this.translation.t('menu.travel_assistance.subMenu.item3')}>{this.translation.t('menu.travel_assistance.subMenu.item3')}</MenuItem>
											</NavDropdown>

											<NavItem id="menu_item_about" eventKey={8} href={getPagePath('about')}>{this.translation.t('menu.about')}</NavItem>
											<NavItem id="menu_item_blog" eventKey={9} href={getPagePath('blog')}>{this.translation.t('menu.blog')}</NavItem>
										</Nav>
									</div>

								</Menu>
							</Col>
						</Row>	
					</div>
				</header>


				{/* <div className={classNames('main-header is-visible hidden', {'isMenuOpen': this.state.isMenuOpen})}>
					<div className="container-fluid">
						<Row>
							<Col xs={6}>
								<LinkContainer to={_.isEqual(currentLang, 'en') ? '/' : '/es'}>
									<NavItem eventKey={1} className="logo-trac">
										TOYOTA Rent a Car
									</NavItem>
								</LinkContainer>
								{this.callPhone(64, 'Call free line 1')}
								{this.callPhone(65, 'Call free line 2')}
							</Col>
							<Col xs={6} className="pull-right ">
								<Button className="menu-icon pull-right" onClick={this.toggleMenu} dangerouslySetInnerHTML={{__html: this.getMenuIcon()}} />
								<Menu
									pageWrapId={'main-wrap'}
									outerContainerId={'trac'}
									isOpen={this.state.isMenuOpen}
									width={280}
									right
									customBurgerIcon={false}
									customCrossIcon={false}
									styles={menuStyle}
									onStateChange={this.handleMenuState}
								>
									<div>
										<Nav className="main-menu">
											<NavItem eventKey={2} id="menu_item_our_cars" href={getPagePath('cars')} title={this.translation.t('menu.cars.title')}>{this.translation.t('menu.cars.title')}</NavItem>
											<NavItem eventKey={3} id="menu_item_products" href={getPagePath('products_services')} title={this.translation.t('menu.products_services.title')}>{this.translation.t('menu.products_services.title')}</NavItem>
											<NavItem eventKey={4} id="menu_item_locations" href={getPagePath('locations')} title={this.translation.t('menu.locations.title')}>{this.translation.t('menu.locations.title')}</NavItem>
											<NavItem eventKey={5} id="menu_item_offers" href={getPagePath('special_offers')} title={this.translation.t('menu.special_offers')}>{this.translation.t('menu.special_offers')}</NavItem>
											<NavItem eventKey={6} id="menu_item_customer_care" href={getPagePath('customer_care')} title={this.translation.t('menu.customer_care')}>{this.translation.t('menu.customer_care')}</NavItem>

											<NavDropdown eventKey={7} title={this.translation.t('menu.travel_assistance.title')} id="travel-assistance">
												<MenuItem eventKey={7.1} id="menu_item_customer_practical_tips" href={getPagePath('practical_tips')} title={this.translation.t('menu.travel_assistance.subMenu.item1')}>{this.translation.t('menu.travel_assistance.subMenu.item1')}</MenuItem>
												<MenuItem eventKey={7.2} id="menu_item_customer_transit_law" href={getPagePath('transit_law')} title={this.translation.t('menu.travel_assistance.subMenu.item2')}>{this.translation.t('menu.travel_assistance.subMenu.item2')}</MenuItem>
												<MenuItem eventKey={7.3} id="menu_item_customer_faq" href={getPagePath('faq')} title={this.translation.t('menu.travel_assistance.subMenu.item3')}>{this.translation.t('menu.travel_assistance.subMenu.item3')}</MenuItem>
											</NavDropdown>

											<NavItem id="menu_item_about" eventKey={8} href={getPagePath('about')}>{this.translation.t('menu.about')}</NavItem>
											<NavItem id="menu_item_blog" eventKey={9} href={getPagePath('blog')}>{this.translation.t('menu.blog')}</NavItem>
										</Nav>
									</div>

								</Menu>
							</Col>
						</Row>
						<Menu
							pageWrapId={'main-wrap'}
							outerContainerId={'trac'}
							isOpen={this.state.isMenuOpen}
							width={280}
							right
							customBurgerIcon={false}
							customCrossIcon={false}
							styles={menuStyle}
							onStateChange={this.handleMenuState}
						>
							<div>
								<Nav className="main-menu">
									<NavItem eventKey={2} id="menu_item_our_cars" href={getPagePath('cars')} title={this.translation.t('menu.cars.title')}>{this.translation.t('menu.cars.title')}</NavItem>
									<NavItem eventKey={3} id="menu_item_products" href={getPagePath('products_services')} title={this.translation.t('menu.products_services.title')}>{this.translation.t('menu.products_services.title')}</NavItem>
									<NavItem eventKey={4} id="menu_item_locations" href={getPagePath('locations')} title={this.translation.t('menu.locations.title')}>{this.translation.t('menu.locations.title')}</NavItem>
									<NavItem eventKey={5} id="menu_item_offers" href={getPagePath('special_offers')} title={this.translation.t('menu.special_offers')}>{this.translation.t('menu.special_offers')}</NavItem>
									<NavItem eventKey={6} id="menu_item_customer_care" href={getPagePath('customer_care')} title={this.translation.t('menu.customer_care')}>{this.translation.t('menu.customer_care')}</NavItem>

									<NavDropdown eventKey={7} title={this.translation.t('menu.travel_assistance.title')} id="travel-assistance">
										<MenuItem eventKey={7.1} id="menu_item_customer_practical_tips" href={getPagePath('practical_tips')} title={this.translation.t('menu.travel_assistance.subMenu.item1')}>{this.translation.t('menu.travel_assistance.subMenu.item1')}</MenuItem>
										<MenuItem eventKey={7.2} id="menu_item_customer_transit_law" href={getPagePath('transit_law')} title={this.translation.t('menu.travel_assistance.subMenu.item2')}>{this.translation.t('menu.travel_assistance.subMenu.item2')}</MenuItem>
										<MenuItem eventKey={7.3} id="menu_item_customer_faq" href={getPagePath('faq')} title={this.translation.t('menu.travel_assistance.subMenu.item3')}>{this.translation.t('menu.travel_assistance.subMenu.item3')}</MenuItem>
									</NavDropdown>

									<NavItem id="menu_item_about" eventKey={8} href={getPagePath('about')}>{this.translation.t('menu.about')}</NavItem>
									<NavItem id="menu_item_blog" eventKey={9} href={getPagePath('blog')}>{this.translation.t('menu.blog')}</NavItem>
								</Nav>
							</div>

						</Menu>

						<div className="lang-selector">
							<button className={classNames({'active': _.isEqual(currentLang, 'es')})} onClick={() => this.changeLang('es')}>ES</button> / <button className={classNames({'active': _.isEqual(currentLang, 'en')})} onClick={() => this.changeLang('en')}>EN</button>
						</div>
					</div>
				</div> */}
			</div>
		);
	}
}));
