import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import Layout from '../shared/internal-layout';
import LazyLoad from 'react-lazy-load';
import {Element, scroller} from 'react-scroll';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Special Offers',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	componentDidMount() {
		if (!_.isUndefined(this.props.params.offer)) {
			this.goToOffer();
		}
	},

	goToOffer() {
		_.delay(() => {
			scroller.scrollTo(this.props.params.offer, {
				smooth: true
			});
		}, 200);

	},
	getSpecialOffers() {
		const offers = this.translation.t('pages.special_offers.sections');
		return _.map(offers, (offer, index) => {
			const {title, discount, image, url, content} = offer;
			const offerId = `${_.kebabCase(title)}`;
			const haveDiscount = !_.isEmpty(discount) ? (
				<h4 dangerouslySetInnerHTML={{__html: `${discount}% ${this.translation.t('discount')}`}} />
				) : null;

			return (
				<Element key={index} id={offerId} name={offerId}>
					<div className="container">
						<h2>{title}</h2>
						{haveDiscount}
						<figure>
							<LazyLoad>
								<img src={IMG_S3_PATH + image} alt={title} className="img-responsive" />
							</LazyLoad>
						</figure>
						<div className="description" dangerouslySetInnerHTML={{__html: content}} />
						<div className="contact-offer">
							<p className="url"><a href={url} rel="nofollow" target="_blank">{url}</a></p>
						</div>
					</div>
					<hr />
				</Element>
			);
		});
	},
	render() {
		const pageTitle = this.translation.t('menu.special_offers');
		const seo = this.translation.t('pages.special_offers.seo');
		const specialOffersList = this.getSpecialOffers();
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
			<section className="special-offers main-section">
				{specialOffersList}
			</section>
		</Layout>
		);
	}
}));
