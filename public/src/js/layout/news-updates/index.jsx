import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col} from 'react-bootstrap';

import Layout from '../shared/internal-layout';

export default observer(['store'], React.createClass({
	displayName: 'News & Updates',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},

	render() {
		const pageTitle = this.translation.t('pages.news_updates.title');
		const content = this.translation.t('pages.news_updates.content');
		const seo = this.translation.t('pages.news_updates.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
				<section className="main-section about">
					<div className="container">
						<Row>
							<Col xs={12}>
								<div className="fb-page" data-href="https://www.facebook.com/toyotarentacarcostarica" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true" data-width="500">
									<blockquote cite="https://www.facebook.com/toyotarentacarcostarica" className="fb-xfbml-parse-ignore">
										<a href="https://www.facebook.com/toyotarentacarcostarica">Toyota Rent a Car Costa Rica</a>
									</blockquote>
								</div>
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
