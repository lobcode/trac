import React from 'react';
import {observer} from 'mobx-react';

import {Row, Col} from 'react-bootstrap';

import Layout from '../shared/internal-layout';

export default observer(['store'], React.createClass({
	displayName: 'Practical Tips',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},

	render() {
		const pageTitle = this.translation.t('pages.practical_tips.title');
		const content = this.translation.t('pages.practical_tips.content');
		const seo = this.translation.t('pages.practical_tips.seo');

		return (
			<Layout pageTitle={pageTitle} seo={seo}>
				<section className="main-section practical-tips">
					<div className="container">
						<Row>
							<Col xs={12}>
								<div dangerouslySetInnerHTML={{__html: content}} />
							</Col>
						</Row>
					</div>
				</section>
		</Layout>
		);
	}
}));
