import React from 'react';
import {observer} from 'mobx-react';
import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router';
import _ from 'lodash';

export default observer(['store'], React.createClass({
	displayName: 'Sub Section',
	propTypes: {
		subSections: React.PropTypes.object
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	getSubSection() {
		return _.map(this.props.subSections, (subSection, index) => {
			const {title, description, classname} = subSection;

			return (
				<Col xs={12} md={6} key={index} id={_.kebabCase(title)}>
					<h3 className={`sub-title ${classname}`}>{title}</h3>
					<div className="info">
						<div dangerouslySetInnerHTML={{__html: description}} />
					</div>
				</Col>
			);
		});
	},
	render() {

		const subSections = this.getSubSection();
		return (
			<div className="list-type">
				<div className="container">
					<Row>
						{subSections}
					</Row>
				</div>
				<hr/>
			</div>
		);
	}
}));
