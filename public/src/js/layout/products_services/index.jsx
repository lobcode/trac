import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';

import {Link, Element, Events, scrollSpy, animateScroll as scroll} from 'react-scroll';

import SubSection from './partials/sub-section';
import Layout from '../shared/internal-layout';
import MenuInternal from '../shared/menu-internal';

export default observer(['store'], React.createClass({
	displayName: 'Products and Services',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;

	},
	componentDidMount() {
		Events.scrollEvent.register('begin', function(to, element) {
			console.log("begin", arguments);
		});
		scrollSpy.update();
	},
	getSubMenu() {
		const subMenu = this.translation.t('pages.products_services.subMenu');
		return _.reduce(subMenu, (items, item) => {
			items.push({
				name: item,
				link: _.kebabCase(item)
			});
			return items;
		}, []);
	},
	scrollToBottom: function() {
		scroll.scrollToBottom();
	},
	getSection() {
		const categories = this.translation.t('pages.products_services');
		return _.map(categories, (category, index) => {

			if(_.isObject(category)) {
				return (
					<div key={index} id={_.lowerCase(category.title)}>
						<header className="container">
							<h2>{category.title}</h2>
						</header>
						<SubSection
							subSections={category.sections}
						/>
					</div>
				);
			}
		});
	},
	render() {
		const pageTitle = this.translation.t('menu.products_services.title');
		const subMenuItems = this.getSubMenu();
		const sections = this.getSection();
		const seo = this.translation.t('pages.products_services.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
			<section className="main-section products-services">
				{sections}
			</section>
		</Layout>
		);
	}
}));
