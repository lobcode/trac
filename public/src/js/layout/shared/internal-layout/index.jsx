import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import _ from 'lodash';
import {Row, Col} from 'react-bootstrap';
import {animateScroll} from 'react-scroll';

import Helmet from '../helmet';
import WidgetBook from '../widget-book';


export default observer(['store'], React.createClass({
	displayName: 'Internal',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.metatags = this.props.store.metaTags;
		this.header = this.props.store.header;

		animateScroll.scrollToTop({
			duration: 0
		});
	},
	componentDidMount() {
		// const scroll = animateScroll;
		// scroll.scrollToTop();
	},
	render() {
		const pageTitle = !_.isEmpty(this.props.pageTitle) ? <h1>{this.props.pageTitle}</h1> : null;
		const pageSubTitle = !_.isEmpty(this.props.pageSubTitle) ? <h4>{this.props.pageSubTitle}</h4> : null;
		const helmet = !_.isEmpty(this.props.seo) ? (
				<Helmet
					title={this.props.seo.title}
					description={this.props.seo.description}
				/>
			) : null;

		return (
			<div className={classNames('internal', this.props.className)}>
				{helmet}
				<header className="section-header">
					<div className="container">
						<Row>
							<Col xs={12} className="section-title">
								{pageTitle}
								{pageSubTitle}
							</Col>
						</Row>
					</div>
				</header>
				<div className="internal-banner">
					<WidgetBook />
				</div>
				<div className="main-content">
					{this.props.children}
				</div>
				<WidgetBook />
			</div>
		);
	}
}));
