import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import _ from 'lodash';
import {Row, Col} from 'react-bootstrap';
import {animateScroll} from 'react-scroll';

import Helmet from '../helmet';
import WidgetBook from '../widget-book';


export default observer(['store'], React.createClass({
	displayName: 'Landing',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.metatags = this.props.store.metaTags;
		this.header = this.props.store.header;

		animateScroll.scrollToTop({
			duration: 0
		});
	},
	componentDidMount() {
		// const scroll = animateScroll;
		// scroll.scrollToTop();
	},
	render() {
		const helmet = !_.isEmpty(this.props.seo) ? (
				<Helmet
					title={this.props.seo.title}
					description={this.props.seo.description}
				/>
			) : null;

		return (
			<div className={classNames('internal landing', this.props.className)}>
				{helmet}
				<div className="main-content">
					{this.props.children}
				</div>
			</div>
		);
	}
}));
