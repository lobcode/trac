import React from 'react';
import _ from 'lodash';
import { observer } from 'mobx-react';
import classNames from 'classnames';
import { Row, Col, Button } from 'react-bootstrap';
import Slider from 'react-slick';
import { Link } from 'react-router';
import YouTube from 'react-youtube';

import Helmet from '../helmet';
import WidgetBook from '../widget-book';
import HotDeals from '../hot-deals';
import TopDestinations from '../top-destinations';
import YoutubeVideo from '../youtube-video';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
  displayName: 'Home',
  getInitialState() {
    return {
      sidebarOpen: false
    };
  },
  componentWillMount() {
    this.translation = this.props.store.translation;
    this.metatags = this.props.store.metaTags;
  },

  getSlides() {
    const slides = this.translation.t('promociones');
    return _.map(slides, (slide, index) => {
      const img = `${IMG_S3_PATH}promociones/${slide.img}.jpg`;

      if (slide.type === 'clean') {
        return (
          <div key={index}>
            <img src={img} className="img-responsive w-100" />
          </div>
        );
      } else {
        return (
          <div key={index}>
            <Link id={slide.btn_id} to={slide.link}>
              <img src={img} className="img-responsive w-100" />
            </Link>
          </div>
        );
      }

    });
  },

  video() {
    const opts = {
      height: '300',
      width: '100%',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
      <YouTube
        videoId="2g811Eo7K8U"
        opts={opts}
        onReady={this._onReady}
      />
    );
  },

  getPromo() {
    const slides = this.translation.t('promociones');
    return _.map(slides, (slide, index) => {
      const img = `${IMG_S3_PATH}promociones/${slide.img}.jpg`;

      if (slide.type === 'clean') {
        return (
          <Col xs={6} className="p-0" key={index}>
            <img src={img} className="img-responsive w-100" />
          </Col>
        );
      } else {
        return (
          <Col xs={6} className="p-0" key={index}>
            <Link id={slide.btn_id} to={slide.link}>
              <img src={img} className="img-responsive w-100" />
            </Link>
          </Col>
        );
      }

    });
  },

  mainImgs() {
    const slides = this.translation.t('promociones');
    return _.map(slides, (slide, index) => {
      const img1 = `${IMG_S3_PATH}nuevo/home/Yaris.jpg`;
      const img2 = `${IMG_S3_PATH}nuevo/home/Renting.jpg`;

      if (slide.type === 'clean') {
        return (
          <Col className="space-img" xs={12} sm={6} key={index}>
            <img src={img1} className="img-responsive w-100" />
            <Link className="btn full btn-green btn-small-green">{this.translation.t('cta.book_car')}</Link>
          </Col>
        );
      } else {
        return (
          <Col className="space-img" xs={12} sm={6} key={index} >
              <img src={img2} className="img-responsive w-100" />
              <Link className="btn full btn-green btn-small-green" id={slide.btn_id} to={slide.link}>{this.translation.t('cta.book_car')}</Link>
            
          </Col>
        );
      }

    });
  },

  render() {
    const helmet = !_.isEmpty(this.props.seo) ? (
      <Helmet
        title={this.props.seo.title}
        description={this.props.seo.description}
      />
    ) : null;
    const content = !_.isEmpty(this.translation.t('pages.home.content')) ? (
      <div>
        {/* <hr /> */}
        <div className="container">
          <Row>
            <Col xs={12} className="page-main-content">
              <div dangerouslySetInnerHTML={{ __html: this.translation.t('pages.home.content') }} />
            </Col>
          </Row>
        </div>
      </div>
    ) : null;

    const promo = this.getSlides();
    const promoContainer = this.getPromo();
    const mainImgs = this.mainImgs();
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 0,
      arrows: false,
      autoplay: true,
      fade: true,
      draggable: false,
      adaptiveHeight: true,
      className: 'visible-xs',
      lazyLoad: true,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <div className={classNames('home', this.props.className)}>
        {helmet}
        <div className="main-banner">
          <WidgetBook hiddenIn='visible-xs'/>
          {this.props.children}
          <WidgetBook hiddenIn='hidden-xs' />
          <Slider {...settings}>
            {promo}
          </Slider>
          <div className="hidden-xs">
            {promoContainer}
          </div>
        </div>
        <HotDeals />
        <div className="container section-padding">
          <Row>
            {mainImgs}
          </Row>
        </div>

        <YoutubeVideo />

        <TopDestinations />
        {/* {content} */}
      </div>
    );
  }
}));
