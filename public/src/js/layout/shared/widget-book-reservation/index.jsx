import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import {Input, Row, Col} from 'react-bootstrap';
import classNames  from 'classnames';
import numeral from 'numeral';
import moment from 'moment';

import 'moment/locale/es';

export default observer(['store'], React.createClass({
	displayName: 'Widget Book Reservation',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
		this.extraOptionList = this.props.store.extraOptionList;

	},
	handleVisibility(currentStep) {
		if (currentStep === this.book.booking.step) {
			return 'active';
		} else if (currentStep < this.book.booking.step) {
			return 'visited';
		} else if (currentStep > this.book.booking.step) {
			return 'hide';
		}
	},
	getAdditionalAccessoriesList() {
		const options = _.get(this.book.booking, 'options', []);
		return _.map(options, (option, index) => {
			// const optionName = _.chain(this.extraOptionList.options)
			// 	.find(['code', option])
			// 	.get('name', '')
			// 	.value();
			const optionName = _.get(this.translation.t('pages.booking.product_options'), option);

			return (
				<li key={index}>{optionName}</li>
			);
		});
	},

	render() {
		const title = {
			__html: this.translation.t('widget_book.title')
		};
		const additional_accessories = this.getAdditionalAccessoriesList();
		const dateFrom = moment(this.book.booking.dateFrom, 'DD/MM/YYYY').toDate();
		const dateTo= moment(this.book.booking.dateTo, 'DD/MM/YYYY').toDate();
		return (
			<div className="widget-book widget-book-reservation container">
				<Row>
					<h3>{this.translation.t('widget_book.booking_summary')}</h3>
				</Row>
				<div className={`step-2`}>
					<div className="col">
						<p className="title"><strong>{this.translation.t('widget_book.start_date')}</strong></p>
						<p>{moment(dateFrom).locale(this.translation.language).format('DD MMM, YYYY')}</p>
						<p>{this.book.booking.timeFrom}</p>
					</div>
					<div className="col">
						<p className="title"><strong>{this.translation.t('widget_book.end_date')}</strong></p>
						<p>{moment(dateTo).locale(this.translation.language).format('DD MMM, YYYY')}</p>
						<p>{this.book.booking.timeTo}</p>
					</div>
					<div className="col">
						<p className="title"><strong>{this.translation.t('widget_book.total_days')}</strong></p>
						<p>{this.book.booking.duration}</p>
					</div>
				</div>
				<div className={`step-3`}>
					<div className="col">
						<p className="title"><strong>{this.translation.t('widget_book.model')}</strong></p>
						<p>{this.book.booking.model}</p>
					</div>
					<div className={classNames('col')}>
						<p className="title"><strong>{this.translation.t('widget_book.additional_accessories')}</strong></p>
						<ul className="options-selected">
							{additional_accessories}
						</ul>
					</div>
					<div className={classNames('col')}>
						<p className="title"><strong>{this.translation.t('widget_book.price')}</strong></p>
						<p className="price price-mount"><strong>${this.book.booking.totalrate}</strong></p>
					</div>
				</div>
			</div>
		);
	}
}));
