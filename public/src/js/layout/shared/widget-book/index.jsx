import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import {observer} from 'mobx-react';
import {Col, Row, Input, Button, Modal, Label, Alert, Checkbox} from 'react-bootstrap';
import {DateTimePicker, Combobox} from 'react-widgets';
import {browserHistory} from 'react-router';
import Moment from 'moment';
import {Element} from 'react-scroll';

import Notify from '../../../shared/notify';

import CarDetail from '../../book/partials/car-detail';

import momentLocalizer from 'react-widgets/lib/localizers/moment';

momentLocalizer(Moment);

export default observer(['store'], React.createClass({
	displayName: 'WidgetBook',
	getInitialState() {
		return {
			showAlert: false,
			dataPickUp: []
		}
	},
	componentWillMount() {
		this.groupsCarList = this.props.store.groupsCarList;
		this.translation = this.props.store.translation;
		this.stations = this.props.store.stations;
		this.book = this.props.store.book;

	},
	componentWillUpdate(nextProps) {
		if(!_.isEmpty(nextProps.store.book.cars) && _.isEmpty(this.book.booking.code) && !nextProps.store.book.bookingActive) {
			this.nextStep();
		}

		if(!_.isEmpty(this.book.booking.id) && _.isEmpty(nextProps.store.book.cars)) {
			this.showError('ERR/10');
		}

		if (!_.isEmpty(this.book.booking.pickupStation, this.book.booking.returnStation) &&
			!_.isEqual(this.book.booking.pickupStation, this.book.booking.returnStation) &&
			!this.state.showAlert) {
			this.handleAlertShow();
		}
	},
	getNextDay() {
		const days = 1;
		const date = _.isEmpty(this.book.booking.dateFrom) ?
			Moment().utc().valueOf() :
			Moment(Moment(this.book.booking.dateFrom, 'DD/MM/YYYY').toDate()).utc().valueOf();
		return new Date(date + days * 24 * 60 * 60 * 1000);
	},
	bookCar() {
		const car = _.find(this.book.cars, {'code': this.book.booking.code});
		this.book.booking.model = car.model;
		this.book.booking.baserate = car.baserate;
		this.book.booking.totalrate = car.totalrate;
		this.book.bookingActive = true;
		dataLayer.push({
			"event": "click",
			"btnName": "btnStep3"
		});
		browserHistory.push('/step3');
	},
	nextStep() {
		this.book.bookingActive = true;
		dataLayer.push({
			"event": "click",
			"btnName": "btnStep2"
		});
		browserHistory.push('/step2');
	},
	checkAvailableCars() {
		this.book.bookingActive = false;
		this.props.store.book.getAvailableCars();
		// if (this.book.bookingActive) {
		// 	browserHistory.push('/step2');
		// } else {
		// 	this.book.bookingActive = false;
		// 	this.props.store.book.getAvailableCars();
		// }

	},
	_setFormatDate(value) {
		return Moment(value).format('DD/MM/YYYY');
	},
	_getFormattedDate(value) {
		return Moment(value, 'DD/MM/YYYY').toDate();
	},
	_setFormatTime(value) {
		return Moment(value).format('HH:mm');
	},
	_getFormattedTime(value) {
		return Moment(value, 'HH:mm').toDate();
	},
	_getMinTime() {
		return Moment({ hour:6, minute: 0 }).toDate();
	},
	_getMaxTime() {
		return Moment({ hour:22, minute: 0 }).toDate();
	},
	onFocus(e) {

	},
	showError(errorType) {

		Notify(this.translation.t(`pages.booking.error.${errorType}`), '', 'error');
		_.delay(() => {
			this.book.errorType = '';
		}, 100)
	},
	closeModal() {
		this.book.booking.code = '';
	},
	getCarByCode(code) {
		return _.find(this.book.cars, {'code': code});
	},
	setPickupStation({code, name}) {
		this.book.booking.pickupStation = code;
		this.book.booking.pickupStationName = name;

		//Set Default Return station
		if (_.isEmpty(this.book.booking.returnStation)) {
			this.setReturnStation({
				code,
				name
			})
		}
	},
	setReturnStation({code, name}) {
		this.book.booking.returnStation = code;
		this.book.booking.returnStationName = name;
	},
	getModalContent() {
		const isCarAvailable = this.book.isSelectedCarAvailable;
		const car = this.book.getSelectedCar;
		const title = isCarAvailable ? <Label bsStyle="success">{this.translation.t('pages.booking.car_available')}</Label> : <Label bsStyle="danger">{this.translation.t('pages.booking.car_no_available')}</Label>;
		return (
			<div>
				<Modal.Header>
					<Modal.Title className="text-center">{title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<CarDetail
						carInfo={car}
						days={this.book.booking.duration}
					/>
				</Modal.Body>
				<Modal.Footer>
					<Button className={classNames('secondary', {'hidden': !isCarAvailable})} onClick={() => this.bookCar()}>{this.translation.t('pages.booking.book_car')}</Button>
					<Button className={classNames('secondary', {'hidden': isCarAvailable})} onClick={() => this.nextStep()}>{this.translation.t('pages.booking.pick_available_car')}</Button>
					<Button className={classNames('btn', {'hidden': isCarAvailable})} onClick={() => this.closeModal()}>{this.translation.t('pages.booking.change_car')}</Button>
				</Modal.Footer>
			</div>
		);
	},
	getStationList() {
		const stationLang = this.translation.t('pages.locations.code');
		return _.transform(this.stations.getStationList, (result, {code}) => {

			result.push({
				code,
				name: _.get(stationLang, code)
			});
		}, []);
	},
	handleAlertDismiss() {
		this.setState({showAlert: false});
	},
	handleAlertShow() {
		this.setState({showAlert: true});
	},

	updateOptionLocation(){
		this.book.booking.stationSameActive = true;
	},

	render() {
		const title = {
			__html: this.translation.t('widget_book.title')
		};

		if(!_.isEmpty(this.book.errorType)) {
			this.showError(this.book.errorType);
		}

		// const stationList = this.getStationList();
		const minDay = this.getNextDay();
		const showModal = !_.isEmpty(this.book.cars) && !_.isEmpty(this.book.booking.code) && !this.book.bookingActive;
		const modalContent = !_.isEmpty(this.book.cars) && !_.isEmpty(this.book.booking.code) ? this.getModalContent() : null;
		const defaultReturnStation = _.isEmpty(this.book.booking.returnStation) ? this.book.booking.pickupStationName : this.book.booking.returnStationName;
		const alert = this.book.isDifferentLocation ?
			(
				<Alert bsStyle="warning" onDismiss={this.handleAlertDismiss}>
					{this.translation.t('widget_book.different_location')}
				</Alert>
			) : null;
		const currentLang = this.translation.language;
		const stationSameActive = (!this.book.booking.stationSameActive ? 'hidden' : '');
		
		return (
			<Element className={`widget-book container ${this.props.hiddenIn}`} name="widget-book">
				<Modal show={showModal} onHide={this.closeModal}>
					{modalContent}
				</Modal>
				{alert}
				<Row className="widget-container">
					<Col xs={12} className="visible-xs">
						<div className="title-container col">
							<p dangerouslySetInnerHTML={title} />
						</div> 
					</Col>
					<Col md={6} xs={12} >
						<Col className="removeP spaceMovil" md={6} xs={12} >
							<DateTimePicker
								value={this.book.booking.dateFrom === '' ? null : this._getFormattedDate(this.book.booking.dateFrom)}
								onChange={value => {
									this.book.booking.dateFrom = this._setFormatDate(value);
									this.book.booking.timeFrom = this._setFormatTime(value);
								}}
								min={new Date()}
								culture={currentLang}
								placeholder="Pick up"
							/>
							<p className="m-0">{this.translation.t('widget_book.start_date')}</p>
						</Col>
						<Col className="removeP spaceMovil" md={6} xs={12} >
							<DateTimePicker
								value={this.book.booking.dateTo === '' ? null : this._getFormattedDate(this.book.booking.dateTo)}
								onChange={value => {
									this.book.booking.dateTo = this._setFormatDate(value)
									this.book.booking.timeTo = this._setFormatTime(value);
									}}
								min={minDay}
								culture={currentLang}
								placeholder="drop off"
							/>
							<p className="m-0">{this.translation.t('widget_book.end_date')}</p>
						</Col>

						{/* <DateTimePicker
							value={this.book.booking.timeFrom === '' ? null : this._getFormattedTime(this.book.booking.timeFrom)}
							calendar={false}
							onChange={value => {this.book.booking.timeFrom = this._setFormatTime(value)}}
							format="h : mm A"
							min={this._getMinTime()}
							max={this._getMaxTime()}
						/> */}



					</Col>
					<Col md={3} xs={12} className="location-select spaceMovil">
						<Combobox
							valueField="code"
							textField="name"
							data={this.state.dataPickUp}
							disabled={!this.book.isBookStepDateValid}
							onChange={value => {this.setPickupStation(value)}}
							onFocus={()=>{this.setState({dataPickUp: this.book.booking.locationsPickUp});}}
							placeholder="Pick up & return location"
						/>
						<p className={stationSameActive}>{this.translation.t('widget_book.pick_up_location')}</p>
						
						
						<Checkbox className={"m-0 " +(this.book.booking.stationSameActive ? 'hidden' : '')}  value='true' onChange={() => this.updateOptionLocation(true)} disabled={!this.book.isBookStepDateValid}>use another return location</Checkbox>

						<Combobox
							value={defaultReturnStation}
							valueField="code"
							textField="name"
							defaultValue={this.book.booking.pickupStation}
							data={this.book.booking.locationsDropOff}
							className={stationSameActive}
							disabled={!this.book.isBookStepDateValid}
							onChange={value => {this.setReturnStation(value)}}
						/>
						<p className={stationSameActive}>{this.translation.t('widget_book.drop_off_location')}</p>
						
					</Col>
					<Col md={3} xs={12} className="spaceMovil">
							<div className="btn-container">
									<Button
										onClick={this.checkAvailableCars}
										className="btn md full btn-green"
										id={this.translation.t('widget_book.btn_next_id')}
										disabled={!this.book.isBookStep1Valid}
									>
										{this.translation.t('widget_book.btn_next')}
									</Button>
							</div>
					</Col>
						{/* <div className="reserve-container col">
							<div className="col">

							</div>
							<div className="col">

								<DateTimePicker
									value={this.book.booking.timeTo === '' ? null : this._getFormattedTime(this.book.booking.timeTo)}
									calendar={false}
									onChange={value => {this.book.booking.timeTo = this._setFormatTime(value)}}
									format="h : mm A"
									min={this._getMinTime()}
									max={this._getMaxTime()}
								/>
							</div>
						</div> */}
						{/* <div className="reserve-container col">
							<div className="selectors">
									<div className="selector">
										<p>{this.translation.t('widget_book.pick_up_location')}</p>
										<Combobox
											valueField="code"
											textField="name"
											data={this.state.dataPickUp}
											disabled={!this.book.isBookStepDateValid}
											onChange={value => {this.setPickupStation(value)}}
											onFocus={()=>{this.setState({dataPickUp: this.book.booking.locationsPickUp});}}
											placeholder="Pick up & return location"
										/>
										onChange={() => this.updateOptionSelected(index)}
										<Checkbox value='true' >use another return location</Checkbox>
									</div>
									<div className="selector">
										<p>{this.translation.t('widget_book.drop_off_location')}</p>
										<Combobox
											value={defaultReturnStation}
											valueField="code"
											textField="name"
											defaultValue={this.book.booking.pickupStation}
											data={this.book.booking.locationsDropOff}

											
											disabled={!this.book.isBookStepDateValid}
											onChange={value => {this.setReturnStation(value)}}
										/>
									</div>
								</div>
						</div> */}
						{/* <div className="reserve-container location-selectors col">
								<div className="btn-container">
										<Button
											onClick={this.checkAvailableCars}
											className="btn md full"
											id={this.translation.t('widget_book.btn_next_id')}
											disabled={!this.book.isBookStep1Valid}
										>
											{this.translation.t('widget_book.btn_next')}
										</Button>
								</div>
						</div> */}
				</Row>
			</Element>
		);
	}
}));
