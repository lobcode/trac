import React from 'react';
import {observer} from 'mobx-react';
import {Col, Row, Image} from 'react-bootstrap';
import LazyLoad from 'react-lazy-load';
import Slider from 'react-slick';
import {Link} from 'react-router';
import _ from 'lodash';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Top Destinations',
	componentWillMount() {
		this.translation = this.props.store.translation;
	},
	getDestinations() {
    const Destinations = [
      {
        title: "Nosara",
        image: "destination/img1.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Santa Teresa",
        image: "destination/img2.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Dominical",
        image: "destination/img3.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Monteverde",
        image: "destination/img4.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Puerto Viejo",
        image: "destination/img5.jpg",
        url: "https://www.google.com/"
      }
    ];

    return _.map(Destinations, (destination, index) => {
      return (
        <Col xs={12} sm={4} md={2}  key={index}>
          <div className="great-deal-item">
            {/* <Link to={destination.url}> */}
              <figure>
                <LazyLoad><Image className="img-responsive" src={IMG_S3_PATH + destination.image} alt={destination.title} circle /></LazyLoad>
                <figcaption>
                  {destination.title}
                </figcaption>
              </figure>
            {/* </Link> */}
          </div>
        </Col>
      );
    })
  },
  

  destinationsLiders() {
    const Destinations = [
      {
        title: "Nosara",
        image: "destination/img1.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Santa Teresa",
        image: "destination/img2.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Dominical",
        image: "destination/img3.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Monteverde",
        image: "destination/img4.jpg",
        url: "https://www.google.com/"
      },
      {
        title: "Puerto Viejo",
        image: "destination/img5.jpg",
        url: "https://www.google.com/"
      }
    ];

    return _.map(Destinations, (destination, index) => {
      const img = `${IMG_S3_PATH}${destination.image}`;
        return (
          <div key={index} className="great-deal-item">
            <figure>
                <LazyLoad><Image className="img-responsive" src={IMG_S3_PATH + destination.image} alt={destination.title} circle /></LazyLoad>
                <figcaption>
                  {destination.title}
                </figcaption>
              </figure>
          </div>
        );
    });
  },

	render() {
		const destinations = this.getDestinations();
    const desSli = this.destinationsLiders();
    const settings = {
      // dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      arrows: false,
      autoplay: true,
      draggable: false,
      adaptiveHeight: true,
      lazyLoad: true,
			responsive: [
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        }
      ]
    };

		return (
			<section className="top-destinations container">
				<Row>
					<Col xs={12} sm={3}></Col>
					<Col xs={12} sm={6}>
						<h2 className="section-title">Top Destinations</h2>
						<p className="section-subtitle">Choose one of the best place in paradise and let us suggest the best option for your rent.</p>
					</Col>
					<Col xs={12} sm={3}></Col>
				</Row>			
				<Row className="list-top-destinations">
					<Col xs={12} md={1}></Col>
					{destinations}
				</Row>
        <Row className="list-top-destinations-slider">
          <Slider {...settings}>
              {desSli}
          </Slider>

        </Row>
			</section>
		);
	}
}));
