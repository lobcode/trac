import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import {Col, Row, Button, Modal, Label, Alert, Glyphicon} from 'react-bootstrap';
import {DateTimePicker, Combobox} from 'react-widgets';
import {browserHistory} from 'react-router';
import Moment from 'moment';
import {Element} from 'react-scroll';

import Notify from '../../../shared/notify';

import momentLocalizer from 'react-widgets/lib/localizers/moment';
import 'moment/locale/es';
momentLocalizer(Moment);

export default observer(['store'], React.createClass({
	displayName: 'Widget Reserve Form',
	getInitialState() {
		return {
			isModalVisible: false
		};
	},
	componentWillMount() {
		this.groupsCarList = this.props.store.groupsCarList;
		this.translation = this.props.store.translation;
		this.stations = this.props.store.stations;
		this.reserve = this.props.store.reserve;

		this.reserve.information.vehicle = 'Hiace';
	},
	getNextDay() {
		const days = 1;
		const date = _.isEmpty(this.reserve.information.dateFrom) ?
			Moment().utc().valueOf() :
			Moment(Moment(this.reserve.information.dateFrom, 'DD/MM/YYYY').toDate()).utc().valueOf();
		return new Date(date + days * 24 * 60 * 60 * 1000);
	},
	doReserve() {
		this.reserve.information.landingName = _.get(this.props, 'landing', '');
		this.sendEmail();
	},
	sendEmail() {
		const notification = this.translation.t('widget_reserve_form.form.notification');
		const {success, warning, error} = notification;

		this.reserve.sendEmail()
			.then(({data}) => {
				if (data.emailConfirmation) {
					this.setState({
						isModalVisible: true
					});
					this.reserve.clearReservation();
				} else {
					Notify(warning.title, warning.text, warning.type);
				}
				this.reserve.disableSubmit = false;
			})
			.catch((err) => {
				console.log(err);
				//Notify(error.title, error.text, error.type);
			});
	},
	getCarList() {
		if (this.props.type) {
			return _.chain(this.props.type.vehicles)
				.sortBy()
				.value();
		} else {
			return _.chain(this.groupsCarList.getCarsList)
				.transform((result, {active, name}) => {
					if (active) {
						result.push(name);
					}
				}, [])
				.uniq()
				.sortBy()
				.value();
		}

	},
	_setFormatDate(value) {
		return Moment(value).format('DD/MM/YYYY');
	},
	_getFormattedDate(value) {
		return Moment(value, 'DD/MM/YYYY').toDate();
	},
	_setFormatTime(value) {
		return Moment(value).format('HH:mm');
	},
	_getFormattedTime(value) {
		return Moment(value, 'HH:mm').toDate();
	},
	_getMinTime() {
		return Moment({ hour:6, minute: 0 }).toDate();
	},
	_getMaxTime() {
		return Moment({ hour:17, minute: 0 }).toDate();
	},
	onFocus(e) {

	},
	showError(errorType) {
		Notify(this.translation.t(`pages.booking.error.${errorType}`), 'error');
		_.delay(() => {
			this.reserve.errorType = '';
		}, 100)
	},
	setPickupStation({code, name}) {
		this.reserve.information.pickupStation = code;
		this.reserve.information.pickupStationName = name;

		//Set Default Return station
		if (_.isEmpty(this.reserve.information.returnStation)) {
			this.setReturnStation({
				code,
				name
			})
		}
	},
	setReturnStation({code, name}) {
		this.reserve.information.returnStation = code;
		this.reserve.information.returnStationName = name;
	},
	getModalContent() {
		const title = this.translation.t('widget_reserve_form.form.notification.success.title');
		const content = this.translation.t('widget_reserve_form.form.notification.success.text');

		return (
			<div>
				<Modal.Header closeButton bsStyle="success">
					<Modal.Title className="text-center"><Glyphicon glyph="ok-sign" /> {title}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div dangerouslySetInnerHTML={{__html: content}} />
				</Modal.Body>
			</div>
		);
	},
	getStationList() {
		const stationLang = this.translation.t('pages.locations.code');
		return _.transform(this.stations.getStationList, (result, {code}) => {

			result.push({
				code,
				name: _.get(stationLang, code)
			});
		}, []);
	},
	handleAlertDismiss() {
		this.setState({showAlert: false});
	},
	handleAlertShow() {
		this.setState({showAlert: true});
	},
	closeModal() {
		this.setState({
			isModalVisible: false
		});
	},
	render() {

		if(!_.isEmpty(this.reserve.information.errorType)) {
			this.showError(this.reserve.information.errorType);
		}

		const stationList = this.getStationList();
		const carList = this.getCarList();
		const minDay = this.getNextDay();
		const modalContent = this.getModalContent();
		const defaultReturnStation = _.isEmpty(this.reserve.information.returnStation) ? this.reserve.information.pickupStationName : this.reserve.information.returnStationName;
		const additionalInputs = this.reserve.isTypeCarReserve ?
			(
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.form.driver_licence_number')}</p>
						<input
							ref="licence"
							className="form-control"
							value={this.reserve.information.driverLicenceNumber}
							onChange={({target}) => this.reserve.information.driverLicenceNumber = target.value}
							/>
					</Col>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.form.insurance')}</p>
						<Combobox
							data={this.props.type.insurance}
							onChange={value => {this.reserve.information.insurance = value}}
						/>
					</Col>
				</Row>
			) : null;
		return (
			<Element className="widget-reserve-form" name="widget-book">
				<Modal show={this.state.isModalVisible} onHide={this.closeModal}>
					{modalContent}
				</Modal>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_book.start_date')}</p>
					</Col>
					<Col xs={6}>
						<DateTimePicker
							value={this.reserve.information.dateFrom === '' ? null : this._getFormattedDate(this.reserve.information.dateFrom)}
							onChange={value => {this.reserve.information.dateFrom = this._setFormatDate(value)}}
							time={false}
							min={new Date()}
              max={new Date(2017,10,30)}	
							format="DD/MM/YYYY"
						/>
					</Col>
					<Col xs={6}>
						<DateTimePicker
							value={this.reserve.information.timeFrom === '' ? null : this._getFormattedTime(this.reserve.information.timeFrom)}
							calendar={false}
							onChange={value => {this.reserve.information.timeFrom = this._setFormatTime(value)}}
							format="h : mm A"
							min={this._getMinTime()}
							max={this._getMaxTime()}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_book.end_date')}</p>
					</Col>
					<Col xs={6}>
						<DateTimePicker
							value={this.reserve.information.dateTo === '' ? null : this._getFormattedDate(this.reserve.information.dateTo)}
							onChange={value => {this.reserve.information.dateTo = this._setFormatDate(value)}}
							time={false}
							min={minDay}
							max={new Date(2017,10,30)}	
							format="DD/MM/YYYY"
						/>
					</Col>
					<Col xs={6}>
						<DateTimePicker
							value={this.reserve.information.timeTo === '' ? null : this._getFormattedTime(this.reserve.information.timeTo)}
							calendar={false}
							onChange={value => {this.reserve.information.timeTo = this._setFormatTime(value)}}
							format="h : mm A"
							min={this._getMinTime()}
							max={this._getMaxTime()}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={6}>
						<p>{this.translation.t('widget_book.pick_up_location')}</p>
						<Combobox
							valueField="code"
							textField="name"
							data={stationList}
							onChange={value => {this.setPickupStation(value)}}
						/>
					</Col>
					<Col xs={6}>
						<p>{this.translation.t('widget_book.drop_off_location')}</p>
						<Combobox
							value={defaultReturnStation}
							valueField="code"
							textField="name"
							defaultValue={this.reserve.information.pickupStation}
							data={stationList}
							onChange={value => {this.setReturnStation(value)}}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.vehicle')}</p>
						<Combobox
							data={['Hiace']}
              defaultValue={"Hiace"}
							onChange={value => {this.reserve.information.vehicle = value}}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.full_name')}</p>
						<input
							ref="fullName"
							className="form-control"
							value={this.reserve.information.customerFullName}
							onChange={({target}) => this.reserve.information.customerFullName = target.value}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.email')}</p>
						<input
							ref="email"
							className="form-control"
							value={this.reserve.information.customerEmail}
							onChange={({target}) => this.reserve.information.customerEmail = target.value}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p>{this.translation.t('widget_reserve_form.phone_number')}</p>
						<input
							ref="phoneNumber"
							className="form-control"
							value={this.reserve.information.customerPhone}
							onChange={({target}) => this.reserve.information.customerPhone = target.value}
						/>
					</Col>
				</Row>
				{additionalInputs}
				<Row>
					<Col xs={12} className="btn-container">
						<Button
							onClick={this.doReserve}
							className="btn md full"
							disabled={!this.reserve.isValid || this.reserve.disableSubmit}
						>
							{this.translation.t('widget_reserve_form.book_now_btn')}
						</Button>
					</Col>
				</Row>
			</Element>
		);
	}
}));
