import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import _ from 'lodash';
import {Row, Col} from 'react-bootstrap';
import {animateScroll} from 'react-scroll';

import Helmet from '../helmet';
import WidgetBookReservation from '../widget-book-reservation';
import HotDeals from '../hot-deals';
import EnjoyGreatDeals from '../enjoy-great-deals';

const NUMBER_STEPS = 4;

export default observer(['store'], React.createClass({
	displayName: 'Book',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.metatags = this.props.store.metaTags;
		this.header = this.props.store.header;
		this.book = this.props.store.book;
	},
	componentDidMount() {
		const scroll = animateScroll;
		scroll.scrollToTop();
	},
	render() {
		const pageTitle = !_.isEmpty(this.props.pageTitle) ? <h1>{this.props.pageTitle}</h1> : null;
		const pageSubTitle = !_.isEmpty(this.props.pageSubTitle) ? <h4>{this.props.pageSubTitle}</h4> : null;
		const progressBar = (
			<ul className="progressbar">
				{
					_.chain(NUMBER_STEPS)
					.range()
					.map((num) => {
						return <li key={num} className={classNames({'done': num + 1 < this.book.booking.step, 'current': num + 1 === this.book.booking.step})}><span>{this.translation.t(`pages.booking.step${num + 1}_title`)}</span></li>;
					})
					.value()
				}
			</ul>
		);

		return (
			<div className={classNames('internal booking', this.props.className)}>
				<Helmet />
				<header className="section-header">
					<div className="container">
						<Row>
							<Col xs={12} className="section-title">
								{this.translation.t(`pages.booking.page_title`)}
							</Col>
						</Row>
					</div>
				</header>
				<WidgetBookReservation />
				{progressBar}
				<div className="main-content">
					{this.props.children}
				</div>
				<EnjoyGreatDeals />
			</div>
		);
	}
}));
