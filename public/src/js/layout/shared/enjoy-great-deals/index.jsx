import React from 'react';
import {observer} from 'mobx-react';
import {Col, Row, Image} from 'react-bootstrap';
import LazyLoad from 'react-lazy-load';
import {Link} from 'react-router';
import _ from 'lodash';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Enjoy Great Deals',
	componentWillMount() {
		this.translation = this.props.store.translation;
	},
	getGreatDeals() {
		const greatDeals = _.filter(this.translation.t('pages.special_offers.sections'), ['greatDeal', true]);
		const currentLang = this.props.store.translation.language;
		return _.map(greatDeals, (deal, index) => {
			const offerUrl = `${_.isEqual(currentLang, 'es') ? '/es' : ''}/${this.translation.t('pages.special_offers.permalink')}/${_.kebabCase(deal.title)}`;

			const discountNumber = `${deal.discount}%`;
			const n = parseInt(index,10) + 1;

			const haveDiscount = !_.isEmpty(deal.discount) ? (
				<figcaption dangerouslySetInnerHTML={{__html: `<span>${discountNumber}</span> ${this.translation.t('discount')}`}} />
				) : null;
			return (
				<Col xs={6} sm={4} md={2}  key={index}>
					<div className="great-deal-item">
						<Link to={offerUrl} id={'great_deal_btn_' + n}>
							<figure>
								<LazyLoad><Image className="img-responsive" src={IMG_S3_PATH + deal.image} alt={deal.title} circle /></LazyLoad>
								<figcaption>
									{deal.title}
								</figcaption>
							</figure>
						</Link>
					</div>
				</Col>
			);
		});
	},

	render() {
		const greatDealsList = this.getGreatDeals();

		return (
			<section className="enjoy_great-deals container">
				<Row>
					<Col xs={12} sm={3}></Col>
					<Col xs={12} sm={6}>
						<h2 className="section-title">Top Destinations</h2>
						<p className="section-subtitle">Choose one of the best place in paradise and let us suggest the best option for your rent.</p>
					</Col>
					<Col xs={12} sm={3}></Col>
				</Row>			
				<Row>
					{greatDealsList}
				</Row>
			</section>
		);
	}
}));
