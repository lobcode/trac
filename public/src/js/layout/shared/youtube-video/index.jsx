import React from 'react';
import {observer} from 'mobx-react';
import {Col, Row, Image} from 'react-bootstrap';
import LazyLoad from 'react-lazy-load';
import {Link} from 'react-router';
import _ from 'lodash';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Youtube Video',
	componentWillMount() {
		this.translation = this.props.store.translation;
	},

	render() {
		return (
			<section className="l-video">
				<LazyLoad>
					<img className="w-100" src={IMG_S3_PATH + 'video/img.jpg'}/>
				</LazyLoad>
			</section>
		);
	}
}));
