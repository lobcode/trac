import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import {Link, Events, scrollSpy, animateScroll} from 'react-scroll';

export default observer(['store'], React.createClass({
	displayName: 'Menu Internal',
	propTypes: {
		items: React.PropTypes.array
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.metatags = this.props.store.metaTags;
		this.header = this.props.store.header;
	},
	componentDidMount() {
	},
	goToSection(itemId) {
		//TODO: Change this to scroller
		document.getElementById(`${itemId}`).scrollIntoView(true);
	},
	getItems() {
		return _.map(this.props.items, ({name, link}, index) => {
			return (
				<li key={index}>
					<Link activeClass="active" spy={true} smooth={true} offset={5} duration={500} to={link}>{name}</Link>
				</li>
			)
		});
	},
	render() {

		return (
			<div className="menu-internal">
				<ul>
					{this.getItems()}
				</ul>
			</div>
		);
	}
}));
