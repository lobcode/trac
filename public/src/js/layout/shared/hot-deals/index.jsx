import React from 'react';
import { observer } from 'mobx-react';
import { Col, Row } from 'react-bootstrap';
import Slider from 'react-slick';
import _ from 'lodash';

import Arrow from '../../../shared/images/arrow';

import { scroller } from 'react-scroll';

import CarDeal from './partials/car-deal';

export default observer(['store'], React.createClass({
	displayName: 'Hot Deals',
	componentWillMount() {
		this.groupsCarList = this.props.store.groupsCarList;
		this.translation = this.props.store.translation;
		this.cars = this.props.store.book.cars;
		this.book = this.props.store.book;
		this.fleetCars = [];
		this.state = {
			category: 'all'
		}
	},
	bookCar(carCode) {
		this.book.booking.code = carCode;
		scroller.scrollTo('widget-book', {
			smooth: true,
			duration: 1000,
		});
		//this.book.bookingSidebarOpen = true;
	},
	setCategory(category) {
		this.setState({
			category
		});
	},

	getDeals() {
		const { category } = this.state;
		let showCars;
		this.fleetCars = this.groupsCarList.getCarsListFleet;

		// Filter 
		if (category === 'all') {
			showCars = this.fleetCars
		} else {
			showCars = this.fleetCars.filter(car => car.category === category);
		}

		return _.map(showCars, (carDetail, index) => {
			const n = parseInt(index, 10) + 1;

			if (!_.isUndefined(carDetail)) {
				return (
					<div key={index} >
						<CarDeal
							title={carDetail.name}
							subTitle={carDetail.group}
							code={carDetail.code}
							id={'hot_deal_btn_' + n}
							eco={carDetail.eco}
							image={carDetail.image}
							bookCar={this.bookCar}
							discount={carDetail.discount}
						/>
					</div>
				);
			}
		});

	},

	getCategory() {
		const catAll = {
			'all': {
				title: 'All',
				category: 'all'
			}
		};
		const categories = this.translation.t('pages.cars.categories');
		const all = Object.assign({}, catAll, categories);

		return _.map(all, (category, index) => {
			return (
				<span
					key={_.uniqueId()}
					href="/"
					className="link-category"
					key={index}
					id={category.category}
					onChange={this.updateParams}
					onClick={() => this.setCategory(category.category)}
					name={category.category}>{category.title}</span>
			);
		});
	},

	slider() {
		const dealsList = this.getDeals();

    function PrevButton({ onClick }) {
      return (
				<button onClick={onClick} className="slick-prev circle-green" style={{ left: '-5%' }}> 
					<Arrow width={15} height={15} color={'white'}/>	
				</button>
			);
    }

    function NextButton({ onClick }) {
      return (
				<button onClick={onClick} className="slick-next circle-green" style={{ right: '-5%' }}> 
					<Arrow width={15} height={15} color={'white'}/>
				</button>
			);
		}
		

		const settings = {
			// dots: false,
			// speed: 500,
			// slidesToShow: 3,
			// slidesToScroll: 3,
			// arrows: false,
			// autoplay: true,
			// fade: true,
			// draggable: false,
			className: 'slider-hot-deals',
			// lazyLoad: true,

			prevArrow: <PrevButton />,
      nextArrow: <NextButton />,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 3,
			dots: false,
			arrows: true,
			adaptiveHeight: true,
			lazyLoad: true,

			responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
		};

		return (
			<Slider {...settings}>
				{dealsList}
			</Slider>
		)
	},

	render() {
		const categories = this.getCategory();
		const slider = (this.groupsCarList.getCarsListFleet.length > 0) ? this.slider() : null;

		return (
			<section className="hot-deals">
				<div className="container">
					<Row className="hidden">
						<Col xs={12}>
							<h2 className="section-title mb-none">{this.translation.t('hot_deals.main_title')}</h2>
							<h3 className="section-title small">{this.translation.t('hot_deals.sub_title')}</h3>
						</Col>
					</Row>
					<Row >
						<Col xs={12} className="list-items-category">
							<Col xs={12} md={4} xl={5} className="p-0">
								<h3 className="text-uppercase">Our Fleet</h3>
							</Col>
							<Col xs={12} md={8} xl={7} className="d-flex justify-content-end p-0 pn-ProductNav">
								{categories}
							</Col>

							<Col className="list-category-line">
								<hr />
							</Col>
						</Col>

					</Row>
				</div>
				<div className="bg-slider">
					<div className="container">
						<Row >
							<Col xs={12}>
								{slider}
							</Col>
						</Row>
					</div>
				</div>
			</section>
		);
	}
}));
