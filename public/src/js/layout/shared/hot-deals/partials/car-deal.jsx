import React from 'react';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import {Button} from 'react-bootstrap';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Car Deal',
	propTypes: {
		title: React.PropTypes.string.isRequired,
		subTitle: React.PropTypes.string.isRequired,
		eco: React.PropTypes.bool.isRequired,
		image: React.PropTypes.string.isRequired,
		code: React.PropTypes.string.isRequired,
		bookCar: React.PropTypes.func.isRequired
	},
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},

	render() {
		const subTitle = `${this.props.subTitle} ${this.translation.t('pages.booking.or_similar')}`;
		const image = IMG_S3_PATH + 'nuevo/' + this.props.image.replace('.jpg', '.png');

		return (
			<div className="car-deal">
				<div className="car-deal-header text-left">
					<p className="car-deal-title">{this.props.title}</p>
					{/* <p className="car-deal-price">$80/day*</p> */}
					{/* <p className="car-deal-sub-title">{subTitle}</p> */}
				</div> 
				<figure className={classNames({'eco': this.props.eco})}>
					<img src={image} alt={this.props.title} className="img-responsive car-thumbnail car-deal-img" />
				</figure>
				<div className="wrap-btn">
					<Button id={this.props.id} className="btn full btn-green" onClick={() => this.props.bookCar(this.props.code)}>{this.translation.t('cta.book_car')}</Button>
				</div>
			</div>
		);
	}
}));
