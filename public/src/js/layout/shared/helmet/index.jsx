import React from 'react';
import _ from 'lodash';
import {observer} from 'mobx-react';
import Helmet from 'react-helmet';


export default observer(['store'], React.createClass({
	displayName: 'Helmet',
	componentWillMount() {
		this.metatags = this.props.store.metaTags;
	},
	render() {
		const {title, description} = this.metatags;
		const host = window.location.host;
		const path = host.replace('www.', '') + window.location.pathname;
		const canonical = (path.substr(path.length - 1) === '/') ? path.slice(0, -1) : path;
		const isStepPage = _.chain(window.location.pathname)
				.split('/', 2)
				.last()
				.startsWith('step')
				.value();

		return (
			<Helmet
				link={[
					{ rel: 'canonical', href: `https://www.${canonical}` }
				]}
				link={[
					{  href: `https://fonts.googleapis.com/css?family=Roboto:300,400,700` }
				]}
				title={_.get(this.props, 'title', title)}
				meta={[
						{"name": "description", "content": _.get(this.props, 'description', description)},
						{"name": "robots", "content": (isStepPage) ? 'noindex' : 'index'},
      // Open graph
      {"property": "og:type", "content": 'website'},
      {"property": "og:url", "content": `https://www.${canonical}`},
      {"property": "og:title", "content": _.get(this.props, 'title', title)},
      {"property": "og:description", "content": _.get(this.props, 'description', description)},
      {"property": "og:site_name", "content":'Toyota Rent a Car'},
      {"property": "og:image", "content": "https://s3-us-west-2.amazonaws.com/toyotarent/shareFB.jpg"}
				]}
			/>
		);
	}
}));
