import React from 'react';
import { observer } from 'mobx-react';
import Slider from 'react-slick';
import Layout from '../shared/home-layout';
import _ from 'lodash';
import { Link } from 'react-router';

import OnScroll from '../../shared/scroll-detect';

const IMG_S3_PATH = 'https://s3-us-west-2.amazonaws.com/toyotarent/';

export default observer(['store'], React.createClass({
	displayName: 'Slider',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.book = this.props.store.book;
	},
	componentDidMount() {
		OnScroll('.main-banner', '.main-header');
		if (this.book.bookingActive) {
			this.book.clearReservation();
		}
	},
	getSlides() {
		const slides = this.translation.t('portada');
		return _.map(slides, (slide, index) => {
			const img = `${IMG_S3_PATH}slider/${slide.img}.jpg`;

			if (slide.type === 'clean') {
				return (
					<div key={index}>
						<img src={img} className="img-responsive w-100" />
					</div>
				);
			} else {
					return (
						<div key={index}>
							<Link id={slide.btn_id} to={slide.link}>
								<img src={img} className="img-responsive w-100" />
							</Link>
						</div>
					);
				} 

		});
	},
	jsonLD(obj) {
		return (
			<script
				type="application/ld+json"
				dangerouslySetInnerHTML={{ __html: JSON.stringify(obj) }}
			/>
		);
	},
	render() {
		const settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			autoplay: true,
			fade: true,
			draggable: false,
			className: 'slider',
			adaptiveHeight: true,
			lazyLoad: true
		};
		const slides = this.getSlides();
		const seo = this.translation.t('pages.home.seo');

		let schemaReview = {
			"@context": "http://schema.org/",
			"@type": "Review",
			"itemReviewed": {
				"@type": "Thing",
				"name": "Toyota Car Rental Costa Rica"
			},
			"author": {
				"@type": "Person",
				"name": "Toyota"
			},
			"datePublished": "2019-01/10",
			"reviewRating": {
				"@type": "Rating",
				"description": "Welcome. Toyota is your best Costa Rica car rental option, we have one of the best rates and customer service, you can find us in the best locations such as San Jose, Airport, Liberia and more.",
				"ratingValue": "5"
			}
		};

		let obj2 = {
			"@context": "http://schema.org/",
			"@type": "Review",
			"itemReviewed": {
				"@type": "Thing",
				"image": {
					"@type": "ImageObject",
					"height": "",
					"width": "",
					"url": "https://s3-us-west-2.amazonaws.com/toyotarent/cars/L.jpg"
				},
				"name": "Costa Rica Car Rentals"
			},
			"author": {
				"@type": "Person",
				"name": "Toyota Costa Rica"
			},
			"datePublished": "",
			"reviewRating": {
				"@type": "Rating",
				"description": "Toyota is your best Costa Rica car rental option, we have one of the best rates and customer service, you can find us in the best locations such as San Jose, Airport, Liberia and more.",
				"ratingValue": "5"
			}
		};

		const insertJson = this.jsonLD(schemaReview);
		const insertJson2 = this.jsonLD(obj2);

		return (
			<Layout className="slider-container" seo={seo}>
				<Slider {...settings}>
					{slides}
				</Slider>
				{insertJson}
				{insertJson2}
			</Layout>
		);
	}
}));
