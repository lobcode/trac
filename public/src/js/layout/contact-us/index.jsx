import React from 'react';
import {observer} from 'mobx-react';

import Layout from '../shared/internal-layout';

import ContactForm from './partials/contact-form';

export default observer(['store'], React.createClass({
	displayName: 'Contact Us',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	render() {
		const pageTitle = this.translation.t('menu.contact_us');
		const seo = this.translation.t('pages.contact_us.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
			<section className="main-section customer-care">
				<div className="container">
					<ContactForm />
				</div>
			</section>
		</Layout>
		);
	}
}));
