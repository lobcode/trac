import React from 'react';
import {observer} from 'mobx-react';
import _ from 'lodash';
import {Row, Col, Button} from 'react-bootstrap';
import {DropdownList} from 'react-widgets';
import ReCAPTCHA from 'react-google-recaptcha';

import Notify from '../../../shared/notify';

export default observer(['store'], React.createClass({
  displayName: 'Customer Form',
  componentWillMount() {
    this.translation = this.props.store.translation;
    this.stations = this.props.store.stations;
    this.google = this.props.store.google;
    this.customerForm = this.props.store.customerForm;
    this.google = this.props.store.google;
    this.tracking = this.props.store.tracking;
  },
  onReCaptchaChange(value) {
    this.google.recaptcha = value;
    this.google.verify();
  },
  sendEmail() {
    const notification = this.translation.t('customer_form.notification');
    const {success, warning, error} = notification;

    this.customerForm.sendEmail()
      .then(({data}) => {

        if (data.emailConfirmation) {
          this.tracking.trackLead();
          Notify(success.title, success.text, success.type);
          this.cleanForm();
          dataLayer.push({
            'event': 'formSubmitted',
            'formName': 'Toyota Retals - Customer Care'
          });
        } else {
          Notify(warning.title, warning.text, warning.type);
        }

      })
			.catch((err) => {
				console.log(err);
        Notify(error.title, error.text, error.type);
      });
  },
  cleanForm() {
    this.customerForm.cleanForm();
    this.refs.fullName.focus();
  },
  render() {
    const tCustomerForm = this.translation.t('customer_form');

    // TODO: Change MockReservedThrough and MockTypeOfIssue for real data.

    // Default option was included (value -1) as a placeholder workaround
    const MockReservedThrough = [{
      value: -1, text: `${tCustomerForm.reserved_through.title} *`
    }, {
      value: 0, text: `${tCustomerForm.reserved_through.options.option_1}`
    }, {
      value: 1, text: `${tCustomerForm.reserved_through.options.option_2}`
    }, {
      value: 2, text: `${tCustomerForm.reserved_through.options.option_3}`
    }];

    // Default option was included (value -1) as a placeholder workaround
    // Default option was included (value -1) as a placeholder workaround
    const MockTypeOfIssue = _.transform(this.translation.t('pages.contact_us.issues'), (result, issue, index) => {
      result.push({
        value: index - 1,
        text: issue
      })
    }, []);

    // Default option was included (code -1) as a placeholder workaround
    let locations = [{code: '-1', name: `${tCustomerForm.location} *`}];
    _.map(this.stations.getStationList, (location) => locations.push(location));

    return (
      <div>
        <div className="instructions">
          <Row>
            <Col xs={12}>
              <p>{tCustomerForm.step_description_1}</p>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <p>{tCustomerForm.step_description_2}</p>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <p className="phone">{tCustomerForm.support_description}</p>
            </Col>
          </Row>          
        </div>

        <div className="form-container">

          <Row>
            <Col xs={12} sm={6}>
              <input
                ref="fullName"
                type="text"
                className="form-control"
                value={this.customerForm.fullName}
                placeholder={`${tCustomerForm.full_name} *`}
                onChange={({target}) => this.customerForm.fullName = target.value}
              />
            </Col>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                value={this.customerForm.email}
                placeholder={`${tCustomerForm.email} *`}
                onChange={({target}) => this.customerForm.email = target.value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                value={this.customerForm.telephone}
                placeholder={tCustomerForm.telephone}
                onChange={({target}) => this.customerForm.telephone = target.value}
              />
            </Col>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                value={this.customerForm.ext}
                placeholder={`${tCustomerForm.ext}`}
                onChange={({target}) => this.customerForm.ext = target.value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12} sm={6}>
              <DropdownList
                data={MockReservedThrough}
                textField="text"
                value={(_.size(this.customerForm.reservedThrough) > 0) ? this.customerForm.reservedThrough : MockReservedThrough[0]}
                onChange={(value) => this.customerForm.reservedThrough = value}
              />
            </Col>
            <Col xs={12} sm={6}>
              <DropdownList
                data={locations}
                textField="name"
                value={(_.size(this.customerForm.location) > 0) ? this.customerForm.location : locations[0]}
                onChange={(value) => this.customerForm.location = value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                placeholder={tCustomerForm.rental_agreement_number}
                value={this.customerForm.rentalAgreementNumber}
                onChange={({target}) => this.customerForm.rentalAgreementNumber = target.value}
              />
            </Col>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                placeholder={tCustomerForm.confirmation}
                value={this.customerForm.confirmation}
                onChange={({target}) => this.customerForm.confirmation = target.value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12} sm={6}>
              <input
                type="text"
                className="form-control"
                placeholder={tCustomerForm.perks_number}
                value={this.customerForm.perksNumber}
                onChange={({target}) => this.customerForm.perksNumber = target.value}
              />
            </Col>
            <Col xs={12} sm={6}>
              <DropdownList
                data={MockTypeOfIssue}
                textField="text"
                value={(_.size(this.customerForm.typeOfIssue) > 0) ? this.customerForm.typeOfIssue : MockTypeOfIssue[0]}
                onChange={(value) => this.customerForm.typeOfIssue = value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12}>
              <input
                type="text"
                className="form-control"
                placeholder={`${tCustomerForm.subject} *`}
                value={this.customerForm.subject}
                onChange={({target}) => this.customerForm.subject = target.value}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12}>
							<textarea
         cols="10"
         rows="5"
         className="form-control"
         placeholder={tCustomerForm.message}
         value={this.customerForm.message}
         onChange={({target}) => this.customerForm.message = target.value}
       />
            </Col>
          </Row>

          <Row>
            <Col xs={12} className="recaptcha-container">
              <ReCAPTCHA
                ref="recaptcha"
                sitekey={this.google.sitekey}
                onChange={this.onReCaptchaChange}
              />
            </Col>
          </Row>

          <Row>
            <Col xs={12} sm={6} className="hidden-xs">
              <Button
                bsStyle="danger"
                onClick={this.cleanForm}
              >
                {tCustomerForm.cancel}
              </Button>
            </Col>
            <Col xs={12} sm={6}>
              <Button
                bsStyle="danger"
                className="secondary"
                disabled={!this.customerForm.isValid}
                onClick={this.sendEmail}
              >
                {tCustomerForm.submit}
              </Button>
            </Col>
          </Row>

        </div>


      </div>
    );
  }
}));
