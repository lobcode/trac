import React from 'react';
import {observer} from 'mobx-react';
import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router';

import Layout from '../shared/internal-layout';

import CustomerForm from './partials/customer-form';

export default observer(['store'], React.createClass({
	displayName: 'Customer Care',
	componentWillMount() {
		this.translation = this.props.store.translation;
		this.header = this.props.store.header;
	},
	render() {
		const pageTitle = this.translation.t('menu.customer_care');
		const seo = this.translation.t('pages.customer_care.seo');
		return (
			<Layout pageTitle={pageTitle} seo={seo}>
			<section className="main-section customer-care">
				<div className="container">
					<CustomerForm />
				</div>
			</section>
		</Layout>
		);
	}
}));
