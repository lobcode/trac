import React from 'react';
import cookie from 'react-cookies';

export default React.createClass({
	displayName: 'Invalid',
	componentDidMount() {
		cookie.save('notfound', 'true');
		window.location.href = `${window.location.href}`;
	},
  render() {
		return (
			<h1>Oops 404</h1>
		);
  }
});
