import React from 'react';
import { router } from 'react-router';
import { observer } from 'mobx-react';
import { Row, Col, Button } from 'react-bootstrap';
import classNames from 'classnames';
import _ from 'lodash';
import { Link, browserHistory, Redirect } from 'react-router';

import MapCR from '../../shared/images/cr-map';
import loadGoogleMapsAPI from 'load-google-maps-api';

import Layout from '../shared/internal-layout';
import MenuInternal from '../shared/menu-internal';

const MAP_STYLES = {
  height: '450px',
  width: '100%'
}
const API_CONFIG = {
  key: 'AIzaSyCz47Qy5GcQksCDckAMWxI4QC0nvYSSqgU',
  language: 'es'
}

export default observer(['store'], React.createClass({
  displayName: 'Locations',
  getInitialState() {
    return {
      provSelected: ''
    }
  },
  componentWillUnmount() {
    // limpiando despues el component ya no es usado
    // evita errores en la console
    const allScripts = document.getElementsByTagName('script');
    // recopilar todos los scripts,
    // filtrar los que contengan la key en 'src'
    // eliminarlo
    [].filter.call(
      allScripts,
      (scpt) => scpt.src.indexOf(`key=${API_CONFIG.key}`) >= 0
    )[0].remove();
    // resetear la variable de Google
    window.google = {};
  },
  componentWillMount() {
    this.translation = this.props.store.translation;
    this.header = this.props.store.header;
    const OPTIONS = {
      center: {
        lat: 9.935697,
        lng: -84.1483647
      },
      zoom: 8
    };
  },
  getLocations() {
    const slug = _.get(this.props.params, 'location');
    const location = _.find(this.translation.t('pages.locations.sections'), { slug });

    if (slug && !location) {
      browserHistory.push('/404');
    }
    const locations = (location) ? [location] : this.translation.t('pages.locations.sections');

    return _.map(locations, (location, index) => {
      const { title, address, phone, fax, waze_url, province, article, slug, coordenadas, schedule } = location;
      let insertFax = null;
      let insertArticle = null;

      if (!_.isEmpty(fax)) {
        insertFax =
          <p className="location-fax"><span>{`${this.translation.t('pages.locations.fax')}: `}</span>{fax}</p>;
      }

      if (!_.isEmpty(article)) {
        insertArticle = <div dangerouslySetInnerHTML={{ __html: article }} />;
      }

      return (
        <div key={index} id={index}
          className={classNames(province, { 'active': _.isEqual(province, this.state.provSelected) })}>
          <h3 className="location-name">
            <Link className="c-green" to={`${this.translation.t('pages.locations.linkSection')}/${slug}`}>{title}</Link>
          </h3>
          <p className="location-address"><span>{`${this.translation.t('pages.locations.address')}: `}</span>{address}
          </p>
          <p className="location-phone"><span>{`${this.translation.t('pages.locations.phone')}: `}</span>{phone}</p>
          {insertFax}
          <p className="location-horario"><span>{`${this.translation.t('pages.locations.horario')}: `}</span>{schedule}</p>
          <div className="btn-container">
            <Link to="/contact-us" className="btn revert">{this.translation.t('cta.contact_us')}</Link>
            <Button onClick={() => this._showMap(waze_url)}
              className="btn revert">{this.translation.t('cta.waze')}</Button>
          </div>
          {insertArticle}
        </div>
      );
    });
  },
  getSEO() {
    const slug = _.get(this.props.params, 'location');
    const locationSEO = _
      .chain(this.translation.t('pages.locations.sections'))
      .find({ slug })
      .get('seo')
      .value();

    return (locationSEO) ? locationSEO : this.translation.t('pages.locations.seo');

  },
  getTitleLocation() {
    const slug = _.get(this.props.params, 'location');
    const locationTitle = _
      .chain(this.translation.t('pages.locations.sections'))
      .find({ slug })
      .get('titleLocation')
      .value();

    return (locationTitle) ? locationTitle : this.translation.t('pages.locations.title');;

  },

  _showMap({ lat, lon }) {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      window.open(`waze://?z=10&ll=${lat},${lon}`, '_blank')
    } else {

    }
    window.open(`https://www.waze.com/livemap?zoom=16&lat=${lat}&lon=${lon}`, '_blank')
  },
  provSelected(provSelected) {
    if (_.get(this.props, 'params.location')) {
      browserHistory.push(`/${_.replace(this.props.route.path, / *\([^)]*\) */g, '')}`)
    }
    if (!_.isEqual(provSelected, this.state.provSelected)) {
      this.setState({
        provSelected
      });
    }
  },
  showAllLocation() {
    this.setState({
      provSelected: ''
    });
    if (_.get(this.props, 'params.location')) {
      browserHistory.push(`/${_.replace(this.props.route.path, / *\([^)]*\) */g, '')}`)
    }

  },
  render() {
    const locationList = this.getLocations();
    const phoneText = `${this.translation.t('pages.locations.phone')}`;
    const seo = this.getSEO();
    const slug = _.get(this.props.params, 'location');
    const location = _.find(this.translation.t('pages.locations.sections'), { slug });
    const pageTitle = this.getTitleLocation();

    return (
      <Layout pageTitle={pageTitle} seo={seo}>
        <MenuInternal />
        <section
          className={classNames('main-section locations', { 'filter-active': !_.isEmpty(this.state.provSelected) })}>
          <div className="container">
            <Row>
              <Col xs={12} sm={6}>
                <MapCR
                  width="308"
                  height="296"
                  className="cr-map"
                  onMapClick={this.provSelected}
                  currentSelected={this.state.provSelected}
                />
              </Col>
              <Col xs={12} sm={6}>
                <p className="call-number">{phoneText}</p>
                <button
                  className={classNames('btn btn-show-all-locations', { 'hidden': _.isEmpty(this.state.provSelected) })}
                  onClick={this.showAllLocation}>{this.translation.t('pages.locations.showLocationBtn')}</button>

                <div className="location-info">
                  {locationList}
                </div>
              </Col>
            </Row>
          </div>
        </section>
      </Layout>
    );
  }
}));
