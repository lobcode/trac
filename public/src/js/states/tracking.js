import {observable, action} from 'mobx';
import _ from 'lodash';
import q from '../q';

export default observable({
  utm: {
    utm_source: '',
    utm_medium: '',
    utm_campaign: '',
    utm_term: '',
    utm_content: '',
    date: new Date(),
    referrer: document.referrer
  },
  init: action(function getParams() {
    const href = window.location.href;
    const _this = this;

    _.mapKeys(this.utm, (val, key) => {
      const reg = new RegExp('[?&]' + key + '=([^&#]*)', 'i');
      const param = reg.exec(href);

      if (param) {
        this.utm[key] = param[1]
      }

      if (_.isEqual(key, 'utm_campaign') && !_.isEmpty(this.utm.utm_campaign)) {
        _this.saveTracking();
      }


    });

  }),
  saveTracking: action(function saveTracking() {
    let _this = this;

    q({
      method: 'GET',
      url: '/api/saveTracking',
      params: {
        tracking: _this.utm
      }
    })
  }),
		trackLead: action(function trackLead() {
      if(fbq){
        fbq('track', 'Lead');
      }
    const pixelAdWord = document.createElement('img');
    pixelAdWord.src = 'https://www.facebook.com/tr?id=852681510&ev=Lead&noscript=1"';
  })
});
