import {observable, action} from 'mobx';
import _ from 'lodash';
import axios from 'axios';
import {parseString} from 'xml2js';
import {AGENT} from '../shared/constants';
import {history} from 'react-router';
import {isEmail} from 'validator';

import translation from '../../../../shared/translation';

import q from '../q';

export default observable({
	information: {
		dateFrom: '',
		 timeFrom: '', 
		dateTo: '', 
		timeTo: '', 
		pickupStation: '',
		pickupStationName: '',
		returnStation: '',
		returnStationName: '',
		customerFullName: '',
		customerEmail: '',
		customerPhone: '',
		landingName: '',
		vehicle: '',
		insurance: '',
		driverLicenceNumber: '',
		offerType: ''
	},
	disableSubmit: false,
	errorType: '',
	isTypeCarReserve: false,
	isOfferType: false,
	cars: [],
	showSelectedCarModal: false,
	showLoading: false,
	// Computeds
	get parameters() {
		const {customerFullName, customerPhone, customerEmail, dateFrom, timeFrom, pickupStationName, dateTo, timeTo, returnStationName, landingName, vehicle, insurance, driverLicenceNumber, offerType} = this.information;

		const {language} = translation;
		const {isTypeCarReserve, isOfferType} = this;


		return {
			customerFullName, customerPhone, customerEmail, dateFrom, timeFrom, pickupStationName, dateTo, timeTo, returnStationName, landingName, vehicle, insurance, driverLicenceNumber, language, isTypeCarReserve, isOfferType, offerType
		}
	},
	get isValid() {
		// >> Valid Required Fields <<
		// Note: reservedThrough, typeOfIssue and location contains items with value and code properties which
		// are not part of the valid items in the drop downs, those are used for placeholder purposes.
		if (
			_.isEmpty(this.information.dateFrom) ||
			_.isEmpty(this.information.timeFrom) ||
			_.isEmpty(this.information.dateTo) ||
			_.isEmpty(this.information.timeTo) ||
			_.isEmpty(this.information.pickupStation) ||
			_.isEmpty(this.information.returnStation) ||
			_.isEmpty(this.information.vehicle) ||
			_.isEmpty(this.information.customerFullName) ||
			_.isEmpty(this.information.customerEmail)) {
			return false;
		}

		if (
			this.isTypeCarReserve &&
			_.isEmpty(this.information.insurance) &&
			_.isEmpty(this.information.driverLicenceNumber)
		) {
			return false;
		}

		if (!isEmail(this.information.customerEmail)) {
			return false;
		}

		return true;
	},

	clearReservation: action(function clearReservation() {
		this.information.dateFrom = '';
		this.information.timeFrom = '';
		this.information.dateTo = '';
		this.information.timeTo = '';
		this.information.pickupStation = '';
		this.information.pickupStationName = '';
		this.information.returnStation = '';
		this.information.returnStationName = '';
		this.information.customerFullName = '';
		this.information.customerEmail = '';
		this.information.customerPhone = '';
		this.information.landingName = '';
		this.information.vehicle = '';
		this.information.insurance = '';
		this.information.driverLicenceNumber = '';
		this.information.offerType = '';
		this.cars = [];
		this.errorType = '';
		this.isTypeCarReserve = false;
		this.isOfferType = false;
	}),
	isParam: action(function isParam(key) {
		const href = window.location.href;
		const reg = new RegExp( '[?&]' + key + '=([^&#]*)', 'i' );
		const param = reg.exec(href);

		return param ? param[1] : null;
	}),
	//Check from URL params if comes a customerId in order to set the user and agent code
	isCustomer: action(function isCustomer() {
		const href = window.location.href;
		const key = 'v';
		const reg = new RegExp( '[?&]' + key + '=([^&#]*)', 'i' );
		const param = reg.exec(href);
		const paramValue = param ? param[1] : null;

		if (param) {
			this.isOfferType = true;
			switch (paramValue) {
				case 'x':
					this.information.offerType = '7x5';
					break;
				case 'y':
					this.information.offerType = '4x3';
					break;
				case 'z':
					this.information.offerType = 'Special Offer';
					break;
				default:
					this.information.offerType = `${paramValue}`;
					this.information.landingName = `Promo ${paramValue}`;

					break;
			}
		}

	}),
	sendEmail: action(function sendEmail() {
		if (this.isValid) {
			this.disableSubmit = true;
			return axios.post('/api/reserve/email/', this.parameters);
		} else {
			return Promise.reject("Error");
		}
	})
});
