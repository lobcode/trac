import {observable, action} from 'mobx';

export default observable({
	isLogin: true,
	pageTitle: '',
	pageSubTitle: '',
	setIsLogin: action((isLogin) => this.isLogin = isLogin)
});
