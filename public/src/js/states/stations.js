import {observable, action} from 'mobx';
import _ from 'lodash';
import axios from 'axios';
import {parseString} from 'xml2js';
import {AGENT} from '../shared/constants';

import q from '../q';

export default observable({
	stations: [],
	get getStationList() {
		return this.stations.toJS();
	},
	// Actions
	getStations: action(function getStations() {
		axios.get('https://endpoint.wheelsys.io/10235/link/v3/stations_wbstyr567.html', {
			params: {
				AGENT
			}
		})
			.then(({data}) => {
				parseString(data, function (err, result) {

					_this.stations = _.reduce(result.response.station, (station, {$: {code, name}}) => {
						//code 0 === Administration
						if (code !== '0') {
							station.push({
								code,
								name: _.lowerCase(name)
							})
						}

						return station;

					}, []);

				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	getMockStations: action(function getMockStations() {
		let _this = this;
		axios.get('/api/stations')
			.then(({data}) => {
				parseString(data, function (err, result) {

					_this.stations = _.reduce(result.response.station, (station, {$: {code, name}}) => {
						//code 0 === Administration
						if (code !== '0') {
							station.push({
								code,
								name: _.lowerCase(name)
							})
						}

						return station;

					}, []);
				});
			})
			.catch(function (error) {
				console.log(error);
			});

	}),
	//Local Request
	loadStations: action(function loadStations() {
		q({
			method: 'GET',
			url:'/api/stations/loadStations'
		})
			.then(({data}) => {
				this.stations = data;
			});
	})
});
