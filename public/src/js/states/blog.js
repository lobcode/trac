import {observable, action} from 'mobx';
import _ from 'lodash';
import axios from 'axios';

import metaTags from '../states/metaTags';

export default observable({
	posts: [],
	post: null,
	isFetching: true,
	getPosts: action(function getPosts() {
		const _this = this;
		axios.post('/api/blog/all')
			.then((result) =>{
				_this.posts = result.data;
			})
	}),
	getPost: action(function getPost(slug, lang) {
		const _this = this;
		axios.post(`/api/blog/post/${slug}`,{lang})
			.then((result) =>{
				_this.isFetching =  false;
				_this.post = result.data;
				if(_this.post && _this.post[lang]){
					if(_this.post[lang].seo && _this.post[lang].seo.title) {
						metaTags.title = _this.post[lang].seo.title;
					}else{
						metaTags.title = _this.post[lang].titulo;
					}
					if(_this.post[lang].seo && _this.post[lang].seo.description) metaTags.description = _this.post[lang].seo.description;
				}
			})
	})	
});
