import {observable, action} from 'mobx';
import axios from 'axios';
import _ from 'lodash';

export default observable({
	// Observables
	sitekey: '',
	recaptcha: '',
	success: false,
	// Actions
	getSiteKey: action(function getSiteKey() {
		axios.get('/api/google/sitekey')
			.then(({data}) => {
				this.sitekey = data;
			})
			.catch((err) => {
				console.log(err);
				this.sitekey = '';
			});
	}),
	verify: action(function verify() {
		return axios.post('/api/google/verify', {recaptcha: this.recaptcha})
			.then(({data}) => {
				this.success = _.get(data, 'success', false);
			});
	})
});
