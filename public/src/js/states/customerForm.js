import {observable, action} from 'mobx';
import axios from 'axios';
import _ from 'lodash';
import {isEmail} from 'validator';

import translation from '../../../../shared/translation';
import google from '../states/google';
import tracking from '../states/tracking';

export default observable({
	// Observables
	fullName: '',
	email: '',
	telephone: '',
	ext: '',
	reservedThrough: {},
	location: {},
	rentalAgreementNumber: '',
	confirmation: '',
	perksNumber: '',
	typeOfIssue: {},
	subject: '',
	message: '',
	recaptcha: {},
	utms: '',
	// Computes
	get parameters() {
		const {fullName, email, telephone, ext, reservedThrough, location, rentalAgreementNumber, confirmation,
			perksNumber, typeOfIssue, subject, message} = this;

		const {language} = translation;

		return {
			fullName,
			email,
			telephone,
			ext,
			reservedThrough,
			location,
			rentalAgreementNumber,
			confirmation,
			perksNumber,
			typeOfIssue,
			subject,
			message,
			language,
			utm: tracking.utm
		}

		
	},
	get isValid() {
		// >> Valid Required Fields <<

		// Note: reservedThrough, typeOfIssue and location contains items with value and code properties which
		// are not part of the valid items in the drop downs, those are used for placeholder purposes.
		if (_.isEmpty(this.fullName) || _.isEmpty(this.email) ||
			_.isEqual(_.get(this.reservedThrough, 'value'), -1) || _.isEqual(_.get(this.location, 'code'), '-1') ||
			_.isEqual(_.get(this.typeOfIssue, 'value'), -1) || _.isEmpty(this.subject)) {
			return false;
		}

		if (!isEmail(this.email)) {
			return false;
		}

		// Google ReCaptcha validation
		if (!google.success) {
			return false;
		}

		return true;
	},
	// Action
	cleanForm: action(function cleanForm() {
		this.fullName = '';
		this.email = '';
		this.telephone = '';
		this.ext = '';
		this.reservedThrough = '';
		this.location = '';
		this.rentalAgreementNumber = '';
		this.confirmation = '';
		this.perksNumber = '';
		this.typeOfIssue = '';
		this.subject = '';
		this.message = '';
	}),
	sendEmail: action(function sendEmail() {
		if (this.isValid) {
			return axios.post('/api/customer-form/email', this.parameters);
		} else {
			return Promise.reject("Error");
		}
	})
});
