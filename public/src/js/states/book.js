import {observable, action} from 'mobx';
import _ from 'lodash';
import numeral from 'numeral';
import axios from 'axios';
import {parseString} from 'xml2js';
import {AGENT} from '../shared/constants';
import {history} from 'react-router';
import Moment from 'moment';
import {isEmail} from 'validator';
import md5 from 'md5';
import crypto from 'crypto';

import translation from '../../../../shared/translation';
import locations from '../../../../shared/locations';

import q from '../q';

import groupsCarList from '../states/groupsCarList';
import extraOptionList from '../states/extraOptionList';
import stations from '../states/stations';

const PAY_KEY = '8D8f4dzJ34AJUasyHG8f7tGx846Tt6SJ';//yXn2Ad6Zh42KS45KB6bYEaQv8hba5Jxc
const PAY_KEY_ID = '7892096';//7757363
const Time = Moment().unix();

export default observable({
	booking: {
		dateFrom: '',
		 timeFrom: '', 
		dateTo: '', 
		timeTo: '', 
		stationSameActive: false,
		pickupStation: '',
		pickupStationName: '',
		returnStation: '',
		returnStationName: '',
		id: '',
		group: '',
		model: '',
		code: '',
		carOptionsRate: [],
		duration: '',
		totalrate: 0,
		outOfOurs: '',
		delivery: '',
		oneway: '',
		baserate: '',
		pickupInfo: '',
		returnInfo: '',
		customerName: '',
		customerLastName: '',
		customerEmail: '',
		customerPhone: '',
		voucherno: '',
		country: '',
		city: '',
		zipcode: '',
		address: '',
		airline: '',
		flight: '',
		options: [],
		noInsurance: false,
		optionsName: [],
		step: 1,
		refno: '',
		irn: '',
		utm: {},
		paymentMethod: '',
		paymentInfo : {
			ccnumber: '',
			ccexp: '',
			checkname: '',
			cvv: '',
			checkaba: '',
			checkaccount: '',
			accountHolderType: '',
			accountType: 'sale',
			authcode: '',
			transactionId: '',
			orderId: '',
			response: '',
			status: '',
			avsResponse: '',
			cvvResponse: '',
			ipAddress: '',
		},
		userCode: 'wbstyr567',
		agentCode: 'TYRW001'
	},
	errorType: '',
	cars: [],
	locationsPickUp: [],
	locationsDropOff: [],
	bookingActive: false,
	bookingSidebarOpen: false,
	showSelectedCarModal: false,
	showLoading: false,
	// Computeds
	get parameters() {
		const {customerName, customerLastName, customerPhone, customerEmail, country, city, zipcode, address,
			airline, flight, dateFrom, timeFrom, pickupStationName, dateTo, timeTo, returnStationName, duration,
			model, optionsName, agentCode, refno, noInsurance, paymentMethod, irn} = this.booking;

		const {language} = translation;
		const remarks = this.booking.paymentInfo.transactionId;
		const {checkname, cvv, ipAddress} = this.booking.paymentInfo;
		const ccnumber = this.ccNumber;
		const remarkMessage = this.remarksMessage;

		const baserate = Math.round(this.booking.baserate * 100) / 100;
		const totalrate = Math.round(this.booking.totalrate * 100) / 100;
		
		return {
			customerName, customerLastName, customerPhone, customerEmail, country, city, zipcode, address,
			airline, flight, dateFrom, timeFrom, pickupStationName, dateTo, timeTo, returnStationName, duration,
			model, baserate, totalrate, optionsName, agentCode, refno, paymentMethod, language, remarks, remarkMessage, noInsurance, checkname,
			ccnumber, irn, cvv, ipAddress
		}
	},
	get ccNumber() {
		const ccNumber = this.booking.paymentInfo.ccnumber;
		return `**** **** **** ${ccNumber.substr(ccNumber.length - 4)}`;
	},
	get selectedOptions() {
		const optionsFormatted = _.transform(this.booking.options.toJS(), (result, item) => {
			if (_.isEqual(item, 'E001')) {
				result.push('A001=1&A002=1');
			} else {
				result.push(`${item}=1`);
			}

		}, []);

		if (!_.isEmpty(this.booking.paymentInfo.transactionId)) {
			optionsFormatted.push('H002=1&H004=1');
		}

		return  optionsFormatted.join('&');
	},
	get getSelectedCar() {
		if(this.isSelectedCarAvailable) {
			return _.find(this.cars, {'code': this.booking.code});
		} else {
			return _.find(groupsCarList.cars, {'code': this.booking.code});
		}
	},
	get isSelectedCarAvailable() {
		const car = _.find(this.cars, {'code': this.booking.code});
		return !_.isUndefined(car);
	},
	get isDifferentLocation() {
		return !_.isEmpty(this.booking.pickupStation)
			&& !_.isEmpty(this.booking.returnStation)
			&& !_.isEqual(this.booking.pickupStation, this.booking.returnStation);
	},
	get isValid() {
		// >> Valid Required Fields <<
		// Note: reservedThrough, typeOfIssue and location contains items with value and code properties which
		// are not part of the valid items in the drop downs, those are used for placeholder purposes.
		if (_.isEmpty(this.booking.userCode) ||
			_.isEmpty(this.booking.agentCode) ||
			_.isEmpty(this.booking.dateFrom) ||
			_.isEmpty(this.booking.timeFrom) ||
			_.isEmpty(this.booking.dateTo) ||
			_.isEmpty(this.booking.timeTo) ||
			_.isEmpty(this.booking.pickupStation) ||
			_.isEmpty(this.booking.returnStation) ||
			_.isEmpty(this.booking.code) ||
			_.isEmpty(this.booking.customerName) ||
			_.isEmpty(this.booking.customerLastName) ||
			_.isEmpty(this.booking.customerPhone) ||
			_.isEmpty(this.booking.customerEmail)) {
			return false;
		}

		if (!isEmail(this.booking.customerEmail)) {
			return false;
		}

		return true;
	},
	get isBookStep1Valid() {
		if (
			_.isEmpty(this.booking.dateFrom) ||
			_.isEmpty(this.booking.timeFrom) ||
			_.isEmpty(this.booking.dateTo) ||
			_.isEmpty(this.booking.timeTo) ||
			_.isEmpty(this.booking.pickupStation) ||
			_.isEmpty(this.booking.returnStation)
		) {
			return false;
		}

		return true;
	},
	get isBookStepDateValid() {
		if (
			_.isEmpty(this.booking.dateFrom) ||
			_.isEmpty(this.booking.dateTo) ||
			_.isEmpty(this.booking.timeTo) ||
			_.isEmpty(this.booking.timeFrom)			
		) {
			return false;
		}

		const stationLang = translation.t('pages.locations.code');
		const timeTo = 	this._getFormattedTime(this.booking.timeTo);
		const timeFrom = 	this._getFormattedTime(this.booking.timeFrom);

		// Clear
		this.booking.locationsDropOff = [];
		this.booking.locationsPickUp = [];
		// this.booking.pickupStation = '';
		// this.booking.pickupStationName = '';
		// this.booking.returnStation = '';
		// this.booking.returnStationName= ''	;
		
		_.transform(stations.getStationList, (result, {code}) => {
			let l = locations[code];

		// Desde
		if(timeFrom  >= l.horario.abren && timeFrom  <= l.horario.cierran){
			this.booking.locationsPickUp.push({
				code,
				name: _.get(stationLang, code)
			});
	}

		// Hasta
		if(timeTo  <= l.horario.cierran){
			this.booking.locationsDropOff.push({
					code,
					name: _.get(stationLang, code)
			});
		}

		}, []);

		return true;
	},	
	get isMoreThanDay() {
		const hourFrom = Moment(new Date(`01/01/2007  ${this.booking.timeFrom}`));
		const hourTo = Moment(new Date(`01/01/2007  ${this.booking.timeTo}`));

		return hourTo.diff(hourFrom, 'hours') >= 4;

	},
	get guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
	},
	get hash() {
		const orderId = this.booking.paymentInfo.orderId;
		const amount = numeral(this.booking.totalrate).format('0.00');

		//orderid|amount|time|Key
		return crypto.createHash('md5').update(`${orderId}|${amount}|${Time}|${PAY_KEY}`).digest("hex");
	},
	get isPaymentInfoValid() {
		if (_.isEqual(this.booking.paymentMethod, 'now') &&
			_.isEmpty(this.booking.paymentInfo.checkname) ||
			_.isEmpty(this.booking.paymentInfo.ccnumber) ||
			_.isEmpty(this.booking.paymentInfo.ccexp) ||
			_.isEmpty(this.booking.paymentInfo.cvv) ||
			_.size(this.booking.paymentInfo.ccexp) < 4) {
			return false;
		}

		return true;
	},
	get remarksMessage() {
		const date = Moment(new Date).format('DD-MM-YYYY');
		const transactionID = this.booking.paymentInfo.transactionId;
		const checkName = this.booking.paymentInfo.checkname;
		const ccNumber = this.booking.paymentInfo.ccnumber;
		const ccNumberFirst = ccNumber.substring(0,4);
		const ccNumberLast = ccNumber.substr(ccNumber.length - 4);
		const ccExp = this.booking.paymentInfo.ccexp;
		return `Pago realizado en Linea desde TRAC, fecha de transaccion: ${date}, numero de comprobante: ${transactionID}, nombre en tarjeta de credito: ${checkName}, ${ccNumberFirst} **** **** ${ccNumberLast}, fecha de expiracion de la tarjeta de credito: ${ccExp}.`
	},
	get ratePerDay() {
		return (Number(this.booking.baserate) / this.booking.duration);
	},
	get totalRate() {

	},
	
	getStationList() {
		const stationLang = translation.t('pages.locations.code');
		const timeTo = 	this._getFormattedTime(this.booking.timeTo);
		const timeFrom = 	this._getFormattedTime(this.booking.timeFrom);

		this.booking.locationsDropOff = [];
		this.booking.locationsPickUp = [];

		_.transform(stations.getStationList, (result, {code}) => {
			let l = locations[code];

		// locations pick up
		if(timeTo  <= l.horario.cierran){
			this.booking.locationsDropOff.push({
					code,
					name: _.get(stationLang, code)
			});
		}

		if(timeFrom  >= l.horario.abren){
				this.booking.locationsPickUp.push({
					code,
					name: _.get(stationLang, code)
				});
		}

			
		}, []);
	},			
	// Actions
	updateTotalRate(mont) {
		this.booking.totalrate = Math.round((this.booking.baserate + mont) * 100) / 100;
		},
		_getFormattedTime(value) {
			return Moment(value, 'HH:mm').hour();
		},
	clearReservation: action(function clearReservation() {
		this.booking.dateFrom = '';
		this.booking.timeFrom = '';
		this.booking.dateTo = '';
		this.booking.timeTo = '';
		this.booking.pickupStation = '';
		this.booking.pickupStationName = '';
		this.booking.returnStation = '';
		this.booking.returnStationName = '';
		this.booking.id = '';
		this.booking.group = '';
		this.booking.model = '';
		this.booking.code = '';
		this.booking.duration = '';
		this.booking.totalrate = '';
		this.booking.outOfOurs = '';
		this.booking.delivery = '';
		this.booking.oneway = '';
		this.booking.baserate = '';
		this.booking.pickupInfo = '';
		this.booking.returnInfo = '';
		this.booking.customerName = '';
		this.booking.customerLastName = '';
		this.booking.customerEmail = '';
		this.booking.customerPhone = '';
		this.booking.voucherno = '';
		this.booking.country = '';
		this.booking.city = '';
		this.booking.zipcode = '';
		this.booking.address = '';
		this.booking.airline = '';
		this.booking.flight = '';
		this.booking.options = [];
		this.booking.optionsName = [];
		this.booking.step = 1;
		this.booking.noInsurance = false;
		this.booking.refno = '';
		this.booking.irn = '';
		this.booking.utm = {};
		this.booking.paymentMethod = '';
		this.booking.paymentInfo = {
			ccnumber: '',
			ccexp: '',
			checkname: '',
			checkaba: '',
			cvv: '',
			checkaccount: '',
			accountHolderType: '',
			accountType: '',
			authcode: '',
			transactionId: '',
			orderId: '',
			response: '',
			status: '',
			avsResponse: '',
			cvvResponse: '',
			ipAddress: ''
		};
		this.bookingActive = false;
		this.cars = [];
		this.errorType = '';
	}),
	newReservation: action(function newReservation() {
		//Generate a number based on date
		const voucherNo = Moment().valueOf();
		this.showLoading = true;
		return q({
			method: 'GET',
			url:'/api/book/newReservation/',
			params: {
				userCode: this.booking.userCode,
				agentCode: this.booking.agentCode,
				dateFrom: this.booking.dateFrom,
				timeFrom: this.booking.timeFrom,
				dateTo: this.booking.dateTo,
				timeTo: this.booking.timeTo,
				pickupStation: this.booking.pickupStation,
				returnStation: this.booking.returnStation,
				group: this.booking.code,
				customerName: `${this.booking.customerName} ${this.booking.customerLastName}`,
				customerEmail: this.booking.customerEmail,
				voucherNo: voucherNo,
				optionCode:  !_.isEmpty(this.booking.options) ? this.selectedOptions : '',
				remarks: !_.isEmpty(this.booking.paymentInfo.transactionId) ? this.remarksMessage : ''
			}
		});
	}),
	payReservation: action(function payReservation() {
		this.showLoading = true;
		// OrdeId se cambio por book.booking.refno para que puedan identificar de donde venia
		this.booking.paymentInfo.orderId = _.isEmpty(this.booking.refno) ? this.guid : this.booking.refno;
		const redirect = window.location.href;
		const orderdescription = `Nombre: ${this.booking.paymentInfo.checkname} | CVV: ${this.booking.paymentInfo.cvv} | IRN= ${this.booking.irn}`;
		return q({
			method: 'GET',
			url: '/api/book/payReservation/',
			params: {
				type: this.booking.paymentInfo.accountType,
				key_id: PAY_KEY_ID,
				hash: this.hash,
				time: `${Time}`,
				orderid: this.booking.paymentInfo.orderId,
				ccnumber: this.booking.paymentInfo.ccnumber,
				ccexp: this.booking.paymentInfo.ccexp,
				checkname: this.booking.paymentInfo.checkname,
				amount: numeral(this.booking.totalrate).format('0.00'),
				orderdescription,
				address1: this.booking.paymentInfo.ipAddress,
				redirect
			}
		});
	}),
	cancelPayReservation: action(function payReservation() {
    const redirect = window.location.href;
    return q({
      method: 'GET',
      url: '/api/book/cancelPayReservation/',
      params: {
        type: this.booking.paymentInfo.accountType,
        key_id: PAY_KEY_ID,
        hash: this.hash,
        time: `${Time}`,
        transactionId: this.booking.paymentInfo.transactionId,
        redirect
      }
    });
  }),
	getAvailableCars: action(function getAvailableCars() {
		let _this = this;
		this.showLoading = true;
		this.b

		q({
			method: 'GET',
			url:'/api/book/priceQuote/',
			params: {
				userCode: this.booking.userCode,
				agentCode: this.booking.agentCode,
				dateFrom: this.booking.dateFrom,
				timeFrom: this.booking.timeFrom,
				dateTo: this.booking.dateTo,
				timeTo: this.booking.timeTo,
				pickupStation: this.booking.pickupStation,
				returnStation: this.booking.returnStation
			}
		})
			.then(({data}) => {

				this.showLoading = false;
				
				parseString(data, function (err, data) {
					//Check API Errors
					if(data.pricequote && data.pricequote.errors && data.pricequote.errors.length && !_.isEmpty(data.pricequote.errors[0])) {
						_this.errorType = data.pricequote.errors[0].error[0].$.code;
					}

					//Get Available cars
					_this.cars = _.chain(data.pricequote.rates[0].category)
						.flattenDeep()
						.transform((result, car) => {
							// Verifico el estado
							if (car['$'].availability ==='AVAILABLE') {
								const carDetail = _.find(groupsCarList.cars, {code: car['$'].code});

								
								const options = _.chain(car)
									.get('options[0].option')
									.transform((result, {$}) => {
										const option = _.find(extraOptionList.options, {code: $.code});
										if (option) {
											result.push({
												rate: $.rate,
												...option
											});
										}

									}, [])
									.value();


								// Option E001 requires rate of A001 + A002
								const optionE001 = _.find(options, {code: 'E001'});
								const optionA001 = _.find(options, {code: 'A001'});
								const optionA002 = _.find(options, {code: 'A002'});
								const optionH002 = _.find(options, {code: 'H002'});
								const optionH004 = _.find(options, {code: 'H004'});

								optionE001['rate'] = _.chain(options)
									.reduce((result, option) => {
										if (option.code === 'A001' || option.code === 'A002') {
											result.push(Number(option.rate));
										}
										return result;
									}, [])
									.sum()
									.value();

								// const rateBasePerDay = (Number(car['$'].baserate) / 100) / data.pricequote.$.duration;
								// const rateTotalPerDay = (Number(car['$'].totalrate) / 100) / data.pricequote.$.duration;

								// El resultado dará el total de Tiempo y Kilometraje, luego es necesario dividir ese monto 
								let checkOptionH004 = (Number(_.get(optionH004, 'rate', 0)));					
								// Fix de dato erroneo
								if(checkOptionH004 === 1094) checkOptionH004 = checkOptionH004 - 99;

								let rateTotalPerDay = (
									(Number(car['$'].baserate)) - 
									// Seguros
									// (Number(_.get(optionA001, 'rate', 0))) - 
									// (Number(_.get(optionA002, 'rate', 0))) + 
									// (Number(_.get(optionH002, 'rate', 0)))  - 
									// H004 deberia ser 1.99
									checkOptionH004
								);

								// let rateTotalPerDay = (Number(car['$'].baserate)) ;

								// if (_this.booking.pickupStation == 3 || _this.booking.returnStation == 5 || _this.booking.pickupStation == 5 || _this.booking.returnStation == 5) {
								// 	//Apply 10% over total cost on Alajuela and Liberia stations
								// 	rateTotalPerDay =  rateTotalPerDay - (rateTotalPerDay * 0.10);
								// }

								let rateBasePerDay = (rateTotalPerDay / data.pricequote.$.duration) / 100;

								// Redondeo para q se note mas limpio el precio
								rateBasePerDay = numeral(rateBasePerDay).format('0');
								rateTotalPerDay = numeral(rateTotalPerDay).format('0');

								// console.log(' ');
								// console.log(carDetail.name, ' ');
								// console.log(' ');

								// console.log(rateTotalPerDay, 'RateTotalPerDay API 3');
								// console.log(rateTotalPerDay / data.pricequote.$.duration, 'Por dia API');

								// console.log(car, 'car');
								// console.log(carDetail, 'carDetail');
								// console.log(options, 'options');

								// console.log(' ');
								// console.log('Options');
								// console.log(' ');

								// console.log(Number(_.get(optionA001, 'rate', 0)),  'A001 CDW - Seguro obligatorio');
								// console.log(Number(_.get(optionA002, 'rate', 0)), 'A002 LDW - Seguro obligatorio');								
								// console.log(Number(_.get(optionH002, 'rate', 0)), 'H002 - Green Heart Fee (LPF)');
								// console.log(checkOptionH004 , 'checkOptionH004');									

								
								
								// console.log(' ');
								// console.log('Precios a mostrar');
								// console.log(' ');

								// console.log(rateBasePerDay, 'Precio normal');
								// console.log(rateBasePerDay, 'rateBasePerDay');
								// console.log(rateTotalPerDay, 'rateTotalPerDay');



								const duration = _this.isMoreThanDay ?
									`${Number(data.pricequote.$.duration) + 1}` :
									data.pricequote.$.duration;

								// model: car['$'].model,
								result.push({
									optionList: options,
									baserate: rateBasePerDay * duration,
									totalrate: rateTotalPerDay * duration,
									model: carDetail.name,
										...carDetail
								});

							}
						}, [])
						.value();

					if(!_.isUndefined(data.pricequote.$)) {
						_this.booking.id = data.pricequote.$.id;
						// Verify if diff of hours are more than 4 hrs
						// More than 4hrs = 1 day
						_this.booking.duration = _this.isMoreThanDay ?
							`${Number(data.pricequote.$.duration) + 1}` :
							data.pricequote.$.duration;
					}

				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	//Check from URL params if comes a customerId in order to set the user and agent code
	isCustomer: action(function isCustomer() {
		let _this = this;
		const href = window.location.href;
		const key = 'customerId';
		const reg = new RegExp( '[?&]' + key + '=([^&#]*)', 'i' );
		const param = reg.exec(href);

		if (param) {
			q({
				method: 'GET',
				url:'/api/customer',
				params: {
					customerId: Number(param[1])
				}
			})
				.then(({data}) => {
					if(_.size(data)) {
						_this.booking.userCode = data.code;
						_this.booking.agentCode = data.agency;
					}
				})
				.catch(function (err) {
					console.log(err);
				});
		}
	}),
	saveReservation: action(function isCustomer() {
		let _this = this;
		const reservation = _.extend(_this.booking, {
			date: new Date()
		});

		q({
			method: 'GET',
			url:'/api/book/save/',
			params: {
				reservation
			}
		})
	}),
	sendEmail: action(function sendEmail() {
		if (this.isValid) {
			return axios.post('/api/book/email/', this.parameters);
		} else {
			return Promise.reject("Error");
		}
	}),
	sendEmailPay: action(function sendEmailPay() {
			if (this.isValid) {
					return axios.post('/api/book/email/pay', this.parameters);
			} else {
					return Promise.reject("Error");
			}
	})
});
