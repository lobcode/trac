import {observable, action} from 'mobx';
import _ from 'lodash';
import axios from 'axios';
import {parseString} from 'xml2js';
import {AGENT} from '../shared/constants';

import q from '../q';

export default observable({
	cars: [],
	get getCarsList() {
		return this.cars.toJS();
	},
	get getCarsListFleet() {
		const cars = this.cars.toJS();
		return  cars.filter(car=> car.fleetHome === true);
	},
	get getCarsCategories() {
		const cars = this.cars.toJS();
		return  _.chain(cars).map('category').uniq().value()
	},

	// Actions
	getCars: action(function getCars() {
		q({
			method: 'GET',
			url:'/api/cars/groups/',
			params: {
				userCode: 'wbstyr567',
				agentCode: 'TYRW001'
			}
		})
			.then(({data}) => {
				console.log(data);
			});

	}),
	getMockCars: action(function getMockCars() {
		let _this = this;
		axios.get('/api/groups')
			.then(({data}) => {
				parseString(data, function (err, result) {
					_this.cars = _.map(result.response.cargroup, (car) => {
						return {
							...car['$']
						}
					});
				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	//Local Request
	loadCars: action(function loadCars() {
		q({
			method: 'GET',
			url:'/api/cars/loadCars'
		})
		.then(({data}) => {
			this.cars = data;
		});

	})
});
