import {observable, action} from 'mobx';
import _ from 'lodash';
import axios from 'axios';
import {parseString} from 'xml2js';
import {AGENT} from '../shared/constants';

import q from '../q';

export default observable({
	options: [],
	get getOptionList() {
		return _.filter(this.options,['quant', true]);
	},
	get getOptionsName() {
		return _.map(this.options, 'name');
	},
	// Actions
	getOptions: action(function getOptions() {
		let _this = this;
		axios.get('https://endpoint.wheelsys.io/10235/link/v3/options_wbstyr567.html', {
			params: {
				AGENT
			}
		})
			.then(({data}) => {
				parseString(data, function (err, {response}) {

					_this.options = _.map(response.option, ({$: option}) => {
						return {
							...option
						}
					});

				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	getMockOptions: action(function getMockOptions() {
		let _this = this;
		axios.get('/api/options')
			.then(({data}) => {
				parseString(data, function (err, {response}) {

					_this.options = _.map(response.option, ({$: option}) => {
						return {
							...option
						}
					});
				});
			})
			.catch(function (error) {
				console.log(error);
			});
	}),
	//Local Request
	loadOptions: action(function loadOptions() {
		q({
			method: 'GET',
			url:'/api/options/loadOptions'
		})
			.then(({data}) => {
				this.options = data;
			});
	})
});
