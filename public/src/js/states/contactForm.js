import {observable, action} from 'mobx';
import axios from 'axios';
import _ from 'lodash';
import {isEmail} from 'validator';

import translation from '../../../../shared/translation';
import google from '../states/google';

export default observable({
	// Observables
	fullName: '',
	email: '',
	telephone: '',
	location: {},
	department: {},
	subject: '',
	message: '',
	recaptcha: {},
	// Computes
	get parameters() {
		const {fullName, email, telephone, reservedThrough, location, department, subject, message} = this;

		const {language} = translation;

		return {
			fullName,
			email,
			telephone,
			reservedThrough,
			location,
			department,
			subject,
			message,
			language
		}
	},
	get isValid() {
		// >> Valid Required Fields <<

		// Note: reservedThrough, typeOfIssue and location contains items with value and code properties which
		// are not part of the valid items in the drop downs, those are used for placeholder purposes.
		if (_.isEmpty(this.fullName) || _.isEmpty(this.email) || _.isEqual(_.get(this.location, 'code'), '-1') ||
			_.isEqual(_.get(this.department, 'value'), -1) || _.isEmpty(this.subject)) {
			return false;
		}

		if (!isEmail(this.email)) {
			return false;
		}

		// Google ReCaptcha validation
		if (!google.success) {
			return false;
		}

		return true;
	},
	// Action
	cleanForm: action(function cleanForm() {
		this.fullName = '';
		this.email = '';
		this.telephone = '';
		this.location = '';
		this.department = '';
		this.subject = '';
		this.message = '';
	}),
	sendEmail: action(function sendEmail() {
		if (this.isValid) {
			return axios.post('/api/customer-form/email', this.parameters);
		} else {
			return Promise.reject("Error");
		}
	})
});
