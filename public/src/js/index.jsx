import React from 'react';
import {render} from 'react-dom';
import _ from 'lodash';
import {Router, Route, IndexRoute, browserHistory, Switch, Redirect} from 'react-router';
import {Provider} from 'mobx-react';
import GA from 'react-ga';

import App from './layout/app';
import Invalid from './layout/invalid';
import Home from './layout/home';
import Step2 from './layout/book/step2';
import Step3 from './layout/book/step3';
import Step4 from './layout/book/step4';
import ThankYouReserve from './layout/book/thank_you_reserve';
import ThankYouPayment from './layout/book/thank_you_payment';
import Cars from './layout/cars';
import About from './layout/about';
import NoHiddenFees from './layout/no-hidden-fees';
import SpecialOffers from './layout/special-offers';
import PracticalTips from './layout/practical-tips';
import TransitLaw from './layout/transit-law';
import ProductsServices from './layout/products_services';
import Locations from './layout/locations';
import FAQ from './layout/faq';
import CustomerCare from './layout/customer-care';
import ContactUs from './layout/contact-us';
import OnLinePaymentPolicies from './layout/on-line-payment-policies';
import Awards from './layout/awards';
import NewsUpdates from './layout/news-updates';
// import IronMan from './layout/landings/iron-man';
import Promo7x5 from './layout/landings/promo-7x5';
import Bego from './layout/landings/bego';
import SantasPromociones from './layout/landings/santasPromociones';
import Hiace from './layout/landings/hiace';
import BlackBego from './layout/landings/blackNovember/bego';
import BlackYaris from './layout/landings/blackNovember/yaris';
import Envision from './layout/landings/envision';
import JungleJam from './layout/landings/jungle-jam';
import Promo from './layout/landings/promo';
import Resources from './layout/resources';

import Policy from './layout/policy';
import Interbus from './layout/interbus';
import Blog from './layout/blog/posts';
import Post from './layout/blog/post';
import store from './store';

import translation from '../../../shared/translation';

//Initialize Google Analytic2
GA.initialize('UA-32138156-1');

// Staging 112
// GA.initialize('UA-113108751-1');


function validateBook(nextState, replace) {
	const {pathname, query} = nextState.location;
	switch (pathname) {
		case '/step2':
		case '/step3':
		case '/step4':
			if (!store.book.bookingActive && !_.has(query, 'response')) {
				replace({pathname: '/'});
			}
			break;
		case '/thank-you-reserve':
		case '/thank-you-payment':

			if (!store.book.bookingActive && _.isEmpty(store.book.booking.irn)) {
				replace({pathname: '/'});
			}
			break;
	}
}

function security(nextState, replace) {
	const {pathname} = nextState.location;

	switch (pathname) {
		case '/':
		case '/signup':
		case '/login':
			if (_.size(store.auth.user) > 0) {
				replace({pathname: '/dashboard'});
			}
			break;
		case '/logout':
			store.auth.remove();
			replace({pathname: '/login'});
			window.location.reload();
			break;
		default:
			if (_.size(store.auth.user) === 0) {
				replace({pathname: '/login'});
			}
			break;
	}
}

function validateLang(nextState) {
	//Check is booking is active
	if (store.book.bookingActive) {
		//store.book.clearReservation();
	}

	const isEs = _.chain(nextState)
		.get('location.pathname')
		.split('/', 2)
		.last()
		.value();

	if (_.isEqual(isEs, 'es') && _.isEqual(translation.getLang, 'en')) {
		translation.setLanguage('es');
	} else if (!_.isEqual(isEs, 'es') && _.isEqual(translation.getLang, 'es')) {
		translation.setLanguage('en');
	}
}

function fireTracking() {
	// GA.set({ page: window.location.pathname });
	GA.pageview(window.location.pathname);
}

const enRoute = (
	<Route>
		<Route path="locations(/:location)" component={Locations} onEnter={validateLang} />
		<Route path="car-rental-costa-rica" component={Cars} onEnter={validateLang} />
		<Route path="about" component={About} onEnter={validateLang} />
		<Route path="no-hidden-fees" component={NoHiddenFees} onEnter={validateLang} />		
		<Route path="/special-offers(/:offer)" component={SpecialOffers} onEnter={validateLang} />
		<Route path="transit-law" component={TransitLaw} onEnter={validateLang} />
		<Route path="practical-tips" component={PracticalTips} onEnter={validateLang} />
		<Route path="products-services" component={ProductsServices} onEnter={validateLang} />
		<Route path="faq" component={FAQ} onEnter={validateLang} />
		<Route path="customer-care" component={CustomerCare} onEnter={validateLang} />
		<Route path="contact-us" component={ContactUs} onEnter={validateLang} />
		<Route path="online-payment-policies" component={OnLinePaymentPolicies} onEnter={validateLang} />
		<Route path="awards" component={Awards} onEnter={validateLang} />
		<Route path="news-updates" component={NewsUpdates} onEnter={validateLang} />
		{/* <Route path="ironman" component={IronMan} onEnter={validateLang} /> */}
		<Route path="7x5" component={Promo7x5} onEnter={validateLang} />
		<Route path="bego" component={Bego} onEnter={validateLang} />
		<Route path="envision" component={Envision} onEnter={validateLang} />
		<Route path="jungle-jam-2" component={JungleJam} onEnter={validateLang} />
		<Route path="promo" component={Promo} onEnter={validateLang} />
		<Route path="resources" component={Resources} onEnter={validateLang} />	
		<Route path="privacy-and-security-policy" component={Policy} onEnter={validateLang} />
		<Route path="/easter-rates" component={SantasPromociones} onEnter={validateLang} />		
		<Route path="/blog" component={Blog} onEnter={validateLang} />		
		<Route path="/blog/:slug" component={Post} onEnter={validateLang}/>  
		<Route path="/interbus" component={Interbus} onEnter={validateLang} /> 		
		{/* <Route path="/blog/categoria/:categoria" component={Blog} /> */}
		<Redirect status={301} from="/cars" to="/car-rental-costa-rica" />
		<Redirect from="/category/news_updates" to="/news-updates" />
		<Redirect from="/travel-assistant/faq" to="/faq" />
		<Redirect from="/travel-assistant/transit-law" to="/transit-law" />
	</Route> 
);

const esRoute = (
	<Route>
		<Route path="es/sin-cargos-adicionales" component={NoHiddenFees} onEnter={validateLang} />		
		<Route path="es/ubicaciones(/:location)" component={Locations} onEnter={validateLang} />
		<Route path="es/alquiler-de-carros-costa-rica" component={Cars} onEnter={validateLang} />
		<Route path="es/nosotros" component={About} onEnter={validateLang} />
		<Route path="/es/ofertas-especiales(/:offer)" component={SpecialOffers} onEnter={validateLang} />
		<Route path="es/ley-de-transito" component={TransitLaw} onEnter={validateLang} />
		<Route path="es/consejos-practicos" component={PracticalTips} onEnter={validateLang} />
		<Route path="es/productos-servicios" component={ProductsServices} onEnter={validateLang} />
		<Route path="es/preguntas-frecuentes" component={FAQ} onEnter={validateLang} />
		<Route path="es/servicio-al-cliente" component={CustomerCare} onEnter={validateLang} />
		<Route path="es/contactenos" component={ContactUs} onEnter={validateLang} />
		<Route path="es/politicas-pago-en-linea" component={OnLinePaymentPolicies} onEnter={validateLang} />
		<Route path="es/premios" component={Awards} onEnter={validateLang} />
		<Route path="es/noticias-notificaciones" component={NewsUpdates} onEnter={validateLang} />
		{/* <Route path="es/ironman" component={IronMan} onEnter={validateLang} /> */}
		<Route path="es/7x5" component={Promo7x5} onEnter={validateLang} />
		<Route path="es/envision" component={Envision} onEnter={validateLang} />
		<Route path="es/jungle-jam-2" component={JungleJam} onEnter={validateLang} />
		<Route path="es/promo" component={Promo} onEnter={validateLang} />
		<Route path="es/bego" component={Bego} onEnter={validateLang} />	
		<Route path="es/politica-de-privacidad-y-seguridad" component={Policy} onEnter={validateLang} />	
		<Route path="/es/santas-promociones" component={SantasPromociones} onEnter={validateLang} />		
		<Route path="/es/blog" component={Blog} onEnter={validateLang} />		
		<Route path="/es/blog/:slug" component={Post} onEnter={validateLang}/> 
		<Route path="/es/interbus" component={Interbus} onEnter={validateLang} /> 		
		<Redirect from="/es/vehiculos" to="/es/carros" />
		<Redirect from="/es/vehiculos/sedanes" to="/es/carros" />
		<Redirect from="/es/vehiculos/4x4" to="/es/carros" />
		<Redirect from="/es/flota-premium" to="/es/carros" />
		<Redirect from="/es/vehiculos/mini-buses" to="/es/carros" />
		<Redirect from="/es/vehiculos/pick-ups" to="/es/carros" />
		<Redirect from="/es/servicios" to="/es/productos-servicios" />
		<Redirect from="/es/servicios/servicio-de-chofer" to="/es/productos-servicios" />
		<Redirect from="/es/servicios/alquiler-y-servicio-de-chofer-por-horas" to="/es/productos-servicios" />
		<Redirect from="/es/servicios/fast-lane" to="/es/productos-servicios" />
		<Redirect from="/es/servicios/servicio-de-bodas" to="/es/productos-servicios" />
		<Redirect from="/es/servicios/combustible-prepago" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/mobile-hotspot" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/gps" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/quick-pass" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/canastas" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/sillas-de-seguridad-para-bebes-y-ninos" to="/es/productos-servicios" />
		<Redirect from="/es/accesorios-2/hieleras" to="/es/productos-servicios" />
		<Redirect from="/es/ofertas-especiales" to="/es/ofertas-especiales" />
		<Redirect from="/es/contactenos-2" to="/es/contactenos" />
		<Redirect from="/es/asistente-de-viaje" to="/es/consejos-practicos" />
		<Redirect from="/es/acerca" to="/es/nosotros" />
		<Redirect from="/es/reconocimientos" to="/es/premios" />
		<Redirect from="/es/noticias" to="/es/noticias-notificaciones" />
		<Redirect from="/es/los-doce-mejores-deportes-de-aventura-en-costa-rica" to="/es/noticias-notificaciones" />
		<Redirect from="/es/como-sobrevivir-a-las-direcciones-a-la-tica" to="/es/noticias-notificaciones" />
		<Redirect from="/es/tradiciones-de-navidad-en-costa-rica" to="/es/noticias-notificaciones" />
		<Redirect from="/asistente-de-viaje/ley-de-transito-de-costa-rica" to="/es/ley-de-transito" />
		<Redirect from="/asistente-de-viaje/preguntas-frecuentes" to="/es/preguntas-frecuentes" />
	</Route>
);

render(
	<Provider store={store}>
		<Router onUpdate={fireTracking} history={browserHistory}>
			<Route path="/" component={App} onEnter={validateLang}>,
				<IndexRoute component={Home}/>
				<Route path="home" component={Home} onEnter={validateLang} />
				<Route path="/es" component={Home} onEnter={validateLang} />
				<Route path="step2" component={Step2} onEnter={validateBook} />
				<Route path="step3" component={Step3} onEnter={validateBook} />
				<Route path="step4" component={Step4} onEnter={validateBook} />
				<Route path="thank-you-reserve" component={ThankYouReserve} onEnter={validateBook} />
				<Route path="thank-you-payment" component={ThankYouPayment} onEnter={validateBook} />
				{enRoute}
				{esRoute}
			</Route>
			<Route path="/404" status={404} component={Invalid} />
			<Route path="*" component={Invalid} />
		</Router>
	</Provider>,
	document.getElementById('trac')
);
