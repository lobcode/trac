import translation from '../../../shared/translation';
import header from './states/header';
import metaTags from './states/metaTags';
import book from './states/book';
import groupsCarList from './states/groupsCarList';
import extraOptionList from './states/extraOptionList';
import stations from './states/stations';
import google from './states/google';
import customerForm from './states/customerForm';
import tracking from './states/tracking';
import reserve from './states/reserve';
import blog from './states/blog';

export default {
	translation,
	header,
	metaTags,
	book,
	groupsCarList,
	extraOptionList,
	stations,
	google,
	customerForm,
	tracking,
	reserve,
	blog
};
