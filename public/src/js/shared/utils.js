import _ from 'lodash';
import translation from '../../../../shared/translation';

export function getPagePath(pageName) {
	const currentLang = translation.language;

	return `${_.isEqual(currentLang, 'es') ? '/es' : ''}/${translation.t(`pages.${pageName}.permalink`)}`;
}
