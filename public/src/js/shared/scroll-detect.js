import $ from 'jquery';
import _ from 'lodash';

const win = $(window);


$.fn.visible = function(partial) {

	var $t            = $(this),
		$w            = $(window),
		viewTop       = $w.scrollTop(),
		viewBottom    = viewTop + $w.height(),
		_top          = $t.offset().top,
		_bottom       = _top + $t.height(),
		compareTop    = partial === true ? _bottom : _top,
		compareBottom = partial === true ? _top : _bottom;

	return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

};

export default function onScroll(ele, elemToApplyClass, className = 'is-visible', visible = true) {
	const applyTo = (elemToApplyClass === undefined) ? ele : elemToApplyClass;
	win.scroll(() => {
		$(ele).each((i, el) => {

			if (visible) {
				if ($(el).visible(true)) {
					$(applyTo).addClass(className);
				} else {
					$(applyTo).removeClass(className);
				}
			} else {
				if (!$(el).visible(true)) {
					$(applyTo).addClass(className);
				} else {
					$(applyTo).removeClass(className);
				}
			}


		});
	});

}
