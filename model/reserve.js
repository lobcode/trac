const mongoose = require('mongoose');
const axios = require('axios');

const Schema = mongoose.Schema;

const reserveSchema = new Schema({
	id: String,
	dateFrom: String,
	timeFrom: String,
	dateTo: String,
	timeTo: String,
	pickupStation: String,
	pickupStationName: String,
	returnStation: String,
	returnStationName: String,
	group: String,
	model: String,
	code: String,
	carOptionsRate: [],
	duration: String,
	totalrate: String,
	outOfOurs: String,
	delivery: String,
	oneway: String,
	baserate: String,
	pickupInfo: String,
	returnInfo: String,
	customerName: String,
	customerLastName: String,
	customerEmail: String,
	customerPhone: String,
	voucherno: String,
	country: {},
	city: String,
	zipcode: String,
	address: String,
	airline: String,
	flight: String,
	options: [],
	optionsName: [],
	refno: String,
	irn: String,
	utm: Object,
	paymentMethod: String,
	paymentInfo : {
		ccnumber: String,
		ccexp: String,
		checkname: String,
		checkaba: String,
		checkaccount: String,
		accountHolderType: String,
		accountType: String,
		authcode: String,
		transactionId: String,
		orderId: String,
		response: String,
		status: String,
		avsResponse: String,
		cvvResponse: String
	},
	userCode: String,
	agentCode: String,
	date: Date
});
reserveSchema.statics.saveReserve = (reserveInfo) => {
	const reservation = new Reserve(JSON.parse(reserveInfo));

	new Promise((resolve, reject)=>{
		reservation.save((err, data) => {
			if (!err) {
				resolve(data);
				console.log('Reservation saved 2!');
			} else {
				reject();
				console.log(err);
			}
		});
	})
	.then((data)=>{
		var dataString = '';
		var arr = ['dateFrom', 'timeFrom', 'dateTo', 'timeTo', 'pickupStation', 'pickupStationName', 'returnStation', 'returnStationName', 'group', 'model',
		'code', 'duration', 'totalrate', 'outOfOurs', 'delivery', 'oneway', 'baserate', 'pickupInfo', 'returnInfo', 'customerName', 'customerLastName',
		'customerEmail', 'customerPhone', 'voucherno', 'country', 'city', 'zipcode', 'address', 'airline', 'flight', 'refno', 'irn', 'utm',
		'paymentMethod', 'optionsName', 'options', 'carOptionsRate'];
		var obj = {};

		// Formo array de datos para enviar a data dump
		for (item in arr) {
			let n = arr[item];
			obj[n] = data[n];
		}


		for (var key in obj) {
				if(key === 'utm'){
					for (var keyUtm in obj[key]) {
						dataString +=  keyUtm + '=' + obj[key][keyUtm] + '&';
					}
				}else if(key === 'paymentMethod'){
					var v = (obj[key] === 'later') ? 'trac-reserva-pay-later': 'trac-reserva-pay-now';
					dataString +=  'form=' + v  + '&';
				}else if(key === 'country'){
					dataString +=  'country=' + obj[key].text + '&';
				}else if(key === 'optionsName' || key === 'options' || key === 'carOptionsRate'){
					dataString +=  key + '=' + obj[key].join(', ') + '&';
				}else{
					dataString += key + '=' + obj[key] + '&';
				}
		}
		
		axios.post(`http://reymisterio.net/data-dump?${dataString}`)
			.then((result) => {
				// console.log('result', result);
			});
	})
	.catch(reason => {
		console.error(reason);
	});



};

const Reserve = mongoose.model('Reserve', reserveSchema);

module.exports = Reserve;
