const mongoose = require('mongoose');
const _ = require('lodash');

const Schema = mongoose.Schema;

const sliderSchema = new Schema({
	es:{
		type: String,
		img: String,
		landing: String,
		title: String,
		subtitle: String,
		btn_1: String,
		btn_url_1: String,
		btn_2: String,
		btn_url_2: String,
		btn_id: String,
		hiddenButton: {type: Boolean, default: false}
	},
	en:{
		type: String,
		img: String,
		landing: String,
		title: String,
		subtitle: String,
		btn_1: String,
		btn_url_1: String,
		btn_2: String,
		btn_url_2: String,
		btn_id: String,
		hiddenButton: {type: Boolean, default: false}
	},	
	// Options
	order: {type: Number},
	publicar: {type: Boolean, default: false},
	// categoria: [{ type: Schema.Types.ObjectId, ref: 'Categoria' }],
	fecha: {
		type: Date,
		default: Date.now
	}
});

const Slider = mongoose.model('Slider', sliderSchema);

module.exports = Slider;
