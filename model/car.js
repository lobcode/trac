const mongoose = require('mongoose');
const _ = require('lodash');

const Cars = require('../db/trac.json');

const Schema = mongoose.Schema;

const carSchema = new Schema({
	code: String,
	name: String,
	group: String,
	active: {type: Boolean, default: false},
	category: String,
	image: String,
	type: String,
	eco: Boolean,
	options: Array,
	specs: {
		motor: String,
		transmission: String,
		fuel: String,
		doors: String,
		seats: String,
		large_bag: String,
		small_bag: String
	}
});

carSchema.statics.loadLocalCars = () => {
	const listCars = _.get(Cars, 'cars', []);

	_.map(listCars, (carInfo) => {
		const car = new Car(carInfo);
		Car.find({code: carInfo.code}, (err, reg) => {
			if(!_.size(reg)) {
				car.save((err) => {
					if(!err) {

					}
				});
			}
		})
	});
};

const Car = mongoose.model('Car', carSchema);

module.exports = Car;
