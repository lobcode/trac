const mongoose = require('mongoose');
const _ = require('lodash');

const Schema = mongoose.Schema;

const postSchema = new Schema({
	es:{
		titulo: String,
		contenido: String,
		excerpt: String,
		slug: String,
		imagen:{
			thumbnail: String,
			full: String
		},
		seo:{
			titulo: String,
			contenido: String,
		}
	},
	en:{
		titulo: String,
		contenido: String,
		excerpt: String,
		slug: String,
		imagen:{
			thumbnail: String,
			full: String
		},
		seo:{
			titulo: String,
			contenido: String,
		}
	},	
	// Options
	publicar: {type: Boolean, default: false},
	// categoria: [{ type: Schema.Types.ObjectId, ref: 'Categoria' }],
	fecha: {
		type: Date,
		default: Date.now
	}
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post;
