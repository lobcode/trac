const mongoose = require('mongoose');
const _ = require('lodash');

const Customers = require('../db/trac.json');

const Schema = mongoose.Schema;

const customerSchema = new Schema({
	id: Number,
	code: String,
	agency: String,
	email: String
});

customerSchema.statics.loadLocalCustomers = () => {
	const listCustomers = _.get(Customers, 'customers', []);

	_.map(listCustomers, (customerInfo) => {
		const customer = new Customer(customerInfo);
		Customer.find({id: customerInfo.id}, (err, reg) => {
			if(!_.size(reg)) {
				customer.save((err) => {
					if(!err) {
						console.log('saved!');
					}
				});
			}
		})
	});
};

customerSchema.statics.getCustomer = (id, cb) => {
	return Customer.find({id});
};

const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;
