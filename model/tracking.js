const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const trackingSchema = new Schema({
	utm_source: String,
	utm_medium: String,
	utm_campaign: String,
	utm_term: String,
	utm_content: String,
	referrer: String,
	date: Date
});

trackingSchema.statics.saveTracking = (trackingInfo) => {
	const tracking = new Tracking(JSON.parse(trackingInfo));

	tracking.save((err) => {

		if (!err) {
			console.log('Tracking saved!');
		} else {
			console.log(err);
		}
	});
};

const Tracking = mongoose.model('Tracking', trackingSchema);

module.exports = Tracking;
