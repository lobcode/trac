const gulp = require('gulp');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const awspublish = require('gulp-awspublish');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const pkg = require('./package.json');
const aws = require('./aws-keys.json');

const config = { pkg, aws };

const debug = false;
const frontAppFile = './public/src/js/index.jsx';
const cssSassFile = './public/src/css/locals/index.scss';
const ENV = 'development';

process.env.NODE_ENV = ENV;

function handleError(err) {
	console.log(err);
}

function Build(compFileName) {
	return browserify({
		entries: compFileName,
		extensions: ['.js', '.jsx'],
		debug
	})
		.transform('babelify', {
			presets: ['es2015', 'stage-3', 'react'],
			plugins: ['transform-object-rest-spread']
		})
		.transform('brfs')
		.bundle();
}

gulp.task('compile-front', () => {
	Build(frontAppFile)
		.on('error', handleError)
		.pipe(source('bundle.js'))
		.pipe(buffer())
		//.pipe(uglify())
		.pipe(gulp.dest('./public/dist/js'))
		.pipe(plumber());
});

gulp.task('watch-front', () => {
	gulp.watch(['./public/src/js/**/*.{js,jsx}'], ['compile-front']);
});

// SASS -> CSS
gulp.task('sass', function () {
	return gulp.src(cssSassFile)
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./public/dist/css'));
});

//Images
gulp.task('imagemin', function () {
	return gulp.src(imgFile)
		.pipe(imagemin().on('error', imagemin.logError))
		.pipe(gulp.dest('./public/dist/images'));
});

// AWS publisher
gulp.task('publish', (cb) => {
  const publisher = awspublish.create({
    params: {
      Bucket: config.pkg.cdn.bucket,
      Region: config.pkg.cdn.region
    },
    accessKeyId: config.aws.accessKeyId,
    secretAccessKey: config.aws.secretAccessKey
  });

  const headers = {
    CacheControl: 'max-age=630720000, public',
    Expires: new Date(Date.now() + 63072000000)
  };

  gulp.src('./public/src/s3/**/*')
    .pipe(rename((path) => {
      // eslint-disable-next-line no-param-reassign
      path.dirname = `/${path.dirname}`;
    }))
    .pipe(awspublish.gzip())
    .pipe(publisher.publish(headers))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter())
    .on('end', cb);
});

gulp.task('watch-sass', function () {
	gulp.watch('./public/src/css/locals/**/*.scss', ['sass']);
});

gulp.task('watch-imagemin', function () {
	gulp.watch('./public/src/images/**/*.{png, svg, gif, jpg}', ['imagemin']);
});

gulp.task('default', ['watch-front', 'watch-sass']);
