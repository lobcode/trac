const express = require('express');
const router = express.Router({
	mergeParams: true
});

const customerForm = require('../routes/customer-form');

router.post('/customer-form/email', customerForm.sendEmailCustomerForm);

module.exports = router;
