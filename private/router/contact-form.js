const express = require('express');
const router = express.Router({
	mergeParams: true
});

const customerForm = require('../routes/contact-form');

router.post('/contact-form/email', customerForm.sendEmailContactForm);

module.exports = router;
