const express = require('express');
const router = express.Router({
	mergeParams: true
});

const book = require('../routes/book');

router.get('/priceQuote/', book.priceQuote);
router.get('/newReservation/', book.newReservation);
router.get('/payReservation/', book.payReservation);
router.get('/cancelPayReservation/', book.cancelPayReservation);
router.get('/save/', book.saveReservation);
router.post('/email/', book.sendEmailReservation);
router.post('/email/pay', book.sendEmailReservationPay);

module.exports = router;
