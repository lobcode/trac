const express = require('express');
const router = express.Router({
	mergeParams: true
});

const stations = require('../routes/stations');

router.get('/loadStations', stations.loadStations);

module.exports = router;
