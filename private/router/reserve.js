const express = require('express');
const router = express.Router({
	mergeParams: true
});

const reserve = require('../routes/reserve');

router.post('/email/', reserve.sendEmailReservation);

module.exports = router;
