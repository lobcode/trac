const express = require('express');
const router = express.Router({
	mergeParams: true
});

const cars = require('../routes/cars');

router.get('/loadCars', cars.loadCars);
router.get('/groups/', cars.groups);

module.exports = router;
