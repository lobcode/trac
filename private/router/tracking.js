const express = require('express');
const router = express.Router({
	mergeParams: true
});

const tracking = require('../routes/tracking');

router.get('/saveTracking/', tracking.saveTracking);

module.exports = router;
