const express = require('express');
const router = express.Router({
	mergeParams: true
});

const google = require('../routes/google');

router.get('/google/sitekey', google.getSiteKey);
router.post('/google/verify', google.verifyCaptcha);

module.exports = router;
