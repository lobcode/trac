const express = require('express');
const router = express.Router({
	mergeParams: true
});

const options = require('../routes/options');

router.get('/loadOptions', options.loadOptions);

module.exports = router;
