const express = require('express');
const router = express.Router({
	mergeParams: true
});

const customer = require('../routes/customer');

router.get('/customer/', customer.getCustomer);

module.exports = router;
