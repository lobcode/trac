const express = require('express');
const router = express.Router({
	mergeParams: true
});

const blog = require('../routes/blog');

router.post('/blog/all', blog.findlAll);
router.post('/blog/post/:slug', blog.findOne);

module.exports = router;
