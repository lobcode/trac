const config = require('config');

module.exports = (req, res) => {
  let stringRobots = `User-Agent: *\nDisallow: \nSitemap: ${config.get('app.protocol')}${config.get('app.domain')}/sitemap.xml`;

  if (req.headers.host !== config.get('app.domain')) {
    stringRobots = `User-Agent: *\nDisallow: /`;
  }

  res.type('text/plain');
  res.send(stringRobots);
};
