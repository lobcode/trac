const _ = require('lodash');
const config = require('config');
const uuid = require('uuid');
const axios = require('axios');

const Email = require('../utils/emails');
const translation = require('../../shared/translation');

const Reserve = require('../../model/reserve');

function sendEmailCode(form, language = 'en') {
	translation.set(language);
	const formFields = translation.t('pages.booking.form');
	const emailSettings = config.get('app.email.reservation_form');
	if (!_.isEmpty(form.remarks)) {

		return Email.send({
			template: 'reservation-by-pay-form.ejs',
			fromName: emailSettings.fromName,
			from: emailSettings.from,
			subject: formFields.email_settings.subject,
			to: 'pagoweb@toyotarent.com',
			values: {form, formFields}
		})
			.then((result) => {
				return { emailConfirmation: _.size(result.accepted) > 0};
			})
			.catch((err) => {
				console.log(err);
				return { emailConfirmation: false};
			});
	} else {
   return Email.send({
     template: 'reservation-form.ejs',
     fromName: emailSettings.fromName,
     from: emailSettings.from,
     bcc: [emailSettings.bcc],
     subject: formFields.email_settings.subject,
     to: form.customerEmail,
     values: {form, formFields}
   })
     .then((result) => {
       return { emailConfirmation: _.size(result.accepted) > 0};
     })
     .catch((e) => {
			console.log(err);
       return { emailConfirmation: false};
     });
	}
}

module.exports = {
	priceQuote(req, res) {
		const {userCode, agentCode, dateFrom, timeFrom, dateTo, timeTo, pickupStation, returnStation} = req.query;
		axios(`https://endpoint.wheelsys.io/10235/link/v3/price-quote_${userCode}.html?AGENT=${agentCode}&DATE_FROM=${dateFrom}&TIME_FROM=${timeFrom}&DATE_TO=${dateTo}&TIME_TO=${timeTo}&PICKUP_STATION=${pickupStation}&RETURN_STATION=${returnStation}`)
			.then(({data}) => {
				res.send(data);
			})
			.catch(function (error) {
				console.log(error);
			});
	},

	newReservation(req, res) {
		const {
			userCode,
			agentCode,
			dateFrom,
			timeFrom,
			dateTo,
			timeTo,
			pickupStation,
			returnStation,
			group,
			customerName,
			customerEmail,
			voucherNo,
			optionCode,
			remarks
		} = req.query;
		axios(`https://endpoint.wheelsys.io/10235/link/v3/new-res_${userCode}.html?AGENT=${agentCode}&DATE_FROM=${dateFrom}&TIME_FROM=${timeFrom}&DATE_TO=${dateTo}&TIME_TO=${timeTo}&PICKUP_STATION=${pickupStation}&RETURN_STATION=${returnStation}&GROUP=${group}&CUSTOMER_NAME=${customerName}&CUSTOMER_EMAIL=${customerEmail}&VOUCHERNO=${voucherNo}&${optionCode}&REMARKS=${remarks}`)
			.then(({data}) => {
				res.send(data);
			})
			.catch(function (error) {
				console.log(error);
			});
	},
	payReservation(req, res) {
		const {
			type,
			key_id,
			hash,
			time,
			orderid,
			ccnumber,
			ccexp,
			checkname,
			amount,
			redirect
		} = req.query;
		axios.post(`
		https://credomatic.compassmerchantsolutions.com/api/transact.php?type=${type}&key_id=${key_id}&hash=${hash}&time=${time}&orderid=${orderid}&ccnumber=${ccnumber}&ccexp=${ccexp}&checkname=${checkname}&amount=${amount}&redirect=${redirect}`)
			.then(({data}) => {
				console.log(data, 'credomatic');
				res.send(data);
			})
			.catch(function (error) {
				console.log(error, 'error credomatic');
			});
	},
	cancelPayReservation(req, res) {
    const {
      type,
      key_id,
      hash,
      time,
      transactionId,
      redirect
    } = req.query;
    axios.post(`https://credomatic.compassmerchantsolutions.com/api/transact.php?type=${type}&key_id=${key_id}&hash=${hash}&time=${time}&Transactionid=${transactionId}&redirect=${redirect}`)
      .then(({data}) => {
        res.send(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  },
	saveReservation(req, res) {
		const {reservation} = req.query;
		Reserve.saveReserve(reservation);
		res.send('ok');
	},
	sendEmailReservation(req, res) {
		const {language} = req.body;

		sendEmailCode(req.body, language)
			.then((result) => res.send(result))
			.catch((err) => res.send(err));
	},
	sendEmailReservationPay(req, res) {
			const {language} = req.body;

			sendEmailCode(req.body, language)
					.then((result) => res.send(result))
					.catch((err) => res.send(err));
	}
};
