const _ = require('lodash');
const axios = require('axios');

const Blog = require('../../model/post');

module.exports = {
  findlAll(req, res) {
    Blog.find({publicar: true}, function (err, data) {
      if (err) res.status(500).send({error: 'error'});
      res.send(data);
    });
  },

  findOne(req, res) {
		var obj = {publicar: true};
    obj[`${req.body.lang}.slug`] = req.params.slug;
    console.log(obj);
    Blog.findOne(obj, function (err, data) {
      if (err) res.status(500).send({error: 'error'});
      console.log(data, 'data');
      res.send(data);
    });
  }
};
