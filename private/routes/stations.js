const _ = require('lodash');
const uuid = require('uuid');

const db = require('../utils/db');
const {STATIONS_TABLE} = db.tables;

module.exports = {

	loadStations(req, res) {
		const dbStations = db.table(STATIONS_TABLE) || [];

		res.send(dbStations);
	}
};
