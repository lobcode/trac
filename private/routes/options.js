const _ = require('lodash');
const uuid = require('uuid');

const db = require('../utils/db');
const {OPTIONS_TABLE} = db.tables;

module.exports = {

	loadOptions(req, res) {
		const dbOptions = db.table(OPTIONS_TABLE) || [];

		res.send(dbOptions);
	}
};
