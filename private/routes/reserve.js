const _ = require('lodash');
const config = require('config');
const uuid = require('uuid');
const axios = require('axios');

const Email = require('../utils/emails');
const translation = require('../../shared/translation');

function sendEmailCode(form, language = 'en') {
	translation.set(language);
	const formFields = translation.t('widget_reserve_form.form');
	const emailSettings = config.get('app.email.reservation_form');

	return Email.send({
		template: 'reserve-form.ejs',
		fromName: emailSettings.fromName,
		from: emailSettings.from,
		bcc: [emailSettings.bcc],
		subject: form.landingName,
		to: form.customerEmail,
		values: {form, formFields}
	})
		.then((result) => {
			return { emailConfirmation: _.size(result.accepted) > 0};
		})
		.catch(() => {
			return { emailConfirmation: false};
		});
}

module.exports = {
	sendEmailReservation(req, res) {
		const {language} = req.body;

		sendEmailCode(req.body, language)
			.then((result) => res.send(result))
			.catch((err) => {
				console.log(err);
				res.send(err)
			});
	}
};
