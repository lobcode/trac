const _  = require('lodash');
const axios = require('axios');

const config = require('config');
const {sitekey, secret, apiUrl} = config.get('app.recaptcha');

module.exports = {
	getSiteKey(req, res) {
		res.send(sitekey);
	},
	verifyCaptcha(req, res) {
		const {recaptcha} = req.body;

		const parameters = {
			secret,
			response: recaptcha
		};

		if (recaptcha) {
			axios.get(apiUrl, {params: parameters})
			.then(({data}) => {
				res.send(data);
			})
			.catch((err) => {
				console.log(err);
				res.status(406).end();
			});
		} else {
			res.status(406).end();
		}

	}
};
