const _ = require('lodash');
const uuid = require('uuid');
const axios = require('axios');

const Customer = require('../../model/customer');


module.exports = {

	getCustomer(req, res) {
		const {customerId} = req.query;
		Customer.getCustomer(customerId)
			.then((resp) => {
				res.send(resp[0]);
			})
			.catch((err) => {
				console.log(err);
			});
	}
};
