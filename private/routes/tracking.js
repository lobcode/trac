const _ = require('lodash');
const config = require('config');
const uuid = require('uuid');
const axios = require('axios');

const Tracking = require('../../model/tracking');

function sendEmailCode(form, language = 'en') {
	translation.set(language);
	const formFields = translation.t('pages.booking.form');
	const emailSettings = config.get('app.email.reservation_form');

	return Email.send({
		template: 'reservation-form.ejs',
		fromName: emailSettings.fromName,
		from: emailSettings.from,
		bcc: [emailSettings.bcc],
		subject: formFields.email_settings.subject,
		to: form.customerEmail,
		values: {form, formFields}
	})
		.then((result) => {
			return { emailConfirmation: _.size(result.accepted) > 0};
		})
		.catch((err) => {
			console.log(err);
			return { emailConfirmation: false};
		});
}

module.exports = {
	saveTracking(req, res) {
		const {tracking} = req.query;
		Tracking.saveTracking(tracking);
		return res;
	}
};
