const _  = require('lodash');
const config = require('config');
const {isEmail} = require('validator');

const Email = require('../utils/emails');
const translation = require('../../shared/translation');

function sendEmailCode(form, language = 'en') {
	translation.set(language);
	const formFields = translation.t('contact_form');
	const emailSettings = config.get('app.email.contact_form');

	return Email.send({
		template: 'contact-form.ejs',
		fromName: emailSettings.fromName,
		from: emailSettings.from,
		bcc: [emailSettings.bcc],
		subject: formFields.email_settings.subject,
		to: form.email,
		values: {form, formFields}
	})
		.then((result) => {
			return { emailConfirmation: _.size(result.accepted) > 0};
		})
		.catch((err) => {
			console.log(err);
			return { emailConfirmation: false};
		});
}

module.exports = {
	sendEmailContactForm(req, res) {
		const {language} = req.body;

		sendEmailCode(req.body, language)
			.then((result) => res.send(result))
			.catch((err) => {
				console.log(err);
				res.send(err)
			});
				
	}
};
