const fs = require('fs');

module.exports = (req, res) => {
  res.header('Content-Type', 'application/xml');
  res.send(fs.readFileSync('./public/sitemapes.xml', {encoding: 'utf-8'}));
};
