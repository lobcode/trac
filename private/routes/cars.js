const _ = require('lodash');
const uuid = require('uuid');
const axios = require('axios');

const db = require('../utils/db');
const {CARS_TABLE} = db.tables;

module.exports = {

	loadCars(req, res) {
		const dbCars = db.table(CARS_TABLE) || [];

		res.send(dbCars);
	},

	groups(req, res) {
		const {userCode, agentCode} = req.query;
		axios(`https://endpoint.wheelsys.io/10235/link/v3/groups_${userCode}.html?AGENT=${agentCode}`)
			.then(({data}) => {
				res.send(data);
			});
	}
};
