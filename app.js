const express = require('express');
const fs = require('fs');
const http = require('http');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const cors = require('cors');
const morgan = require('morgan');
const ejs = require('ejs-locals');
const expressJwt = require('express-jwt');
const config = require('config');
const methodOverride = require('method-override');
const axios = require('axios');
const mongoose = require('mongoose');
const enforce = require('express-sslify');

const DBCarsLocal = require('./model/car');
const DBCustomersLocal = require('./model/customer');
const app = express();
const dirName = __dirname;

const secret = config.get('app.security.secret');

const {authenticate} = require('./private/utils/authentication');
// app.keystone = require('./blog');

app.use(cors());
app.use(morgan('tiny'));
app.use(methodOverride('_method'));

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use(cookieParser());

app.use(express.static(`${dirName}/public`));
app.engine('html', ejs);

app.set('views', `${dirName}/views`);
app.set('view engine', 'html');

// Robots
const robots = require('./private/routes/robots');
app.get('/robots.txt', robots);

// Site map es
const siteMap = require('./private/routes/site-map');
app.get('/es/sitemap.xml', siteMap);

// app.keystone.mount('/blog', app, function () {
//   console.log('blog ready!');
// // put your app's static content and error handling middleware here and start your server
// });

app.get('*', (req, res, next) => {
  const host = req.header('host');

  // 404
  if(req.cookies && req.cookies.notfound){
    res.clearCookie('notfound');
    res.status(404).send('Oops 404');
  }

  // Check if indexOf
  if (req.url.indexOf('/api') !== -1 || req.url.indexOf('google') !== -1) {
    return next();
  }

  // Redirects
  if (req.url === '/cars') {
    res.redirect(301, '/car-rental-costa-rica');
    res.end();
  }

  if (req.url === '/es/carros') {
    res.redirect(301, '/es/alquiler-de-carros-costa-rica');
    res.end();
  }

  // Check slash
  if (req.method === 'GET' && !req.xhr) {
    var path = req.path;
    var query = '';
    var url = '';

    // Check path dont end with /
    if (req.path !== '/' && req.path.endsWith('/')) {
      //Remove slash
      path = path.substring(0, path.length - 1);
      // Check query, prevent problem url with slash and query like /path/?data=1
      if (Object.keys(req.query).length > 0) {
        query = `?${req.originalUrl.split("?")[1]}`;
      }

      url = path + query;
      // Clear url with slash and query
      url = url.replace('/?', '?');
      res.redirect(301, url);
      res.end();
    }
  }

  if (host.match(/^toyotarent\.*/i) && !host.match(/^toyotarent-2019.herokuapp\.*/i)   ) {
    if (host.match(/^www\..*/i)) {
      return next();
    } else {
      res.redirect(301, 'https://www.' + host);
      res.end();
    }
  }else {
    res.render('index');
  }
});

// Local data request
// Cars
const carsRouter = require('./private/router/cars');
app.use('/api/cars', carsRouter);
// Stations
const stationsRouter = require('./private/router/stations');
app.use('/api/stations', stationsRouter);
// Options
const optionsRouter = require('./private/router/options');
app.use('/api/options', optionsRouter);

// book
const bookRouter = require('./private/router/book');
app.use('/api/book', bookRouter);

// Reserve 'Landing Page'
const reserveRouter = require('./private/router/reserve');
app.use('/api/reserve', reserveRouter);

// Google ReCaptcha
const googleRouter = require('./private/router/google');
app.use('/api/', googleRouter);

// Customer Form
const customerForm = require('./private/router/customer-form');
app.use('/api/', customerForm);

// Contact Form
const contactForm = require('./private/router/contact-form');
app.use('/api/', contactForm);

// Customers
const customer = require('./private/router/customer');
app.use('/api/', customer);

// Tracking
const tracking = require('./private/router/tracking');
app.use('/api/', tracking);

// Tracking
const blog = require('./private/router/blog');
app.use('/api/', blog);
// const jwtMiddleware = expressJwt({secret})
// 	.unless({path: ['/', '/favicon.ico']})
// app.use(jwtMiddleware)

app.use(enforce.HTTPS());

const httpServer = http.createServer(app);
httpServer.listen(process.env.PORT || 4001, () => {
  console.log('Running server in port 4001');
});

// DB  
// mongoose.connect('mongodb://tracmin:tR4c4L1F3$@toyotarent.com:27017/trac')
// mongoose.connect('mongodb://localhost:27017/trac');
mongoose.connect('mongodb://heroku_dgqfpsvt:rgmc4nh8mgr5kdlq1r5c8sovc6@ds121262.mlab.com:21262/heroku_dgqfpsvt');

const db = mongoose.connection;
db.once('error', function (err) {
  var d = new Date();
  var fecha = `${d.getDay()}-${d.getMonth()}-${d.getYear()}`;

  console.log('Data error', err);

  fs.writeFile(`errorDB/${fecha}.txt`, err, function (err) {
      if (err) 
          return console.log(err);
      console.log('Wrote file error');
  });
});

db.once('open', function () {
  console.log('db connected! :)');
});

// Load Cars
DBCarsLocal.loadLocalCars();

// Load Customers
DBCustomersLocal.loadLocalCustomers();
